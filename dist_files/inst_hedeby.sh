#!/bin/sh
#/*___INFO__MARK_BEGIN__*/
#/*************************************************************************
# *
# *  The Contents of this file are made available subject to the terms of
# *  the Sun Industry Standards Source License Version 1.2
# *
# *  Sun Microsystems Inc., March, 2001
# *
# *
# *  Sun Industry Standards Source License Version 1.2
# *  =================================================
# *  The contents of this file are subject to the Sun Industry Standards
# *  Source License Version 1.2 (the "License"); You may not use this file
# *  except in compliance with the License. You may obtain a copy of the
# *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
# *
# *  Software provided under this License is provided on an "AS IS" basis,
# *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
# *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
# *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
# *  See the License for the specific provisions governing your rights and
# *  obligations concerning the Software.
# *
# *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
# *
# *   Copyright: 2006 by Sun Microsystems, Inc.
# *
# *   All Rights Reserved.
# *
# ************************************************************************/
#/*___INFO__MARK_END__*/

BASEDIR=`dirname $0`
BASEDIR=`cd $BASEDIR; pwd`
GRM_DIST=$BASEDIR

if [ "$JAVA_HOME" = "" ]; then
   JAVA=`which java`
else
   JAVA=$JAVA_HOME/bin/java
fi

if [ ! -x "$JAVA" ]; then
   echo "Java not found in your PATH. Please set JAVA_HOME!"
   exit 1
fi

JVM_ARGS=-Djava.security.manager=java.rmi.RMISecurityManager
JVM_ARGS="$JVM_ARGS -Djava.security.policy=$BASEDIR/grm_util/inst_hedeby.policy"
APPL_ARGS=""

# parse the command args
while [ $# -gt 0 ]; do
    case $1 in
      -D) JVM_ARGS="$JVM_ARGS  -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y"
          shift
          ;;
      *)  APPL_ARGS="$APPL_ARGS \"$1\""
          shift
          ;;
    esac

done

ARCH=`$GRM_DIST/grm_util/arch`

export ARCH

case $ARCH in
    solaris64) JVM_ARGS="$JVM_ARGS -d64";;
  sol-sparc64) JVM_ARGS="$JVM_ARGS -d64";;
    sol-amd64) JVM_ARGS="$JVM_ARGS -d64";;
esac

JVM_ARGS="$JVM_ARGS -Djava.library.path=$GRM_DIST/lib/$ARCH"

eval $JAVA $JVM_ARGS -jar $BASEDIR/lib/hedeby-config-center.jar $APPL_ARGS
exit $?
