/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * AbstractInstallerTask.java
 *
 * Created on October 24, 2006, 4:38 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.sun.grid.grm.install;

import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.Control.State;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.ControlStateChangedListener;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 */
public abstract class AbstractInstallerTask implements InstallerTask {

    protected Logger log = Logger.getLogger(getClass().getName());
    
    private String name;
    private State state = State.PENDING;
    private InstallerController controller;
    
    public RB rb() {
        return RB.getInstance(getClass());
    }
    
    
    /** Creates a new instance of AbstractInstallerTask */
    public AbstractInstallerTask(InstallerController controller, String name) {
        this.name = name;
        this.controller = controller;
    }
    
    public String getName() {
        return name;
    }
    
    
    public State getState() {
        return state;
    }

    public void setState(State state) {
       this.state = state; 
    }
    
    public Object getData(String key) {
        return controller.getModel().getData(key);
    }
    
    public void setData(String key, Object value) {
        controller.getModel().setData(key, value);
    }
    
    
    public String getAdminUser() {
        return (String) controller.getModel().getData(GrmInstallConstants.GRM_ADMIN_USER);
    }
    
    protected static GridBagConstraints gbc(int x, int y, int width, int height) {
        
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = x;
        c.gridy = y;
        c.gridwidth = width;
        c.gridheight = height;
//        c.weightx = 1.0;
//        c.weighty = 1.0;
        c.fill = GridBagConstraints.NONE;
        c.insets = new Insets(3, 3, 3, 3);
        return c;
    }
    
    protected static GridBagConstraints gbc(int x, int y, int width, int height, int fill) {
        GridBagConstraints c = gbc(x,y,width, height);
        c.fill = fill;
        return c;
    }
    
    protected static GridBagConstraints gbc(int x, int y, int width, int height, int fill, int anchor) {
        GridBagConstraints c = gbc(x,y,width, height, fill);
        c.anchor = anchor;
        return c;
    }
    
    private List<InstallerTaskListener> listeners = Collections.synchronizedList(new LinkedList<InstallerTaskListener>());
    
    public void addInstallerTaskListener(InstallerTaskListener lis) {
        listeners.add(lis);
    }
    
    public void removeInstallerTaskListener(InstallerTaskListener lis) {
        listeners.remove(lis);
    }
    
    protected void fireStarted() {
        Object [] lis = listeners.toArray();
        for(int i = 0; i < lis.length; i++) {
            ((InstallerTaskListener)lis[i]).started(this);
        }
    }
    
    protected void fireFinished() {
        Object [] lis = listeners.toArray();
        for(int i = 0; i < lis.length; i++) {
            ((InstallerTaskListener)lis[i]).finished(this);
        }
    }
    
    protected void fireProgress(String description, int progress) {
        Object [] lis = listeners.toArray();
        for(int i = 0; i < lis.length; i++) {
            ((InstallerTaskListener)lis[i]).progress(this, description, progress);
        }
    }

    public boolean equals(Object obj) {
        return obj instanceof AbstractInstallerTask &&
               ((AbstractInstallerTask)obj).name.equals(name);
    }

    public int hashCode() {
        return name.hashCode();
    }

    public InstallerController getController() {
        return controller;
    }

    public void setController(InstallerController controller) {
        this.controller = controller;
    }
    
    public InstallerModel getModel() {
        return controller.getModel();
    }
    
    protected GenericNextObserver createGenericNextObserver(Control ui) {
        return new GenericNextObserver(ui);
    }
    
    public class GenericNextObserver implements ControlStateChangedListener {
        
        private Control ui;
        
        public GenericNextObserver(Control ui) {
            this.ui = ui;
        }
        
        public void register() {
            ui.addStateChangedListener(this);
            update();
        }
        
        public void unregister() {
            ui.removeStateChangedListener(this);
        }
        
        public void stateChanged(Control control) {  
            update();
        }
        
        public void update() {
            Control.State state = ui.getState();
            boolean nextEnabled;
            switch(state) {
                case OK:
                    nextEnabled = true;
                    break;
                default: 
                    nextEnabled = false;
            }
            
            getController().setNextEnabled(nextEnabled);            
        }
        
        
    }
    
    
}
