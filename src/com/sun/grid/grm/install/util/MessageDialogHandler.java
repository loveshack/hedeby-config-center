/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.util;

import com.sun.grid.grm.install.*;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.ErrorManager;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * This <code>handler</code> opens message dialog for each loggable
 * log record with level equal or higher then <code>INFO</code>.
 *
 */
public class MessageDialogHandler extends Handler {

    private static JFrame frame;
    
    private static boolean showMessageBox = true;
    
    /**
     *  Create a new <code>MessageDialogHandler</code>
     */
    public MessageDialogHandler() {
        setFormatter(new SimpleFormatter());
    }
    
    /**
     * Set the frame for the message dialog elements
     * @param aFrame 
     */
    public static void setFrame(JFrame aFrame) {
        frame = aFrame;
    }
    
    
    private void showMessageBox(final LogRecord record) {
        if(showMessageBox) {
            if(SwingUtilities.isEventDispatchThread()) {
                int level = record.getLevel().intValue();
                if(level >= Level.SEVERE.intValue()) {
                    GrmMessageDialog.showErrorDialog(frame, getFormatter().formatMessage(record), record.getThrown() );
                } else if (level >= Level.WARNING.intValue()) {
                    GrmMessageDialog.showWarnDialog(frame, getFormatter().formatMessage(record));
                } else if (level >= Level.INFO.intValue()) {
                    GrmMessageDialog.showInfoDialog(frame, getFormatter().formatMessage(record));
                }
            } else {
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            showMessageBox(record);
                        }
                    });
                } catch (InvocationTargetException ex) {
                    reportError(ex.getMessage(), ex, ErrorManager.GENERIC_FAILURE);
                } catch (InterruptedException ex) {
                    // ingore
                }
            }
        }
    }
    
    private static Handler delegate;
    
    public static void setDelegateHandler(Handler aDelegate) {
        synchronized(MessageDialogHandler.class) {
            delegate = aDelegate;
        }
    } 
    
    /*
     * Inherited from parent
     */
    public void publish(LogRecord record) {
        if(isLoggable(record)) {
            showMessageBox(record);
            Handler handler = null;
            synchronized(MessageDialogHandler.class) {
                handler = delegate;
            }
            if(handler != null) {
                handler.publish(record);
            }
        }
    }

    public void flush() {
    }

    public void close() throws SecurityException {
    }

    public static boolean isShowMessageBox() {
        return showMessageBox;
    }

    public static void setShowMessageBox(boolean flag) {
        showMessageBox = flag;
    }
    
}
