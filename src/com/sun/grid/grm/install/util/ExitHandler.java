
/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.install.util;

import java.io.UnsupportedEncodingException;
import java.util.logging.ConsoleHandler;
import java.util.logging.ErrorManager;
import java.util.logging.Filter;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;

/**
 *
 */
public class ExitHandler extends Handler {
    
    private Handler delegateHandler;
    /** Creates a new instance of ExitHandler */
    public ExitHandler() {
        
        LogManager manager = LogManager.getLogManager();
        String delegate = manager.getProperty(getClass().getName() + ".delegate");
        if(delegate != null) {
            Class delegateClass = null;
            try {
                delegateClass = Class.forName(delegate);
                try {
                    delegateHandler = (Handler)delegateClass.newInstance();
                } catch (IllegalAccessException ex) {
                    reportError("delegate class " + delegate + " is not accessible", ex, ErrorManager.GENERIC_FAILURE);
                } catch (InstantiationException ex) {
                    reportError("can not create delegate handler", ex, ErrorManager.GENERIC_FAILURE);
                }
            } catch (ClassNotFoundException ex) {
                reportError("delegate class " + delegate + " not found", ex, ErrorManager.GENERIC_FAILURE);
            }
        }
        if(delegateHandler == null) {
            delegateHandler = new ConsoleHandler();
        }
    }

    public void publish(LogRecord record) {
        delegateHandler.publish(record);
        if(record.getLevel().intValue() >= Level.WARNING.intValue()) {
            System.exit(1);
        }
    }

    public void flush() {
        delegateHandler.flush();
    }

    public void close() throws SecurityException {
        delegateHandler.close();
    }

    
    public void setEncoding(String encoding) throws SecurityException, UnsupportedEncodingException {
        delegateHandler.setEncoding(encoding);
    }

    public void setFilter(Filter newFilter) throws SecurityException {
        delegateHandler.setFilter(newFilter);
    }

    public void setErrorManager(ErrorManager em) {
        delegateHandler.setErrorManager(em);
    }

    public void setFormatter(Formatter newFormatter) throws SecurityException {
        delegateHandler.setFormatter(newFormatter);
    }

    public boolean isLoggable(LogRecord record) {
        boolean retValue;
        
        retValue = delegateHandler.isLoggable(record);
        return retValue;
    }

    public void setLevel(Level newLevel) throws SecurityException {
        delegateHandler.setLevel(newLevel);
    }

    public Level getLevel() {
        Level retValue;
        
        retValue = delegateHandler.getLevel();
        return retValue;
    }

    public Formatter getFormatter() {
        Formatter retValue;
        
        retValue = delegateHandler.getFormatter();
        return retValue;
    }

    public Filter getFilter() {
        Filter retValue;
        
        retValue = delegateHandler.getFilter();
        return retValue;
    }

    public ErrorManager getErrorManager() {
        ErrorManager retValue;
        
        retValue = delegateHandler.getErrorManager();
        return retValue;
    }

    public String getEncoding() {
        String retValue;
        
        retValue = delegateHandler.getEncoding();
        return retValue;
    }
    
}
