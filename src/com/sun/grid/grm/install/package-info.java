/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * package-info.java
 *
 * Created on 17. Mai 2006, 09:12
 *
 */

/**
 * The installer is created to install all of the GRM components. To give the
 * user the possibility to install a certain component on one host and to setup
 * an automatic install, the installer provides different switches. These switches
 * will be static, but additional switches for additional features are possible.
 * <p>
 *
 * The used switches are:(switch names: TBD)<p>
 *
 *      -ex            install Executor<br>
 *      -sc            install Service Container<br>
 *      -rp            install Resource Provider<br>
 *      -osp           install OS Providsioner<br>
 *      -osd           install OS Dispatcher<br>
 *      -nodisplay     run installer in text only (non graphical) mode<br>
 *      -noconsole     don't display non graphical text. Used with<br>
 *      -no            Used to run through the installer without installing any <br>
 *                     bits nodisplay option for silent installs.<br>
 *      -novalidate    no text field validation checking<br>
 *      -saveState [statefile] save state of installer input<br>
 *      -state [statefile] use the statefile for setting input values<p>
 *
 *
 * The installer provides 2 installation modes. First the user can install all
 * of the GRM components locally on each machine. The other way is a remote
 * installation using an already installed executor. The installer check's the
 * already installed executors within the system and installes the different
 * components remotely by triggering and sending commands to the remote executor. <p>
 *
 * During installation the following directory stucture and configuration
 * is created:<p>
 *
 *<ul>
 *  <li>global bootstrap information (still discussing)</li>
 *      <ul>
 *          <li>distribution directory, where the binaries are located:</li>
 *              <ul>
 *              <li>dist (default=/opt/SUNWgrm/&lt;system_name&gt;)
 *                  <li>lib directory: jar files</li>
 *                  <li>lib/&lt;arch&gt;: native schared libraries</li>
 *                  <li>docs: documentation</li>
 *                  <li>man: man pages</li>
 *                  <li> bin: rcgrm, grmadmin, grmsh</li>
 *              </li>
 *              </ul>
 *      </ul>
 *</ul>
 *
 *<ul>
 *  <li>local directory, containing security files and spooling directories</li>
 *      <ul>
 *         <li>local spool (default=/var/spool/hedeby/&lt;system_name&gt;)</li>
 *          <ul>
 *          <li>ca (only admin_user has access to this directory)</li>
 *              <ul>
 *              <li>&lt;component_name&gt;.keystore</li>
 *              </ul>
 *         <li>spool</li>
 *             <ul>
 *           <li>&lt;component_name&gt;
 *             <li>tmp_dir/temp_files</li>
 *             <li>&lt;component_name&gt;.log</li>
 *             <li>&lt;component_name&gt;_debug.log ?</li>
 *             <li>&lt;component_name&gt;_exceptions.log.1-1000 ?
 *          </li>
 *          </ul>
 *          </ul>
 *      </ul>
 *</ul>
 *
 *<ul>
 *    <li>etc directory, containing startup scripts and configuration files</li>
 *         
 *         <li>etc (default=/etc/grm/&lt;system_name&gt;)</li>
 *          <ul>
 *          <li>roles.xml (gen by installer)</li>
 *          <li>jaas.config (gen by installer)</li>
 *          <li>grm.policies (cp by installer)</li>
 *          <li>logging.properties (cp by installer)</li>
 *          </ul>
 *</ul>
 *
 *<ul>
 *    <li>global directory, shared over nfs, containing the global configuration</li>
 *    <ul>
 *      <li>truststore (security)</li>
 *      <li>run/</li>
 *        <ul>
 *          <li>a file holding the contac string for each running component.</li>
 *        </ul>
 *      <li>config</li>
 *        <ul>
 *          <li>components.xml</li>
 *              <li>&lt;host&gt;&lt;portnr&gt;&lt;class&gt;=&lt;component_name&gt;</li>
 *              <li>&lt;host&gt;&lt;portnr&gt;&lt;class&gt;=&lt;component_name2&gt;</li>
 *              <ul>
 *              <li>config.xml (specifies which &lt;sub_system&gt; to start)
 *              <li>&lt;sub_system&gt; port number ( 0 choose one free port or specific port)</li>
 *              <li>&lt;*&gt;&lt;sub_system&gt;com.sun.grid.grm.Executor (starting executor on all hosts)</li>
 *              <li>&lt;*&gt;&lt;sub_system&gt;SC (starting service container on all hosts</li>
 *              <li>&lt;HOST&gt;&lt;sub_system&gt;RP (starting resource provider on host &lt;HOST&gt;</li>
 *          </ul>
 *              <li>&lt;component_name&gt;</li>
 *              <ul>
 *              <li>config.xml (D)</li>
 *              <li>mlet.xml (D)</li>
 *              </ul>
 *              <li>&lt;component_name2&gt;</li>
 *              <ul>
 *              <li>config.xml (D)</li>
 *              <li>mlet.xml (D)</li>
 *              </ul>
 *          </li>
 *        </ul>
 *  </ul>
 *</ul>
 *
 * Package structure:<br><br>
 *
 *  - 1 jar file per component<br>
 *  - 1 common jar file<br>
 *  - packages on jar? (optional on a web-server)<br>
 *  - each component has its own class loader (mlet)<p><br>
 *
 *  optional:<br>
 *      - rpm<br>
 *      - package_add<br>
 *      - tgz<p><br>
 *
 *
 *  open discussion:<br><br>
 *
 *      - how to upgrade?<br>
 *      - how many file will the installer be?<br>
 *      - how to start the components under windows?<br>
 *      - does the java system startup via preferences work?<br>
 *      - how do we locate the JVM to use<p><br>
 *
 *  component names: tbd<br><br>
 *
 *  run multible components in 1 JVM<br>
 *  run multible components in multible JVM<p><br>
 *
 *  The N1SM adapter needs a local config, storing passwords locally.<br>
 *      - config located where?<br>
 *      - named?<p><br>
 *
 *  The installer copies a static startup script. (rcgrm)<br><br>
 *
 *      switches:<br>
 *      LSB compliant switches:<br><br>
 *
 *                      - start<br>
 *                      - stop<br>
 *                      - restart<br>
 *                      - forced-reload<br>
 *                      - status<br>
 *
 *      grm specific switches:<br>
 *
 *             the same as LSB but addtitional options<br><br>
 *
 *                  eg -start sc rp starts the sc and rp component<br>
 *                  the same for stop and all other switches<br>
 *
 *  return values rcgrm:<br>
 *
 *      -
 *
 *  <H2>Directory structure</H2>
 *
 * <pre>
 * &lt;GRM_DIST&gt; (root user)
 *   |
 *   +-- bin
 *   |   +---  gconf
 *   |   +---  gstat
 *   |
 *   +-- man
 *   |   +--- man1
 *   |         +-- gconf.1
 *   |         +-- gstat.1
 *   |    
 *   +-- lib
 *   |    +-- Haithabu.jar
 *   |            (may be grm_common.jar, grm_security.jar, grm_sc.jar, ...)
 *   |    +-- ext
 *   |         +-- nis.jar         (JNDI nis provider, needed by JNDILoginModule)
 *   |         +-- ldap.jar        (JNDI ldap provider, needed by JNDILoginModule)
 *   |         +-- &lt;pam-login&gt;.jar (PAMLoginModule)
 *   +-- grm_util
 *   |     +-- install_grm
 *   |     +-- templates
 *   |           +-- jaas.config.template
 *   |           +-- java.policy.template
 *   |           +-- logging.properties.template
 *   |           +-- roles.config.template
 *   |           +-- rcgrm.sh.template     (startup script for parent startup service)
 *   
 * &lt;GRM_ETC&gt; (admin user)
 *   +-- logging.properties
 *   
 * &lt;GRM_SHARED&gt; (admin user)
 *   +-- config
 *   |    +-- config.xml  (global configuration)
 *   |    +-- startup_logging.properties (logging configuration for ParentStartupService)
 *   |    +-- logging.properties (default logging configuration for child jvms, used 
 *   |    |                       if &lt;GRM_ETC&gt;/logging.properties is not available)
 *   |    +-- &lt;configuration name&gt;
 *   |          +-- config.xml
 *   |
 *   +-- run
 *   |    +-- &lt;component identifier&gt;_&lt;host&gt;_&lt;interface&gt;
 *   +-- security
 *   |    +-- java.policy
 *   |    +-- jaas.config
 *   |    +-- roles.config
 *   |    +-- ca
 *         
 *   
 * &lt;GRM_LOCAL_SPOOL&gt; (root user)
 *   +-- lib
 *   |    +-- <vm name>     (contains native libs for a jvm instance)
 *   +-- log (admin user)
 *   |    +-- vm?_&lt;host&gt;.log
 *   |
 *   +-- run (admin user)
 *   |    +-- vm?.pid
 *   +-- security  (root user)
 *   |     +-- ca
 *   |     +-- daemons
 *   |     +-- users
 *   +-- spool (admin user)
 *   |     +-- &lt;component identifier&gt;
 *   |
 *   +-- tmp (admin user)
 *   |     +-- &lt;component identifier&gt;
 *
 * </pre>   
 *
 * <H2>Dependencies to Gridengine</H2>
 *
 * <pre>
 * &lt;SGE_DIST&gt;  == &lt;SGE_ROOT&gt;
 *   |
 *   +-- lib
 *   |    +-- jgdi.jar        (java wrapper for gdi)
 *   |    +-- sge_ca.jar      (java wrapper for sge_ca script)
 *   |    +-- sge_juti.jar    (getpid for java, ...)
 *   |    +-- &lt;arch&gt;
 *   |          +-- libsge_juti.so (native parts of sge_juti.jar)
 *   |          +-- libjgdi.so     (SGE, native parts of jgdi.jar)
 *   |          +-- libdrmaa.so    (SGE)
 *   |          +-- libsll.so      (SGE)
 *   |          +-- libcrypto.so   (SGE)
 *   |
 *   +-- util
 *   |     +-- arch          (SGE)
 *   |     +-- sgeCA         (SGE)
 *   |          +-- sge_ca   (SGE)
 *   |
 *   +-- utilbin
 *   |     +-- &lt;arch&gt;
 *   |           +-- adminrun      (used by sge_ca)
 *   |           +-- infotext      (used by sge_ca)
 *   |           +-- authuser     
 * 
 * </pre>
 *
 *  
 *  
 *
 */
package com.sun.grid.grm.install;
