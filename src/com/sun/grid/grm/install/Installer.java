/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install;

import com.sun.grid.grm.cli.CLIFormatter;
import com.sun.grid.grm.cli.DebugOptions;
import com.sun.grid.grm.install.util.ExitHandler;
import com.sun.grid.grm.install.util.MessageDialogHandler;
import com.sun.grid.grm.util.GrmFormatter;
import java.io.File;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.Arrays;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 */
public class Installer {
    
    private static Logger log = Logger.getLogger(Installer.class.getName());
    
    private InstallerModel model = new DefaultInstallerModel();
    private InstallerUI ui;
    private InstallerController controller;
    
    private static String [] BASIC_CLASSPATH = {
        "./hedeby-common.jar",
        "./hedeby-config-center-tasks.jar"
    };
    
    private static String [] ADDITIONAL_CLASSPATH = {
        InstallClassLoaderDefinition.DIST_DIR_PATTERN + "/lib/hedeby-common.jar",
        InstallClassLoaderDefinition.DIST_DIR_PATTERN + "/lib/hedeby-config-center-tasks.jar",
        InstallClassLoaderDefinition.DIST_DIR_PATTERN + "/lib/hedeby-security.jar",
        InstallClassLoaderDefinition.DIST_DIR_PATTERN + "/lib/hedeby-ge-adapter.jar",
        InstallClassLoaderDefinition.DIST_DIR_PATTERN + "/lib/hedeby-n1sm-adapter.jar",
        InstallClassLoaderDefinition.SGE_ROOT_PATTERN + "/lib/juti.jar",
        InstallClassLoaderDefinition.SGE_ROOT_PATTERN + "/lib/jgdi.jar"
    };
    
    private static InstallClassLoaderDefinition BASIC_CLASSLOADER_DEF = new
            InstallClassLoaderDefinition("base classes", BASIC_CLASSPATH);
    
    private static InstallClassLoaderDefinition ADDITIONAL_CLASSLOADER_DEF = new
            InstallClassLoaderDefinition("additional classes", ADDITIONAL_CLASSPATH, null);
    
    public Installer() throws InstallerException {
        controller = new DefaultInstallerController(model);
        
        controller.registerTask("grmInstallationModeSelection", "com.sun.grid.grm.install.tasks.InstallationModeSelectionTask", BASIC_CLASSLOADER_DEF);
        controller.registerTask("grmSelectSystemFromShared", "com.sun.grid.grm.install.tasks.SelectSystemFromShared", BASIC_CLASSLOADER_DEF);
        controller.registerTask("grmSelectSystemFromPrefs", "com.sun.grid.grm.install.tasks.SelectSystemFromPrefsTask", BASIC_CLASSLOADER_DEF);
        controller.registerTask("grmSystemBasics", "com.sun.grid.grm.install.tasks.GrmSystemBasicsTask", BASIC_CLASSLOADER_DEF);
        controller.registerTask("grmCompSelect", "com.sun.grid.grm.install.tasks.GrmCompSelectTask", ADDITIONAL_CLASSLOADER_DEF);
        controller.registerTask("grmRemoteCaPassword", "com.sun.grid.grm.install.tasks.GrmRemoteCaPasswordTask", ADDITIONAL_CLASSLOADER_DEF);
        controller.registerTask("grmSecuritySystem"   , "com.sun.grid.grm.install.security.GrmSecuritySystemTask", ADDITIONAL_CLASSLOADER_DEF);
        controller.registerTask("grmSecurityAuth"   , "com.sun.grid.grm.install.security.GrmSecurityAuthTask", ADDITIONAL_CLASSLOADER_DEF);
        controller.registerTask("grmSecurityAdminUser", "com.sun.grid.grm.install.security.GrmSecurityAdminUserTask", ADDITIONAL_CLASSLOADER_DEF);
        controller.registerTask("grmExecutorConfig", "com.sun.grid.grm.install.tasks.GrmExecutorConfigTask", ADDITIONAL_CLASSLOADER_DEF);
        controller.registerTask("grmRpConfiguration", "com.sun.grid.grm.install.tasks.GrmRpConfigurationTask", ADDITIONAL_CLASSLOADER_DEF);
        controller.registerTask("grmScConfiguration", "com.sun.grid.grm.install.tasks.GrmScConfigurationTask", ADDITIONAL_CLASSLOADER_DEF);
        controller.registerTask("grmGeAdapterConfiguration", "com.sun.grid.grm.install.tasks.GrmGeAdapterConfigurationTask", ADDITIONAL_CLASSLOADER_DEF);
        controller.registerTask("grmSloConfiguration", "com.sun.grid.grm.install.tasks.GrmSloConfigurationTask", ADDITIONAL_CLASSLOADER_DEF);
        controller.registerTask("grmOspConfiguration", "com.sun.grid.grm.install.tasks.GrmOspConfigurationTask", ADDITIONAL_CLASSLOADER_DEF);
        controller.registerTask("grmOsdN1smBasicConfiguration", "com.sun.grid.grm.install.tasks.GrmOsdN1smBasicConfigurationTask", ADDITIONAL_CLASSLOADER_DEF);
        controller.registerTask("grmN1smConfiguration", "com.sun.grid.grm.install.tasks.GrmN1smConfigurationTask", ADDITIONAL_CLASSLOADER_DEF);
        controller.registerTask("grmRcConfiguration", "com.sun.grid.grm.install.tasks.GrmRcConfigurationTask", ADDITIONAL_CLASSLOADER_DEF);
        controller.registerTask("install", "com.sun.grid.grm.install.tasks.InstallExecutorTask", ADDITIONAL_CLASSLOADER_DEF);
        
        String [] defaultPath = {
            "grmInstallationModeSelection",
            "grmCompSelect",
            "grmRcConfiguration",
            "install"
        };
        controller.getSequence().setTaskPath(Arrays.asList(defaultPath));
    }
    
    private void initUi() {
        switch(controller.getMode()) {
            case GUI:
                ui = new InstallerFrame();
                MessageDialogHandler.setFrame((JFrame)ui);
                ui.setController(controller);
                controller.setUi(ui);
                break;
            case CONSOLE:
                throw new IllegalStateException("CONSOLE mode not yet implemeneted");
            case AUTO:
                break;
            default:
                throw new IllegalStateException("unknown install mode " + controller.getMode());
        }
    }
    
    
    private void initDefaultData() {
        
        URL jarUrl = RB.getJarUrl(Installer.class);
        
        File dist = null;
        if(jarUrl.getProtocol().equals("file")) {
            File jarFile = new File(jarUrl.getFile());
            dist = jarFile.getParentFile().getParentFile();
        } else {
            dist = new File(System.getProperty("user.dir"));
        }
        File localSpool = new File("<local_spool>");
        File shared = new File("/net/<shared>");
        File sgeRoot = new File("/opt/sge");

        model.setData(GrmInstallConstants.SYSTEM_INSTALL_MODE, GrmInstallConstants.NEW_SYSTEM);
        model.setData(GrmInstallConstants.GRM_SYSTEM_NAME, "svcmgmt");
        model.setData(GrmInstallConstants.GRM_ADMIN_USER, System.getProperty("user.name"));
        model.setData(GrmInstallConstants.GRM_DIST_DIR, dist.getAbsolutePath());
        model.setData(GrmInstallConstants.GRM_LOCAL_SPOOL_DIR, localSpool.getAbsolutePath());
        model.setData(GrmInstallConstants.GRM_SHARED_DIR, shared.getAbsolutePath());
        model.setData(GrmInstallConstants.GRM_SGE_DIST_DIR, sgeRoot.getAbsolutePath());
        model.setData(GrmInstallConstants.SECURITY_KEYSTORE_SOURCE, GrmInstallConstants.SECURITY_KEYSTORE_FROM_STATE_FILE);
        model.setData(GrmInstallConstants.SECURITY_AUTH_METHOD, GrmInstallConstants.AUTH_NIS);
        model.setData(GrmInstallConstants.SECURITY_AUTH_CA_JVM, "jvm0");
        // following line can mess cc if "executor" component name is already used - i leave it here as a warning
        // model.setData(GrmInstallConstants.EXECUTOR_ID_CONFIG, "executor");
        model.setData(GrmInstallConstants.EXECUTOR_SHUTDOWTIMEOUT_CONFIG, "60");
        model.setData(GrmInstallConstants.EXECUTOR_KEEPFILES_CONFIG, "false" );
        model.setData(GrmInstallConstants.EXECUTOR_CPS_CONFIG, "3");
        model.setData(GrmInstallConstants.EXECUTOR_MPS_CONFIG, "10");
        model.setData(GrmInstallConstants.EXECUTOR_IDLETIMEOUT_CONFIG, "60");
        model.setData(GrmInstallConstants.GRM_PREFERENCE_TYPE, "SYSTEM");
        model.setData(GrmInstallConstants.INSTALL_REMOTE_SECURITY, "false");
        model.setData(GrmInstallConstants.SC_SLA_MINVALUE, 3);
        model.setData(GrmInstallConstants.SC_SLA_URGENCY, 30);

        model.setData(GrmInstallConstants.SC_ADAPTERCLASSNAME_CONFIG, "com.sun.grid.grm.service.impl.n1ge.N1geAdapterImpl");
        model.setData(GrmInstallConstants.SC_SHUTDOWN_TIME_CONFIG, "20");
        
        model.setData(GrmInstallConstants.SECURITY_AUTH_METHOD, "SYSTEM");
        
        model.setData(GrmInstallConstants.GRM_RP_USAGE, "10");
        model.setData(GrmInstallConstants.GRM_RP_POLLING_PERIOD, "30");
        
    }
    
    private DebugOptions debugOptions;
    private File stateFile;
    
    
    private void usage() {
        System.err.println("Usage:");
        System.err.println(this.getClass().getName() + " --log | --state <filename> | --mode [GUI|AUTO|CONSOLE]");
        System.exit(1);
    }
    
    private void parseArgs(String [] args) {
        
        for(int i = 0; i < args.length; i++) {
            if(args[i].equals("--log")) {
                i++;
                if(i >= args.length) {
                    usage();
                    System.err.println("--log options requires an argument");
                }
                
                try {
                    debugOptions = new DebugOptions(args[i]);
                } catch (IllegalArgumentException ex) {
                    System.err.println(ex.getLocalizedMessage());
                    System.exit(1);
                }
                
            } else if (args[i].equals("--state")) {
                i++;
                controller.setStateFile(new File(args[i]));
            } else if (args[i].equals("--mode")) {
                i++;
                if(i >= args.length) {
                    usage();
                    System.err.println("--mode options requires an argument");
                }
                try {
                    controller.setMode(InstallerController.Mode.valueOf(args[i].toUpperCase()));
                } catch(IllegalArgumentException ex) {
                    usage();
                    System.err.println("Invalid mode '" + args[i] + "'");
                }
                
            } else {
                usage();
                System.err.println("Unknown option " + args[i]);
            }
            
        }
        if(debugOptions == null) {
            try {
                debugOptions = new DebugOptions("SEVERE:com.sun.grid.grm.install=INFO");
            } catch (IllegalArgumentException ex) {
                // Can not happen
            }
        }
        
        initLogging();
        
    }
    
    /**
     * Main method for the installer
     * @param args arguments for the installer
     */
    public static void main(String [] args) {
        try {
            Installer installer = new Installer();
            installer.initDefaultData();
            installer.parseArgs(args);
            installer.initUi();
            installer.run();
        } catch(Exception ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            System.exit(1);
        }
    }
    
    public void run() {
        controller.run();
    }

    private File logFile;
    private File getLogFile() {
        if(logFile == null) {
            String tmpDirStr = System.getProperty("java.io.tmpdir");
            File tmpDir = new File(tmpDirStr);
            String filename = "hedeby_install_" + System.getProperty("user.name");        
            
            File file = new File(tmpDirStr, filename + ".log");
            int i = 0;
            while(file.exists()) {
                i++;
                file = new File(tmpDirStr, filename + "_" + i + ".log");
            }
            logFile = file;
        }
        return logFile;
    }
    
    private void initLogging() {
        
        try {
            PipedInputStream pin = new PipedInputStream();
            final PipedOutputStream pout = new PipedOutputStream(pin);
            
            Thread logConfWriter = new Thread("InstallUtil.logConfWriter") {
                public void run() {
                    
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    
                    String handler = ConsoleHandler.class.getName();
                    String fileHandler = FileHandler.class.getName();
                    String formatter = CLIFormatter.class.getName();
                    String fileFormatter = GrmFormatter.class.getName();
                    String dlgHandler = MessageDialogHandler.class.getName();
                    
                    pw.print("handlers=");
                    pw.print(handler);
                    pw.print(" ");
                    pw.print(fileHandler);
                    switch(controller.getMode()) {
                        case GUI:
                            pw.print(" ");
                            pw.print(dlgHandler);
                            pw.print(" ");
                            pw.print(fileHandler);
                            break;
                        case CONSOLE:
                            pw.print(" ");
                            pw.print(fileHandler);
                            break;
                        case AUTO:
                            pw.print(" ");
                            pw.print(fileHandler);
                    }
                    pw.println();
                    
                    pw.print(handler);
                    pw.print(".level=");
                    pw.println(debugOptions.getLowerestLevel().getName());
                    
                    if(!controller.getMode().equals(InstallerController.Mode.AUTO)) {
                        pw.print(dlgHandler);
                        pw.print(".level=");
                        pw.println(debugOptions.getLowerestLevel().getName());
                    }
                    
                    pw.print(fileHandler);
                    pw.print(".level=");
                    pw.println(debugOptions.getLowerestLevel().getName());
                    pw.print(fileHandler);

                    String logFile = getLogFile().getAbsolutePath().replace(File.separatorChar, '/');
                    pw.print(".pattern=");
                    pw.println(logFile);
                    
                    // print formatter of the handler
                    pw.print(handler);
                    pw.print(".formatter=");
                    pw.println(formatter);
                    
                    pw.print(fileHandler);
                    pw.print(".formatter=");
                    pw.println(fileFormatter);
                    
                    pw.print(fileFormatter);
                    pw.println(".columns = time thread source level message");
                    pw.print(fileFormatter);
                    pw.println(".delimiter = |");
                    pw.print(fileFormatter);
                    pw.println(".withStacktrace =true");
                    
                    // print global log level
                    pw.print(".level=");
                    pw.println(debugOptions.getGlobalLevel().getName());
                    
                    pw.print("java.level=");
                    pw.println(Level.SEVERE);
                    
                    // print the log levels for individual loggers
                    Map<String, Level> loggerMap = debugOptions.getLoggerMap();
                    for(String logger: loggerMap.keySet()) {
                        pw.print(logger);
                        pw.print(".level=");
                        pw.println(loggerMap.get(logger).getName());
                    }
                    pw.close();
                    
//                    System.out.println("------ logging -----------------");
//                    System.out.println(sw.getBuffer().toString());
                    
                    try {
                        pout.write(sw.getBuffer().toString().getBytes());
                        pout.close();
                    } catch(IOException ioe) {
                        ioe.printStackTrace();
                    }
                }
            };
            
            logConfWriter.start();
            
            // Reconfigure the log manager
            LogManager.getLogManager().readConfiguration(pin);
            
            logConfWriter.join();
            
            System.out.println(RB.getInstance(Installer.class).getFormattedString("useLogFile", getLogFile()));
            
        } catch(InterruptedException ex) {
            throw new AssertionError("logging setup has been interrupted");
        } catch(IOException ioe) {
            // Should not happen
            throw new AssertionError("IO Error while setting up logging");
        }
    }
    
}
