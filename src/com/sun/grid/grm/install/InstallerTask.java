/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install;

import java.util.ResourceBundle;
import javax.swing.JPanel;

/**
 * Commaon interface for a installation task.
 *
 */
public interface InstallerTask {
    
    public enum State {
        PENDING,
        STARTED,
        FINISHED,
        ERROR
    }
    
    /**
     * Get the graphical user interface for this task
     * @throws com.sun.grid.grm.install.InstallerException 
     * @return the graphical user interface of this task
     */
    public JPanel getUI() throws InstallerException;
    
    /**
     *  Get the name of the task
     * @return  the name of the task
     */
    public String getName();
    
    /**
     * Get the state of this started
     * @return the state of this task
     */
    public State getState();
    
    /**
     * Set the state of this task
     * @param state  the new state
     */
    public void setState(State state);
    
    
    /**
     * The controller informs with this method
     * the task that the configuration phase begins.
     * The task should initialize it's configuration components
     * within this method.
     *
     * @throws com.sun.grid.grm.install.InstallerException 
     */
    public void beginDisplay() throws InstallerException;
    
    
    /**
     * The controller informs with this method the task that
     * the configuration phase for this task has been finished.
     * The task can do some cleanup jobs.
     * @throws com.sun.grid.grm.install.InstallerException 
     */
    public void finishDisplay() throws InstallerException;
    
    /**
     * Execute the installation steps of this task
     * @throws com.sun.grid.grm.install.InstallerException 
     */
    public void execute() throws InstallerException;
    
    /**
     *  Undo the changes which has been made during the
     *  execution of this task
     */
    public void undo();
    
    /**
     * Add <code>InstallerTaskListener</code> to this task.
     * @param lis  the <code>InstallerTaskListener</code>
     */
    public void addInstallerTaskListener(InstallerTaskListener lis);
    
    /**
     * Remove <code>InstallerTaskListener</code> to this task.
     * @param lis  the <code>InstallerTaskListener</code>
     */
    public void removeInstallerTaskListener(InstallerTaskListener lis);

    /**
     *   Get the controller of this task
     *   @return the controller of this task
     */
    public InstallerController getController();
    
    /**
     *   Get the model of this task
     *   @return the model of this task
     */
    public InstallerModel getModel();
    
    /**
     * Get the resource bundle for this task.
     *
     * @return the resource bundle of this task
     */
    public RB rb();
}
