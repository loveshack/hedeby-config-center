/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.install;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
public class InstallerTaskFactory {

    private static Logger log = Logger.getLogger(InstallerTaskFactory.class.getName());
    
    public static final String SGE_ROOT_PATTERN = "${sge.root}";
    public static final String DIST_DIR_PATTERN = "${dist.dir}";
    
    private static String [] ADDITIONAL_CLASSPATH = {
        DIST_DIR_PATTERN + "/lib/grm_install_tasks.jar",
        DIST_DIR_PATTERN + "/lib/grm_security.jar",
        SGE_ROOT_PATTERN + "/lib/juti.jar"
    };
    
    
    static class TaskDef {
        private String name;
        private String sequenceName;
        private String implClass;
        private String [] classpath;
        
        public TaskDef(String name, String sequenceName, String implClass, String [] classpath) {
            this.name = name;
            this.sequenceName = sequenceName;
            this.implClass = implClass;
            this.classpath = classpath;
        }
    }
    
    private final static Map<String, TaskDef> TASKDEF_MAP = new HashMap<String,TaskDef>();
    
    private static void reg(String name, String sequenceName, String implClass, String [] classpath) {
        TASKDEF_MAP.put(name, new TaskDef(name, sequenceName, implClass, classpath));
    }
    
    static {
        reg("grmSecuritySystem"   , "grmSecurityInstall", "com.sun.grid.grm.install.security.GrmSecuritySystemTask", ADDITIONAL_CLASSPATH);
        reg("grmSecurityAdminUser", "grmSecurityInstall", "com.sun.grid.grm.install.security.GrmSecurityAdminUserTask", ADDITIONAL_CLASSPATH);
    }
    
    private static ClassLoader additionalClassLoader;
    
    private static String [][] PATTERNS = {
        { SGE_ROOT_PATTERN, GrmInstallConstants.GRM_SGE_DIST_DIR },
        { DIST_DIR_PATTERN, GrmInstallConstants.GRM_DIST_DIR }
    };
    
    
    private static ClassLoader createClassLoader(InstallerModel data, String [] classpath) throws InstallerException {
        URL [] classpathURLs = new URL[classpath.length];
        for(int i = 0; i < classpath.length; i++) {
            String fileStr = classpath[i];
            for(String [] pattern: PATTERNS) {
                String value = data.getData(pattern[1]).toString();
                
                if(value != null ) {
                    fileStr = fileStr.replace(pattern[0], value);
                } else {
                    throw new InstallerException("pattern " + pattern[0] + " can not be replaced, because " +
                            pattern[1] + " is not set in wizard state" );
                }
            }
            File file = new File(fileStr.toString().replace('/', File.separatorChar));
            
            if(!file.exists()) {
                log.log(Level.WARNING, "File {0} from classpath does not exists", file);
            }
            if(log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "Add {0} to classpath", file);
            }
            try {
                classpathURLs[i] = file.toURL();
            } catch (MalformedURLException ex) {
                throw new InstallerException("File.toURL throwed MalformedURLException", ex);
            }
            
            
        }
        return new URLClassLoader(classpathURLs, InstallerTaskFactory.class.getClassLoader());
    }
    
    
    public static ClassLoader getAdditionalClassLoader(InstallerModel data) throws InstallerException {
        ClassLoader ret = (ClassLoader)data.getData("additionalClassLoader");
        if(ret == null) {
            ret = createClassLoader(data, ADDITIONAL_CLASSPATH);
            data.setData("additionalClassLoader", ret);
        }
        return ret;
    }
    

    public static InstallerTask newInstance(String panelName, InstallerModel data) throws InstallerException {
        
        TaskDef taskDef = TASKDEF_MAP.get(panelName);
        
        if(taskDef == null) {
            throw new InstallerException("no installer task for panel " + panelName + " defined");
        }
        

        InstallerTask installerTask = null;
        ClassLoader classLoader = null;
        if(taskDef.classpath != null) {
            classLoader = createClassLoader(data, taskDef.classpath);
        } else {
            classLoader = InstallerTaskFactory.class.getClassLoader();
        }

        Class clazz = null;
        try {
            clazz = classLoader.loadClass(taskDef.implClass);
        } catch(ClassNotFoundException ex) {
            throw new InstallerException("implClass " + taskDef.implClass + " not found", ex);
        }
        if(log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "found implClass {0} ({1})", 
                    new Object [] { clazz.getName(), clazz.getProtectionDomain().getCodeSource() } );
        }
        Constructor cons;
        try {
            cons = clazz.getConstructor(String.class, InstallerModel.class, String.class);
        } catch (NoSuchMethodException ex) {
           throw new InstallerException("Constructor for class  " + taskDef.implClass + " not found", ex);
        }
        
        
        try {
            return (InstallerTask)cons.newInstance(taskDef.name, data, taskDef.sequenceName);
        } catch (InvocationTargetException ex) {
           throw new InstallerException("Can not create instance of " + taskDef.implClass, ex.getTargetException());
        } catch (InstantiationException ex) {
           throw new InstallerException("Can not create instance of " + taskDef.implClass, ex);
        } catch (IllegalAccessException ex) {
           throw new InstallerException("Can not create instance of " + taskDef.implClass, ex);
        }
        
    }
    
}
