/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  This class defines the classpath for a classloader which
 *  is used for a installation task.
 *
 *  The pathes in the classpath for the classloader can contain the patterns
 *  <code>${sge.root}</code> and ${dist.dir}
 */
public class InstallClassLoaderDefinition {
    
    private static Logger log = Logger.getLogger(InstallClassLoaderDefinition.class.getName());
    
    public static final String SGE_ROOT_PATTERN = "${sge.root}";
    public static final String DIST_DIR_PATTERN = "${dist.dir}";
    
    private String name;
    private String [] classpath;
    private ClassLoader classLoader;
    private InstallClassLoaderDefinition parent;
    
    private static String [][] PATTERNS = {
        { SGE_ROOT_PATTERN, GrmInstallConstants.GRM_SGE_DIST_DIR },
        { DIST_DIR_PATTERN, GrmInstallConstants.GRM_DIST_DIR }
    };
    
    /** Creates a new instance of InstallClassLoaderDefinition */
    public InstallClassLoaderDefinition(String name, String [] classpath) {
        this(name, classpath, null);
    }
    
    public InstallClassLoaderDefinition(String name, String [] classpath, InstallClassLoaderDefinition parent) {
        this.name = name;
        this.classpath = classpath;
        this.parent = parent;
    }
    
    
    /**
     * Get the classloader
     * @param controller  the installer controller
     * @throws com.sun.grid.grm.install.InstallerException if the class loader could not be created
     * @return the classloader
     */
    public ClassLoader getClassLoader(InstallerController controller) throws InstallerException {
        if(classLoader == null) {
            classLoader = createClassLoader(controller);
        }
        return classLoader;
    }
    
    private ClassLoader createClassLoader(InstallerController controller) throws InstallerException {

        log.log(Level.FINE, "Create classloader {0}", name);
        URL [] classpathURLs = new URL[classpath.length];
        for(int i = 0; i < classpath.length; i++) {
            String fileStr = classpath[i];
            for(String [] pattern: PATTERNS) {
                
                if(fileStr.indexOf(pattern[0]) >= 0) {
                    Object value = controller.getModel().getData(pattern[1]);

                    if(value != null ) {
                        fileStr = fileStr.replace(pattern[0], value.toString());
                    } else {
                        throw new InstallerException("pattern " + pattern[0] + " can not be replaced, because " +
                                pattern[1] + " is not set in model of the installer controller" );
                    }
                }
            }
            if(fileStr.startsWith("./")) {
                URL url = InstallClassLoaderDefinition.class.getProtectionDomain().getCodeSource().getLocation();
                String path = url.getPath();
                int index = path.lastIndexOf('/');
                if(index < 0) {
                    throw new IllegalStateException("invalid path to dist directory: " + path);
                }
                path = path.substring(0, index);
                fileStr = path +  "/" + fileStr.substring(2);
            }
            File file = new File(fileStr.toString().replace('/', File.separatorChar));
            
            if(!file.exists()) {
                log.log(Level.WARNING, "File {0} from classpath does not exists", file);
            }
            if(log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "Add {0} to classpath", file);
            }
            try {
                classpathURLs[i] = file.toURL();
            } catch (MalformedURLException ex) {
                throw new InstallerException("File.toURL throwed MalformedURLException", ex);
            }
            
            
        }
        
        if(parent == null) {
            return new MyURLClassLoader(classpathURLs, InstallerTaskFactory.class.getClassLoader());
        } else {
            return new MyURLClassLoader(classpathURLs, parent.getClassLoader(controller));
        }
    }

    /**
     * Get the name of this class loader definition
     * @return the name of this class loader definition
     */
    public String getName() {
        return name;
    }
    
    
    class MyURLClassLoader extends URLClassLoader {
        
        public MyURLClassLoader(URL [] urls, ClassLoader parent) {
            super(urls, parent);
        }
        
        public String toString() {
            StringBuilder ret = new StringBuilder();
            boolean first = true;
            ret.append("[name=");
            ret.append(name);
            ret.append(",classpath=");
            for(URL url: getURLs()) {
                if(first) {
                    first = false;
                } else {
                    ret.append(File.pathSeparatorChar);
                }
                ret.append(url);
            }
            ret.append("]");
            return ret.toString();
        }
        
    }
    
}
