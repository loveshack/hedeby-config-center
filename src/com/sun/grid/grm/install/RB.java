/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.KeyStroke;

/**
 * ResourceBundle helper class.
 *
 * Makes it easy to create localized swing components
 */
public class RB {
    
    private final static Logger log = Logger.getLogger(RB.class.getName());
    
    private ResourceBundle rb;
    
    private static Map<Package, RB> instances = Collections.synchronizedMap(new WeakHashMap<Package, RB>());
    
    
    /**
     * Create a new resource bundle
     * @param rb the real resource bundle
     */
    private RB(ResourceBundle rb) {
        this.rb = rb;
    }
    
    
    /**
     * Get the resource bundle for a class
     * @param clazz the class
     * @return the resource bundle
     */
    public static RB getInstance(Class clazz) {
        return getInstance(clazz.getPackage(), clazz.getClassLoader());
    }
    
    
    /**
     * Get a resource bundle for a package
     * @param pkg the package
     * @param cl the classloader of this package
     * @return the resource bundle
     */
    public static RB getInstance(Package pkg, ClassLoader cl) {
        RB ret = instances.get(pkg);
        if(ret == null) {
            String name = getResourceBundleName(pkg);
            ret = new RB(ResourceBundle.getBundle(name, Locale.getDefault(), cl));
            instances.put(pkg, ret);
        }
        return ret;
    }
    
    public static String getResourceBundleName(Package pkg) {
        String name = pkg.getName();
        int index = name.lastIndexOf('.');
        return name + "." + Character.toUpperCase(name.charAt(index+1)) + name.substring(index+2) + "Resources";
    }
    
    public static String getResourceBundlePath(Package pkg) {
        return getResourceBundleName(pkg).replace('.', '/') + ".properties";
    }
    
    
    /**
     * Get the url of the jar file which contains a class
     * @param clazz the class
     * @return URL of the jar file
     */
    public static URL getJarUrl(Class clazz) {
        String path = clazz.getName().replace('.', '/') + ".class";
        URL resource = clazz.getClassLoader().getResource(path);
        if(resource == null) {
            throw new IllegalStateException("Resource " + path + " not found");
        }
        path = resource.getPath();
        int index = path.lastIndexOf('!');
        if(index >= 0) {
            path = path.substring(0, index);
        } else {
            throw new IllegalStateException("no jar specified in url " + path);
        }
        try {
            return new URL(path);
        } catch (MalformedURLException ex) {
            throw new IllegalStateException(ex.getLocalizedMessage(), ex);
        }
    }
    
    
    /**
     * Get a string from the resource bundle. 
     * 
     * If the key is not defined in the resource bundle, a warning
     * is written into the logging system and the key is returned
     * 
     * @param key the key
     * @return the string for this key
     */
    public String getString(String key) {
        try {
            return rb.getString(key);
        } catch(MissingResourceException ex) {
            log.log(Level.WARNING,"resource key ''{0}'' not found", key);
            return key;
        }
    }
    
    
    /**
     * Get the string from the resource bundle.
     *
     * If the key is not defined the default value is returned
     * @param key   the key
     * @param defaultValue  the default value
     * @return the string for that key
     */
    public String getString(String key, String defaultValue) {
        try {
            return rb.getString(key);
        } catch(MissingResourceException ex) {
            return defaultValue;
        }
    }
    
    /**
     * Get a formatted string from the resource bundle.
     * 
     * The class <code>java.text.MessageFormat</code> is used for formatting
     * the stirng
     *
     * @param key     key of the string
     * @param params  parameters for formatting
     * @return the string
     */
    public String getFormattedString(String key, Object... params) {
        key = getString(key);
        if(params != null) {
            key = MessageFormat.format(key, params);
        }
        return key;
    }
    
    /**
     * Create a label.
     *
     *
     * @param name   name of the label
     * @return the label
     */
    public JLabel createLabel(String name) {
        JLabel ret = new JLabel(getString(name));
        ret.setToolTipText(getString(name + ".tooltip", null));
        ret.setIcon(getImage(name + ".icon"));
        return ret;
    }
    
    /**
     * Initialize a action with the values from the resource bundle
     * @param action the action
     * @param name  the name of the action
     */
    public void initAction(AbstractAction action, String name) {
        action.putValue(Action.NAME, getString(name));
        String mnemonic = getString( name + ".mnemonic", null);
        if(mnemonic != null) {
            action.putValue(Action.MNEMONIC_KEY, (int)mnemonic.charAt(0));
        }
        
        String accelerator = getString(name + ".accelerator", null);
        if(accelerator != null) {
            KeyStroke stroke = KeyStroke.getKeyStroke(accelerator);
            if(stroke != null) {
                action.putValue(Action.ACCELERATOR_KEY, stroke);
            } else {
                log.log(Level.WARNING, name + ".accelerator defines an unkonwn keystroke ({0})", accelerator);
            }
        }
    }
    
    /**
     * Create button
     * @param name  the name of the button
     * @return the button
     */
    public JButton createButton(String name) {
        
        JButton ret = new JButton();
        
        ret.setText(getString(name));
        
        String mnemonic = getString( name + ".mnemonic", null);
        if(mnemonic != null) {
            ret.setMnemonic(mnemonic.charAt(0));
        }
        return ret;
    }
    
    private static Map<String, ImageIcon> icons = Collections.synchronizedMap(new WeakHashMap<String, ImageIcon>());
    
    
    /**
     * get an image.
     *
     * The path to the image is defined in the resource bundle
     * @param key the key of the image path definition
     * @return the image or <code>null</code> if the path is not defined
     */
    public ImageIcon getImage(String key) {
        String path = getString(key, null);
        ImageIcon ret = null;
        if(path != null) {
            ret = icons.get(path);
            if (ret == null) {
                URL url = RB.class.getResource(path);
                if(url != null) {
                    ret = new ImageIcon(url);
                    icons.put(path, ret);
                }
            }
        }
        return ret;
    }
    
    
}
