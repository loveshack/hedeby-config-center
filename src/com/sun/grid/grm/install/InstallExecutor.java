/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 */
public class InstallExecutor implements Runnable {
    
    private static Logger log = Logger.getLogger(InstallExecutor.class.getName());
    
    private List<InstallerTask> tasks;
    
    /** Creates a new instance of InstallExecutor */
    public InstallExecutor(List<InstallerTask> tasks) {
        this.tasks = tasks;
    }
    
    private List<InstallerExecutorListener> listeners = Collections.synchronizedList(new LinkedList<InstallerExecutorListener>());
    
    public void addListener(InstallerExecutorListener lis) {
        listeners.add(lis);
    }
    public void removeListener(InstallerExecutorListener lis) {
        listeners.remove(lis);
    }
    
    protected void fireTaskStarted(InstallerTask task) {
       Object [] lis = listeners.toArray();
       if(lis != null) {
           for(Object aLis: lis) {
               ((InstallerExecutorListener)aLis).taskStarted(task);
           }
       }
    }
    
    protected void fireTaskFinished(InstallerTask task) {
       Object [] lis = listeners.toArray();
       if(lis != null) {
           for(Object aLis: lis) {
               ((InstallerExecutorListener)aLis).taskFinished(task);
           }
       }
    }
    
    protected void fireTaskFailed(InstallerTask task, Exception ex) {
       Object [] lis = listeners.toArray();
       if(lis != null) {
           for(Object aLis: lis) {
               ((InstallerExecutorListener)aLis).taskFailed(task, ex);
           }
       }
    }
    
    protected void fireFinished() {
       Object [] lis = listeners.toArray();
       if(lis != null) {
           for(Object aLis: lis) {
               ((InstallerExecutorListener)aLis).finished(this);
           }
       }
    }

    protected void fireStarted() {
       Object [] lis = listeners.toArray();
       if(lis != null) {
           for(Object aLis: lis) {
               ((InstallerExecutorListener)aLis).started(this);
           }
       }
    }
    
    public List<InstallerTask> getTasks() {
        return Collections.unmodifiableList(tasks);
    }
    
    public int getTaskCount() {
        return tasks.size();
    }
    
    public void run() {
        
        try {
            fireStarted();
            for(InstallerTask task: tasks) {
                try {
                    task.setState(InstallerTask.State.STARTED);
                    fireTaskStarted(task);
                    task.execute();
                    task.setState(InstallerTask.State.FINISHED);
                    fireTaskFinished(task);
                } catch (Exception ex) {
                    task.setState(InstallerTask.State.ERROR);
                    fireTaskFailed(task, ex);
                    break;
                }
            }
        } finally {
            fireFinished();
        }
        
    }

    public List<InstallerExecutorListener> getListeners() {
        return listeners;
    }
    
    
}
