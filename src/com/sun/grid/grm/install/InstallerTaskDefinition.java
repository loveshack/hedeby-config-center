/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.install;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
public class InstallerTaskDefinition {

    private static Logger log = Logger.getLogger(InstallerTaskFactory.class.getName());
    
    private String name;
    private String implClass;
    private InstallClassLoaderDefinition classLoaderDef;
        
    public InstallerTaskDefinition(String name, String implClass, InstallClassLoaderDefinition classLoaderDef) {
        this.name = name;
        this.implClass = implClass;
        this.classLoaderDef = classLoaderDef;
    }
    
    private InstallerTask task;
    
    public InstallerTask getTask(InstallerController controller) throws InstallerException {
        if(task == null) {
            task = createTask(controller);
        }
        return task;
    }
    
    private InstallerTask createTask(InstallerController controller) throws InstallerException {
        
        InstallerTask installerTask = null;
        ClassLoader classLoader = null;
        if(classLoaderDef != null) {
            classLoader = classLoaderDef.getClassLoader(controller);
        } else {
            classLoader = InstallerTaskFactory.class.getClassLoader();
        }

        Class clazz = null;
        try {
            clazz = classLoader.loadClass(implClass);
        } catch(ClassNotFoundException ex) {
            throw new InstallerException("implClass " + implClass + " not found", ex);
        }
        if(log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "found implClass {0} ({1})", 
                    new Object [] { clazz.getName(), clazz.getProtectionDomain().getCodeSource() } );
        }
        Constructor cons;
        try {
            cons = clazz.getConstructor(InstallerController.class, String.class);
        } catch (NoSuchMethodException ex) {
           throw new InstallerException("Constructor for class  " + implClass + " not found", ex);
        }
        
        
        try {
            return (InstallerTask)cons.newInstance(controller, name);
        } catch (InvocationTargetException ex) {
           throw new InstallerException("Can not create instance of " + implClass, ex.getTargetException());
        } catch (InstantiationException ex) {
           throw new InstallerException("Can not create instance of " + implClass, ex);
        } catch (IllegalAccessException ex) {
           throw new InstallerException("Can not create instance of " + implClass, ex);
        }
        
    }

    public String getName() {
        return name;
    }

    public String getImplClass() {
        return implClass;
    }

}
