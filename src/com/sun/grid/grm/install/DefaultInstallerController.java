/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.install;

import com.sun.grid.grm.install.InstallerController.Mode;
import java.awt.BorderLayout;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.help.HelpSetException;
import javax.help.JHelpContentViewer;
import javax.help.JHelpNavigator;
import javax.swing.JDialog;


/**
 *
 */
public class DefaultInstallerController implements InstallerController {
    
    private static Logger log = Logger.getLogger(DefaultInstallerController.class.getName());
    
    private InstallerUI ui;
    private InstallerModel model;
    
    private File stateFile;
    
    private Map<String,InstallerTaskDefinition> taskMap = new HashMap<String,InstallerTaskDefinition>();
    
    private InstallerTaskSequence sequence = new InstallerTaskSequence(this, "main");

    private Mode mode = Mode.GUI;
    
    public void setMode(Mode mode) {
        this.mode = mode;
    }
    
    public Mode getMode() {
        return mode;
    }
    
    public InstallerUI getUi() {
        return ui;
    }

    public void setUi(InstallerUI ui) {
        this.ui = ui;
    }
    
    /**
     * Creates a new instance of DefaultInstallerController
     */
    public DefaultInstallerController(InstallerModel model) {
        this.model = model;
    }
    
    public void registerTask(String name, String implClass, InstallClassLoaderDefinition classLoaderDef) {
        if(log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "Register task {0} using classloader {1}",
                    new Object [] { name, classLoaderDef.getName() } );
        }
        InstallerTaskDefinition taskDef = new InstallerTaskDefinition(name, implClass, classLoaderDef);
        taskMap.put(name, taskDef);
        sequence.addTask(taskDef);
    }
    
    public void next() {
        try {
            InstallerTask task = sequence.getCurrentTask();
            
            if(task != null) {
                task.finishDisplay();
            }
            if(sequence.hasNext()) {
                sequence.next();
                setTaskInUI();
            }
        } catch (InstallerException ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
    }
    
    private void setTaskInUI() throws InstallerException {
        InstallerTask task = sequence.getCurrentTask();
        ui.setBackEnabled(sequence.hasBack());
        ui.setNextEnabled(sequence.hasNext());
        ui.setExitAction(InstallerUI.ExitAction.INVISIBLE);
        task.beginDisplay();
        ui.setTaskPanel(task.getUI());
    }
    
    public void back() {
        try {
            if(sequence.hasBack()) {
                sequence.back();
                setTaskInUI();
            }
        } catch (InstallerException ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
    }
    
    
    public void install() {
        
        if(!mode.equals(Mode.AUTO)) {
            ui.setBackEnabled(false);
            ui.setNextEnabled(false);
            ui.setExitAction(InstallerUI.ExitAction.DISABLED);
        }
        try {
            InstallerTaskDefinition task = getTask("install");

            task.getTask(this).execute();
        } catch(InstallerException ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            if(mode.equals(Mode.AUTO)) {
                System.exit(1);
            }
        }
        
    }

    private boolean writeStateFile(boolean force) {
        if(stateFile != null) {
            InstallerUI.Confirmation writeFile = InstallerUI.Confirmation.YES;
            if(!force && stateFile.exists() ) {
                writeFile = ui.confirm("Overwrite state file '" + stateFile + "'?");
            }
            switch(writeFile) {
                case YES:
                try {
                    FileOutputStream fout = new FileOutputStream(stateFile);
                    model.write(fout);
                    fout.close();
                    log.log(Level.INFO,"data storted in {0}", stateFile);
                    return true;
                } catch (IOException ex) {
                    log.log(Level.SEVERE, "Can not write state file: " + ex.getLocalizedMessage(), ex);
                    return false;
                }            
                case NO:
                    return true;
                default:
                    return false;
            }
        }
        return true;
    }
    
    public void cancel() {
        if(stateFile != null) {
             InstallerUI.Confirmation writeFile = ui.confirm("Save data into file '" + stateFile + "'?" );
             switch(writeFile) {
                 case YES:
                     if(!writeStateFile(true)) {
                         return;
                     }
                     break;
                 case NO:
                     break;
                 default:
                     return;
             }
        }
        System.exit(1);
    }

    public void exit() {
        if(!writeStateFile(false)) {
            return;
        }
        System.exit(0);
    }
    
    HelpSet helpSet;
    HelpBroker helpBroker;
    
    private HelpSet initHelp() throws HelpSetException {
        
        if(helpSet == null) {
            
            URL hsURL = HelpSet.findHelpSet(getClass().getClassLoader(), "help/jhelpset.hs");
            helpSet = new HelpSet(null, hsURL);
            
            helpBroker = helpSet.createHelpBroker();
        }
        return helpSet;
    }

    public HelpBroker getHelpBroker() throws HelpSetException {
        initHelp();
        return helpBroker;
    }
    
    public void help() {
        
        try {
            
            getHelpBroker().setDisplayed(true);
            
        } catch (HelpSetException ex) {
            ex.printStackTrace();
        }
    }

    public InstallerModel getModel() {
        return model;
    }
    
    public void run() {
        try {
            boolean readStateFile = false;
            if(stateFile != null && stateFile.canRead()) {
                model.read(new FileInputStream(stateFile));
                readStateFile = true;
            }
            switch(mode) {
                case GUI:
                    runInterative();
                    break;
                case CONSOLE:
                    throw new UnsupportedOperationException("console mode not yet implemented");
                case AUTO:
                    if(readStateFile == false) {
                        throw new InstallerException("in auto mode a state file is required");
                    }
                    runAuto();
                    break;
                default:
                    throw new IllegalStateException("Unknown mode " + mode);
            }
        } catch (Exception ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            if(mode.equals(InstallerController.Mode.AUTO)) {
                System.exit(1);
            }
        }
    }
    
    private void runAuto() throws InstallerException {
    
        // first we go through all tasks and tell
        // them that in phase has bee finished.
        // All transient data should be stored in
        // the model
        sequence.getCurrentTask().finishDisplay();
        while(sequence.hasNext()) {
            sequence.next().finishDisplay();
        }
        
        install();
    }
    
    private void runInterative() throws InstallerException {
            ui.setExitAction(InstallerUI.ExitAction.INVISIBLE);
            setTaskInUI();
            ui.start();
    }
    
    public void setNextEnabled(boolean enabled) {
        ui.setNextEnabled(enabled);
    }

    public void setBackEnabled(boolean enabled) {
        ui.setBackEnabled(enabled);
    }
    
    
    public InstallerTaskDefinition getTask(String name) throws InstallerException {
        InstallerTaskDefinition ret = taskMap.get(name);
        if(ret == null) {
            throw new InstallerException("task " + name + " is not defined");
        }
        return taskMap.get(name);
    }
    
    public InstallerTaskSequence getSequence() {
        return sequence;
    }
    
    public File getStateFile() {
        return stateFile;
    }

    public void setStateFile(File stateFile) {
        this.stateFile = stateFile;
    }
    
    public InstallerFrame getFrame() {
        if(mode.equals(Mode.GUI)) {
            return (InstallerFrame)ui;
        } else {
            return null;
        }
    }
    
}
