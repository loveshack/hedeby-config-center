/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.install;


public class GrmInstallConstants {
    
    /**
     *
     * This constant is used logfile writing or exitcodes
     */
    public static final int GRM_OK = 0;
    
    /**
     *
     * This constant is used logfile writing or exitcodes
     */
    public static final int GRM_ERROR = 1;
    
    /**
     *
     * This constant is used logfile writing or exitcodes
     */
    public static final int GRM_INFO = 2;
    
    /**
     *
     * This constant is used logfile writing or exitcodes
     */
    public static final int GRM_DEBUG = 3;
    
    /**
     *
     * Constant for grm.cwd
     * This constant is used to store the current working
     * directory within the wizardState.
     */
    public static final String GRM_CWD = "grm.cwd";
    
    /**
     *
     * Constant for grm.install.logfile
     * This constant is used to store the install logfile
     * is located within the wizardState.
     */
    public static final String GRM_INSTALL_LOG_FILE = "grm.install.logfile";
    
    /**
     *
     * Constant for grm.systemname.exists
     * This constant is used to store, if a basic configuration
     * installation already has been done, for the given systemname
     * within the wizardState
     */
    public static final String GRM_SYSTEMNAME_EXISTS = "grm.systemname.exists";
    
    /**
     *
     * Constant for grm.basic.conf.exists
     * This constant is used to store, if a basic configuration
     * installation already has been done, for the given systemname
     * within the wizardState
     */
    public static final String GRM_BASIC_CONF_EXISTS = "grm.basic.conf.exists";
    
    /**
     *
     * Constant for grm.shared.basic.conf.exists
     * This constant is used to store, if a shared basic configuration
     * installation already has been done, for the given systemname
     * within the wizardState
     */
    public static final String GRM_SHARED_BASIC_CONF_EXISTS = "grm.shared.basic.conf.exists";
    
    /**
     *
     * Constant for grm.this.hostname
     * This constant is used to store the grm hostname
     * of the local host, where th e installer is running
     * within the wizardState
     */
    public static final String GRM_THIS_HOSTNAME = "grm.this.hostname";
    
    public static final String GRM_PREFERENCE_TYPE = "grm.preference.type";
    
    /**
     *
     * Constant for grm.admin.user
     * This constant is used to store the grm admin user name
     * within the wizardState
     */
    public static final String GRM_ADMIN_USER = "grm.admin.user";

    /**
     *
     * Constant for grm.system.name
     * This constant is used to store the grm system name
     * within the wizardState
     */
    public static final String GRM_SYSTEM_NAME = "grm.system.name";
    
    /**
     *
     * Constant for grm.dist.dir
     * This constant is used to store the grm dist dir
     * within the wizardState
     */
    public static final String GRM_DIST_DIR = "grm.dist.dir";
    
    /**
     *
     * Constant for grm.spool.dir
     * This constant is used to store the grm spool dir
     * within the wizardState
     */
    public static final String GRM_LOCAL_SPOOL_DIR = "grm.spool.dir";
    
    /**
     *
     * Constant for grm.shared.dir
     * This constant is used to store the grm shared dir
     * within the wizardState
     */
    public static final String GRM_SHARED_DIR = "grm.shared.dir";
    
    /**
     *
     * Constant for grm.sge.dist.dir
     * This constant is used to store the grm sge dist dir
     * within the wizardState
     */
    public static final String GRM_SGE_DIST_DIR = "grm.sge.dist.dir";    

    /**
     *
     * Constant for grm.install.executor
     * This constant is used to store if the executor should be installed
     * within the wizardState
     */
    public static final String INSTALL_EXECUTOR = "grm.install.executor";
    
    /**
     *
     * Constant for grm.install.bootstrap
     * This constant is used to store if the bootstrapping should be installed
     * within the wizardState
     */
    public static final String INSTALL_BOOTSTRAP = "grm.install.bootstrap";
    
    /**
     *
     * Constant for grm.install.resource.provider
     * This constant is used to store if the RP should be installed
     * within the wizardState
     */
    public static final String INSTALL_RESOURCE_PROVIDER = "grm.install.resource.provider";
    
    /**
     *
     * Constant for grm.install.service.container
     * This constant is used to store if the SC should be installed
     * within the wizardState
     */
    public static final String INSTALL_SERVICE_CONTAINER = "grm.install.service.container";
    
    /**
     *
     * Constant for grm.install.os.provisioner
     * This constant is used to store if the OSP should be installed
     * within the wizardState
     */
    public static final String INSTALL_OS_PROVISIONER = "grm.install.os.provisioner";
    
    /**
     *
     * Constant for grm.install.os.dispatcher
     * This constant is used to store if the OSD should be installed
     * within the wizardState
     */
    public static final String INSTALL_OS_DISPATCHER_ADAPTER = "grm.install.os.dispatcher";
    
    /**
     *
     * Constant for grm.install.n1sm.adapter
     * This constant is used to store if the N1SM Adapter should be installed
     * within the wizardState
     */
    public static final String INSTALL_N1SM_ADAPTER = "grm.install.n1sm.adapter";
    
    /**
     *
     * Constant for grm.install.nosecurity
     * This constant is used to store if security should be installed
     * within the wizardState
     */
    public static final String INSTALL_NO_SECURITY = "grm.install.nosecurity";

    /**
     *
     * Constant for grm.install.remote.security
     * This constant is used to store if the Security should be installed
     * from remote or local within the wizardState
     */
    public static final String INSTALL_REMOTE_SECURITY = "grm.install.remote.security";
    
    /**
     *
     * Constant for grm.system.config
     * This constant is used to store the SystemConfiguration Object
     * withing the wizardState during its creation
     */
    public static final String SYSTEM_CONFIG = "grm.system.config";
    
    /**
     *
     * Constant for grm.path.config
     * This constant is used to store the PathConfiguration Object
     * withing the wizardState during its creation
     */
    public static final String PATH_CONFIG = "grm.path.config";
    
    /**
     *
     * Constant for grm.executor.port
     * This constant is used to store the executor jvm port
     * withing the wizardState during its creation
     */
    public static final String GRM_EXECUTOR_PORT = "grm.executor.port";
    
    /**
     *
     * Constant for grm.component.port
     * This constant is used to store the component jvm port
     * withing the wizardState during its creation
     */
    public static final String GRM_COMPONENT_PORT = "grm.component.port";
    
    /**
     *
     * Constant for grm.n1sm.adapter.config
     * This constant is used to store the N1SmAdapterConfiguration Object
     * withing the wizardState during its creation
     */
    public static final String N1SM_ADAPTER_CONFIG = "grm.n1sm.adapter.config";
    
    /**
     *
     * Constant for grm.osd.config
     * This constant is used to store the OsDispatcherImplConfiguration Object
     * withing the wizardState during its creation
     */
    public static final String OS_DISPATCHER_CONFIG = "grm.osd.config";
    
    /**
     *
     * Constant for grm.security.config
     * This constant is used to store the BootstrapSecurityInstaller Object
     * withing the wizardState during its creation
     */
    public static final String SECURITY_CONFIG = "grm.security.config";

    /**
     *
     * Constant for grm.sc.config
     * This constant is used to store the ModifiableServiceContainerConfig Object
     * withing the wizardState during its creation
     */
    public static final String SERVICE_CONFIG = "grm.sc.config";
    
    /**
     *
     * Constant for grm.executor.id.config
     * This constant is used to store the executor id
     * withing the wizardState during its creation
     */
    public static final String EXECUTOR_ID_CONFIG = "grm.executor.id.config";
    
    /**
     *
     * Constant for grm.executor.shutdowntimeout.config
     * This constant is used to store the executor shutdown timeout
     * withing the wizardState during its creation
     */
    public static final String EXECUTOR_SHUTDOWTIMEOUT_CONFIG = "grm.executor.shutdowtimeout.config";
    
    /**
     *
     * Constant for grm.executor.tmpdir.config
     * This constant is used to store the executor temp dir
     * withing the wizardState during its creation
     */
    public static final String EXECUTOR_TMPDIR_CONFIG = "grm.executor.tmpdir.config";
    
    /**
     *
     * Constant for grm.executor.keepfiles.config
     * This constant is used to store the executor keepfiles
     * withing the wizardState during its creation
     */
    public static final String EXECUTOR_KEEPFILES_CONFIG = "grm.executor.keepfiles.config";

    /**
     *
     * Constant for grm.executor.cps.config
     * This constant is used to store the executor core pool size
     * withing the wizardState during its creation
     */
    public static final String EXECUTOR_CPS_CONFIG = "grm.executor.cps.config";
    
    /**
     *
     * Constant for grm.executor.mps.config
     * This constant is used to store the executor max pool size
     * withing the wizardState during its creation
     */
    public static final String EXECUTOR_MPS_CONFIG = "grm.executor.mps.config";
    
    /**
     *
     * Constant for grm.executor.idletimeout.config
     * This constant is used to store the executor idle timeout
     * withing the wizardState during its creation
     */
    public static final String EXECUTOR_IDLETIMEOUT_CONFIG = "grm.executor.idletimeout.config";

    public static final String CA_COUNTRY_CODE = "grm.security.ca.countrycode.config"; 
    public static final String CA_STATE = "grm.security.ca.state.config";
    public static final String CA_LOCATION = "grm.security.ca.location.config";
    public static final String CA_ORGANISATION = "grm.security.ca.organisation.config";
    public static final String CA_ORGA_UNIT = "grm.security.ca.orga.unit.config";
    public static final String CA_ADMIN_EMAIL = "grm.security.ca.admin.mail.config";
    
   
    /**
     *
     * Constant for grm.security.causername.config
     * This constant is used to store the ca user name
     * withing the wizardState for remote security creation
     */
    public static final String SECURITY_CAUSERNAME_CONFIG = "grm.security.causername.config";

    /**
     *
     * Constant for grm.security.capassword.config
     * This constant is used to store the ca password
     * withing the wizardState for remote security creation
     */
    public static final String SECURITY_CAPASSWORD_CONFIG = "grm.security.capassword.config";

    /**
     * Constant for the grm.security.keystore. 
     * This constant is used to define the source of the keystore
     * possible values are "file" or "statefile".
     */
    public static final String SECURITY_KEYSTORE_SOURCE = "grm.security.keystore.source";
    
    /**
     *  Value for <code>SECURITY_KEYSTORE_SOURCE</code> if the keystore is stored in the installer model
     */
    public static final String SECURITY_KEYSTORE_FROM_STATE_FILE = "STATE_FILE";
    
    /**
     *  Value for <code>SECURITY_KEYSTORE_SOURCE</code> if the keystore should be taken from an
     *  external file
     */
    public static final String SECURITY_KEYSTORE_FROM_FILE = "EXT_FILE";
    
    /**
     *  Base datakey for the keystore defintion
     */
    public static final String SECURITY_KEYSTORE = "grm.security.keystore";
    
    /**
     *  Constants for the path to the extern keystore file
     */
    public static final String SECURITY_KEYSTORE_FILE = "grm.security.keystore.file";
            
    /**
     *
     * Constant for grm.security.auth.method
     * This constant is used to store the ca password
     * withing the wizardState for remote security creation
     */
    public static final String SECURITY_AUTH_METHOD = "grm.security.auth.method";
    
    public static final String AUTH_NIS = "NIS";
    public static final String AUTH_PAM = "PAM";
    public static final String AUTH_SYSTEM = "SYSTEM";
    public static final String SECURITY_AUTH_NIS_SERVER = "grm.security.auth.nis.server";
    public static final String SECURITY_AUTH_NIS_DOMAIN = "grm.security.auth.nis.domain";
    public static final String SECURITY_AUTH_PAM_SERVICE = "grm.security.auth.pam.service";
    public static final String SECURITY_AUTH_CA_JVM = "grm.security.auth.ca.jvm";
                
    /**
     *
     * Constant for grm.osp.id.config
     * This constant is used to store the osp id
     * withing the wizardState during its creation
     */
    public static final String OSP_ID_CONFIG = "grm.osp.id.config";
    
    /**
     *
     * Constant for grm.osp.lookup.interval.config
     * This constant is used to store the osp lookup interval
     * withing the wizardState during its creation
     */
    public static final String OSP_LOOKUP_INTERVAL_CONFIG = "grm.osp.lookupinterval.config";

    /**
     *
     * Constant for grm.sc.id.config
     * This constant is used to store the sc id
     * withing the wizardState during its creation
     */
    public static final String SC_ID_CONFIG = "grm.sc.id.config";
    
    /**
     *
     * Constant for grm.sc.shutdowntime.config
     * This constant is used to store the sc shutdowntime
     * withing the wizardState during its creation
     */
    public static final String SC_SHUTDOWN_TIME_CONFIG = "grm.sc.shutdowntime.config";

    /**
     *
     * Constant for grm.sc.adaptername.config
     * This constant is used to store the sc adaptername
     * withing the wizardState during its creation
     */
    public static final String SC_ADAPTERNAME_CONFIG = "grm.sc.adaptername.config";
    
    /**
     *
     * Constant for grm.sc.adapterclassname.config
     * This constant is used to store the sc adapterclassname
     * withing the wizardState during its creation
     */
    public static final String SC_ADAPTERCLASSNAME_CONFIG = "grm.sc.adapterclassname.config";
    
    /**
     *
     * Constant for grm.sc.adapterclasspath.config
     * This constant is used to store the sc adapterclasspath
     * withing the wizardState during its creation
     */
    public static final String SC_ADAPTERCLASSPATH_CONFIG = "grm.sc.adapterclasspath.config";
    
    /**
     *
     * Constant for grm.sc.conn.sgeroot.config
     * This constant is used to store the sc connection sgeroot
     * withing the wizardState during its creation
     */
    public static final String SC_CONN_SGEROOT_CONFIG = "grm.sc.conn.sgeroot.config";
    
    /**
     *
     * Constant for grm.sc.conn.sgecell.config
     * This constant is used to store the sc connection sgecell
     * withing the wizardState during its creation
     */
    public static final String SC_CONN_SGECELL_CONFIG = "grm.sc.conn.sgecell.config";
    
    /**
     *
     * Constant for grm.sc.conn.sgemasterport.config
     * This constant is used to store the sc connection sgemasterport
     * withing the wizardState during its creation
     */
    public static final String SC_CONN_SGEMASTERPORT_CONFIG = "grm.sc.conn.sgemasterport.config";
    
    /**
     *
     * Constant for grm.sc.conn.sgeexecdport.config
     * This constant is used to store the sc connection sgeexecdport
     * withing the wizardState during its creation
     */
    public static final String SC_CONN_SGEEXECDPORT_CONFIG = "grm.sc.conn.sgeexecdport.config";
    
    /**
     *
     * Constant for grm.sc.sla.sgeattribute.config
     * This constant is used to store the sc sla sgeattribute
     * withing the wizardState during its creation
     */
    public static final String SC_SLA_SGEATTRIBUTE_CONFIG = "grm.sc.sla.sgeattribute.config";
    
    /**
     *
     * Constant for grm.sc.sla.sgevalue.config
     * This constant is used to store the sc sla sgevalue
     * withing the wizardState during its creation
     */
    public static final String SC_SLA_SGEVALUE_CONFIG = "grm.sc.sla.sgevalue.config";
    
    /**
     *
     * Constant for grm.sc.sla.minvalue.config
     * This constant is used to store the sc sla minvalue
     * withing the wizardState during its creation
     */
    public static final String SC_SLA_MINVALUE_CONFIG = "grm.sc.sla.minvalue.config";
    
    /**
     *
     * Constant for grm.sc.sla.urgency.config
     * This constant is used to store the sc sla urgency
     * withing the wizardState during its creation
     */
    public static final String SC_SLA_URGENCY_CONFIG = "grm.sc.sla.urgency.config";

    /**
     *
     * Constant for grm.sc.sla.fixedhosts.config
     * This constant is used to store the sc sla fixedhosts
     * withing the wizardState during its creation
     */
    public static final String SC_SLA_FIXEDHOSTS_CONFIG = "grm.sc.sla.fixedhosts.config";
    
    /**
     *
     * Constant for grm.rc.startcomp
     * This constant is used to store if the components
     * have to be started withing the wizardState during its creation
     */
    public static final String RC_START_COMP = "grm.rc.startcomp";
    
    /**
     *
     * Constant for grm.rc.install.classic
     * This constant is used to store if the rc scripts
     * are installed classical (init.d) withing the wizardState during its creation
     */
    public static final String RC_INSTALL_CLASSIC = "grm.rc.install.classic";
    
    /**
     *
     * Constant for grm.rc.install.typical
     * This constant is used to store if the rc scripts
     * are installed typical (greenline/insserv) withing the wizardState during its creation
     */
    public static final String RC_INSTALL_TYPICAL = "grm.rc.install.typical";
    
    /**
     *
     * Constant for grm.windows.install.as.svc
     * This constant is used to store if the windows service is installed
     * as a service (true) or in autostart folder like any programm (false)
     * within the wizardState
     */
    public static final String WINDOWS_INSTALL_AS_SVC = "grm.windows.install.as.svc";
    
    /**
     *
     * Constant for grm.windows.start.after.install
     * This constant is used to store if the windows service is started after
     * the installation (default: true) within the wizardState
     */
    public static final String WINDOWS_START_AFTER_INSTALL = "grm.windows.strat.after.install";

    /**
     *
     * Constant for grm.debug.level
     * This constant is used to store the debug level within the wizardState
     */
    public static final String GRM_DEBUG_LEVEL = "grm.debug.level";

    public static final String CONFIG_LOCATION_PREFS = "PREFERENCES";

    public static final String CONFIG_LOCATION_SHARED = "SHARED";

    public static final String GRM_INSTALL_NEWSYSTEM = "grm.install.newsystem";

    public static final String GRM_ADDHOST_TO_SYSTEM = "grm.install.addhost.to.system";

    public static final String GRM_ADDCOMPONENT_TO_SYSTEM = "grm.install.addcomponent.to.system";

    public static final String GRM_LD_LIBRARY_PATH = "grm.jvm.config.ld.library.path";

    public static final String GRM_AVAILABLE_JVMS = "grm.jvm.config.available.jvms";

    public static final String GRM_AVAILABLE_JVM_PORTS = "grm.jvm.config.available.jvm.ports";

    public static final String GRM_RP_ID = "grm.rp.config.id";

    public static final String GRM_RP_USAGE = "grm.rp.config.usage";

    public static final String GRM_RP_POLLING_PERIOD = "grm.rp.config.polling.period";

    public static final String GRM_RP_SELECTED_POLICIES = "grm.rp.config.selected.policies";

    public static final String GRM_RP_SELECTED_POLICY_NAME = "grm.rp.config.selected.policy.name";

    public static final String GRM_RP_SELECTED_POLICY_TYPE = "grm.rp.config.selected.policy.type";

    public static final String GRM_RP_SELECTED_POLICY_SETTINGS = "grm.rp.config.selected.policy.settings";

    public static final String GRM_EXECUTOR_JVM = "grm.executor.jvm";

    public static final String GRM_RP_JVM = "grm.rp.jvm";

    public static final String GRM_SC_JVM = "grm.sc.jvm";

    public static final String GRM_SELECTED_GE_ADAPTER = "grm.selected.gridengine.adapter";

    public static final String GRM_OSP_JVM = "grm.osp.jvm";

    public static final String GRM_OSP_ID_CONFIG ="grm.osp.id.config";

    public static final String GRM_OSP_LOOKUP_INTERVAL ="grm.osp.lookup.interval";
    
    public static final String GRM_OSD_ID_CONFIG = "grm.osp.id.config";

    public static final String GRM_OSD_CLASSPATH_CONFIG = "grm.osp.classpath.config";

    public static final String GRM_OSD_CLASS_CONFIG = "grm.osp.class.config";

    public static final String GRM_N1SM_N1SHPATH_CONFIG = "grm.n1sm.n1shpath.config";

    public static final String GRM_N1SM_EXPECT_CONFIG = "grm.n1sm.expect.config";

    public static final String GRM_OSD_JVM = "grm.osd.jvm";

    public static final String GRM_N1SM_ADAPTER_ID = "grm.n1sm.adapter.id";

    public static final String GRM_SELECTED_N1SM_ADAPTER = "grm.selected.n1sm.adapter";

    public static final String N1SM_ADAPTER_CONFIG_IS_EMPTY = "grm.n1sm.adapter.is.empty";
    
    public static final String SC_SLA_SGEATTRIBUTE = "sc.sla.sgeattribute";
    
    public static final String SC_SLA_SGEVALUE     = "sc.sla.sgevalue";
    
    public static final String SC_SLA_MINVALUE     = "sc.sla.minvalue";
    
    public static final String SC_SLA_URGENCY      = "sc.sla.urgency";

    public static final String SLA_CONF_ENTRY      = "sc.sla.conf.entry";;

    public static final String SYSTEM_INSTALL_MODE = "system.install.mode";

    public static final String ADD_COMPONENT_TO_HOST = "add.component.to.host";

    public static final String NEW_SYSTEM = "new.system";

    public static final String ADD_HOST_TO_SYSTEM = "add.host.to.system";

}
