/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install;

import com.sun.grid.grm.bootstrap.PreferencesType;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.EventListenerList;

/**
 *
 */
public class DefaultInstallerModel implements InstallerModel {
    
    private static Logger log = Logger.getLogger(DefaultInstallerModel.class.getName());
    private Properties data = new Properties();
    private Map<String, Object> tmpData = new HashMap<String, Object>();
    
    protected EventListenerList listenerList = new EventListenerList();
    private Map<String, EventListenerList> listenerMap = new HashMap<String, EventListenerList>();
    
    public void addModelEventListener(InstallerModelListener listener, String key) {
        EventListenerList listeners = listenerMap.get(key);
        if(listeners == null) {
            listeners = new EventListenerList();
            listenerMap.put(key, listeners);
        }
        listeners.add(InstallerModelListener.class, listener);
    }
    
    public void removeModelEventListener(InstallerModelListener listener) {
        for(EventListenerList listeners: listenerMap.values()) {
            listeners.remove(InstallerModelListener.class, listener);
        }
    }
    
    public void fireInstallerModelEvent(InstallerModelEvent evt) {
        
        EventListenerList listenerList = listenerMap.get(evt.getKey());
        if(listenerList != null) {
            Object[] listeners = listenerList.getListenerList();

            for (int i=0; i<listeners.length; i+=2) {
                if (listeners[i] == InstallerModelListener.class) {
                    ((InstallerModelListener)listeners[i+1]).changedModel(evt);
                }
            }
        }
    }
    
    /**
     * Store temporary data into the model (will not be stored in the state file)
     * @param key     the key of the data
     * @param value   the value of the data
     */
    public void setTmpData(String key, Object value) {
        boolean changed = false;
        Object oldValue = tmpData.get(key);
        if(oldValue == null) {
            changed = value != null;
        } else {
            changed = !oldValue.equals(value);
        }
        if(changed) {
            if(log.isLoggable(Level.FINER)) {
                if(value == null) {
                    log.log(Level.FINER,"setData: {0}=null", key);
                } else {
                    log.log(Level.FINER,"setData: {0}={1} ({2}, {3})", new Object [] {
                        key, value, value.getClass().getName(), value.getClass().getProtectionDomain().getCodeSource()
                    });
                }
            }
            tmpData.put(key, value);
            InstallerModelEvent evt = new InstallerModelEvent(this, key, oldValue, value);
            fireInstallerModelEvent(evt);
        }
    }
    
    public Object getTmpData(String key) {
        return tmpData.get(key);
    }
    
    public Object getData(String key) {
        return data.get(key);
    }
    
    public void setData(String key, Object value) {
        boolean changed = false;
        Object oldValue = data.get(key);
        if(oldValue == null) {
            changed = value != null;
        } else {
            changed = !oldValue.equals(value);
        }
        if(changed) {
            if(log.isLoggable(Level.FINER)) {
                if(value == null) {
                    log.log(Level.FINER,"setData: {0}=null", key);
                } else {
                    log.log(Level.FINER,"setData: {0}={1} ({2}, {3})", new Object [] {
                        key, value, value.getClass().getName(), value.getClass().getProtectionDomain().getCodeSource()
                    });
                }
            }
            data.put(key, value);
            InstallerModelEvent evt = new InstallerModelEvent(this, key, oldValue, value);
            fireInstallerModelEvent(evt);
        }
//        if (data.get(key) != null) {
//            if (!key.equals(GrmInstallConstants.SYSTEM_CONFIG) &&
//                    !key.equals(GrmInstallConstants.GRM_SGE_DIST_DIR) &&
//                    !key.equals(GrmInstallConstants.GRM_LOCAL_SPOOL_DIR) &&
//                    !key.equals(GrmInstallConstants.GRM_ETC_DIR) &&
//                    !key.equals(GrmInstallConstants.GRM_ADMIN_USER)) {
//                if (!data.get(key).toString().equals(value.toString())){
//                    System.out.println("Saved data:" + data.get(key).toString());
//                    System.out.println("New data:" + value.toString());
//                    log.log(Level.FINER,"setData fireChangeEvent: {0}", key);
//                    this.fireInstallerModelEvent(new InstallerModelEvent(this));
//                }
//            }
//        } else {
//            this.fireInstallerModelEvent(new InstallerModelEvent(this));
//        }
//        data.put(key, value);
    }
    
    public boolean getBoolean(String key) {
        Object ret = getData(key);
        if(ret instanceof Boolean) {
            return ((Boolean)ret).booleanValue();
        } else if (ret instanceof String) {
            return Boolean.valueOf((String)ret);
        } else {
            return false;
        }
    }
    
    public int getInt(String key) {
        Object ret = getData(key);
        if (ret instanceof Integer) {
            return ((Integer)ret).intValue();
        } else if (ret instanceof String) {
            return Integer.valueOf((String)ret);
        } else {
            return 1;
        }
    }
    
    public double getDouble(String key)  {
        Object ret = getData(key);
        if (ret instanceof Number) {
            return ((Number)ret).doubleValue();
        } else if (ret instanceof String) {
            return Double.valueOf((String)ret);
        } else {
            return 0.0;
        }
    }
    
    public PreferencesType getPreferencesType(String key) {
        Object ret = getData(key);
        
        if (ret instanceof String) {
            if (ret.equals("SYSTEM")) {
                return PreferencesType.SYSTEM;
            } else if (ret.equals("USER")) {
                return PreferencesType.USER;
            }
        }
        
        return (PreferencesType) ret;
    }
    
    public File getFile(String key) {
        Object ret = getData(key);
        if(ret instanceof File) {
            return (File)ret;
        } else if(ret instanceof String) {
            return new File((String)ret);
        } else {
            throw new IllegalStateException("Invalid data value for key " + key + "(" + ret + ")");
        }
    }

    
    public void write(OutputStream out) throws IOException {
        for(Object key: data.keySet()) {
            Object value = data.get(key);
            if(value instanceof File) {
                data.put(key, ((File)value).getAbsolutePath());
            } else if(!(value instanceof String)) {
                data.put(key, value.toString());
            }
        }
        data.store(out, "Written by Hedeby installer");
    }
    
    public void read(InputStream in) throws IOException {
        data.load(in);
    }
    
    public void clear() {
        Properties oldData = data;
        data = new Properties();
        for(Object key: oldData.keySet()) {
            fireInstallerModelEvent(new InstallerModelEvent(this, (String)key, oldData.get(key), null));
        }
    }
    
    
}
