/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.install.AbstractInstallerTask;
import com.sun.grid.grm.install.GrmInstallConstants;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerModelListener;
import com.sun.grid.grm.install.ui.CheckBoxControl;
import com.sun.grid.grm.install.ui.ComboBoxControl;
import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.FileControl;
import com.sun.grid.grm.install.ui.GrmNameControl;
import com.sun.grid.grm.install.ui.SgeRootControl;
import com.sun.grid.grm.install.ui.TextControl;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import javax.swing.JFileChooser;
import javax.swing.JPanel;



/**
 *
 * 
 */
public class SelectSystemFromShared  extends AbstractInstallerTask {
    
    private UI ui;

    public static final String SYSTEM_SOURCE = "systemSource";
    public static final String USE_SYSTEM_FROM_PREFS  = "prefs";
    public static final String USE_SYSTEM_FROM_SHARED = "shared";
    
    public static final String OVER_WRITE_SHARED_SETTINGS = "OverwriteSharedSettings";
    
    private class UI extends ControlPanel {
        
        private ComboBoxControl prefControl;
        private GrmNameControl  nameControl;
        private TextControl     adminControl;
        private FileControl     distControl;
        private FileControl     localSpoolControl;
        private FileControl     sgeRootControl;
        public UI() {
            
            InstallerController controller = getController();
            InstallerModel model = controller.getModel();
            setTitle(rb().getString("selectSystemFromShared.header"));
            
            prefControl = new ComboBoxControl(SelectSystemFromShared.this, model, GrmInstallConstants.GRM_PREFERENCE_TYPE, new String[] { "SYSTEM", "USER"});            
            prefControl.setEnabled(false);
            
            nameControl = new GrmNameControl(SelectSystemFromShared.this, model, GrmInstallConstants.GRM_SYSTEM_NAME, 20);
            nameControl.setEnabled(false);
            
            adminControl = new TextControl(SelectSystemFromShared.this, getModel(), GrmInstallConstants.GRM_ADMIN_USER, 20);
            adminControl.setEnabled(false);
            
            distControl = new FileControl(SelectSystemFromShared.this, model, GrmInstallConstants.GRM_DIST_DIR);
            distControl.setEnabled(false);
            distControl.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            localSpoolControl  = new FileControl(SelectSystemFromShared.this, model, GrmInstallConstants.GRM_LOCAL_SPOOL_DIR);
            localSpoolControl.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            localSpoolControl.setEnabled(false);
            sgeRootControl = new SgeRootControl(SelectSystemFromShared.this, model, GrmInstallConstants.GRM_SGE_DIST_DIR);
            sgeRootControl.setEnabled(false);
            
            String name = SelectSystemFromShared.this.getName();
            setControls(
               new String [] {
                "SharedDir",
                null,
                "PreferenceType",
                "SystemName",
                "AdminUser",
                "DistDir",
                "LocalSpoolDir",
                "SgedistDir",
               },
               new Control [] {
                  new FileControl(SelectSystemFromShared.this, model, GrmInstallConstants.GRM_SHARED_DIR).fileSelectionMode(JFileChooser.DIRECTORIES_ONLY),
                  new CheckBoxControl(SelectSystemFromShared.this, model, OVER_WRITE_SHARED_SETTINGS, "selectSystemFromShared.overwrite"),
                  prefControl,
                  nameControl,
                  adminControl,
                  distControl,
                  localSpoolControl,
                  sgeRootControl
               },
               rb()
            );
        }
        
    }
    
    
    class SystemObserver implements InstallerModelListener {
        
        boolean isChecked;
        
        public void register(InstallerModel model) {
            model.addModelEventListener(this, OVER_WRITE_SHARED_SETTINGS);
            model.addModelEventListener(this, GrmInstallConstants.GRM_SHARED_DIR);
        }
        
        public void unregister(InstallerModel model) {
            model.removeModelEventListener(this);
        }
        
        public void changedModel(InstallerModelEvent evt) {

            if(evt.getKey().equals(OVER_WRITE_SHARED_SETTINGS)) {
                boolean enabled = false;
                Object value = evt.getNewValue();
                if(value instanceof Boolean) {
                    enabled = ((Boolean)value).booleanValue();
                } else {
                    enabled = Boolean.parseBoolean(value.toString());
                }
                ui.prefControl.setEnabled(enabled);
                ui.nameControl.setEnabled(enabled);
                ui.adminControl.setEnabled(enabled);
                ui.distControl.setEnabled(enabled);
                ui.localSpoolControl.setEnabled(enabled);
                ui.sgeRootControl.setEnabled(enabled);
            } else if (evt.getKey().equals(GrmInstallConstants.GRM_SHARED_DIR)) {
                initFromShared((String)evt.getNewValue());
            }
            
        }
        
    }
    
    private void initFromShared(String sharedDirname) {
        
        
        File sharedDir = new File(sharedDirname);
        if(sharedDir.exists()) {
            
            File instStateFile = new File(sharedDir, "inst_hedeby.properties");
                
            try {
                FileInputStream fin = new FileInputStream(instStateFile);
                Properties props = new Properties();

                props.load(fin);

                String propNames [] = new String [] {
                    GrmInstallConstants.GRM_PREFERENCE_TYPE,
                    GrmInstallConstants.GRM_SYSTEM_NAME,
                    GrmInstallConstants.GRM_LOCAL_SPOOL_DIR,
                    GrmInstallConstants.GRM_DIST_DIR,
                    GrmInstallConstants.GRM_SGE_DIST_DIR
                };
                for(String prop: propNames) {
                    Object value = props.get(prop);
                    if(value != null) {
                        getModel().setData(prop, value);
                    }
                }
                ui.init();
            } catch (InstallerException ex) {
                log.log(Level.WARNING,ex.getLocalizedMessage(), ex);
            } catch(IOException ioe) {
                log.log(Level.FINE,ioe.getLocalizedMessage(), ioe);
            }
        }
    }
    
    private SystemObserver systemObserver = new SystemObserver();
    
    /**
     * Creates a WelcomePanel with the specified name, the specified
     * route and  wizard manager.
     *
     * @param controller the installer controller
     * @param name  of the the task
     */
    public SelectSystemFromShared(InstallerController controller, String name) {
        super(controller, name);
    }
    
  
    public JPanel getUI() {
        if(ui == null) {
            ui = new UI();
        }
        return ui;
    }
    
    public void beginDisplay() throws InstallerException {
        ((UI)getUI()).init();
        systemObserver.register(getController().getModel());
    }
    
    public void finishDisplay() throws InstallerException {
        systemObserver.unregister(getController().getModel());
    }
    
    public void execute() throws InstallerException {
        
        
    }
    
    public void undo() {
    }
    
    
    
    
    
}
