/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.bootstrap.GrmName;
import com.sun.grid.grm.bootstrap.InvalidGrmNameException;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.install.GrmInstallConstants;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.GrmComponentNameControl;
import com.sun.grid.grm.install.ui.JvmListControl;
import com.sun.grid.grm.install.ui.SpinnerControl;
import com.sun.grid.grm.os.impl.OsProvisionerImplConfiguration;
import com.sun.grid.grm.util.Hostname;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import javax.swing.DefaultListModel;
import javax.swing.JPanel;

/**
 *
 * 
 */
public class GrmOspConfigurationTask extends ExtendedAbstractInstallerTask {
    
    private GenericNextObserver nextObserver;
    private DefaultListModel componentListModel;
    private UI ui;
    private JvmListModel jvmListModel;
    
    private class UI extends ControlPanel {
        
        public UI() throws InstallerException {
            InstallerController controller = getController();
            
            setTitle(rb().getString("grmOspConfiguration.Header"));
            setControls(
                    new String [] {
                "grmOspConfiguration.Identifier",
                "grmOspConfiguration.LookupInterval",
                "grmOspConfiguration.Jvm"
            },
                    new Control [] {
                new GrmComponentNameControl(GrmOspConfigurationTask.this, getModel(), GrmInstallConstants.GRM_OSP_ID_CONFIG, componentListModel, getSystemConfiguration()),
                new SpinnerControl(GrmOspConfigurationTask.this, getModel(), GrmInstallConstants.GRM_OSP_LOOKUP_INTERVAL, 0, 60, 1),
                new JvmListControl(GrmOspConfigurationTask.this, getModel(), GrmInstallConstants.GRM_OSP_JVM, jvmListModel)
            },
                    rb()
                    );
        }
    }
    
    /**
     * Creates a new instance of GrmRpConfigurationTask
     */
    public GrmOspConfigurationTask(InstallerController controller, String name) {
        super(controller, name);
        jvmListModel = JvmListModel.getInstance(controller.getModel());
        componentListModel = ComponentListModel.getInstance(getModel());
    }
    
    public JPanel getUI() throws InstallerException {
        if(ui == null) {
            ui = new UI();
            nextObserver = createGenericNextObserver(ui);
        }
        return ui;
    }
    
    public void beginDisplay() throws InstallerException {
        ((UI)getUI()).init();
        nextObserver.register();
    }
    
    public void finishDisplay() throws InstallerException {
        if(nextObserver != null) {
            nextObserver.unregister();
        }
    }
    
    public void execute() throws InstallerException {
        fireStarted();
        log.log(Level.FINER, "execute Method grmOspConfiguration");
        log.log(Level.FINER, "systemConfiguration in grmOspConfiguration: " + getSystemConfiguration().toString());
        int stepCount = 2;
        double percentPerStep = 100/stepCount;
        double percent = 0;
        copyRemoteCa();
        fireProgress("...ca complete!", (int)percent);
        createOspConfig();
        fireProgress("...os provisioner configuration created!", (int)percent);
        log.log(Level.FINER, "execute Method grmOspConfiguration");
        log.log(Level.FINER, "systemConfiguration in grmOspConfiguration after execute: " + getSystemConfiguration().toString());
        fireFinished();
    }
    
    public void undo() {
    }
    
    
    public void createOspConfig() throws InstallerException {
        OsProvisionerImplConfiguration ospc = new OsProvisionerImplConfiguration();
        SystemConfiguration sc = getSystemConfiguration();
        File confFile = null;
        com.sun.grid.grm.util.Printer printer = null;
        String config = (String)getData(GrmInstallConstants.GRM_OSP_ID_CONFIG);
        int lookUpInterval = getController().getModel().getInt(GrmInstallConstants.GRM_OSP_LOOKUP_INTERVAL);
        
        try {
            
            ospc.setIdentifier(new GrmName(config));
        } catch (InvalidGrmNameException ex) {
            //ignore, not possible
        }
        ospc.setLookupInterval(lookUpInterval);
        
        confFile = new File(sc.getPathConfiguration().getConfigPath().getAbsolutePath() +
                File.separatorChar + config + File.separatorChar + "config.xml");
        try {
            printer = new com.sun.grid.grm.util.Printer(confFile);
            InstallUtil.changeFileOwner(getModel(), confFile);
        } catch (IOException ex) {
            throw new InstallerException("grm osp config creation reported error", ex); 
        }
        ospc.writeTo(printer, config);
        printer.close();
        
        
        GrmName name;
        try {
            name = new GrmName(config);
        } catch (InvalidGrmNameException ex) {
            throw new InstallerException("{0} is not a valid grm name", rb(), config);
        }
        GrmName jvmName;
        try {
            jvmName = new GrmName((String) getData(GrmInstallConstants.GRM_OSP_JVM));
        } catch (InvalidGrmNameException ex) {
            throw new InstallerException("{0} is not a valid grm name", rb(), (String) getData(GrmInstallConstants.GRM_OSP_JVM));
        }
        Hostname hostname = Hostname.getLocalHost();
        addComponent(name, "com.sun.grid.grm.os.impl.OsProvisionerImpl", config, jvmName, hostname);
    }
}
