/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.bootstrap.PathConfiguration;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.install.GrmInstallConstants;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.ui.CheckBoxControl;
import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.ControlStateChangedListener;
import com.sun.grid.grm.util.FileUtil;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import javax.swing.JPanel;

/**
 *
 * 
 */
public class GrmRcConfigurationTask extends ExtendedAbstractInstallerTask {
    
    
    private UI ui;
    private CheckBoxControl startComp;
    private CheckBoxControl rcClassic;
    private CheckBoxControl rcTypical;
    
    private class UI extends ControlPanel {
        
        public UI() {
            InstallerController controller = getController();
            
            startComp = new CheckBoxControl(GrmRcConfigurationTask.this, getModel(), GrmInstallConstants.RC_START_COMP, "grmRcConfiguration.StartComponents");
            rcClassic = new CheckBoxControl(GrmRcConfigurationTask.this, getModel(), GrmInstallConstants.RC_INSTALL_CLASSIC, "grmRcConfiguration.InstallRcClassic");
            rcTypical = new CheckBoxControl(GrmRcConfigurationTask.this, getModel(), GrmInstallConstants.RC_INSTALL_TYPICAL, "grmRcConfiguration.InstallRcOsTypical");
            
            setTitle(rb().getString("grmRcConfiguration.Header"));
            setControls(
                    new String [] {
                null,
                null,
                null
            },
                    new Control [] {
                startComp,
                rcClassic,
                rcTypical
            },
                    rb()
                    );
        }
    }
    
    /**
     * Creates a new instance of GrmRpConfigurationTask
     */
    public GrmRcConfigurationTask(InstallerController controller, String name) {
        super(controller, name);
    }
    
    public JPanel getUI() {
        if(ui == null) {
            ui = new UI();
            ui.addStateChangedListener(new ControlStateChangedListener() {
                public void stateChanged(Control control) {
                    getController().setNextEnabled(control.getState().equals(Control.State.OK));
                }
            });
        }
        return ui;
    }
    
    public void beginDisplay() throws InstallerException {
        getUI();
        ui.init();
        if (!Platform.getPlatform().isSuperUser()) {
            startComp.setEnabled(true);
            rcClassic.setEnabled(false);
            rcTypical.setEnabled(false);
        } else {
            startComp.setEnabled(true);
            rcClassic.setEnabled(true);
            rcTypical.setEnabled(false);
        }
    }
    
    public void finishDisplay() throws InstallerException {
        getModel().setData(GrmInstallConstants.SYSTEM_CONFIG, "");
    }
    
    public void execute() throws InstallerException {
        fireStarted();
        log.log(Level.FINER, "execute Method grmRcConfiguration");
        log.log(Level.FINER, "systemConfiguration in grmRcConfiguration: " + getSystemConfiguration().toString());
        int stepCount = 4;
        double percentPerStep = 100/stepCount;
        double percent = 0;
        log.finer("... creating system configuration instance!");
        
        editRcScript();
        fireProgress("...rc is set up!", (int)percent);
        createConfiguration();
        fireProgress("...system configuration created!", (int)percent);
        startComponent();
        fireProgress("...components started!", (int)percent);
        installRcScript();
        fireProgress("...rc startup scripts installed", (int)percent);
        makeSystemDefault();
        fireProgress("...made system to default system", (int)percent);
        if(GrmInstallConstants.NEW_SYSTEM.equals(getModel().getData(GrmInstallConstants.SYSTEM_INSTALL_MODE))) {
            writeInstProperties();
            fireProgress("...wrote installer properties", (int)percent);
        }
        log.log(Level.FINER, "execute Method grmRcConfiguration");
        log.log(Level.FINER, "systemConfiguration in grmRcConfiguration after task: " + getSystemConfiguration().toString());
        fireFinished();
    }
    
    public void undo() {
    }
    
    
    public void startComponent() throws InstallerException {
        
        if (getModel().getBoolean(GrmInstallConstants.RC_START_COMP) == false) {
            log.log(Level.FINE, "starting component not selected");
            return;
        }
        SystemConfiguration sc;
        try {
            sc = getSystemConfiguration();
        } catch (InstallerException ex) {
            throw new InstallerException(ex.getLocalizedMessage());
        }
        PathConfiguration pc = sc.getPathConfiguration();
        File gstatCMD = new File(pc.getDistPath().getAbsolutePath() + File.separator + "bin" + File.separator + "gstat");
        File rcCWD = new File(pc.getDistPath().getAbsolutePath() + File.separator + "bin");
        try {
            if (gstatCMD.exists()) {
               Platform.getPlatform().exec(gstatCMD.getAbsolutePath() + " --system "
                       + getModel().getData(GrmInstallConstants.GRM_SYSTEM_NAME)
                       + " --prefs " 
                       + getModel().getPreferencesType(GrmInstallConstants.GRM_PREFERENCE_TYPE)
                       + " start", null, rcCWD, null, null);
                
            } else {
               log.log(Level.SEVERE, "cannot find gstat binary: \"" + gstatCMD.getAbsolutePath() + "\"");
            }
        } catch (InterruptedException ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(),ex);
        } catch (IllegalStateException ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(),ex);
        } catch (IOException ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(),ex);
        }
    }
    
    public void installRcScript() throws InstallerException{
        log.entering(getClass().getName(), "installRcScript");
        if (getModel().getBoolean(GrmInstallConstants.RC_INSTALL_CLASSIC) == false && getModel().getBoolean(GrmInstallConstants.RC_INSTALL_TYPICAL) == false) {
            log.exiting(getClass().getName(), "installRcScript");
            return;
        }
        SystemConfiguration sc;
        try {
            sc = getSystemConfiguration();
        } catch (InstallerException ex) {
            throw new InstallerException(ex.getLocalizedMessage(), ex);
        }
        PathConfiguration pc = sc.getPathConfiguration();
        File svcgrm_src = new File(pc.getDistPath().getAbsolutePath() + File.separator + "bin" + File.separator + "svcgrm");
        File svcgrm_dest = new File(File.separator + "etc" + File.separator + "init.d" + File.separator + "svcgrm");
        File svcgrm_link = new File(File.separator + "etc" + File.separator + "rc2.d" + File.separator + "S96svcgrm");
        try {
            InstallUtil.copyFile(svcgrm_src, svcgrm_dest);
        } catch (FileNotFoundException ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(),ex);
            log.exiting(getClass().getName(), "installRcScript");
            return;
        }
        try {
            Platform.getPlatform().chmod(svcgrm_dest, "755");
            if (!svcgrm_link.isFile()) {
               Platform.getPlatform().link(svcgrm_dest, svcgrm_link);
            } else {
               log.log(Level.INFO, "link \"" +  svcgrm_link.getAbsolutePath() + "\" already exists");
            }
        } catch (IllegalStateException ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(),ex);
            log.exiting(getClass().getName(), "installRcScript");
            return;
        } catch (InterruptedException ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(),ex);
            log.exiting(getClass().getName(), "installRcScript");
            return;
        } catch (IOException ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(),ex);
            log.exiting(getClass().getName(), "installRcScript");
            return;
        }
        log.exiting(getClass().getName(), "installRcScript");
    }
    
    public void makeSystemDefault() throws InstallerException {
        SystemConfiguration sc;
        try {
            sc = getSystemConfiguration();
        } catch (InstallerException ex) {
            throw new InstallerException(ex.getLocalizedMessage());
        }
        PathConfiguration pc = sc.getPathConfiguration();
        String gconfCmd = pc.getDistPath() + File.separator + "bin" + File.separator + "gconf";
        try {
            Platform.getPlatform().exec(gconfCmd + " -s " + getModel().getData(GrmInstallConstants.GRM_SYSTEM_NAME) + " set_system_property -n auto_start -v true", null, null, null, null);
        } catch (IllegalStateException ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(),ex);
        } catch (IOException ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(),ex);
        } catch (InterruptedException ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(),ex);
        }
    }
    
    
    private void createConfiguration() throws InstallerException {
        
        SystemConfiguration sc = getSystemConfiguration();
        
        try {
            sc.isValid();
            sc.writeToBackStore();
            InstallUtil.changeFileOwner(getModel(), sc.getPathConfiguration().getConfigFile());
        } catch (Exception ex) {
            throw new InstallerException(ex.getLocalizedMessage(), ex);
        }
    }
    
    private void editRcScript() throws InstallerException{
        
        SystemConfiguration sc;
        try {
            sc = getSystemConfiguration();
        } catch (InstallerException ex) {
            throw new InstallerException(ex.getLocalizedMessage());
        }
        PathConfiguration pc = sc.getPathConfiguration();
        
        File templateDir = new File(pc.getDistPath(), "grm_util" + File.separatorChar + "templates");
        
        if ( Platform.getPlatform().isSuperUser()) {
            
            if(Platform.getPlatform().isWindowsOs()) {
                log.severe("installation of rc scripts on Windows not yet implemented");
                return;
            }
            
            String buf = null;
            com.sun.grid.grm.util.Printer rcPrinter = null;
            File env_templ = new File(templateDir, "grm.env.template");
            
            File env_cp_path = new File("/etc/grm/svcmgmt".replace('/', File.separatorChar));
            
            env_cp_path.mkdirs();
            
            File env_cp = new File(env_cp_path, "grm.env");
            
            try {
                Map<String, String> patterns = new HashMap<String, String>(1);
                
                patterns.put("@@@GRM_DIST@@@", pc.getDistPath().getAbsolutePath());
                patterns.put("@@@JAVA_HOME@@@", System.getProperty("java.home"));
                
                FileUtil.replace(env_templ, env_cp, patterns);
                
            } catch (FileNotFoundException ex) {
                log.log(Level.SEVERE, "grm.env.template script not found. Could not edit!", ex);
            } catch (IOException ex) {
                log.log(Level.SEVERE, "grm.env.template script not found. Could not edit!", ex);
            } catch (IllegalStateException ex) {
                log.log(Level.SEVERE, ex.getLocalizedMessage(),ex);
            }
        }
        
        File lps = new File(templateDir, "logging.properties.template");
        File lpd = new File(pc.getLocalSpoolPath(), "logging.properties");
        
        try {
            InstallUtil.copyFile(lps,lpd);
            InstallUtil.changeFileOwner(getModel(), lpd);
        } catch (FileNotFoundException ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(),ex);
        }
        
    }
    
    private void writeInstProperties() throws InstallerException {
        
        SystemConfiguration sc = getSystemConfiguration();
        Properties props = new Properties();
        
        props.setProperty(GrmInstallConstants.GRM_PREFERENCE_TYPE, (String)getModel().getData(GrmInstallConstants.GRM_PREFERENCE_TYPE));
        props.setProperty(GrmInstallConstants.GRM_SYSTEM_NAME, (String)getModel().getData(GrmInstallConstants.GRM_SYSTEM_NAME));
        props.setProperty(GrmInstallConstants.GRM_LOCAL_SPOOL_DIR, (String)getModel().getData(GrmInstallConstants.GRM_LOCAL_SPOOL_DIR));
        props.setProperty(GrmInstallConstants.GRM_SGE_DIST_DIR, (String)getModel().getData(GrmInstallConstants.GRM_SGE_DIST_DIR));
        
        File propsFile = new File(sc.getPathConfiguration().getSharedPath(), "inst_hedeby.properties");
        FileOutputStream fout;
        try {
            fout = new FileOutputStream(propsFile);
            props.store(fout, null);
            InstallUtil.changeFileOwner(getModel(), propsFile);
        } catch (IOException ex) {
            log.log(Level.WARNING, "grmRcConfiguration.writeInstPropsFailed", 
                    new Object [] { propsFile, ex.getLocalizedMessage() } );
        } 
    }
    
}


