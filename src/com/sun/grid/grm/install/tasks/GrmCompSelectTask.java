/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.GrmName;
import com.sun.grid.grm.bootstrap.IncorrectFileFormatException;
import com.sun.grid.grm.bootstrap.InvalidConfigurationException;
import com.sun.grid.grm.bootstrap.InvalidGrmNameException;
import com.sun.grid.grm.bootstrap.PathConfiguration;
import com.sun.grid.grm.bootstrap.PreferencesNotAvailableException;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.install.GrmInstallConstants;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerTask;
import com.sun.grid.grm.install.InstallerTaskSequence;
import com.sun.grid.grm.install.ui.CheckBoxListControl;
import com.sun.grid.grm.install.ui.CheckBoxListControlModel;
import com.sun.grid.grm.install.ui.CheckBoxListControlModelListener;
import com.sun.grid.grm.install.ui.ControlPanel;
import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.swing.JPanel;

/**
 * This dialog asks the user, which component has to be installed.
 *
 *
 */
public class GrmCompSelectTask extends ExtendedAbstractInstallerTask implements InstallerTask {
    
    public static final String SELECTED_COMPONENT = "SELECTED_COMPONENT";
    
    public static final String BOOTSTRAP = "BOOTSTRAP";
    public static final String BOOTSTRAP_REMOTE  = "BOOTSTRAP_REMOTE";
    public static final String EXECUTOR = "EXECUTOR";
    public static final String RESOURCE_PROVIDER = "RESOURCE_PROVIDER";
    public static final String SERVICE_CONTAINER = "SERVICE_CONTAINER";
    public static final String OS_PROVISIONER = "OS_PROVISIONER";
    public static final String OS_DISPATCHER_ADAPTER = "OS_DISPATCHER_ADAPTER";
    public static final String SELECTION_FINISHED = "SELECTION_FINISHED";
    
    public static final String COMPONENT_INSTALL_LIST = "COMPONENT_INSTALL_LIST";
    
    private static Map<String, List<String>> PATH_MAP = new HashMap<String, List<String>>();
    
    private static void reg(String component, String [] path) {
        PATH_MAP.put(component, Arrays.asList(path));
    }
    
    static {
        reg(BOOTSTRAP             , new String [] { "grmSecuritySystem", "grmSecurityAuth", "grmSecurityAdminUser" });
        reg(BOOTSTRAP_REMOTE      , new String [] { "grmRemoteCaPassword" } );
        reg(EXECUTOR              , new String [] { "grmExecutorConfig" })    ;
        reg(RESOURCE_PROVIDER     , new String [] { "grmRpConfiguration" })    ;
        reg(SERVICE_CONTAINER     , new String [] { "grmScConfiguration", "grmGeAdapterConfiguration", "grmSloConfiguration" });
        reg(OS_PROVISIONER        , new String [] { "grmOspConfiguration" });
        reg(OS_DISPATCHER_ADAPTER , new String [] { "grmOsdN1smBasicConfiguration", "grmN1smConfiguration" });
    }
    
    private List<String> getPathForComponent(String comp){
        if(comp.equals(BOOTSTRAP)) {
            String mode = (String)getModel().getData(GrmInstallConstants.SYSTEM_INSTALL_MODE);
            if(mode.equals(GrmInstallConstants.ADD_HOST_TO_SYSTEM) ||
               mode.equals(GrmInstallConstants.ADD_COMPONENT_TO_HOST)){
                comp = BOOTSTRAP_REMOTE;
            }
        }
        return PATH_MAP.get(comp);
    }
    
    private CheckBoxListControlModel componentSelectModel = new CheckBoxListControlModel(getModel(), COMPONENT_INSTALL_LIST);
    private UI ui;
    class UI extends ControlPanel {
        
        private CheckBoxListControl executorCheckBox;
        private CheckBoxListControl bootstrapCheckBox;
        private CheckBoxListControl rpCheckBox;
        private CheckBoxListControl scCheckBox;
        private CheckBoxListControl ospCheckBox;
        private CheckBoxListControl osdCheckBox;
        
        public UI() {
            
            setTitle(rb().getString("grmCompSelect.Header"));
            
            String [] labels = {
                null,
                null,
                null,
                null,
                null,
                null,
            };
            
            bootstrapCheckBox = new CheckBoxListControl(GrmCompSelectTask.this, componentSelectModel, BOOTSTRAP, "grmCompSelect.BootstrapInstallation");
            executorCheckBox = new CheckBoxListControl(GrmCompSelectTask.this, componentSelectModel, EXECUTOR, "grmCompSelect.ExecInstall");
            rpCheckBox = new CheckBoxListControl(GrmCompSelectTask.this, componentSelectModel, RESOURCE_PROVIDER, "grmCompSelect.RpInstall");
            scCheckBox = new CheckBoxListControl(GrmCompSelectTask.this, componentSelectModel, SERVICE_CONTAINER, "grmCompSelect.ScInstall");
            ospCheckBox = new CheckBoxListControl(GrmCompSelectTask.this, componentSelectModel, OS_PROVISIONER, "grmCompSelect.OspInstall");
            osdCheckBox = new CheckBoxListControl(GrmCompSelectTask.this, componentSelectModel, OS_DISPATCHER_ADAPTER, "grmCompSelect.OsdSmaInstall");
            
            bootstrapCheckBox.setEnabled(true);
            executorCheckBox.setEnabled(true);
            rpCheckBox.setEnabled(true);
            scCheckBox.setEnabled(true);
            ospCheckBox.setEnabled(true);
            osdCheckBox.setEnabled(true);
            
            CheckBoxListControl controls [] = {
                bootstrapCheckBox,
                executorCheckBox,
                rpCheckBox,
                scCheckBox,
                ospCheckBox,
                osdCheckBox,
            };
            
            setControls(labels, controls, rb());
            
        }
    }
    
    public GrmCompSelectTask(InstallerController controller, String name) {
        super(controller, name);
    }
    
    
    public JPanel getUI() {
        if(ui == null) {
            ui = new UI();
        }
        return ui;
    }
    
    private class CheckBoxObserver implements CheckBoxListControlModelListener {
        
        
        public void update() {
            
            Object mode = getModel().getData(GrmInstallConstants.SYSTEM_INSTALL_MODE);

            if(GrmInstallConstants.ADD_COMPONENT_TO_HOST.equals(mode)) {
            
              ui.bootstrapCheckBox.setEnabled(false);
              ui.executorCheckBox.setEnabled(true);
              ui.rpCheckBox.setEnabled(true);
              ui.scCheckBox.setEnabled(true);
              ui.ospCheckBox.setEnabled(true);
              ui.osdCheckBox.setEnabled(true);
                
            } else {
                if(componentSelectModel.contains(BOOTSTRAP)) {
                    ui.bootstrapCheckBox.setEnabled(componentSelectModel.size() == 1);
                    ui.executorCheckBox.setEnabled(true);
                    ui.rpCheckBox.setEnabled(true);
                    ui.scCheckBox.setEnabled(true);
                    ui.ospCheckBox.setEnabled(true);
                    ui.osdCheckBox.setEnabled(true);
                } else {
                    ui.bootstrapCheckBox.setEnabled(true);
                    ui.executorCheckBox.setEnabled(false);
                    ui.rpCheckBox.setEnabled(false);
                    ui.scCheckBox.setEnabled(false);
                    ui.ospCheckBox.setEnabled(false);
                    ui.osdCheckBox.setEnabled(false);
                }
            }
            
            
        }
        public void added(CheckBoxListControlModel model, Collection<String> names) {
            update();
        }
        
        public void removed(CheckBoxListControlModel model, Collection<String> names) {
            update();
        }
    }
    
    private class NextObserver implements CheckBoxListControlModelListener {
        
        private void update() {
            getController().setNextEnabled(componentSelectModel.size() > 0);
        }
        public void added(CheckBoxListControlModel model, Collection<String> names) {
            update();
        }
        
        public void removed(CheckBoxListControlModel model, Collection<String> names) {
            update();
        }
        
    }
    
    private class ControlObserver implements CheckBoxListControlModelListener {
        
        public void added(CheckBoxListControlModel model, Collection<String> names) {
            InstallerTaskSequence seq = getController().getSequence();
            for(String name: names) {
                try {
                    seq.addTasksBefore("grmRcConfiguration", getPathForComponent(name));
                } catch (InstallerException ex) {
                    log.log(Level.WARNING,"install task not found in current path");
                }
            }
        }
        
        public void removed(CheckBoxListControlModel model, Collection<String> names) {
            for(String name: names) {
                List<String> path = getPathForComponent(name);
                InstallerTaskSequence seq = getController().getSequence();
                for(String task: path) {
                    try {
                        seq.removeTaskFromPath(task);
                    } catch (InstallerException ex) {
                        log.log(Level.WARNING,"Can not remove task {0}", task);
                    }
                }
            }
        }
        
        public void update() throws InstallerException {
            
            InstallerTaskSequence seq = getController().getSequence();
            
            seq.removeTasksBetween(GrmCompSelectTask.this.getName(), "grmRcConfiguration");
            
            for(String name: componentSelectModel.values()) {
                seq.addTasksBefore("grmRcConfiguration", getPathForComponent(name));
            }
            
            
        }
        
    }
    
    private CheckBoxObserver checkBoxObserver = new CheckBoxObserver();
    private ControlObserver observer = new ControlObserver();
    private NextObserver   nextObserver = new NextObserver();
    
    
    public void beginDisplay() throws InstallerException {
        ((UI)getUI()).init();
        
        // We try to load the system configuration
        // This ensures the the JVM model is initialized
        try {
            getSystemConfiguration();
        } catch(Exception ex) {
            // Ignore, the call should only initialize the JVM model
        }
        
        componentSelectModel.addCheckBoxListControlModelListener(observer);
        checkBoxObserver.update();
        componentSelectModel.addCheckBoxListControlModelListener(checkBoxObserver);
        nextObserver.update();
        componentSelectModel.addCheckBoxListControlModelListener(nextObserver);
        
        observer.update();
        
    }
    
    public void finishDisplay() throws InstallerException {
        
        componentSelectModel.removeCheckBoxListControlModelListener(observer);
        componentSelectModel.removeCheckBoxListControlModelListener(checkBoxObserver);
        componentSelectModel.removeCheckBoxListControlModelListener(nextObserver);
        
        if(getController().getMode().equals(InstallerController.Mode.AUTO)) {
            observer.update();
        }
        
    }
    
    
    public void execute() throws InstallerException {
        log.log(Level.FINER, "execute Method grmCompSelect");
        
        /*
         * The jvm definition is complete, we add all jvms to the system
         * configuration since the following task needs them.
         */
        JvmListModel.getInstance(getModel()).store(getSystemConfiguration());
        log.log(Level.FINER, "systemConfiguration in grmCompSelect: " + getSystemConfiguration().toString());
    }
    
    public void undo() {
    }
}
