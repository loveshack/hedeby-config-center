/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.bootstrap.GrmName;
import com.sun.grid.grm.bootstrap.InvalidGrmNameException;
import com.sun.grid.grm.bootstrap.PathConfiguration;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.install.AbstractInstallerTask;
import com.sun.grid.grm.install.GrmInstallConstants;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerModelListener;
import com.sun.grid.grm.install.ui.ComboBoxControl;
import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.ListControl;
import com.sun.grid.grm.install.ui.TextControl;
import java.util.List;
import java.util.logging.Level;
import java.util.prefs.BackingStoreException;
import javax.swing.JPanel;



/**
 *
 * 
 */
public class SelectSystemFromPrefsTask  extends AbstractInstallerTask {
    
    private UI ui;

    private class UI extends ControlPanel {
        
        private ListControl systemListControl;
        private ComboBoxControl prefsComboBoxControl;
        
        private static final String USER_PREFERENCES = "USER";
        private static final String SYSTEM_PREFERENCES = "SYSTEM";
        
        public UI() {
            
            InstallerController controller = getController();
            InstallerModel model = controller.getModel();
            setTitle(rb().getString("grmSelectSystemFromPrefs.header"));
            
            
            prefsComboBoxControl = new ComboBoxControl(SelectSystemFromPrefsTask.this, model, GrmInstallConstants.GRM_PREFERENCE_TYPE, new String[] { "SYSTEM", "USER"});
            prefsComboBoxControl.setStateLabelVisible(false);
            
            systemListControl = new ListControl(SelectSystemFromPrefsTask.this, model, GrmInstallConstants.GRM_SYSTEM_NAME);
            
            String name = SelectSystemFromPrefsTask.this.getName();
            
            setControls(
               new String [] {
                  "PreferenceType",
                  "grmSelectSystemFromPrefs.Systems",
                  "AdminUser"
               },
               new Control [] {
                prefsComboBoxControl,
                systemListControl,
                new TextControl(SelectSystemFromPrefsTask.this, getModel(), GrmInstallConstants.GRM_ADMIN_USER, 20)
            },
            rb()
            );
        }
        
    }
    
    
    class PerferencesObserver implements InstallerModelListener {
        
        public void register(InstallerModel model) {
            model.addModelEventListener(this, GrmInstallConstants.GRM_PREFERENCE_TYPE);
        }
        
        public void unregister(InstallerModel model) {
            model.removeModelEventListener(this);
        }
        
        public void changedModel(InstallerModelEvent evt) {
            if(evt.getKey().equals(GrmInstallConstants.GRM_PREFERENCE_TYPE)) {
                
                PreferencesType prefs = PreferencesType.valueOf((String)evt.getNewValue());
                
                try {
                    List<GrmName> systems = PathConfiguration.getSystemNames(prefs);

                    String [] systemNames = new String [systems.size()];
                    for(int i = 0; i < systemNames.length; i++) {
                        systemNames[i] = systems.get(i).getName();
                    }
                    ui.systemListControl.setItems(systemNames);
                } catch(InvalidGrmNameException ex) {
                    log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
                } catch(BackingStoreException ex) {
                    log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
                }
            }
            
        }
        
    }
    
    private PerferencesObserver systemObserver = new PerferencesObserver();
    
    /**
     * Create a new <code>SelectSystemFromPrefsTask</code>
     *
     * @param controller  The installer controller
     * @param name        name of the test
     */
    public SelectSystemFromPrefsTask(InstallerController controller, String name) {
        super(controller, name);
    }
    
  
    public JPanel getUI() {
        if(ui == null) {
            ui = new UI();
        }
        return ui;
    }
    
    public void beginDisplay() throws InstallerException {
        ((UI)getUI()).init();
        systemObserver.register(getModel());
        readSystems();
    }
    
    public void finishDisplay() throws InstallerException {
        systemObserver.unregister(getController().getModel());
    }
    
    public void execute() throws InstallerException {
        log.log(Level.FINER, "execute Method grmSelectSystemFromPrefs");
        log.log(Level.FINER, "systemConfiguration in grmSelectSystemFromPrefs: " + getData(GrmInstallConstants.SYSTEM_CONFIG).toString());
    }
    
    public void undo() {
    }
    
    private void readSystems() {
        String selectedItem = (String)ui.prefsComboBoxControl.getComboBox().getSelectedItem();
        
        if (selectedItem != null) {
            String newValue = null;
            String oldValue = null;

            if (selectedItem.equals(ui.USER_PREFERENCES)) {
                oldValue = ui.SYSTEM_PREFERENCES;
                newValue = ui.USER_PREFERENCES;
            } else {
                oldValue = ui.USER_PREFERENCES;
                newValue = ui.SYSTEM_PREFERENCES;
            }

            systemObserver.changedModel(new InstallerModelEvent(this,
                        GrmInstallConstants.GRM_PREFERENCE_TYPE,
                        oldValue,
                        newValue));    
            }
    }
}
