/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.bootstrap.GrmName;
import com.sun.grid.grm.bootstrap.IncorrectFileFormatException;
import com.sun.grid.grm.bootstrap.InvalidConfigurationException;
import com.sun.grid.grm.bootstrap.InvalidGrmNameException;
import com.sun.grid.grm.bootstrap.JvmConfiguration;
import com.sun.grid.grm.bootstrap.PathConfiguration;
import com.sun.grid.grm.bootstrap.PreferencesNotAvailableException;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.install.AbstractInstallerTask;
import com.sun.grid.grm.install.GrmInstallConstants;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerModelListener;
import com.sun.grid.grm.install.ui.ComboBoxControl;
import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.ControlStateChangedListener;
import com.sun.grid.grm.install.ui.FileControl;
import com.sun.grid.grm.install.ui.GrmNameControl;
import com.sun.grid.grm.install.ui.SgeRootControl;
import com.sun.grid.grm.install.ui.TextControl;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

/**
 *
 * 
 */
public class GrmSystemBasicsTask extends AbstractInstallerTask {
    
    private UI ui;
    private SystemConfiguration systemConfiguration;
    
    private class UI extends ControlPanel {
        
        public UI() {
            InstallerController controller = getController();
            
            setTitle(rb().getString("grmSystemBasics.header"));
            String name = GrmSystemBasicsTask.this.getName();
            setControls(
                    new String [] {
                "PreferenceType",
                "SystemName",
                "AdminUser",
                "DistDir",
                "LocalSpoolDir",
                "SharedDir",
                "SgedistDir",
            },
                    
                    new Control [] {
                new ComboBoxControl(GrmSystemBasicsTask.this, getModel(), GrmInstallConstants.GRM_PREFERENCE_TYPE, new String[] { "SYSTEM", "USER"}),
                new GrmNameControl(GrmSystemBasicsTask.this, getModel(), GrmInstallConstants.GRM_SYSTEM_NAME, 20),
                new TextControl(GrmSystemBasicsTask.this, getModel(), GrmInstallConstants.GRM_ADMIN_USER, 20),
                new FileControl(GrmSystemBasicsTask.this, getModel(), GrmInstallConstants.GRM_DIST_DIR).fileSelectionMode(JFileChooser.DIRECTORIES_ONLY),
                new FileControl(GrmSystemBasicsTask.this, getModel(), GrmInstallConstants.GRM_LOCAL_SPOOL_DIR).fileSelectionMode(JFileChooser.DIRECTORIES_ONLY),
                new FileControl(GrmSystemBasicsTask.this, getModel(), GrmInstallConstants.GRM_SHARED_DIR).fileSelectionMode(JFileChooser.DIRECTORIES_ONLY),
                new SgeRootControl(GrmSystemBasicsTask.this, getModel(), GrmInstallConstants.GRM_SGE_DIST_DIR),
            },
                    rb()
                    );
        }
        
    }
    
    
    class SystemObserver implements InstallerModelListener {
        
        PreferencesType prefs;
        GrmName         systemName;
        
        public void register(InstallerModel model) {
            model.addModelEventListener(this, GrmInstallConstants.GRM_PREFERENCE_TYPE);
            model.addModelEventListener(this, GrmInstallConstants.GRM_SYSTEM_NAME);
        }
        
        public void unregister(InstallerModel model) {
            model.removeModelEventListener(this);
        }
        
        public void changedModel(InstallerModelEvent evt) {
            boolean changed = false;
            if(evt.getKey().equals(GrmInstallConstants.GRM_PREFERENCE_TYPE)) {
                prefs = PreferencesType.valueOf((String)evt.getNewValue());
                changed = true;
            } else if (evt.getKey().equals(GrmInstallConstants.GRM_SYSTEM_NAME)) {
                try {
                    systemName = new GrmName((String)evt.getNewValue());
                    changed = true;
                } catch(InvalidGrmNameException ex) {
                    // Ignore
                }
            }
            
            if(prefs == null) {
                try {
                    prefs = PreferencesType.valueOf((String)getController().getModel().getData(GrmInstallConstants.GRM_PREFERENCE_TYPE));
                } catch(Exception e) {
                    // Ignore
                }
            }
            
            if(changed && prefs != null && systemName != null) {
                // Try get the values from the system configuration
                SystemConfiguration conf = null;
                try {
                    conf = SystemConfiguration.newInstanceFromPrefs(systemName, prefs);
                    //....
                } catch(PreferencesNotAvailableException ex) {
                    // Ignore
                } catch(InvalidConfigurationException ex) {
                    // Ignore
                } catch(IncorrectFileFormatException ex) {
                    // Ignore
                }
                
                if(conf != null) {
                    PathConfiguration pc = conf.getPathConfiguration();
                    getController().getModel().setData(GrmInstallConstants.GRM_SHARED_DIR, pc.getSharedPath());
                    getController().getModel().setData(GrmInstallConstants.GRM_DIST_DIR, pc.getDistPath());
                    
                    try {
                        conf.readFromBackStore();
                        
                        List<JvmConfiguration> jvms = conf.getJvmConfigurations();
                        if(!jvms.isEmpty()) {
                            getController().getModel().setData(GrmInstallConstants.GRM_ADMIN_USER, jvms.get(0).getUser());
                        }
                    } catch(InvalidConfigurationException ex) {
                        // Ignore
                    } catch(IncorrectFileFormatException ex) {
                        // Ignore
                    }
                }
            }
            
        }
        
    }
    
    private SystemObserver systemObserver = new SystemObserver();
    
    /**
     * @param controller the installer controller
     * @param name name of the task
     */
    public GrmSystemBasicsTask(InstallerController controller, String name) {
        super(controller, name);
    }
    
    
    private File getPath(String path, GrmName name) throws IOException {
        if(path.length() == 0) {
            throw new IOException(name + " must not be null");
        }
        // TODO to further checks
        return new File(path + File.separatorChar + name);
    }
    
    GenericNextObserver nextObserver;
    
    public JPanel getUI() {
        if(ui == null) {
            ui = new UI();
            nextObserver = super.createGenericNextObserver(ui);
        }
        return ui;
    }
    
    public void beginDisplay() throws InstallerException {
        ((UI)getUI()).init();
        systemObserver.register(getController().getModel());
        nextObserver.register();
    }
    
    public void finishDisplay() throws InstallerException {
        log.entering(getClass().getName(), "finishDisplay");
        
        systemObserver.unregister(getController().getModel());
        if(nextObserver != null) {
            nextObserver.unregister();
        }
        
        log.exiting(getClass().getName(), "finishDisplay");
        
    }
    
    public void execute() throws InstallerException {
    }
    
    public void undo() {
    }
}
