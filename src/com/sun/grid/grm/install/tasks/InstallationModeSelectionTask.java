/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.install.AbstractInstallerTask;
import com.sun.grid.grm.install.GrmInstallConstants;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerTask;
import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.ControlStateChangedListener;
import com.sun.grid.grm.install.ui.RadioButtonControl;
import com.sun.grid.grm.util.Hostname;
import java.util.Arrays;
import java.util.logging.Level;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;


/**
 *
 * 
 */
public class InstallationModeSelectionTask  extends AbstractInstallerTask {
    
    
    private UI ui;
    
    private class UI extends ControlPanel {
        
        public UI() {
            super();
            
            InstallerController controller = getController();
            
            setTitle("Installation Mode Selection");
            
            ButtonGroup group = new ButtonGroup();
            
            RadioButtonControl newSystem  = new RadioButtonControl((InstallerTask)InstallationModeSelectionTask.this, getModel(), GrmInstallConstants.SYSTEM_INSTALL_MODE, GrmInstallConstants.NEW_SYSTEM, "grmInstallationModeSelection.NewSystem");
            newSystem.addToGroup(group);
            
            RadioButtonControl addToHost  = new RadioButtonControl((InstallerTask)InstallationModeSelectionTask.this, getModel(), GrmInstallConstants.SYSTEM_INSTALL_MODE, GrmInstallConstants.ADD_HOST_TO_SYSTEM, "grmInstallationModeSelection.AddHostToSystem");
            addToHost.addToGroup(group);

            RadioButtonControl addComponentToSystem  = new RadioButtonControl((InstallerTask)InstallationModeSelectionTask.this, getModel(), GrmInstallConstants.SYSTEM_INSTALL_MODE, GrmInstallConstants.ADD_COMPONENT_TO_HOST, "grmInstallationModeSelection.AddComponentToSystem");
            addComponentToSystem.addToGroup(group);
            
            setControls(
                new String [] {
                   null,
                   null,
                   null,
                },
                new Control [] {
                    newSystem,
                    addToHost,
                    addComponentToSystem
                },
                rb()
            );
            
        }
        
    }
    
    /**
     *  Create new new <code>InstallationModeSelectionTask</code>
     *
     *  @param controller  the installer controller
     *  @param name        the name of the task
     */
    public InstallationModeSelectionTask(InstallerController controller, String name) {
        super(controller, name);
    }
    
  
    public JPanel getUI() {
        if(ui == null) {
            ui = new UI();
            ui.addStateChangedListener(new ControlStateChangedListener() {
                public void stateChanged(Control control) {
                    getController().setNextEnabled(control.getState().equals(Control.State.OK));
                }
            });
        }
        return ui;
    }
    
    public void beginDisplay() throws InstallerException {
        ((UI)getUI()).init();
    }
    
    private String addedTask;
    public void finishDisplay() throws InstallerException {
        if(addedTask != null) {
            getController().getSequence().removeTaskFromPath(addedTask);
        }
        String mode = (String)getModel().getData(GrmInstallConstants.SYSTEM_INSTALL_MODE);
        if(GrmInstallConstants.NEW_SYSTEM.equals(mode)) {
            addedTask = "grmSystemBasics";
            getModel().setData(GrmInstallConstants.INSTALL_REMOTE_SECURITY, false);
        } else if (GrmInstallConstants.ADD_HOST_TO_SYSTEM.equals(mode)) {
            addedTask = "grmSelectSystemFromShared";
            getModel().setData(GrmInstallConstants.INSTALL_REMOTE_SECURITY, true);
        } else if (GrmInstallConstants.ADD_COMPONENT_TO_HOST.equals(mode)) {
            addedTask = "grmSelectSystemFromPrefs";
            getModel().setData(GrmInstallConstants.INSTALL_REMOTE_SECURITY, false);
        } else {
            throw new InstallerException("Unknown " + GrmInstallConstants.SYSTEM_INSTALL_MODE + " (" + mode + ")");
        }
        getController().getSequence().addTaskAfterCurrent(addedTask);
        String thisHost = Hostname.getLocalHost().toString();
        getController().getModel().setData(GrmInstallConstants.GRM_THIS_HOSTNAME, thisHost);
    }
    
    public void execute() throws InstallerException {
        log.log(Level.FINER, "execute Method grmModeSelection");
        log.log(Level.FINER, "systemConfiguration in grmModeSelection: " + getData(GrmInstallConstants.SYSTEM_CONFIG).toString());
    }
    
    public void undo() {
    }
     
}
