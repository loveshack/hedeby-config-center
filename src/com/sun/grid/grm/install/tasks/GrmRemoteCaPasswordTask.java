/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.GrmName;
import com.sun.grid.grm.bootstrap.PathConfiguration;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.install.GrmInstallConstants;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerModelListener;
import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.FileControl;
import com.sun.grid.grm.install.ui.KeystoreControl;
import com.sun.grid.grm.install.ui.PasswordControl;
import com.sun.grid.grm.install.ui.RadioButtonControl;
import com.sun.grid.grm.install.ui.TextControl;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.prefs.BackingStoreException;
import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

/**
 *
 *
 */
public class GrmRemoteCaPasswordTask extends ExtendedAbstractInstallerTask {
    
    
    private UI ui;
    
    private class UI extends ControlPanel {
        
        private TextControl userName;
        private PasswordControl passWord;
        private FileControl keyStoreFileControl;
        private KeystoreControl keyStoreControl;
        
        public UI() {
            InstallerController controller = getController();
            
            
            ButtonGroup buttonGroup = new ButtonGroup();
            
            setTitle(rb().getString("grmRemoteCaPassword.Header"));

            int indent = 30;
            userName = new TextControl(GrmRemoteCaPasswordTask.this, getModel(), GrmInstallConstants.SECURITY_CAUSERNAME_CONFIG, 15);
            userName.setIndent(indent);
            passWord = new PasswordControl(GrmRemoteCaPasswordTask.this, getModel(), GrmInstallConstants.SECURITY_CAPASSWORD_CONFIG, 15);
            passWord.setIndent(indent);
            
            RadioButtonControl rb1 = new RadioButtonControl(GrmRemoteCaPasswordTask.this, getModel(), GrmInstallConstants.SECURITY_KEYSTORE_SOURCE, GrmInstallConstants.SECURITY_KEYSTORE_FROM_STATE_FILE, "grmRemoteCAPassword.provideCredentials");
            RadioButtonControl rb2 = new RadioButtonControl(GrmRemoteCaPasswordTask.this, getModel(), GrmInstallConstants.SECURITY_KEYSTORE_SOURCE, GrmInstallConstants.SECURITY_KEYSTORE_FROM_FILE, "grmRemoteCAPassword.extKeystore");
            rb1.addToGroup(buttonGroup);
            rb2.addToGroup(buttonGroup);
            
            keyStoreControl = new KeystoreControl(GrmRemoteCaPasswordTask.this, GrmInstallConstants.SECURITY_KEYSTORE, GrmInstallConstants.SECURITY_CAUSERNAME_CONFIG, GrmInstallConstants.SECURITY_CAPASSWORD_CONFIG);
            keyStoreControl.setIndent(indent);
            keyStoreFileControl = new FileControl(GrmRemoteCaPasswordTask.this, getModel(), GrmInstallConstants.SECURITY_KEYSTORE_FILE);
            keyStoreFileControl.setFileSelectionMode(JFileChooser.FILES_ONLY);
            keyStoreFileControl.setIndent(indent);
            
            setControls(
               new String [] {
                  null,
                  "grmRemoteCaPassword.Username",
                  "grmRemoteCaPassword.Password",
                  "grmRemoteCaPassword.getKeyStore",
                  null,
                  "grmRemoteCaPassword.keyStoreFile"
              },
              new Control [] {
                 rb1,
                 userName,
                 passWord,
                 keyStoreControl,
                 rb2,
                 keyStoreFileControl
             },
             rb()
             );
        }
    }
    
    

    /**
     * Creates a new instance of GrmRpConfigurationTask
     */
    public GrmRemoteCaPasswordTask(InstallerController controller, String name) {
        super(controller, name);
    }
    
    public JPanel getUI() {
        if(ui == null) {
            ui = new UI();
            nextObserver = createGenericNextObserver(ui);
            controlObserver = new ControlObserver();
        }
        return ui;
    }
    
    private GenericNextObserver nextObserver;
    private ControlObserver controlObserver;
    
    public void beginDisplay() throws InstallerException {
        getUI();
        ui.init();
        nextObserver.register();
        controlObserver.register();
    }
    
    public void finishDisplay() throws InstallerException {
        if(nextObserver != null) {
            nextObserver.unregister();
        }
        if(controlObserver != null) {
            controlObserver.unregister();
        }
    }
    
    public void execute() throws InstallerException {
        fireStarted();
        
        int stepCount = 1;
        double percentPerStep = 100/stepCount;
        double percent = 0;
        copyRemoteCa();
        fireProgress("...ca complete!", (int)percent);
        fireFinished();
    }
    
    public void undo() {
    }
    
    private class ControlObserver implements InstallerModelListener {
        public void register() {            
            getModel().addModelEventListener(this, GrmInstallConstants.SECURITY_KEYSTORE_SOURCE);
            update();
        }
        
        public void unregister() {
            getModel().removeModelEventListener(this);
        }
        public void changedModel(InstallerModelEvent evt) {
            update();
        }        
        private void update() {
            Object value = getModel().getData(GrmInstallConstants.SECURITY_KEYSTORE_SOURCE);
            boolean stateFileSelected = "STATE_FILE".equals(value);
            ui.keyStoreControl.setEnabled(stateFileSelected);
            ui.userName.setEnabled(stateFileSelected);
            ui.passWord.setEnabled(stateFileSelected);
            ui.keyStoreFileControl.setEnabled(!stateFileSelected);
        }
    }
    
    
}


