/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.install.DefaultInstallerModel;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerTask;
import com.sun.grid.grm.install.RB;
import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.ControlStateChangedListener;
import com.sun.grid.grm.install.ui.FileControl;
import com.sun.grid.grm.install.ui.SpinnerControl;
import com.sun.grid.grm.install.ui.TextControl;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * 
 */
public class GeAdapterDialog extends JDialog {
    
    public static final String ADAPTER_SGEROOT      = "ge.adapter.sgeroot";
    public static final String ADAPTER_SGECELL      = "ge.adapter.sgecell";
    public static final String ADAPTER_MASTERPORT   = "ge.adapter.masterport";
    public static final String ADAPTER_EXECDPORT = "ge.adapter.execdport";
    
    private ControlPanel panel;
    private String adapterName;
    private FileControl sgeRootControl;
    private TextControl sgeCellControl;
    private SpinnerControl  masterPortControl;
    private SpinnerControl  execdPortControl;
    private InstallerModel tmpModel;
    private InstallerTask task;
    
    /** Creates a new instance of GeAdapterDialog */
    public GeAdapterDialog(JFrame frame, InstallerTask task) {
        super(frame, "Gridengine Adapter Configuration");
        this.task = task;
        InstallerController controller = task.getController();
        panel = new ControlPanel();
        
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        
        getContentPane().add(panel, BorderLayout.CENTER);
        
        tmpModel = new DefaultInstallerModel();
        
        String adapter = "unknown";
        
        sgeRootControl = new FileControl(task, tmpModel, getSgeRootKey(adapter));
        sgeRootControl.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        
        sgeCellControl = new TextControl(task, tmpModel, getSgeCellKey(adapter));
        masterPortControl = new SpinnerControl(task, tmpModel, getMasterPortKey(adapter));
        execdPortControl = new SpinnerControl(task, tmpModel, getExecdPortKey(adapter));
        
        panel.setControls(
                new String [] {
            "grmGeAdapterConfiguration.SgeRoot",
            "grmGeAdapterConfiguration.SgeCell",
            "grmGeAdapterConfiguration.MasterPort",
            "grmGeAdapterConfiguration.ExecdPort"
        },
        new Control [] {
            sgeRootControl,
            sgeCellControl,
            masterPortControl,
            execdPortControl
        },
         RB.getInstance(getClass())
        );
        
        JPanel buttonPanel = new JPanel();
        OKAction okAction = new OKAction();
        panel.addStateChangedListener(okAction);
        JButton b = new JButton(okAction);
        buttonPanel.add(b);
        
        CancelAction ca = new CancelAction();
        
        b = new JButton(RB.getInstance(getClass()).getString("grmGeAdapterConfiguration.Cancel"));
        b.addActionListener(ca);
        
        buttonPanel.add(b);
        
        addWindowListener(ca);
        
        getContentPane().add(buttonPanel, BorderLayout.SOUTH);
    }

    private static String getPrefix(String adapter) {
        return "adapter[" + adapter + "].";
    }
    
    public static String getSgeRootKey(String adapter) {
        return getPrefix(adapter) + ADAPTER_SGEROOT;
    }
    
    public static String getSgeCellKey(String adapter) {
        return getPrefix(adapter) + ADAPTER_SGECELL;
    }
    
    public static String getMasterPortKey(String adapter) {
        return getPrefix(adapter) + ADAPTER_MASTERPORT;
    }
    
    public static String getExecdPortKey(String adapter) {
        return getPrefix(adapter) + ADAPTER_EXECDPORT;
    }
    
    
    class OKAction extends AbstractAction implements ControlStateChangedListener {
        public OKAction() {
            super("OK");
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {
            InstallerModel realModel = task.getModel();
            copyValues(tmpModel,task.getModel());
            setVisible(false);
        }
        
        public void stateChanged(Control control) {
            setEnabled(control.getState().equals(Control.State.OK));
        }
        
    }
    
    class CancelAction extends WindowAdapter implements ActionListener {
        
        public void actionPerformed(ActionEvent e) {
            setVisible(false);
        }

        public void windowClosed(WindowEvent e) {
            tmpModel.clear();
        }
        
    }
    
    private final String [] keys = { ADAPTER_SGEROOT, ADAPTER_SGECELL, ADAPTER_MASTERPORT, ADAPTER_EXECDPORT};
    
    private void copyValues(InstallerModel src, InstallerModel target) {
        
        String prefix= getPrefix(adapter);
        for(String aKey: keys) {
            String key = prefix + aKey;
            target.setData(key, src.getData(key));
        } 
    }
    
    private String adapter;

    public void setAdapterName(String adapter) throws InstallerException {
        this.adapter = adapter;
        setTitle(RB.getInstance(getClass()).getString("grmGeAdapterConfiguration.FrameTitle"));
        
        panel.setTitle(RB.getInstance(getClass()).getString("grmGeAdapterConfiguration.PanelTitle") + adapter);
        tmpModel.clear();
        copyValues(task.getModel(), tmpModel);
        
        sgeRootControl.setDataKey(getSgeRootKey(adapter));
        sgeCellControl.setDataKey(getSgeCellKey(adapter));
        masterPortControl.setDataKey(getMasterPortKey(adapter));
        execdPortControl.setDataKey(getExecdPortKey(adapter));
        
    }        
        
}
