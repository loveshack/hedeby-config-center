/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.config.ConfigurationException;
import com.sun.grid.grm.config.XMLUtil;
import com.sun.grid.grm.config.common.AbstractServiceConfig;
import com.sun.grid.grm.config.common.ComponentConfig;
import com.sun.grid.grm.config.common.SLOConfig;
import com.sun.grid.grm.config.gridengine.FixedUsageSLOConfig;
import com.sun.grid.grm.config.gridengine.GEServiceConfig;
import com.sun.grid.grm.config.gridengine.MaxPendingJobsSLOConfig;
import com.sun.grid.grm.config.gridengine.MinHostResourceSLO;
import com.sun.grid.grm.install.*;
import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.ListControl;
import com.sun.grid.grm.install.ui.TableControl;
import com.sun.grid.grm.install.ui.TableControlModel;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import static java.awt.GridBagConstraints.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

/**
 *
 *
 */
public class GrmSloConfigurationTask extends ExtendedAbstractInstallerTask implements InstallerTask {
    
    private final static String FIXED_HOSTS = "SLA_FIXED_HOSTS";
    private final static String SLA_CONFIG = "SLA_CONFIGURATION";
    private final static String SLA_MAX_PENDING_JOBS = "SLA_MAX_PENDING_JOBS";
    private final static String SGE_VALUE_COL = "sgeValue";
    private final static String MIN_COL = "min";
    private final static String MAX_COL = "max";
    private final static String URG_COL = "urgency";
    
    private UI ui;
    
    private InstallDataListModel fixedHostsListModel;
    private ListControl fixedHostsControl;
    
    private TableControlModel slaModel;
    private TableControl slaControl;
    
    private TableControlModel maxPendingJobsModel;
    private TableControl      maxPendingJobsControl;
    
    private class UI extends ControlPanel {
        
        public UI() {
            InstallerController controller = getController();
            
            
            fixedHostsControl = new ListControl(GrmSloConfigurationTask.this, getModel(), FIXED_HOSTS, fixedHostsListModel);
            
            fixedHostsControl.addAction(new AddHostAction());
            fixedHostsControl.addAction(new RemoveHostAction(), true, false);
            fixedHostsControl.addAction(new AddHostFileAction());
            
            slaControl = new TableControl(GrmSloConfigurationTask.this, getModel(), SLA_CONFIG, slaModel);
            
            slaControl.addAction(new AddAction(slaModel));
            slaControl.addAction(new RemoveAction(slaControl), true, false);
            
            slaControl.setColumnLabels(rb(), new String [] {
                "grmSloConfiguration.sgeValue",
                "grmSloConfiguration.min",
                "grmSloConfiguration.urgency"
            });
            
            maxPendingJobsControl = new TableControl(GrmSloConfigurationTask.this, getModel(), SLA_MAX_PENDING_JOBS, maxPendingJobsModel);
            maxPendingJobsControl.setColumnLabels(rb(), new String [] {
                "grmSloConfiguration.sgeValue",
                "grmSloConfiguration.max",
                "grmSloConfiguration.urgency"
            });
            
            maxPendingJobsControl.addAction(new AddAction(maxPendingJobsModel));
            maxPendingJobsControl.addAction(new RemoveAction(maxPendingJobsControl), true, false);
            
            
            
            setTitle(rb().getString("grmSloConfiguration.Header"));
            String name = GrmSloConfigurationTask.this.getName();
            setControls(
                new String [] {
                "grmSloConfiguration.AddedSlaHosts",
                "grmSloConfiguration.AddedSlaConfigs",
                "grmSloConfiguration.MaxPendingJobs"
            },
                new Control [] {
                fixedHostsControl,
                slaControl,
                maxPendingJobsControl
            },
                rb()
                );
        }
    }
    
    /**
     * Creates a new instance of GrmSloConfigurationTask
     */
    public GrmSloConfigurationTask(InstallerController controller, String name) {
        super(controller, name);
        slaModel = new TableControlModel(getModel(), SLA_CONFIG,
            new String [] { SGE_VALUE_COL, MIN_COL, URG_COL},
            new Class<?> [] { String.class, Integer.class, Integer.class });
        
        
        maxPendingJobsModel = new TableControlModel(getModel(), SLA_MAX_PENDING_JOBS,
            new String [] { SGE_VALUE_COL, MAX_COL, URG_COL},
            new Class<?> [] { String.class, Integer.class, Integer.class });
        
        fixedHostsListModel = new InstallDataListModel(getModel(), FIXED_HOSTS, "|");
        
        
    }
    
    private GenericNextObserver nextObserver;
    public JPanel getUI() {
        if(ui == null) {
            ui = new UI();
            nextObserver = createGenericNextObserver(ui);
        }
        return ui;
    }
    
    
    
    private class AddAction extends AbstractAction {
        TableControlModel model;
        
        public AddAction(TableControlModel model) {
            rb().initAction(this, "grmSloConfiguration.AddSlaAction");
            this.model = model;
        }
        
        public void actionPerformed( ActionEvent e ) {
            model.addRow(new Object [] { "<new attr>", 0, 0 });
        }
    };
    
    
    private class RemoveAction extends AbstractAction {
        private TableControl tableControl;
        public RemoveAction(TableControl tableControl) {
            rb().initAction(this, "grmSloConfiguration.RemoveSlaAction");
            this.tableControl = tableControl;
        }
        
        public void actionPerformed( ActionEvent e ) {
            int row = tableControl.getSelectedRow();
            if(row >= 0) {
                tableControl.getTableModel().removeRow(row);
            }
        }
    };
    
    public void execute() throws InstallerException {
        fireStarted();
        createScSlaConfig();
        fireFinished();
    }
    
    public void undo() {
    }
    
    public void beginDisplay() throws InstallerException {
        getUI();
        ui.init();
        
        if(maxPendingJobsModel.getRowCount() == 0) {
            maxPendingJobsModel.addRow(new Object [] { "*", 1000, 60 });
        }
        
        
        nextObserver.register();
    }
    
    public void finishDisplay() throws InstallerException {
        if(nextObserver != null) {
            nextObserver.unregister();
        }
    }
    
    public void createScSlaConfig() throws InstallerException {
        
        
        SystemConfiguration sc = getSystemConfiguration();
        File confPath = new File(sc.getPathConfiguration().getConfigPath().getAbsolutePath());
        
        GEServiceConfig serviceConfig = InstallUtil.getGEServiceConfig(getModel());
        
        int size = fixedHostsListModel.size();
        for (int i = 0; i < fixedHostsListModel.size(); i++ ) {
            serviceConfig.getStaticHost().add((String)fixedHostsListModel.getElementAt(i));
        }
        
        {
            FixedUsageSLOConfig slo = new FixedUsageSLOConfig();
            slo.setBaseUsage(10);
            slo.setName("fixedUsage");
            
            serviceConfig.getSLO().add(slo);
        }
        
        int rowCount = slaModel.getRowCount();
        int valueIndex = slaModel.getColumnIndex(SGE_VALUE_COL);
        int minIndex = slaModel.getColumnIndex(MIN_COL);
        int urgIndex = slaModel.getColumnIndex(URG_COL);
        
        for (int row = 0; row < rowCount; row++){
            
            MinHostResourceSLO slo = new MinHostResourceSLO();
            
            Integer min = (Integer)slaModel.getValueAt(row, minIndex);
            Integer  urg =  (Integer)slaModel.getValueAt(row, urgIndex);
            
            slo.setName("minHost" + row);
            slo.setMin(min == null ? 0: min.intValue());
            slo.setUrgency(urg == null ? 0:  urg.intValue());
            slo.setAttribute("arch");
            slo.setValue((String)slaModel.getValueAt(row, valueIndex));
            
            serviceConfig.getSLO().add(slo);
        }
        
        rowCount = maxPendingJobsModel.getRowCount();
        valueIndex = maxPendingJobsModel.getColumnIndex(SGE_VALUE_COL);
        int maxIndex = maxPendingJobsModel.getColumnIndex(MAX_COL);
        urgIndex = maxPendingJobsModel.getColumnIndex(URG_COL);
        
        for (int row = 0; row < rowCount; row++){
            
            
            Integer  max = (Integer)slaModel.getValueAt(row, maxIndex);
            Integer  urg =  (Integer)slaModel.getValueAt(row, urgIndex);
            
            MaxPendingJobsSLOConfig slo = new MaxPendingJobsSLOConfig();
            
            slo.setName("maxPendingJobs" + row);
            slo.setMax(max == null ? 0 : max.intValue());
            slo.setUrgency(urg == null ? 0:  urg.intValue());
            slo.setAttribute("arch");
            slo.setValue((String)maxPendingJobsModel.getValueAt(row, valueIndex));
            
            serviceConfig.getSLO().add(slo);
        }
        
        
        try {
            JAXBElement<ComponentConfig> elem = new com.sun.grid.grm.config.common.ObjectFactory().createComponentConfig(serviceConfig);
            XMLUtil.store(elem, confPath);
            InstallUtil.changeFileOwner(getModel(), confPath);
        } catch (JAXBException ex) {
            throw new InstallerException(ex.getMessage(), ex);
        }
        
    }
    
    private class AddHostAction extends AbstractAction {
        
        public AddHostAction() {
            rb().initAction(this, "grmSloConfiguration.AddHostAction");
        }
        
        public void actionPerformed( ActionEvent e ) {
            String user = (String)JOptionPane.showInputDialog(ui.getParent(),
                rb().getString("grmSloConfiguration.AddHostDialog"), "hostname");
            if(user != null && user.length() > 0) {
                InstallDataListModel tmpList = (InstallDataListModel) fixedHostsControl.getListModel();
                tmpList.addElement(user);
                fixedHostsControl.setListModel(tmpList);
            }
        }
    };
    
    
    private class RemoveHostAction extends AbstractAction {
        
        public RemoveHostAction() {
            rb().initAction(this, "grmSloConfiguration.RemoveHostAction");
        }
        
        public void actionPerformed( ActionEvent e ) {
            if ( fixedHostsControl.getList().isSelectionEmpty() ) {
                return;
            } else {
                InstallDataListModel tmpList = (InstallDataListModel) fixedHostsControl.getListModel();
                for ( Object elem : fixedHostsControl.getList().getSelectedValues()) {
                    tmpList.removeElement(elem);
                }
            }
        }
    };
    
    private class AddHostFileAction extends AbstractAction {
        
        public AddHostFileAction() {
            rb().initAction(this, "grmSloConfiguration.AddHostFileAction");
        }
        
        public void actionPerformed( ActionEvent e ) {
            
            JFileChooser uFileChooser = new JFileChooser();
            InstallDataListModel tmpList = (InstallDataListModel) fixedHostsControl.getListModel();
            
            int returnVal = uFileChooser.showOpenDialog(ui.getParent());
            if(returnVal == JFileChooser.APPROVE_OPTION) {
                BufferedReader buf = null;
                String line = null;
                String tmp = "";
                try {
                    buf = new BufferedReader(new FileReader(uFileChooser.getSelectedFile()));
                    while ((line = buf.readLine()) != null) {
                        tmpList.addElement(line);
                    }
                    buf.close();
                } catch (IOException ex) {
                    log.log(Level.SEVERE, "Error while reading host file " + uFileChooser.getSelectedFile(), ex);
                }
            }
            
        }
    }
    
    
}
