/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.bootstrap.GrmName;
import com.sun.grid.grm.bootstrap.InvalidGrmNameException;
import com.sun.grid.grm.bootstrap.PathConfiguration;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.config.executor.ExecutorConfig;
import com.sun.grid.grm.install.Audience;
import com.sun.grid.grm.install.GrmInstallConstants;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.ui.CheckBoxControl;
import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.GrmComponentNameControl;
import com.sun.grid.grm.install.ui.JvmListControl;
import com.sun.grid.grm.install.ui.SpinnerControl;
import com.sun.grid.grm.util.Hostname;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import javax.swing.DefaultListModel;
import javax.swing.JPanel;

/**
 *
 * 
 */
public class GrmExecutorConfigTask extends ExtendedAbstractInstallerTask {
    
    private UI ui;
    private JvmListModel jvmListModel;
    private DefaultListModel componentListModel;
    
    private class UI extends ControlPanel {
        
        public UI() throws InstallerException {
            super();
            
            setTitle(rb().getString("grmExecutorConfig.Header"));
            
            String [] labels = {
                "grmExecutorConfig.ComponentName",
                null,
                "grmExecutorConfig.executorJVM",
                "grmExecutorConfig.ShutdownTimeout",
                "grmExecutorConfig.CorePoolSize",
                "grmExecutorConfig.MaxPoolSize",
                "grmExecutorConfig.IdleTimeout"
            };
            
            Control controls [] = {
                new GrmComponentNameControl(GrmExecutorConfigTask.this, getModel(), GrmInstallConstants.EXECUTOR_ID_CONFIG, componentListModel, getSystemConfiguration()),
                new CheckBoxControl(GrmExecutorConfigTask.this, getModel(), GrmInstallConstants.EXECUTOR_KEEPFILES_CONFIG, "grmExecutorConfig.KeepFiles"),
                new JvmListControl(GrmExecutorConfigTask.this, getModel(), GrmInstallConstants.GRM_EXECUTOR_JVM, jvmListModel, getSystemConfiguration()),
                new SpinnerControl(GrmExecutorConfigTask.this, getModel(), GrmInstallConstants.EXECUTOR_SHUTDOWTIMEOUT_CONFIG, 1, 120, 1).audience(Audience.EXPERT),
                new SpinnerControl(GrmExecutorConfigTask.this, getModel(), GrmInstallConstants.EXECUTOR_CPS_CONFIG, 1, 16, 1).audience(Audience.EXPERT),
                new SpinnerControl(GrmExecutorConfigTask.this, getModel(), GrmInstallConstants.EXECUTOR_MPS_CONFIG, 1, 32, 1).audience(Audience.EXPERT),
                new SpinnerControl(GrmExecutorConfigTask.this, getModel(), GrmInstallConstants.EXECUTOR_IDLETIMEOUT_CONFIG, 1, 120, 1).audience(Audience.EXPERT),
            };
            setControls(labels, controls, rb());
        }
    }
    
    /** Creates a new instance of GrmExecutorConfigTask */
    public GrmExecutorConfigTask(InstallerController controller, String name) {
        super(controller, name);
        jvmListModel = JvmListModel.getInstance(controller.getModel());
        componentListModel = ComponentListModel.getInstance(getModel());
    }
    
    private GenericNextObserver nextObserver;
    
    public JPanel getUI() throws InstallerException {
        if(ui == null) {
            ui = new UI();
            nextObserver = createGenericNextObserver(ui);
        }
        return ui;
    }
    
    public void beginDisplay() throws InstallerException {
        ((UI)getUI()).init();
        nextObserver.register();
    }
    
    public void finishDisplay() throws InstallerException {
        if(nextObserver != null) {
            nextObserver.unregister();
        }
    }
    
    public void execute() throws InstallerException {
        fireStarted();
        
        int stepCount = 2;
        double percentPerStep = 100/stepCount;
        double percent = 0;
        
        copyRemoteCa();
        
        fireProgress("...ca complete!", (int)percent);
        createExecutorConfig();
        fireProgress("...executor configuration created!", (int)percent);
        
        fireFinished();
    }
    
    public void undo() {
    }
    //TODO: to support this method we need to introduce ExecutionEnv into CC
    // CC not supported currently so this method just compiles
    private boolean createExecutorConfig() throws InstallerException {
        
        ExecutorConfig execConfig = null;
        
        File confFile = null;
        int count = 0;
        com.sun.grid.grm.util.Printer printer = null;
        
        String configName = (String)getData(GrmInstallConstants.EXECUTOR_ID_CONFIG);
        
        GrmName jvmName;
        try {
            jvmName = new GrmName((String) getData(GrmInstallConstants.GRM_EXECUTOR_JVM));
        } catch (InvalidGrmNameException ex) {
            throw new InstallerException("{0} is not a valid grm name", rb(), (String) getData(GrmInstallConstants.GRM_EXECUTOR_JVM));
        }
        Hostname hostname = Hostname.getLocalHost();
       
        //TODO: add here configuration to global config
        //addComponent(configName, "com.sun.grid.grm.executor.impl.ExecutorImpl", new GrmName(configName), jvmName, hostname);
        
            
        execConfig = new ExecutorConfig();
        
            //execConfig.setShutdownTimeout(Integer.parseInt(ws_shutdownTimeout));
            execConfig.setCorePoolSize(getController().getModel().getInt(GrmInstallConstants.EXECUTOR_CPS_CONFIG));
            execConfig.setIdleTimeout(getController().getModel().getInt(GrmInstallConstants.EXECUTOR_IDLETIMEOUT_CONFIG));
            execConfig.setKeepFiles(getController().getModel().getBoolean(GrmInstallConstants.EXECUTOR_KEEPFILES_CONFIG));
            execConfig.setMaxPoolSize(getController().getModel().getInt(GrmInstallConstants.EXECUTOR_MPS_CONFIG));
            
            try {
                printer = new com.sun.grid.grm.util.Printer(confFile);
                InstallUtil.changeFileOwner(getModel(), confFile);
            } catch (IOException ex) {
                log.log(Level.SEVERE, "configuration file can not be created!", ex);
                return false;
            } 
            //Introduce storing using SystemUtil.setComponentConfig 
            //To do this we need ExecutionEnv
            //execConfig.writeTo(printer, (String) getController().getModel().getData(GrmInstallConstants.EXECUTOR_ID_CONFIG));
            printer.close();
            return true;
        
        
    }
    
}
