/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.config.ConfigurationException;
import com.sun.grid.grm.config.XMLUtil;
import com.sun.grid.grm.config.common.ComponentConfig;
import com.sun.grid.grm.config.gridengine.ConnectionConfig;
import com.sun.grid.grm.config.gridengine.GEServiceConfig;
import com.sun.grid.grm.install.AbstractInstallerTask;
import com.sun.grid.grm.install.GrmInstallConstants;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerModelListener;
import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.ControlStateChangedListener;
import com.sun.grid.grm.install.ui.FileControl;
import com.sun.grid.grm.install.ui.GrmNameControl;
import com.sun.grid.grm.install.ui.ListControl;
import com.sun.grid.grm.install.ui.SgeRootControl;
import com.sun.grid.grm.install.ui.SpinnerControl;
import com.sun.grid.grm.install.ui.TextControl;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

/**
 *
 * 
 */
public class GrmGeAdapterConfigurationTask extends ExtendedAbstractInstallerTask {
    
    public static final String ADAPTER_SGEROOT      = "ge.adapter.sgeroot";
    public static final String ADAPTER_SGECELL      = "ge.adapter.sgecell";
    public static final String ADAPTER_MASTERPORT   = "ge.adapter.masterport";
    public static final String ADAPTER_EXECDPORT = "ge.adapter.execdport";
    
    private UI ui;
    private InstallDataListModel geListModel;
    private ListControl geListControl;
    
    private class UI extends ControlPanel {
        
        public UI() {
            InstallerController controller = getController();
            
            setTitle(rb().getString("grmGeAdapterConfiguration.Header"));
            
            
            setControls(
                new String [] {
                    "grmGeAdapterConfiguration.AdapterName",
                    "grmGeAdapterConfiguration.SgeRoot",
                    "grmGeAdapterConfiguration.SgeCell",
                    "grmGeAdapterConfiguration.MasterPort",
                    "grmGeAdapterConfiguration.ExecdPort"
                },
                new Control [] {
                    new GrmNameControl(GrmGeAdapterConfigurationTask.this, getModel(), GrmInstallConstants.GRM_SELECTED_GE_ADAPTER, 20),
                    new SgeRootControl(GrmGeAdapterConfigurationTask.this, getModel(), ADAPTER_SGEROOT),
                    new TextControl(GrmGeAdapterConfigurationTask.this, getModel(), ADAPTER_SGECELL),
                    new SpinnerControl(GrmGeAdapterConfigurationTask.this, getModel(), ADAPTER_MASTERPORT, 0, Integer.MAX_VALUE, 1),
                    new SpinnerControl(GrmGeAdapterConfigurationTask.this, getModel(), ADAPTER_EXECDPORT, 0, Integer.MAX_VALUE, 1),
                },
                rb()
            );
        }
    }
    
    /** Creates a new instance of GrmScConfigurationTask */
    public GrmGeAdapterConfigurationTask(InstallerController controller, String name) {
        super(controller, name);
        geListModel = new InstallDataListModel(controller.getModel(), "grm.available.sa", "|");
    }
    
    private GenericNextObserver nextObserver;
    private PortObserver portObserver;
    
    public JPanel getUI() throws InstallerException {
        if(ui == null) {
            ui = new UI();
            nextObserver = createGenericNextObserver(ui);
            portObserver = new PortObserver();
        }
        return ui;
    }
    
    public void beginDisplay() throws InstallerException {
        getUI();
        ui.init();
        nextObserver.register();
        portObserver.register();
    }
    
    public void finishDisplay() throws InstallerException {
        if(nextObserver != null) {
            nextObserver.unregister();
        }
        if(portObserver != null) {
            getModel().removeModelEventListener(portObserver);
        }
    }
    
    public void execute() throws InstallerException {
        fireStarted();
        
        int stepCount = 1;
        double percentPerStep = 100/stepCount;
        double percent = 0;
        createGeAdapterConfig();
        fireProgress("...gridengine adapter configuration created!", (int)percent);
        
        fireFinished();
    }
    
    public void undo() {
    }
    
    class PortObserver implements InstallerModelListener {
        
        
        public void register() {
            getModel().addModelEventListener(this, ADAPTER_SGEROOT);
            getModel().addModelEventListener(this, ADAPTER_SGECELL);
        }
        
        public void changedModel(InstallerModelEvent evt) {
            
            try {
                File sgeRoot = getModel().getFile(ADAPTER_SGEROOT);
                if(sgeRoot != null) {
                    String cell = (String)getModel().getData(ADAPTER_SGECELL);
                    if(cell != null) {
                        File settingsFile = new File(sgeRoot,  cell + File.separatorChar + "common" + File.separatorChar + "settings.csh");

                        BufferedReader br = new BufferedReader(new FileReader(settingsFile));

                        String line = null;
                        Integer qmasterPort = null;
                        Integer execdPort = null;
                        while( (line=br.readLine()) != null) {

                            if(line.startsWith("setenv SGE_QMASTER_PORT")) {
                                String tmp = line.substring("setenv SGE_QMASTER_PORT".length()).trim();
                                qmasterPort = new Integer(tmp);
                            } else if (line.startsWith("setenv SGE_EXECD_PORT")) {
                                String tmp = line.substring("setenv SGE_EXECD_PORT".length()).trim();
                                execdPort = new Integer(tmp);
                            }
                            if(execdPort != null && qmasterPort != null) {
                                break;
                            }
                        }

                        if(qmasterPort != null) {
                            getModel().setData(ADAPTER_MASTERPORT, qmasterPort);
                        }

                        if(execdPort != null) {
                            getModel().setData(ADAPTER_EXECDPORT, execdPort);
                        }
                        ui.init();
                    }
                }
            } catch(Exception ex) {
                log.log(Level.FINE, "Error while parsing settings file: " + ex.getLocalizedMessage(), ex);
            }
        }
        
    }
            
            
    
    public void createGeAdapterConfig() throws InstallerException {
        
        
        SystemConfiguration sc = getSystemConfiguration();
        
        File confFile = new File(sc.getPathConfiguration().getConfigPath().getAbsolutePath());
        
        GEServiceConfig serviceConfig = InstallUtil.getGEServiceConfig(getController().getModel());
        if(serviceConfig == null) {
            serviceConfig = new GEServiceConfig();
            InstallUtil.setGEServiceConfig(getModel(), serviceConfig);
        }
        
        ConnectionConfig connectionConfig = new ConnectionConfig();
        
        serviceConfig.setConnection(connectionConfig);

        try {
            
            connectionConfig.setCell((String) getController().getModel().getData(ADAPTER_SGECELL));
            connectionConfig.setExecdPort(getController().getModel().getInt(ADAPTER_EXECDPORT));
            connectionConfig.setMasterPort(getController().getModel().getInt(ADAPTER_MASTERPORT));
            connectionConfig.setRoot((String) getController().getModel().getData(ADAPTER_SGEROOT));

            // TODO default parameters for the other settings
//            ModifiableInstallConfig mic = mac.getModifiableInstallConfig();
//            mic.createDefaultConfiguration();
//            ModifiableMappingConfig mmc = mac.getModifiableMappingConfig();
//            mmc.createDefault();
            
            JAXBElement<ComponentConfig> elem = new com.sun.grid.grm.config.common.ObjectFactory().createComponentConfig(serviceConfig);
            XMLUtil.store(elem, confFile);
            InstallUtil.changeFileOwner(getModel(), confFile);
            
        } catch (JAXBException ex) {
            throw new  InstallerException("service adapter configuration reported error", ex);
        } 
    }
    
}
