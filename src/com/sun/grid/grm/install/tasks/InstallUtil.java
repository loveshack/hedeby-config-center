/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.bootstrap.ComponentConfiguration;
import com.sun.grid.grm.bootstrap.GrmName;
import com.sun.grid.grm.bootstrap.InvalidGrmNameException;
import com.sun.grid.grm.bootstrap.JvmConfiguration;
import com.sun.grid.grm.bootstrap.PathConfiguration;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.cli.CLIFormatter;
import com.sun.grid.grm.cli.DebugOptions;
import com.sun.grid.grm.config.gridengine.GEServiceConfig;
import com.sun.grid.grm.config.os.OsDispatcherConfig;
import com.sun.grid.grm.install.*;
import com.sun.grid.grm.install.security.BootstrapSecurityInstaller;
import com.sun.grid.grm.install.util.ExitHandler;
import com.sun.grid.grm.util.GrmFormatter;
import com.sun.grid.grm.install.util.MessageDialogHandler;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.Platform;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * 
 */


//TODO: add get/set for isGuiInstall to InstallUtil

public class InstallUtil {
    
    private final static Logger log = Logger.getLogger(InstallUtil.class.getName());
    
    /**
     *
     * @param data  the installer model
     * @return SystemConfiguration Object
     * @deprecated
     */
    public static SystemConfiguration getSystemConfiguration(InstallerModel data) {
        log.entering(InstallUtil.class.getName(), "getSystemConfiguration");
        if(log.isLoggable(Level.FINER)) {
            log.log(Level.FINER,"{0}: {1} fetching System Conf: {2}", new Object [] {
                InstallUtil.class.getName(), "execute", data.getData(GrmInstallConstants.SYSTEM_CONFIG)
            });
        }
        return (SystemConfiguration)data.getData(GrmInstallConstants.SYSTEM_CONFIG);
    }
    
    
    /**
     *
     * getting the SystemConfiguration Object from WizardState
     * @param ws - wizardState
     * @param osdConf - Object
     */
    public static void setOSDConfiguration(InstallerModel data, OsDispatcherConfig osdConf) {
        data.setData(GrmInstallConstants.OS_DISPATCHER_CONFIG, osdConf);
    }
    
    /**
     *
     * getting the SystemConfiguration Object from WizardState
     * @param ws - wizardState
     * @return SystemConfiguration Object
     */
    public static OsDispatcherConfig getOSDConfiguration(InstallerModel data) {
        return (OsDispatcherConfig)data.getData(GrmInstallConstants.OS_DISPATCHER_CONFIG);
    }
    
    /**
     *
     * getting the BootstrapSecurityInstallerImpl Object from WizardState
     *
     * @param data - install data
     * @param securityConf - Object
     */
    public static void setSecurityConfiguration(InstallerModel data, BootstrapSecurityInstaller securityConf) {
        data.setData(GrmInstallConstants.SECURITY_CONFIG, securityConf);
    }
    
    /**
     *
     * getting the BootstrapSecurityInstallerImpl Object from WizardState
     *
     * @param ws - wizardState
     * @return BootstrapSecurityInstallerImpl Object
     */
    public static BootstrapSecurityInstaller getSecurityConfiguration(InstallerModel data) {
        return (BootstrapSecurityInstaller)data.getData(GrmInstallConstants.SECURITY_CONFIG);
    }
    
    public static void setGEServiceConfig(InstallerModel data, GEServiceConfig serviceConf) {
        data.setData(GrmInstallConstants.SERVICE_CONFIG, serviceConf);
    }
    public static GEServiceConfig getGEServiceConfig(InstallerModel data) {
        return (GEServiceConfig)data.getData(GrmInstallConstants.SERVICE_CONFIG);
    }
    
    /**
     * copyFile function copies the given file from source to destination
     * @param src source file
     * @param dest destination file
     */
    public static void copyFile(File src, File dest) throws FileNotFoundException {
        FileInputStream in  = new FileInputStream(src);
        FileOutputStream out = new FileOutputStream(dest);
        byte[] buf = new byte[512];
        int i = 0;
        try {
            while((i=in.read(buf))!=-1) {
                out.write(buf, 0, i);
            }
            in.close();
            out.close();
            
        } catch (IOException ex) {
            //TODO: Error handling
            //JOptionPane.showMessageDialog(this., "file \"" + src.getName() + "\" not found! Can't copy!", "Error", JOptionPane.ERROR_MESSAGE);
        }
        
    }
    
    /**
     * moveFile function moves the given file from source to destination
     * @ToDo: Should be placed into a installer utility package
     * @param src source file
     * @param dest destination file
     */
    public static void moveFile(File src, File dest) {
        try {
            copyFile(src, dest);
        } catch (FileNotFoundException ex) {
            //TODO: Error handling
            //JOptionPane.showMessageDialog(this, "file \"" + src.getName() + "\" not found! Can't move!", "Error", JOptionPane.ERROR_MESSAGE);
        }
        src.delete();
    }
    
    /**
     * replaceString does a search and replace
     *
     *@param oldStr the old String
     *@param regEx  what should be replaced
     *@param newStr this is the new string
     *@return a string with replaced patterns
     */
    public static String replaceString(String oldStr, String regEx, String newStr) {
        
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(oldStr);
        oldStr = m.replaceAll(newStr);
        
        return oldStr;
    }
    
    public static boolean mkdir(InstallerModel data, File dir, boolean shared ) throws InstallerException {
        String adminUser = (String) data.getData(GrmInstallConstants.GRM_ADMIN_USER);
        String cmd = "mkdir -p ";
        //TODO: check -p option on different paltform (eg. windows, linux, solaris...) or even better add mkdir (with execAs) method to platform class
        boolean ret = true;
        
        if (dir.exists()) {
            boolean isSuperUser = Platform.getPlatform().isSuperUser();
            boolean sameUser = System.getProperty("user.name").equals(adminUser);
            if (isSuperUser && !sameUser){
                try {
                    Platform.getPlatform().chown(dir, adminUser, true);
                } catch (IllegalStateException ex) {
                    throw new InstallerException(ex.getLocalizedMessage(), ex);
                } catch (IOException ex) {
                    throw new InstallerException(ex.getLocalizedMessage(), ex);
                } catch (InterruptedException ex) {
                    throw new InstallerException(ex.getLocalizedMessage(), ex);
                }
            } else if (!dir.canWrite()) {
                throw new InstallerException("Can't create directory " + dir.getAbsolutePath()+ " - permission denied");
            }
            log.log(Level.FINE, "directory \"{0}\" already exists", dir.getAbsolutePath());
            return ret;
        }
        
        if (shared) {
            Platform pf = Platform.getPlatform();
            
            try {
                int retVal = pf.execAs(adminUser, cmd + dir.getAbsolutePath(), null, null, null, null);
                log.log(Level.FINE, "creating directory \"{0}\" in shared mode as user: \"{1}\", with retval: \"{2}\"", new Object[] { dir.getAbsolutePath(), adminUser, retVal });
            } catch (InterruptedException ex) {
                throw new InstallerException("Creating directory " + dir.getAbsolutePath() + " has been interrupted", ex);
            } catch (IOException ex) {
                throw new InstallerException("Can't create directory " + dir.getAbsolutePath(), ex);
            }
        } else {
            ret = dir.mkdirs();
            log.log(Level.FINE, "creating directory \"{0}\"", dir.getAbsolutePath());
            if (ret = false) {
                throw new InstallerException("Can't create directory " + dir.getAbsolutePath());
            }
        }
        return ret;
    }
    
    public static void saveState(InstallerModel data, String params) {
        
        log.entering(InstallUtil.class.getName(), "saveState");
        
        File instStateOld = new File(System.getProperty("user.home") + File.separator + ".hinstaller");
        File instStateNew = new File(System.getProperty("user.home") + File.separator + ".hinstaller_new");
        
        BufferedReader reader = null;
        BufferedWriter writer = null;
        
        
        if (instStateOld.exists()) {
            
            try {
                reader = new BufferedReader(new FileReader(instStateOld));
                writer = new BufferedWriter(new FileWriter(instStateNew));
            } catch (FileNotFoundException ex) {
                log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            } catch (IOException ex) {
                log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            }
            
            String line = null;
            StringTokenizer tok = new StringTokenizer(params,"=");
            String paramKey = tok.nextToken();
            StringTokenizer tmp_tok = null;
            String tmp_key = null;
            boolean passed = false;
            
            while (!passed) {
                try {
                    line = reader.readLine();
                    
                } catch (IOException ex) {
                    log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
                }
                if ( line != null ) {
                    tmp_tok = new StringTokenizer(line, "=");
                    tmp_key = tmp_tok.nextToken();
                    if ( paramKey.equals(tmp_key)) {
                        try {
                            writer.append(params);
                            writer.newLine();
                            writer.flush();
                        } catch (IOException ex) {
                            log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
                        }
                    } else {
                        try {
                            writer.append(line);
                            writer.newLine();
                        } catch (IOException ex) {
                            log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
                        }
                    }
                } else {
                    try {
                        writer.append(params);
                        writer.newLine();
                        writer.flush();
                        passed = true;
                    } catch (IOException ex) {
                        log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
                    }
                }
                
            }
            try {
                reader.close();
                writer.close();
            } catch (IOException ex) {
                log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            }
            
        } else {
            try {
                writer = new BufferedWriter(new FileWriter(instStateNew));
            } catch (FileNotFoundException ex) {
                log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            } catch (IOException ex) {
                log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            }
            try {
                writer.append(params);
                writer.close();
            } catch (IOException ex) {
                log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            }
        }
        
        
        InstallUtil.moveFile(instStateNew, instStateOld);
        
        log.exiting(InstallUtil.class.getName(), "saveState");
    }
    
    public static String readState(InstallerModel data, String key) {
        log.entering(InstallUtil.class.getName(), "readState");
        File instState = new File(System.getProperty("user.home") + File.separator + ".hinstaller");
        String retValue = "empty";
        boolean found = false;
        BufferedReader reader = null;
        
        if (!instState.exists()) {
            log.log(Level.FINE, "state file not found: " + instState.getAbsolutePath());
            log.exiting(InstallUtil.class.getName(), "readState");
            return retValue;
        }
        try {
            
            reader = new BufferedReader(new FileReader(instState));
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        StringTokenizer tmpTok = null;
        String tmpLine = null;
        
        while (!found) {
            try {
                tmpLine = reader.readLine();
                if (tmpLine == null) {
                    return retValue;
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            tmpTok = new StringTokenizer(tmpLine, "=");
            if (tmpTok.nextToken().equals(key)) {
                return tmpLine;
            } else {
                if (tmpLine.equals("null")){
                    found = true;
                }
            }
        }
        log.exiting(InstallUtil.class.getName(), "readState");
        return retValue;
    }
    
    
    private static DebugOptions debugOptions;
    private static DebugOptions getDebugOptions() {
        if(debugOptions == null) {
            String logOptions = System.getProperty("wizard.log");
            if(logOptions != null) {
                try {
                    debugOptions = new DebugOptions(logOptions);
                } catch (IllegalArgumentException ex) {
                    System.err.println("Invalid log options in system property wizard.log");
                    try {
                        debugOptions = new DebugOptions("WARNING:com.sun.grid.grm.install=INFO");
                    } catch (IllegalArgumentException ex1) {
                        // Should not happend
                        throw new AssertionError("Invalid default debug option");
                    }
                }
            } else {
                try {
                    debugOptions = new DebugOptions("WARNING:com.sun.grid.grm.install=INFO");
                } catch (IllegalArgumentException ex1) {
                    // Should not happend
                    throw new AssertionError("Invalid default debug option");
                }
            }
        }
        return debugOptions;
    }
    
    public static void initLogging(final boolean isSilentMode) {
        
        try {
            PipedInputStream pin = new PipedInputStream();
            final PipedOutputStream pout = new PipedOutputStream(pin);
            
            Thread logConfWriter = new Thread("InstallUtil.logConfWriter") {
                public void run() {
                    
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    
                    String handler = ConsoleHandler.class.getName();
                    String fileHandler = FileHandler.class.getName();
                    String formatter = CLIFormatter.class.getName();
                    String fileFormatter = GrmFormatter.class.getName();
                    String dlgHandler = MessageDialogHandler.class.getName();
                    String exitHandler = ExitHandler.class.getName();
                    
                    pw.print("handlers=");
                    pw.print(handler);
                    if(isSilentMode) {
                        pw.print(" ");
                        pw.print(exitHandler);
                    } else {
                        pw.print(" ");
                        pw.print(fileHandler);
                        pw.print(" ");
                        pw.print(dlgHandler);
                    }
                    pw.println();
                    
                    pw.print(handler);
                    pw.print(".level=");
                    pw.println(getDebugOptions().getLowerestLevel().getName());
                    
                    if(isSilentMode) {
                        pw.print(exitHandler);
                        pw.print(".delegate=");
                        pw.println(fileHandler);
                        pw.print(exitHandler);
                        pw.print(".level=");
                        pw.println(getDebugOptions().getLowerestLevel().getName());
                    } else {
                        pw.print(dlgHandler);
                        pw.print(".level=");
                        pw.println(getDebugOptions().getLowerestLevel().getName());
                    }
                    
                    pw.print(fileHandler);
                    pw.print(".level=");
                    pw.println(getDebugOptions().getLowerestLevel().getName());
                    pw.print(fileHandler);
                    pw.println(".pattern=%t/grm_install_%u.log");
                    
                    
                    
                    // print formatter of the handler
                    pw.print(handler);
                    pw.print(".formatter=");
                    pw.println(formatter);
                    
                    pw.print(fileHandler);
                    pw.print(".formatter=");
                    pw.println(fileFormatter);
                    
                    pw.print(fileFormatter);
                    pw.println(".columns = time thread source level message");
                    pw.print(fileFormatter);
                    pw.println(".delimiter = |");
                    pw.print(fileFormatter);
                    pw.println(".withStacktrace =true");
                    
                    // print global log level
                    pw.print(".level=");
                    pw.println(getDebugOptions().getGlobalLevel().getName());
                    
                    pw.print("java.level=");
                    pw.println(Level.SEVERE);
                    
                    // print the log levels for individual loggers
                    Map<String, Level> loggerMap = debugOptions.getLoggerMap();
                    for(String logger: loggerMap.keySet()) {
                        pw.print(logger);
                        pw.print(".level=");
                        pw.println(loggerMap.get(logger).getName());
                    }
                    pw.close();
                    
//                    System.out.println("------ logging -----------------");
//                    System.out.println(sw.getBuffer().toString());
                    
                    try {
                        pout.write(sw.getBuffer().toString().getBytes());
                        pout.close();
                    } catch(IOException ioe) {
                        ioe.printStackTrace();
                    }
                }
            };
            
            logConfWriter.start();
            
            // Reconfigure the log manager
            LogManager.getLogManager().readConfiguration(pin);
        } catch(IOException ioe) {
            // Should not happen
            throw new AssertionError("IO Error while setting up logging");
        }
    }
    
    
    /**
     * isCaComponentInstalled() returns true, if for the given hostname a CA component is
     * configured. returns false if not. This function is used for remote security installation.
     * If a component is installed on an host where the ca componentet is running on, the remote
     * installation has not to be done.
     *
     *@param ws       wizardState
     *@param hostname hostname
     *@return boolean true - ca is running on given host
     *                false - ca is not running on given host -> remote security installation
     */
    public static boolean isCaComponentInstalled(InstallerModel data) {
        log.entering(InstallUtil.class.getName(), "isCaComponentInstalled");
        
        String hostname = (String) data.getData(GrmInstallConstants.GRM_THIS_HOSTNAME);
        log.log(Level.FINE, "checking if ca component is installed for host " + hostname);
        
        SystemConfiguration sc = InstallUtil.getSystemConfiguration(data);
        Class grmCAClass = null;
        try {
            grmCAClass = InstallerTaskFactory.getAdditionalClassLoader(data).loadClass("com.sun.grid.grm.security.ca.GrmCA");
        } catch (InstallerException ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            return false;
        } catch (ClassNotFoundException ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            return false;
        }
        Hostname hostObj = Hostname.newUnresolvedInstance(hostname);
        log.log(Level.FINE, "host " + hostname + " resolved to " + hostObj.getHostname());
        for (JvmConfiguration conf : sc.getJvmConfigurations()) {
            for (ComponentConfiguration comp : conf.getComponents()) {
                if (comp.isHostMatching(hostObj) && comp.providesInterface(grmCAClass)) {
                    return true;
                }
            }
        }
        log.exiting(InstallUtil.class.getName(), "isCaComponentInstalled");
        return false;
    }
    
    /** TBD: if non secure system will be supported
     *
     */
    public static boolean isSecureSystem(InstallerModel data) {
        log.entering(InstallUtil.class.getName(), "isSecureSystem");
        SystemConfiguration sc = getSystemConfiguration(data);
        
        for (JvmConfiguration jvmConf : sc.getJvmConfigurations()) {
            for (ComponentConfiguration compConf : jvmConf.getComponents()) {
                if(compConf.getClazz().contains("security.ca.impl.GrmCAServiceImpl")) {
                    log.fine( "Found installed security");
                    log.exiting(InstallUtil.class.getName(), "isSecureSystem");
                    return true;
                }
            }
        }
        log.exiting(InstallUtil.class.getName(), "isSecureSystem");
        return false;
    }
    
    
    public static boolean existingSystemConfig(String sysName, String location, InstallerController controller) {
        boolean ret = false;
        PathConfiguration pc = null;
        GrmName name = null;
        try {
            name = new GrmName(sysName);
        } catch (InvalidGrmNameException ex) {
            ex.printStackTrace();
        }
        
        if (location.equals(GrmInstallConstants.CONFIG_LOCATION_PREFS)) {
            if (pc.existsSystem(name, controller.getModel().getPreferencesType(GrmInstallConstants.GRM_PREFERENCE_TYPE))) {
                ret = true;
            }
            
        } else if (location.equals(GrmInstallConstants.CONFIG_LOCATION_SHARED)) {
            File confFile = new File(controller.getModel().getData(GrmInstallConstants.GRM_SHARED_DIR) + File.separator + sysName + File.separator + "config" + File.separator + "config.xml");
            if (confFile.exists()) {
                ret = true;
            }
        } else {
            log.severe("Wrong system configuration location");
            System.exit(1);
        }
        
        return ret;
    }
    
    public static void putConfigToModel(InstallerController controller, SystemConfiguration sc) {
        
        if (sc.getRootJvm() != null) {
            JvmConfiguration jvmConf = sc.getRootJvm();
            controller.getModel().setData(GrmInstallConstants.GRM_EXECUTOR_PORT, jvmConf.getPort());
        }
        
        for (JvmConfiguration jvmConf : sc.getJvmConfigurations()) {
            if (!jvmConf.getUser().equals("root") && !jvmConf.getUser().equals("Administrator")) {
                controller.getModel().setData(GrmInstallConstants.GRM_ADMIN_USER, jvmConf.getUser());
                controller.getModel().setData(GrmInstallConstants.GRM_COMPONENT_PORT, jvmConf.getPort());
                break;
            }
        }
    }
    
    public static void changeFileOwner(InstallerModel model, File file) {
        String adminUser = (String) model.getData(GrmInstallConstants.GRM_ADMIN_USER);
        if (model.getData(GrmInstallConstants.GRM_PREFERENCE_TYPE).equals("SYSTEM")) {
            try {
                Platform.getPlatform().chown(file, adminUser, true);
            } catch (IllegalStateException ex) {
                log.log(Level.WARNING, ex.getLocalizedMessage(), ex);
            } catch (InterruptedException ex) {
                log.log(Level.WARNING, ex.getLocalizedMessage(), ex);
            } catch (IOException ex) {
                log.log(Level.WARNING, ex.getLocalizedMessage(), ex);
            }
        }
    }
}
