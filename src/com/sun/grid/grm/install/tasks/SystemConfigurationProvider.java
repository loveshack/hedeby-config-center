/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.bootstrap.ComponentConfiguration;
import com.sun.grid.grm.bootstrap.GrmName;
import com.sun.grid.grm.bootstrap.IncorrectFileFormatException;
import com.sun.grid.grm.bootstrap.InvalidConfigurationException;
import com.sun.grid.grm.bootstrap.InvalidGrmNameException;
import com.sun.grid.grm.bootstrap.JvmConfiguration;
import com.sun.grid.grm.bootstrap.PathConfiguration;
import com.sun.grid.grm.bootstrap.PreferencesNotAvailableException;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.install.GrmInstallConstants;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerModelListener;
import com.sun.grid.grm.util.Hostname;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class loads the SystemConfiguration.
 * If the parameters for the system configuration has changed, the system configuration
 * is destroyed and recreated on demand.
 */
public class SystemConfigurationProvider implements InstallerModelListener {

    private Logger log = Logger.getLogger(SystemConfigurationProvider.class.getName());
    
    private SystemConfiguration sysConfig;
    
    private ExtendedAbstractInstallerTask task;
    
    public SystemConfigurationProvider(ExtendedAbstractInstallerTask task) {
        this.task = task;
        getModel().addModelEventListener(this, GrmInstallConstants.GRM_PREFERENCE_TYPE);
        getModel().addModelEventListener(this, GrmInstallConstants.GRM_DIST_DIR);
        getModel().addModelEventListener(this, GrmInstallConstants.GRM_LOCAL_SPOOL_DIR);
        getModel().addModelEventListener(this, GrmInstallConstants.GRM_SHARED_DIR);
        getModel().addModelEventListener(this, GrmInstallConstants.GRM_SGE_DIST_DIR);
        getModel().addModelEventListener(this, GrmInstallConstants.SYSTEM_INSTALL_MODE);
    }
    
    private InstallerModel getModel() {
        return task.getModel();
    }
    
    /**
     * Get the current system configuration from the model.
     * 
     * If the system configuration is not stored in the model, it is created
     *
     * @throws com.sun.grid.grm.install.InstallerException 
     * @return the system configuration
     */
    public SystemConfiguration getSystemConfiguration() throws InstallerException {
        if(sysConfig == null) {
            sysConfig = createSystemConfigurationInstance();
        }
        return sysConfig;
    }
    
    /**
     * Add a component to the system configuration
     * @param name         name of the component
     * @param implClass    implemenation class of the component
     * @param configPath   path to the configuration
     * @param jvmName      name of the jvm
     * @param hostname     the hostname where the component should run
     * @throws com.sun.grid.grm.install.InstallerException 
     */
    public void addComponent(GrmName name, String implClass, String configPath, GrmName jvmName, Hostname hostname) throws InstallerException {
        SystemConfiguration sc = getSystemConfiguration();
        
        // Search the jvm configuration
        JvmConfiguration jvmConf = sc.getJvmConfiguration(jvmName);
        
        if(jvmConf == null) {
            throw new InstallerException("systemConfigurationProvider.addComponent.jvmNotFound", task.rb(), name, jvmName);
        }
        
        ComponentConfiguration compConf = jvmConf.getComponent(name);
         
        if(compConf == null) {
            compConf = new ComponentConfiguration();
            compConf.setName(name);
            compConf.setClazz(implClass);
            compConf.setConfig(configPath);
            compConf.addHost(hostname.getHostname());
            jvmConf.addComponent(compConf);
        } else {
            if(!compConf.getClazz().equals(implClass)) {
                throw new InstallerException("systemConfigurationProvider.addComponent.differentImplClass", task.rb(),
                                             name, implClass);
            }
            
            if(!compConf.getConfig().equals(configPath)) {
                throw new InstallerException("systemConfigurationProvider.addComponent.differentConfPath", task.rb(),
                                             name, configPath);
            }            
            if(!compConf.isHostMatching(hostname)) {
                compConf.addHost(hostname.getHostname());
            }
        }
    }
    
    
    public void changedModel(InstallerModelEvent evt) {
        sysConfig = null;
    }
    
    private SystemConfiguration createSystemConfigurationInstance() throws InstallerException {
        
        GrmName grmName = null;
        SystemConfiguration sc = null;
        PathConfiguration pc = null;
        String name = (String)getModel().getData(GrmInstallConstants.GRM_SYSTEM_NAME);
        try {
            grmName = new GrmName(name);
        } catch (InvalidGrmNameException ex) {
            throw new InstallerException("error.invalidSystemName", ex, task.rb(), GrmInstallConstants.GRM_SYSTEM_NAME, name);
        }
        
        Object mode = getModel().getData(GrmInstallConstants.SYSTEM_INSTALL_MODE);
        
        if(GrmInstallConstants.NEW_SYSTEM.equals(mode)) {
            
            sc = SystemConfiguration.newInstance(
                    grmName,
                    getModel().getPreferencesType(GrmInstallConstants.GRM_PREFERENCE_TYPE),
                    getModel().getFile(GrmInstallConstants.GRM_DIST_DIR),
                    getModel().getFile(GrmInstallConstants.GRM_LOCAL_SPOOL_DIR),
                    getModel().getFile(GrmInstallConstants.GRM_SHARED_DIR));
            sc.setSgeRoot(getModel().getFile(GrmInstallConstants.GRM_SGE_DIST_DIR).getAbsolutePath());
            
        } else if (GrmInstallConstants.ADD_HOST_TO_SYSTEM.equals(mode)) {
            
            String sharedDir = (String)getModel().getData(GrmInstallConstants.GRM_SHARED_DIR);
            sc = SystemConfiguration.newInstance(
                    grmName,
                    getModel().getPreferencesType(GrmInstallConstants.GRM_PREFERENCE_TYPE),
                    new File((String)getModel().getData(GrmInstallConstants.GRM_DIST_DIR)),
                    new File((String) getModel().getData(GrmInstallConstants.GRM_LOCAL_SPOOL_DIR)),
                    new File((String) getModel().getData(GrmInstallConstants.GRM_SHARED_DIR)));
            
            sc.setSgeRoot((String)getModel().getData(GrmInstallConstants.GRM_SGE_DIST_DIR));
            try {
                sc.readFromBackStore();
            } catch (IncorrectFileFormatException ex) {
                throw new InstallerException("error.invalidSystemFromShared", ex, task.rb(), sharedDir, ex.getLocalizedMessage());
            } catch (InvalidConfigurationException ex) {
                throw new InstallerException("error.invalidSystemFromShared", ex, task.rb(), sharedDir, ex.getLocalizedMessage());
            }
            
            // Merge the jvms into the jvm list model
            JvmListModel jvmListModel = JvmListModel.getInstance(getModel());
            jvmListModel.load(sc);
            
        } else if (GrmInstallConstants.ADD_COMPONENT_TO_HOST.equals(mode)) {
            
            try {
                sc = SystemConfiguration.newInstanceFromPrefs(grmName, getModel().getPreferencesType(GrmInstallConstants.GRM_PREFERENCE_TYPE));
                sc.readFromBackStore();
                
                
                
                // Merge the jvms into the jvm list model
                JvmListModel jvmListModel = JvmListModel.getInstance(getModel());
                jvmListModel.load(sc);
            } catch (IncorrectFileFormatException ex) {
                throw new InstallerException("error.invalidLocalSystem", ex, task.rb(), grmName, ex.getLocalizedMessage());
            } catch (PreferencesNotAvailableException ex) {
                throw new InstallerException("error.invalidLocalSystem", ex, task.rb(), grmName, ex.getLocalizedMessage());
            } catch (InvalidConfigurationException ex) {
                throw new InstallerException("error.invalidLocalSystem", ex, task.rb(), grmName, ex.getLocalizedMessage());
            }
            
        } else {
            throw new IllegalStateException("invalid mode " + mode);
        }
        
        return sc;
    }
    
    
}
