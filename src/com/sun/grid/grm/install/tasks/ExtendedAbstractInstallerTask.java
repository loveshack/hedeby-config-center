/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.bootstrap.ComponentConfiguration;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.GrmName;
import com.sun.grid.grm.bootstrap.IncorrectFileFormatException;
import com.sun.grid.grm.bootstrap.InvalidConfigurationException;
import com.sun.grid.grm.bootstrap.InvalidGrmNameException;
import com.sun.grid.grm.bootstrap.JvmConfiguration;
import com.sun.grid.grm.bootstrap.PathConfiguration;
import com.sun.grid.grm.bootstrap.PreferencesNotAvailableException;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.install.AbstractInstallerTask;
import com.sun.grid.grm.install.GrmInstallConstants;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.security.RemoteSecurityInstaller;
import com.sun.grid.grm.install.security.ExecutionEnvProvider;
import com.sun.grid.grm.security.ClientSecurityContext;
import com.sun.grid.grm.util.Hostname;
import java.io.File;
import java.util.logging.Level;

/**
 *
 */
public abstract class ExtendedAbstractInstallerTask extends AbstractInstallerTask {
    
    private SystemConfiguration systemConfiguration = null;
    
    /** Creates a new instance of AbstractInstallerTask */
    public ExtendedAbstractInstallerTask(InstallerController controller, String name) {
        super(controller, name);
    }

    public void mkdir(File dir, boolean shared) throws InstallerException {
        InstallUtil.mkdir(getController().getModel(), dir, shared);
    }
    
    
    private SystemConfigurationProvider getSystemConfigurationProvider() throws InstallerException {
        SystemConfigurationProvider provider = (SystemConfigurationProvider)getModel().getTmpData(GrmInstallConstants.SYSTEM_CONFIG);
        if(provider == null) {
            provider = new SystemConfigurationProvider(this);
            getModel().setTmpData(GrmInstallConstants.SYSTEM_CONFIG, provider);
        }
        return provider;
    }
    
    public SystemConfiguration getSystemConfiguration() throws InstallerException {
        synchronized(getModel()) {
            return getSystemConfigurationProvider().getSystemConfiguration();
        }
    }
    
    public ExecutionEnv getExecutionEnv() throws InstallerException {
        synchronized(getModel()) {
            return getExecutionEnvProvider().getExecutionEnv();
        }
    }
    
    private ExecutionEnvProvider getExecutionEnvProvider() throws InstallerException {
        ExecutionEnvProvider provider = (ExecutionEnvProvider)getModel().getTmpData("EXECUTION_ENV");
        if(provider == null) {
            provider = new ExecutionEnvProvider(this);
            getModel().setTmpData("EXECUTION_ENV", provider);
        }
        return provider;
    }
    
    
    public void addComponent(GrmName name, String implClass, String componentConfigName, GrmName jvmName, Hostname hostname) throws InstallerException {
        synchronized(getModel()) {
            getSystemConfigurationProvider().addComponent(name, implClass, componentConfigName, jvmName, hostname);
        }
    }
    
    public void copyRemoteCa() throws InstallerException  {
        
        this.log.entering(ExtendedAbstractInstallerTask.class.getName(), "getRemoteCa");
        SystemConfiguration sc = getSystemConfiguration();
        try {

            log.log(Level.FINE, "Setting remote security install to: {0}", getModel().getData(GrmInstallConstants.INSTALL_REMOTE_SECURITY));

            if (getModel().getBoolean(GrmInstallConstants.INSTALL_REMOTE_SECURITY) == false) {
                log.log(Level.FINE, "skip getRemoteCa() because INSTALL_REMOTE_SECURITY equals false");
                log.exiting(InstallUtil.class.getName(), "getRemoteCa");
                return;
            }
            
            log.fine("Fetching login Data from Statefile");


            RemoteSecurityInstaller rsi = new RemoteSecurityInstaller(getExecutionEnv());

            rsi.connect(getExecutionEnv());

            for (JvmConfig jvmConf : SystemUtil.getJvmConfigs(getExecutionEnv()) ) {
                rsi.createDaemonKeyStore(jvmConf);
            }
            
            // TODO get keystores for all users

            log.exiting(ExtendedAbstractInstallerTask.class.getName(), "getRemoteCa");
        } catch(Exception ex) {
            throw new InstallerException("Can not get the remote ca", ex);
        }
    }
    
}
