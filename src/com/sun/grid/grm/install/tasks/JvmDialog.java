/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.install.Audience;
import com.sun.grid.grm.install.DefaultInstallerModel;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerTask;
import com.sun.grid.grm.install.RB;
import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.ControlStateChangedListener;
import com.sun.grid.grm.install.ui.FileControl;
import com.sun.grid.grm.install.ui.PathControl;
import com.sun.grid.grm.install.ui.SgeRootControl;
import com.sun.grid.grm.install.ui.SpinnerControl;
import com.sun.grid.grm.install.ui.TextControl;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * 
 */
public class JvmDialog extends JDialog {
    
    
    public static final String NAME            = "name";
    public static final String ADMIN_USER      = "adminuser";
    public static final String PORT            = "port";
    public static final String DIST            = "dist";
    public static final String LD_LIBRARY_PATH = "ldLibraryPath";
    
    private ControlPanel panel;
    private String jvmName;
    private TextControl userControl;
    private SpinnerControl portControl;
    private FileControl  distControl;
    private PathControl  ldLibControl;
    private InstallerModel tmpModel;
    private InstallerTask task;
    private InstallDataListModel ldLibModel;
            
    /** Creates a new instance of JvmDialog */
    public JvmDialog(JFrame frame, InstallerTask task) {
        super(frame, "JVM Configuration");
        setModal(true);
        this.task = task;
        InstallerController controller = task.getController();
        panel = new ControlPanel();
        
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        
        getContentPane().add(panel, BorderLayout.CENTER);
        
        tmpModel = new DefaultInstallerModel();

        ldLibModel = new InstallDataListModel(tmpModel, getLdLibraryPathKey(null), ":");
        
        String jvm = "unknown";
        userControl = new TextControl(task, tmpModel, getAdminUserKey(null), 20);
        portControl = new SpinnerControl(task, tmpModel, getPortKey(null), 0, Integer.MAX_VALUE, 1);
        distControl = new SgeRootControl(task, tmpModel, getSgeDistKey(null)).optional(true);
        distControl.setAudience(Audience.EXPERT);
        
        ldLibControl = new PathControl(task, tmpModel, getLdLibraryPathKey(null), ldLibModel);
        ldLibControl.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        ldLibControl.setOptional(true);
        ldLibControl.setAudience(Audience.EXPERT);
        
        
        panel.setControls(
                new String [] {
            "grmJvmConfiguration.AdminUser",
            "grmJvmConfiguration.JvmPort",
            "grmJvmConfiguration.SgeDistDir",
            "grmJvmConfiguration.LdLibraryPath"
        },
        new Control [] {
            userControl,
            portControl,
            distControl,
            ldLibControl
        },
         RB.getInstance(getClass())
        );
        
        JPanel buttonPanel = new JPanel();
        OKAction okAction = new OKAction();
        panel.addStateChangedListener(okAction);
        JButton b = new JButton(okAction);
        buttonPanel.add(b);
        
        CancelAction ca = new CancelAction();
        
        b = new JButton("Cancel");
        b.addActionListener(ca);
        
        buttonPanel.add(b);
        
        addWindowListener(ca);
        
        getContentPane().add(buttonPanel, BorderLayout.SOUTH);
    }
    
    private static String getKey(String jvm, String key) {
        if(jvm == null) {
            return key;
        } else {
        return getPrefix(jvm) + key;
        }
    }
    
    private static String getPrefix(String jvm) {
        return "jvm[" + jvm + "].";
    }
    
    public static String getAdminUserKey(String jvm) {
        return getKey(jvm, ADMIN_USER);
    }
    
    public static String getPortKey(String jvm) {
        return getKey(jvm, PORT);
    }
    
    public static String getSgeDistKey(String jvm) {
        return getKey(jvm, DIST);
    }
    
    public static String getLdLibraryPathKey(String jvm) {
        return getKey(jvm, LD_LIBRARY_PATH);
    }

    private boolean canceled;
    
    public void setVisible(boolean b) {
        if(b) {
            canceled = true;
        }
        super.setVisible(b);
    }
    
    
    public boolean showDialog() { 
        setVisible(true);        
        return !canceled;
    }
    
    
    class OKAction extends AbstractAction implements ControlStateChangedListener {
        public OKAction() {
            super("Ok");
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {
            InstallerModel realModel = task.getModel();
            copyValues(tmpModel, null, task.getModel(), jvm);
            canceled = false;
            setVisible(false);
        }
        
        public void stateChanged(Control control) {
            setEnabled(control.getState().equals(Control.State.OK));
        }
        
    }
    
    
    
    class CancelAction extends WindowAdapter implements ActionListener {
        
        public void actionPerformed(ActionEvent e) {
            setVisible(false);
        }

        public void windowClosed(WindowEvent e) {
            tmpModel.clear();
        }
        
    }
    
    private final String [] keys = { ADMIN_USER, PORT, DIST, LD_LIBRARY_PATH};
    private final Object [] defaultValues = { "admin", "0", null, null };
    
    private void copyValues(InstallerModel src, String srcJvmName, InstallerModel target, String targetJvmName) {
        
        String prefix= getPrefix(jvm);
        int i = 0;
        for(String aKey: keys) {
            String sourceKey = getKey(srcJvmName, aKey);
            String targetKey = getKey(targetJvmName, aKey);
            Object value = src.getData(sourceKey);
            if(value == null) {
                value = defaultValues[i];
            }
            target.setData(targetKey, value);
            i++;
        } 
    }
    
    private String jvm;

    public void setJvmName(String jvm) throws InstallerException {
        this.jvm = jvm;
        setTitle("JVM Configuration");
        panel.setTitle("Configuration of jvm: " + jvm);
        tmpModel.clear();
        copyValues(task.getModel(), jvm, tmpModel, null);
        ldLibModel.load();
        panel.init();
    }        

        
}
