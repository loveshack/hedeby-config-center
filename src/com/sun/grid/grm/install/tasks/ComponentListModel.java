/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.bootstrap.ComponentConfiguration;
import com.sun.grid.grm.bootstrap.JvmConfiguration;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.install.InstallerModel;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * Data model for the Component names
 *
 * Makes it possible to load and store the <code>ComponentName</code>s
 * of a <code>ComponentConfiguration</code>s used in <code>SystemConfiguration</code>
 * as well as those entered in installer session.
 *
 * @author Michal Bachorik
 */
public class ComponentListModel extends DefaultListModel implements ListDataListener {
    
    private String delimiter;
    private InstallerModel model;
    
    /*
     * Data key for storing the values in 'global' model
     */
    public static final String COMPONENT_NAMES = "COMPONENT_NAMES";
    /*
     * logger
     */
    private final static Logger log = Logger.getLogger(ComponentListModel.class.getName());
    
    /**
     * Creates a new instance of ComponentListModel. To get an instance of the class,
     * use getInstance(InstallerModel model).
     * @param model  the installer model ('global')
     */
    protected ComponentListModel(InstallerModel model) {
        super();
        this.model = model;
        this.delimiter = ":";
        addListDataListener(this);
        load();
    }    
    
    /*
     * Singleton constructor. All component names has to be available during installer
     * session for each wizard task.
     * @param model the installer model ('global')
     */
    public synchronized static ComponentListModel getInstance(InstallerModel model) {
        ComponentListModel ret = null;
        synchronized(model) {
            ret = (ComponentListModel)model.getTmpData(ComponentListModel.class.getName());
            if(ret == null) {
                ret = new ComponentListModel(model);
                model.setTmpData(ComponentListModel.class.getName(), ret);
            }
        }
        return ret;
    }
    
    /*
     * Populates list of used component names from SystemConfiguration. List is
     * not cleared when loaded, items are simply overwritten.
     * @param sc the system configuration
     */
    public void load(SystemConfiguration sc) {
        if(sc != null) {
            for (JvmConfiguration jvm : sc.getJvmConfigurations()) {
                List<ComponentConfiguration> compos = jvm.getComponents();
                for (ComponentConfiguration cc : compos) {
                    InstallerModel i = this.getModel();
                    if (!contains(cc.getName())) {
                        addElement(cc.getName().getName());
                    }
                }
            }
        }
    }
    
    /*
     * Stores used component names to global model.
     */
    private void store() {
        StringBuilder buf = new StringBuilder();
        for(int i = 0; i < getSize(); i++) {
            if(i > 0) {
                buf.append(delimiter);
            }
            buf.append(getElementAt(i));
        }
        getModel().setTmpData(COMPONENT_NAMES, buf.toString());
    }
    
    
    /*
     * Populates list of used component names from value stored in global model.
     * List is cleared when loaded.
     */
    public void load() {
        super.removeAllElements();
        String str = (String)model.getTmpData(COMPONENT_NAMES);
        if(str != null) {
            StringTokenizer st = new StringTokenizer(str, delimiter);
            while(st.hasMoreTokens()) {
                addElement(st.nextToken());
            }
        }
    }
    
    /*
     * Used by itself to store data to global model when component 
     * names are added.
     */
    public void intervalAdded(ListDataEvent e) {
        store();
    }
    
    /*
     * Used by itself to store data to global model when component 
     * names are removed.
     */
    public void intervalRemoved(ListDataEvent e) {
        store();
    }
    
    /*
     * Used by itself to store data to global model when component 
     * names are added/removed.
     */
    public void contentsChanged(ListDataEvent e) {
        store();
    }
    
    /*
     * Gets the global model.
     * @return global model
     */
    public InstallerModel getModel() {
        return model;
    }
    
    /*
     * Sets the global model.
     */
    public void setModel(InstallerModel model) {
        this.model = model;
    }
}
