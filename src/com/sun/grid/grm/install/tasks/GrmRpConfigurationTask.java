/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.bootstrap.GrmName;
import com.sun.grid.grm.bootstrap.InvalidGrmNameException;
import com.sun.grid.grm.bootstrap.PathConfiguration;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.config.ConfigurationException;
import com.sun.grid.grm.config.XMLUtil;
import com.sun.grid.grm.config.resourceprovider.ResourceProviderConfig;
import com.sun.grid.grm.config.resourceprovider.SparePoolConfig;
import com.sun.grid.grm.install.Audience;
import com.sun.grid.grm.install.GrmInstallConstants;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.GrmComponentNameControl;
import com.sun.grid.grm.install.ui.JvmListControl;
import com.sun.grid.grm.install.ui.SpinnerControl;
import com.sun.grid.grm.util.Hostname;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.swing.DefaultListModel;
import javax.swing.JPanel;
import javax.xml.bind.JAXBException;

/**
 *
 * 
 */
public class GrmRpConfigurationTask extends ExtendedAbstractInstallerTask {
    
    
    private UI ui;
    private JvmListModel jvmListModel;
    private DefaultListModel componentListModel;
    private GenericNextObserver nextObserver;        
    
    private class UI extends ControlPanel {
        
        public UI() throws InstallerException {
            InstallerController controller = getController();
            
            setTitle(rb().getString("grmRpConfiguration.Header"));
            setControls(
                    new String [] {
                "grmRpConfiguration.Identifier",
                "grmRpConfiguration.Jvm",
                "grmRpConfiguration.Usage",
                "grmRpConfiguration.PollingPeriod",
//              "grmRpConfiguration.Policies",
//              "grmRpConfiguration.PolicyName",
//              "grmRpConfiguration.PolicyType",
//              "grmRpConfiguration.PolicySettings"),
            },
                    new Control [] {
                new GrmComponentNameControl(GrmRpConfigurationTask.this, getModel(), GrmInstallConstants.GRM_RP_ID, componentListModel, getSystemConfiguration()),
                new JvmListControl(GrmRpConfigurationTask.this, getModel(), GrmInstallConstants.GRM_RP_JVM, jvmListModel),
                new SpinnerControl(GrmRpConfigurationTask.this, getModel(), GrmInstallConstants.GRM_RP_USAGE, 0, 60, 1).audience(Audience.EXPERT),
                new SpinnerControl(GrmRpConfigurationTask.this, getModel(), GrmInstallConstants.GRM_RP_POLLING_PERIOD, 0, 20, 1).audience(Audience.EXPERT),
//                new ComboBoxControl(GrmRpConfigurationTask.this, getModel(), GrmInstallConstants.GRM_RP_SELECTED_POLICIES, new String[] { "policies" }),
//                new TextControl(GrmRpConfigurationTask.this, getModel(), GrmInstallConstants.GRM_RP_SELECTED_POLICY_NAME, 15),
//                new TextControl(GrmRpConfigurationTask.this, getModel(), GrmInstallConstants.GRM_RP_SELECTED_POLICY_TYPE, 15),
//                new TextControl(GrmRpConfigurationTask.this, getModel(), GrmInstallConstants.GRM_RP_SELECTED_POLICY_SETTINGS, 15)
            },
                    rb()
                    );
        }
    }
    
    /**
     * Creates a new instance of GrmRpConfigurationTask
     */
    public GrmRpConfigurationTask(InstallerController controller, String name) {
        super(controller, name);
        jvmListModel = JvmListModel.getInstance(controller.getModel());
        componentListModel = ComponentListModel.getInstance(getModel()); 
    }
    
    public JPanel getUI() throws InstallerException {
        if(ui == null) {
            ui = new UI();
            nextObserver = createGenericNextObserver(ui);
        }
        return ui;
    }
    
    public void beginDisplay() throws InstallerException {
        getUI();
        ui.init();
        nextObserver.register();
    }
    
    public void finishDisplay() throws InstallerException {
        if(nextObserver != null) {
            nextObserver.unregister();
        }
    }
    
    public void execute() throws InstallerException {
        fireStarted();
        
        int stepCount = 2;
        double percentPerStep = 100/stepCount;
        double percent = 0;
        copyRemoteCa();
        fireProgress("...ca complete!", (int)percent);
        createConfig();
        fireProgress("...resource provider configuration created!", (int)percent);
        
        fireFinished();
    }
    
    public void undo() {
    }
    
    private void createConfig() throws InstallerException{
        GrmName componentName;
        String name = (String)getData(GrmInstallConstants.GRM_RP_ID);
        try {
            componentName = new GrmName(name);
        } catch (InvalidGrmNameException ex) {
            throw new InstallerException("{0} is not a valid grm name", rb(), name);
        }
        
        SystemConfiguration sc = getSystemConfiguration();
        PathConfiguration pc = sc.getPathConfiguration();
        ResourceProviderConfig rpConfig = new ResourceProviderConfig();
        ArrayList<String> settingsList = new ArrayList<String>();
        StringTokenizer strTok = null;
        
        File confFile = pc.getComponentFile(name);
        File confPath = confFile.getParentFile();
        try {
            InstallUtil.mkdir(getController().getModel(), confPath, true);
        } catch (InstallerException ex) {
            throw new InstallerException("grm rp config creation reported error", ex);
        }

        rpConfig.setPeriod(getModel().getInt(GrmInstallConstants.GRM_RP_POLLING_PERIOD));
        
        SparePoolConfig sparePoolConfig = new SparePoolConfig();
        sparePoolConfig.setUrgency(getModel().getInt(GrmInstallConstants.GRM_RP_USAGE));
        
        rpConfig.setSparepool(sparePoolConfig);
        
        try {
            XMLUtil.store(rpConfig, confFile);            
            InstallUtil.changeFileOwner(getModel(), confFile);
        } catch(JAXBException ex) {
            throw new InstallerException("can not write rp config", ex);
        }
        
        GrmName jvmName;
        try {
            jvmName = new GrmName((String) getData(GrmInstallConstants.GRM_RP_JVM));
        } catch (InvalidGrmNameException ex) {
            throw new InstallerException("{0} is not a valid grm name", rb(), (String) getData(GrmInstallConstants.GRM_RP_JVM));
        }
        Hostname hostname = Hostname.getLocalHost();
        addComponent(componentName, "com.sun.grid.grm.resource.impl.ResourceProviderImpl", name, jvmName, hostname);
    }
}
