/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.bootstrap.GrmName;
import com.sun.grid.grm.bootstrap.InvalidGrmNameException;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.config.ConfigurationException;
import com.sun.grid.grm.config.XMLUtil;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.Path;
import com.sun.grid.grm.install.Audience;
import com.sun.grid.grm.install.GrmInstallConstants;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.ui.ClasspathControl;
import com.sun.grid.grm.install.ui.ComboBoxControl;
import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.GrmComponentNameControl;
import com.sun.grid.grm.install.ui.JvmListControl;
import com.sun.grid.grm.install.ui.SpinnerControl;
import com.sun.grid.grm.util.Hostname;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.swing.DefaultListModel;
import javax.swing.JPanel;
import javax.xml.bind.JAXBException;

/**
 *
 * 
 */
public class GrmScConfigurationTask extends ExtendedAbstractInstallerTask {
    
    private UI ui;
    private JvmListModel jvmListModel;
    private InstallDataListModel classpathModel;
    private DefaultListModel componentListModel;
    private GenericNextObserver nextObserver;
    
    
    
    private class UI extends ControlPanel {
        
        
        public UI() throws InstallerException {
            InstallerController controller = getController();
            
            setTitle(rb().getString("grmScConfiguration.Header"));
            
            Object items [] = { "com.sun.grid.grm.service.impl.n1ge.N1geAdapterImpl" };
            String labels [] = { rb().getString("grmScConfiguration.GridengineAdapter") };
            
            ComboBoxControl serviceTypeControll = new ComboBoxControl(GrmScConfigurationTask.this, getModel(), GrmInstallConstants.SC_ADAPTERCLASSNAME_CONFIG,
                                                                      items, labels);
                
            ClasspathControl classpathControl = new ClasspathControl(GrmScConfigurationTask.this, getModel(), GrmInstallConstants.SC_ADAPTERCLASSPATH_CONFIG, classpathModel);
            classpathControl.setAudience(Audience.EXPERT);
                
            setControls(
               new String [] {
                "grmScConfiguration.Identifier",
                "grmScConfiguration.AdapterClassName",
                "grmScConfiguration.Jvm",
                "grmScConfiguration.ShutdownTime",
                "grmScConfiguration.AdapterClassPath",
            },
                    new Control [] {
                new GrmComponentNameControl(GrmScConfigurationTask.this, getModel(), GrmInstallConstants.SC_ID_CONFIG, componentListModel, getSystemConfiguration()),
                serviceTypeControll,
                new JvmListControl(GrmScConfigurationTask.this, getModel(), GrmInstallConstants.GRM_SC_JVM, jvmListModel),
                new SpinnerControl(GrmScConfigurationTask.this, getModel(), GrmInstallConstants.SC_SHUTDOWN_TIME_CONFIG, 0, 60, 1).audience(Audience.EXPERT),
                   classpathControl
            },
                    rb()
                    );
        }
    }
    
    /** Creates a new instance of GrmScConfigurationTask */
    public GrmScConfigurationTask(InstallerController controller, String name) {
        super(controller, name);
        jvmListModel = JvmListModel.getInstance(controller.getModel());
        classpathModel = new InstallDataListModel(getModel(), GrmInstallConstants.SC_ADAPTERCLASSPATH_CONFIG,":");
        componentListModel = ComponentListModel.getInstance(getModel());
    }
    
    public JPanel getUI() throws InstallerException {
        if(ui == null) {
            ui = new UI();
            nextObserver = createGenericNextObserver(ui);
        }
        return ui;
    }
    
    public void beginDisplay() throws InstallerException {
        getUI();
        ui.init();
        nextObserver.register();
    }
    
    public void finishDisplay() throws InstallerException {
        if(nextObserver != null) {
            nextObserver.unregister();
        }
    }
    
    public void execute() throws InstallerException {
        fireStarted();
        
        int stepCount = 2;
        double percentPerStep = 100/stepCount;
        double percent = 0;
        copyRemoteCa();
        fireProgress("...ca complete!", (int)percent);
        createScConfig();
        fireProgress("...service container configuration created!", (int)percent);
        
        fireFinished();
    }
    
    public void undo() {
    }
    
    public void createScConfig() throws InstallerException {
        SystemConfiguration sc = getSystemConfiguration();
        File confFile = null;
        String adapterId = (String) getData(GrmInstallConstants.GRM_SELECTED_GE_ADAPTER);
        String name = (String)getData(GrmInstallConstants.SC_ID_CONFIG);

        GrmName componentName;
        try {
            componentName = new GrmName(name);
        } catch (InvalidGrmNameException ex) {
            throw new InstallerException("{0} is not a valid grm name", rb(), name);
        }
        
        Component component = new Component();
        component.setName(componentName.getName());
        //mscc.setTimeout(getController().getModel().getInt(GrmInstallConstants.SC_SHUTDOWN_TIME_CONFIG));
        //mscc.setServiceAdapter(adapterId, false);
        component.setClassname((String) getController().getModel().getData(GrmInstallConstants.SC_ADAPTERCLASSNAME_CONFIG));

        Path classpath = new Path();
        for(int i = 0; i < classpathModel.size(); i++) {
            classpath.getPathelement().add((String)classpathModel.getElementAt(i));
        }
        component.setClasspath(classpath);

        // TODO get the global configuration
        com.sun.grid.grm.config.common.GlobalConfig globalConfig = null;
        
        confFile = new File(sc.getPathConfiguration().getConfigPath(), "global.xml");
        
        try {
            XMLUtil.store(globalConfig, confFile);        
            InstallUtil.changeFileOwner(getModel(), confFile);
        } catch(JAXBException ex) {
            throw new InstallerException(ex.getMessage(), ex);
        }
        
//        GrmName jvmName;
//        try {
//            jvmName = new GrmName((String) getData(GrmInstallConstants.GRM_SC_JVM));
//        } catch (InvalidGrmNameException ex) {
//            throw new InstallerException("{0} is not a valid grm name", rb(), (String) getData(GrmInstallConstants.GRM_SC_JVM));
//        }
//        Hostname hostname = Hostname.getLocalHost();
//        addComponent(componentName, "com.sun.grid.grm.service.impl.ServiceContainerImpl", name, jvmName, hostname);
        
    }
    
}
