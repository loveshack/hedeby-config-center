/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.bootstrap.GrmName;
import com.sun.grid.grm.bootstrap.InvalidGrmNameException;
import com.sun.grid.grm.bootstrap.JvmConfiguration;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerModel;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Model for the JvmListControl
 *
 * Makes it possible to load and store the <code>JvmConfiguration</code>s
 * of a <code>SystemConfiguration</code>.
 * 
 */
public class JvmListModel extends InstallDataListModel  {
    
    public static final String AVAILABLE_JVMS = "AVAILABLE_JVMS";
    
    private final static Logger log = Logger.getLogger(JvmListModel.class.getName());
    
    /**
     * Creates a new instance of JvmListModel
     * @param model  the installer model
     */
    protected JvmListModel(InstallerModel model) {
        super(model, AVAILABLE_JVMS, "|");
    }
    
    
    
    public synchronized static JvmListModel getInstance(InstallerModel model) {
        JvmListModel ret = null;
        synchronized(model) {
            ret = (JvmListModel)model.getTmpData(JvmListModel.class.getName());
            if(ret == null) {
                ret = new JvmListModel(model);
                model.setTmpData(JvmListModel.class.getName(), ret);
            }
        }
        return ret;
    }
    
    
    public void load(SystemConfiguration sc) {
        
        if(sc != null) {
            for (JvmConfiguration jvms : sc.getJvmConfigurations()) {
                String jvm = jvms.getName().getName();                
                if(!contains(jvm)) {
                    log.log(Level.FINE, "Load jvm {0} from system configuration", jvm);
                    addElement(jvm);
                    getModel().setData(JvmDialog.getAdminUserKey(jvm), jvms.getUser());
                    getModel().setData(JvmDialog.getPortKey(jvm), jvms.getPort());
                    getModel().setData(JvmDialog.getSgeDistKey(jvm), jvms.getSgeRoot());
                    getModel().setData(JvmDialog.getLdLibraryPathKey(jvm), jvms.getLdLibraryPath());
                }
            }
        }        
    }
    
    public void store(SystemConfiguration sc) throws InstallerException {
        for(int i = 0; i < getSize(); i++) {
            GrmName jvmName;
            try {
                jvmName = new GrmName((String) get(i));
            } catch (InvalidGrmNameException ex) {
                throw new InstallerException("Invalid jvm name (" + get(i) + ")", ex);
            }

            JvmConfiguration jvmConf = sc.getJvmConfiguration(jvmName);
            
            if(jvmConf == null) {
                jvmConf = new JvmConfiguration();
                jvmConf.setName(jvmName);
                sc.addJvmConfigurations(jvmConf);
            }
            
            jvmConf.setUser((String)getModel().getData(JvmDialog.getAdminUserKey(jvmName.getName())));
            jvmConf.setPort(getModel().getInt(JvmDialog.getPortKey(jvmName.getName())));
            jvmConf.setSgeRoot((String)getModel().getData(JvmDialog.getSgeDistKey(jvmName.getName())));
            jvmConf.setLdLibraryPath((String)getModel().getData(JvmDialog.getLdLibraryPathKey(jvmName.getName())));
        }
    }
}
