/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.corba.se.impl.protocol.giopmsgheaders.CancelRequestMessage;
import com.sun.corba.se.impl.protocol.giopmsgheaders.FragmentMessage_1_1;
import com.sun.corba.se.impl.protocol.giopmsgheaders.FragmentMessage_1_2;
import com.sun.corba.se.impl.protocol.giopmsgheaders.LocateReplyMessage_1_0;
import com.sun.corba.se.impl.protocol.giopmsgheaders.LocateReplyMessage_1_1;
import com.sun.corba.se.impl.protocol.giopmsgheaders.LocateReplyMessage_1_2;
import com.sun.corba.se.impl.protocol.giopmsgheaders.LocateRequestMessage_1_0;
import com.sun.corba.se.impl.protocol.giopmsgheaders.LocateRequestMessage_1_1;
import com.sun.corba.se.impl.protocol.giopmsgheaders.LocateRequestMessage_1_2;
import com.sun.corba.se.impl.protocol.giopmsgheaders.Message;
import com.sun.corba.se.impl.protocol.giopmsgheaders.ReplyMessage_1_0;
import com.sun.corba.se.impl.protocol.giopmsgheaders.ReplyMessage_1_1;
import com.sun.corba.se.impl.protocol.giopmsgheaders.ReplyMessage_1_2;
import com.sun.corba.se.impl.protocol.giopmsgheaders.RequestMessage_1_0;
import com.sun.corba.se.impl.protocol.giopmsgheaders.RequestMessage_1_1;
import com.sun.corba.se.impl.protocol.giopmsgheaders.RequestMessage_1_2;
import com.sun.grid.grm.install.AbstractInstallerTask;
import com.sun.grid.grm.install.GrmMessageDialog;
import com.sun.grid.grm.install.InstallExecutor;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerExecutorListener;
import com.sun.grid.grm.install.InstallerTask;
import com.sun.grid.grm.install.InstallerTaskListener;
import com.sun.grid.grm.install.InstallerUI;
import com.sun.grid.grm.install.util.MessageDialogHandler;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * The <code>InstallExecutorTask</code> is the last task of a task sequence
 * This task has not input controls. it only executes all other tasks.
 *
 */
public class InstallExecutorTask extends AbstractInstallerTask implements InstallerExecutorListener {
    
    private UI ui;
    
    /**
     * Creates a new instance of InstallExecutorTask
     * @param controller The installer controller
     * @param name       the name of this task
     */
    public InstallExecutorTask(InstallerController controller, String name) {
        super(controller, name);
    }
    
    /*
     * Inherited
     */
    public JPanel getUI() {
        if(ui == null) {
            ui = new UI();
        }
        return ui;
    }
    
    /*
     * Inherited
     */
    public void beginDisplay() throws InstallerException {
        getController().getUi().setExitAction(InstallerUI.ExitAction.INSTALL);
    }
    
    /*
     * Inherited
     */
    public void finishDisplay() throws InstallerException {
    }
    
    /**
     * Executes all tasks of the controllers task sequence in a
     * new thread
     * @throws com.sun.grid.grm.install.InstallerException
     */
    public void execute() throws InstallerException {
        
        LinkedList<InstallerTask> tasks = new LinkedList<InstallerTask>(getController().getSequence().getTasks());
        tasks.remove(this);
        InstallExecutor executor = new InstallExecutor(tasks);
        
        Thread t = new Thread(executor);
        executor.addListener(this);
        if(ui != null) {
            ui.setExecutor(executor);
        }
        t.start();
    }
    
    public void undo() {
    }
    
    /*
     *  Inherited
     */
    public void taskStarted(InstallerTask task) {
        if(!getController().getMode().equals(InstallerController.Mode.GUI)) {
            log.log(Level.INFO, "Task {0} started", task.getName());
        }
    }
    
    /*
     *  Inherited
     */
    public void taskFinished(InstallerTask task) {
        if(!getController().getMode().equals(InstallerController.Mode.GUI)) {
            log.log(Level.INFO, "Task {0} finished", task.getName());
        }
    }
    
    /*
     *  Inherited
     */
    public void taskFailed(InstallerTask task, Exception ex) {
        if(!getController().getMode().equals(InstallerController.Mode.GUI)) {
            log.log(Level.SEVERE, "Task " + task.getName() + " failed: " + ex.getLocalizedMessage(), ex);
            System.exit(1);
        }
    }
    
    /**
     *  Disables the exit action in the ui
     */
    public void started(final InstallExecutor executor) {
        if(getController().getMode().equals(InstallerController.Mode.GUI)) {
            if(SwingUtilities.isEventDispatchThread()) {
                getController().getUi().setExitAction(InstallerUI.ExitAction.DISABLED);
                getController().getFrame().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            } else {
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            started(executor);
                        }
                    });
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                } catch (InvocationTargetException ex) {
                    ex.printStackTrace();
                }
            }
        } else {
            log.log(Level.INFO,"Installation started");
        }
    }
    
    /**
     *  Sets the exit action in the ui to finished
     */
    public void finished(final InstallExecutor executor) {
        if(getController().getMode().equals(InstallerController.Mode.GUI)) {
            if(SwingUtilities.isEventDispatchThread()) {
                getController().getUi().setExitAction(InstallerUI.ExitAction.FINISH);
                getController().getFrame().setCursor(Cursor.getDefaultCursor());
                executor.removeListener(this);
            } else {
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            finished(executor);
                        }
                    });
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                } catch (InvocationTargetException ex) {
                    ex.printStackTrace();
                }
            }
        } else {
            log.log(Level.INFO, "Installation Finished");
        }
    }
    
    
    private class UI extends JPanel {
        
        private RootNode rootNode = new RootNode();
        private DefaultTreeModel taskTreeModel = new DefaultTreeModel(rootNode);
        private JTree taskTree = new MyTree(taskTreeModel);
        private MyTreeCellRenderer renderer = new MyTreeCellRenderer();
        public UI() {
            super(new BorderLayout());
            add(new JLabel("<html><body><H3>" + rb().getString("install.header") + "</H3></body></html>"), BorderLayout.NORTH);

            add(new JScrollPane(taskTree), BorderLayout.CENTER);

            setBorder(BorderFactory.createEmptyBorder(20,20,20,20));

            taskTree.setCellRenderer(renderer);
            taskTree.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
            
            taskTree.addMouseListener(new MyMouseListener());
            
            ToolTipManager.sharedInstance().registerComponent(taskTree);
        }
        
        public void setExecutor(InstallExecutor executor) {
            rootNode.setExecutor(executor);
        }
        
        private class MyMouseListener extends MouseAdapter {
             public void mousePressed(MouseEvent e) {
                 if(e.getClickCount() > 1) {
                     int selRow = taskTree.getRowForLocation(e.getX(), e.getY());
                     if(selRow != -1) {                     
                         TreePath selPath = taskTree.getPathForLocation(e.getX(), e.getY());
                         Object lastComponent = selPath.getLastPathComponent();
                         if(lastComponent instanceof InstallerTreeNode) {
                            ((InstallerTreeNode)lastComponent).showDetails();
                         }
                     }
                 }
             }
         }

        private class MyTree extends JTree {
            
            public MyTree(TreeModel model) {
                super(model);
            }
            
            public String getToolTipText(MouseEvent e) {
                int selRow = getRowForLocation(e.getX(), e.getY());
                if(selRow != -1) {
                    TreePath selPath = getPathForLocation(e.getX(), e.getY());
                    Object lastComponent = selPath.getLastPathComponent();
                    if(lastComponent instanceof InstallerTreeNode) {
                        return ((InstallerTreeNode)lastComponent).getToolTip();
                    }
                }
                return null;
            }
        }
        
        private class MyTreeCellRenderer extends DefaultTreeCellRenderer {
            private JPanel panel = new JPanel();
            
            private JLabel iconLabel = new JLabel();
            
            private Component spacer = null;
            public MyTreeCellRenderer() {
                //BoxLayout bl = new BoxLayout(panel, BoxLayout.X_AXIS);
                panel.setLayout(new BorderLayout());
                spacer = Box.createHorizontalStrut(300);
                panel.add(spacer, BorderLayout.NORTH);
                panel.add(iconLabel, BorderLayout.EAST);
                panel.add(this, BorderLayout.CENTER);
                panel.setOpaque(false);
                //panel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
                iconLabel.setBorder(BorderFactory.createEmptyBorder(1,0,1,3));
                Dimension dim = new Dimension(30,25);
                iconLabel.setMinimumSize(dim);
                iconLabel.setPreferredSize(dim);
            }
            
            private void setMaxText(String maxText) {
                JLabel label = new JLabel(maxText);
                Dimension dim = label.getPreferredSize();
                Dimension dim1 = spacer.getMinimumSize();
                if(dim.width > dim1.width) {
                    panel.remove(spacer);
                    spacer = Box.createHorizontalStrut(dim.width);
                    panel.add(spacer, BorderLayout.NORTH);
                }
            }
            
            public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
                
                JLabel ret = (JLabel)super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
                if(value instanceof InstallerTreeNode) {
                    ret.setIcon(((InstallerTreeNode)value).getIcon());
                } else {
                    ret.setIcon(null);
                }
                iconLabel.setIcon(ret.getIcon());
                ret.setIcon(null);
                return panel;
            }
            
        }
        
        private abstract class InstallerTreeNode extends DefaultMutableTreeNode {
            public InstallerTreeNode(Object userObject) {
                super(userObject);
            }
            public abstract Icon getIcon();
            
            public void showDetails() {
            }
            
            public String getToolTip() {
                return null;
            }
        }
        
        private MessageHandler messageHandler = new MessageHandler();
        
        private class MessageHandler extends Handler {
            
            private InstallerTaskNode node;
            
            public void publish(final LogRecord record) {
                
                if(node != null) {
                   if(SwingUtilities.isEventDispatchThread()) {
                       node.add( new LogRecordNode(record));
                   } else {
                        try {
                            SwingUtilities.invokeAndWait(new Runnable() {
                                public void run() {
                                    publish(record);
                                }
                            });
                        } catch (InvocationTargetException ex) {
                            // ignore
                        } catch (InterruptedException ex) {
                            // ignore
                        }
                   }
                }
            }

            public void setCurrentNode(InstallerTaskNode node) {
                this.node = node;
            }
            
            public void flush() {
                // ignore
            }

            public void close() throws SecurityException {
                // ignore
            }
            
            
        }
        private class InstallerTaskNode extends InstallerTreeNode implements InstallerTaskListener {
            
            private int progress;
            private Exception error;
            
            public InstallerTaskNode(InstallerTask task) {
                super(task);
                task.addInstallerTaskListener(this);
            }
            
            public String getToolTip() {
                InstallerTask task = (InstallerTask)getUserObject();
                if(task != null) {
                    return task.getState().toString();
                }
                return null;
            }
            
            public void setError(final Exception error) {
                if(SwingUtilities.isEventDispatchThread()) {
                    this.error = error;
                    ErrorTreeNode errorNode = new ErrorTreeNode(error);
                    add(errorNode);
                    taskTreeModel.nodeStructureChanged(this);
                } else {
                    try {
                        SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                                setError(error);
                            }
                        });
                    } catch (InvocationTargetException ex) {
                        ex.printStackTrace();
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            
            public void started(InstallerTask task) {
                if(SwingUtilities.isEventDispatchThread()) {
                    taskTreeModel.nodeChanged(this);
                } else {
                    try {
                        SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                                taskTreeModel.nodeChanged(InstallerTaskNode.this);
                            }
                        });
                    } catch (InvocationTargetException ex) {
                        ex.printStackTrace();
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            
            public void finished(final InstallerTask task) {
                if(SwingUtilities.isEventDispatchThread()) {
                    taskTreeModel.nodeChanged(this);
                    task.removeInstallerTaskListener(this);
                } else {
                    try {
                        SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                                finished(task);
                            }
                        });
                    } catch (InvocationTargetException ex) {
                        ex.printStackTrace();
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            
            public void progress(final InstallerTask task, final String description, final int percent) {
                if(SwingUtilities.isEventDispatchThread()) {
                    if(task.equals(getUserObject())) {
                        progress = percent;
                        if(description != null) {
                            DefaultMutableTreeNode node = new DefaultMutableTreeNode(description);
                            add(node);
                            taskTreeModel.nodeStructureChanged(this);
                        } else {
                            taskTreeModel.nodeChanged(this);
                        }
                    }
                } else {
                    try {
                        SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                                progress(task, description, percent);
                            }
                        });
                    } catch (InvocationTargetException ex) {
                        ex.printStackTrace();
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            
            public String toString() {
                InstallerTask task = (InstallerTask)getUserObject();
                StringBuilder ret = new StringBuilder();
                ret.append(task.getName());
                if(task.getState().equals(InstallerTask.State.STARTED)) {
                    ret.append("[");
                    ret.append(progress);
                    ret.append("%");
                    ret.append("]");
                }
                return ret.toString();
            }
            
            public Icon getIcon() {

                if(error != null) {
                    return getController().getFrame().rb().getImage("stop.icon");
                } else {
                    InstallerTask task = (InstallerTask)getUserObject();
                    switch(task.getState()) {
                        case FINISHED:
                            return getController().getFrame().rb().getImage("ok.icon");
                        default:
                            return null;
                    }
                }
                
            }
            
        }
        
        private class RootNode extends DefaultMutableTreeNode implements InstallerExecutorListener {
            private InstallExecutor executor;
            int finishCount;
            int failCount;
            public RootNode() {
                super("Root");
            }
            
            public void setExecutor(final InstallExecutor executor) {
                if(SwingUtilities.isEventDispatchThread()) {
                    if(this.executor != null) {
                        this.executor.removeListener(this);
                    }
                    this.executor = executor;
                    executor.addListener(rootNode);
                   
                    String maxText = "";
                    for(InstallerTask task: executor.getTasks()) {
                        add(new InstallerTaskNode(task));
                        if(task.getName().length() > maxText.length()) {
                            maxText = task.getName();
                        }
                    }
                    renderer.setMaxText(maxText);
                    taskTreeModel.nodeStructureChanged(this);
                    taskTree.invalidate();
                } else {
                    try {
                        SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                                setExecutor(executor);
                            }
                        });
                    } catch (InvocationTargetException ex) {
                        ex.printStackTrace();
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            
            
            public void taskStarted(final InstallerTask task) {
                // Nothing to do => is handled in InstallerTaskNode
                messageHandler.setCurrentNode(getNodeForTask(task));                
            }
            
            private InstallerTaskNode getNodeForTask(InstallerTask task) {
                for(int i = 0; i < getChildCount(); i++) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode)getChildAt(i);
                    if(node.getUserObject().equals(task)) {
                        return (InstallerTaskNode)node;
                    }
                }
                return null;
            }
            
            public void taskFinished(final InstallerTask task) {
                if(SwingUtilities.isEventDispatchThread()) {
                    finishCount++;
                    taskTreeModel.nodeChanged(this);
                    messageHandler.setCurrentNode(null);                
                } else {
                    try {
                        SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                                taskFinished(task);
                            }
                        });
                    } catch (InvocationTargetException ex) {
                        ex.printStackTrace();
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            
            public void taskFailed(final InstallerTask task, final Exception ex) {
                if(SwingUtilities.isEventDispatchThread()) {
                    failCount++;
                    InstallerTaskNode node = getNodeForTask(task);
                    node.setError(ex);
                    taskTreeModel.nodeChanged(this);
                    messageHandler.setCurrentNode(null);                
                } else {
                    try {
                        SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                                taskFailed(task, ex);
                            }
                        });
                    } catch (InvocationTargetException ex1) {
                        ex.printStackTrace();
                    } catch (InterruptedException ex1) {
                        ex.printStackTrace();
                    }
                }
            }
            
            public void started(final InstallExecutor executor) {
                if(SwingUtilities.isEventDispatchThread()) {
                    MessageDialogHandler.setDelegateHandler(messageHandler);
                    MessageDialogHandler.setShowMessageBox(false);
                    taskTreeModel.nodeChanged(this);
                } else {
                    try {
                        SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                                started(executor);
                            }
                        });
                    } catch (InvocationTargetException ex) {
                        ex.printStackTrace();
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            
            public void finished(final InstallExecutor executor) {
                if(SwingUtilities.isEventDispatchThread()) {
                    taskTreeModel.nodeChanged(this);
                    MessageDialogHandler.setDelegateHandler(null);
                    MessageDialogHandler.setShowMessageBox(true);
                } else {
                    try {
                        SwingUtilities.invokeAndWait(new Runnable() {
                            public void run() {
                                finished(executor);
                            }
                        });
                    } catch (InvocationTargetException ex) {
                        ex.printStackTrace();
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            
            public String toString() {
                if(executor != null) {
                    return rb().getFormattedString("install.progess.started", finishCount, executor.getTaskCount(), failCount);
                } else {
                    return rb().getFormattedString("install.progess.notstarted");
                }
            }
        }
        
        private class ErrorTreeNode extends InstallerTreeNode {
            
            public ErrorTreeNode(Throwable error) {
                super(error);
            }
            
            public Icon getIcon() {
                return getController().getFrame().rb().getImage("stop.icon");
            }
            
            public String toString() {
                return rb().getFormattedString("install.error", ((Throwable)getUserObject()).getLocalizedMessage());
            }
            
            public void showDetails() {
                
                Throwable error = (Throwable)getUserObject();
                GrmMessageDialog.showErrorDialog(getController().getFrame(), error.getLocalizedMessage(), error);
            }
            
            public String getToolTip() {
                return rb().getString("install.showStacktrace");
            }
        }
        
        private class LogRecordNode extends InstallerTreeNode {
            
            private Formatter formatter = new SimpleFormatter();
            
            public LogRecordNode(LogRecord lr) {
                super(lr);
            }
            
            private LogRecord getLogRecord() {
                return (LogRecord)getUserObject();
            }
            
            public Icon getIcon() {
                return null;
            }
            
            public String toString() {
                return getLogRecord().getLevel().getLocalizedName() + ": " + formatter.formatMessage(getLogRecord());
            }
        }
    }
}
