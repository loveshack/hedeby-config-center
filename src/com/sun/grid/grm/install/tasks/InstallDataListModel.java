/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.tasks;

import com.sun.grid.grm.install.InstallerModel;
import java.util.StringTokenizer;
import javax.swing.DefaultListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 *
 * 
 */
public class InstallDataListModel extends DefaultListModel implements ListDataListener {
    
    private String delimiter = "|";
    private InstallerModel model;
    private String key;
    /** Creates a new instance of InstallDataListModel */
    public InstallDataListModel(InstallerModel model, String key, String delimiter) {
    
        this.setModel(model);
        this.key = key;
        this.delimiter = delimiter;
        load();
        addListDataListener(this);
    }
    
    public void load() {
        super.removeAllElements();
        String str = (String)model.getData(key);
        if(str != null) {
            StringTokenizer st = new StringTokenizer(str, delimiter);
            while(st.hasMoreTokens()) {
                addElement(st.nextToken());
            }
        }
    }
    
    private void store() {
        StringBuilder buf = new StringBuilder();
        for(int i = 0; i < getSize(); i++) {
            if(i > 0) {
                buf.append(delimiter);
            }
            buf.append(getElementAt(i));
        }        
        getModel().setData(key, buf.toString());
    }
    
    public void intervalAdded(ListDataEvent e) {
        store();
    }

    public void intervalRemoved(ListDataEvent e) {
        store();
    }

    public void contentsChanged(ListDataEvent e) {
        store();
    }

    public InstallerModel getModel() {
        return model;
    }

    public void setModel(InstallerModel model) {
        this.model = model;
    }
    
    
    
    
    
}
