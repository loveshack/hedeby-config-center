/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.install;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
public class InstallerTaskSequence {
    private static Logger log = Logger.getLogger(InstallerTaskSequence.class.getName());
    private String name;
    private InstallerController controller;
    
    private LinkedList<InstallerTaskDefinition> tasks = new LinkedList<InstallerTaskDefinition>();
    private int currentTaskIndex = 0;
    
    /** Creates a new instance of InstallerTaskSequence */
    public InstallerTaskSequence(InstallerController controller, String name) {
        this.name = name;
        this.controller = controller;
    }
    
    public void addTask(InstallerTaskDefinition task) {
        if(task == null) {
            throw new IllegalArgumentException("task must no be null");
        }
        tasks.add(task);
    }
    
    public InstallerTask next() throws InstallerException {
        if(hasNext()) {
            currentTaskIndex++;
        }
        logTaskPath();
        return getCurrentTask();
    }
    
    public InstallerTask back() throws InstallerException {
        if(hasBack()) {
            currentTaskIndex--;
        }
        logTaskPath();
        return getCurrentTask();
    }
    
    public boolean hasBack() {
        return currentTaskIndex > 0;
    }
    
    public boolean hasNext() {
        return currentTaskIndex + 1< tasks.size();
    }
    
    public InstallerTask getCurrentTask() throws InstallerException {
        return tasks.get(currentTaskIndex).getTask(controller);
    }
    
    public List<InstallerTask> getTasks() throws InstallerException {
        List<InstallerTask> ret = new ArrayList<InstallerTask>(tasks.size());
        for(InstallerTaskDefinition taskDef: tasks) {
            ret.add(taskDef.getTask(controller));
        }
        return ret;
    }
    
    
    /**
     * Set a new path through the installer tasks
     * @param path array with the name of the taks.
     *
     * @throws com.sun.grid.grm.install.InstallerException if a task is not found
     */
    public void setTaskPath(List<String> path) throws InstallerException {        
        InstallerTaskDefinition currentTaskDef = tasks.get(currentTaskIndex);       
        tasks.clear();
        for(String taskName: path) {
            InstallerTaskDefinition taskDef = controller.getTask(taskName);
            if(taskDef == null) {
                throw new InstallerException("task " + taskName + " is not defined");
            }
            tasks.add(taskDef);            
        }
        currentTaskIndex = 0;
        if(currentTaskDef != null) {            
            // Got to the current task            
            ListIterator<InstallerTaskDefinition> iter = tasks.listIterator();            
            while(iter.hasNext()) {
                InstallerTaskDefinition taskDef = iter.next();
                if(taskDef.getName().equals(currentTaskDef.getName())) {
                    currentTaskIndex = iter.previousIndex();
                    break;
                }
            }
        }        
        logTaskPath();
    }
    
    private void logTaskPath() {
        
        if(log.isLoggable(Level.FINE)) {
            StringBuilder tmp = new StringBuilder();
            int i = 0;
            for(InstallerTaskDefinition taskDef: tasks) {
                if(tmp.length() > 0) {
                    tmp.append(", ");
                }
                if(i == currentTaskIndex) {
                    tmp.append("*");
                }
                tmp.append(taskDef.getName());
                if(i == currentTaskIndex) {
                    tmp.append("*");
                }
                i++;
            }
            log.log(Level.FINE, "taskpath: {0}", tmp);
        }
    }
    
    public List<String> getTaskPath() {
        
        List<String> ret = new ArrayList<String>();
        for(InstallerTaskDefinition taskDef: tasks) {
            ret.add(taskDef.getName());
        }
        
        return ret;
    }
    
    public void addTaskAfterCurrent(String taskName) throws InstallerException  {
        InstallerTaskDefinition taskDef = controller.getTask(taskName);
        tasks.add(currentTaskIndex+1, taskDef);
        logTaskPath();
    }
    
    public void addTasksAfterCurrent(List<String> taskNames) throws InstallerException  {
        int index = currentTaskIndex+1;
        for(String taskName: taskNames) {
            InstallerTaskDefinition taskDef = controller.getTask(taskName);
            tasks.add(index, taskDef);
            index++;
        }
        logTaskPath();
    }
    
    public void addTasksBefore(String postTask, List<String> taskNames) throws InstallerException {
        
         int index = -1;
         int i = 0;
         for(InstallerTaskDefinition taskDef: tasks) {
             if(taskDef.getName().equals(postTask)) {
                 index = i;
                 break;
             }
             i++;
         }
         
         if(index < 0) {
             throw new InstallerException("task " + postTask + " not found in current path");
         }
         
         for(String taskName: taskNames) {
             InstallerTaskDefinition taskDef  = controller.getTask(taskName);
             tasks.add(index,taskDef);
             index++;
         }
         logTaskPath();
    }
   
    
    public void removeTaskFromPath(String taskName) throws InstallerException {
        
        int index = -1;
        int i = 0;
        for(InstallerTaskDefinition taskDef: tasks) {
            if(taskDef.getName().equals(taskName)) {
                index = i;
                break;
            }
            i++;
        }
        if(index < 0) {
            throw new InstallerException("Task " + taskName + " not found in current path");
        }
        
        tasks.remove(index);
        if(index < currentTaskIndex) {
            currentTaskIndex--;
        }
        logTaskPath();
    }
    
    public void removeTasksBetween(String predecessor, String successor) throws InstallerException {

        int i = 0;
        int predecessorIndex = -1;
        int successorIndex = -1;
        
        for(InstallerTaskDefinition taskDef: tasks) {
            if(predecessorIndex < 0) {
                if(taskDef.getName().equals(predecessor)) {
                    predecessorIndex = i;
                }
            } else {
                if(taskDef.getName().equals(successor)) {
                    successorIndex = i;
                    break;
                }
            }
            i++;
        }
        
        if(predecessorIndex < 0) {
            throw new InstallerException("task " + predecessor + " not found in current path");
        }
        if(successorIndex < 0) {
            throw new InstallerException("task " + successor + "  is not a successor of task " + predecessor);
        }

        if(currentTaskIndex > predecessorIndex && currentTaskIndex < successorIndex) {
            throw new InstallerException("Can not remove tasks between " + predecessor + " and " + successor + ", because the current task is in this range");
        }
        
        int removeIndex = predecessorIndex + 1;
        for(i = predecessorIndex + 1; i < successorIndex; i++) {
            tasks.remove(removeIndex);
        }
        logTaskPath();        
    }

    public Logger getLog() {
        return log;
    }
    
}
