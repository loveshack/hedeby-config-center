/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install;

import java.awt.Dimension;
import java.awt.Graphics;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 */
public class ImagePanel extends JPanel {
    
    public final static int MAX_IMAGE_WIDTH = 180;
    
    private ImageIcon backgroundImage;
    private Dimension backgroundImageDim;
    private ImageIcon foregroundImage;
    private int x;
    private final Dimension foregroundImageDim = new Dimension(0,0);
    
    public ImagePanel(String backgroundKey, String foregroundKey, RB rb) {
        this(rb.getImage(backgroundKey), rb.getImage(foregroundKey));
    }
    /** Creates a new instance of ImagePanel */
    public ImagePanel(ImageIcon backgroundImage, ImageIcon foregroundImage) {
        this.backgroundImage = backgroundImage;
        this.foregroundImage = foregroundImage;
        if(backgroundImage != null) {
            backgroundImageDim = new Dimension(backgroundImage.getIconWidth(), backgroundImage.getIconHeight());
        }
        
        if(foregroundImage != null) {
            foregroundImageDim.width = foregroundImage.getIconWidth();
            foregroundImageDim.height = foregroundImage.getIconHeight();
            if(foregroundImageDim.width > MAX_IMAGE_WIDTH) {
                foregroundImageDim.width = MAX_IMAGE_WIDTH;
                x = MAX_IMAGE_WIDTH - foregroundImage.getIconWidth();
            }
            Dimension maxDim = new Dimension(foregroundImageDim.width, Integer.MAX_VALUE);
            setPreferredSize(foregroundImageDim);
            setMaximumSize(maxDim);
        }
    }
    
    

    public void paint(Graphics g) {
        super.paint(g);
        
        Dimension dim = getSize();
        
        if(backgroundImage != null) {
            int rows = dim.height / backgroundImageDim.height + 1;
            int cols = dim.width / backgroundImageDim.width + 1;
            
            for(int row = 0; row < rows; row++) {
                for(int col = 0; col < cols; col++) {
                    g.drawImage(backgroundImage.getImage(), x + col * backgroundImageDim.width, row * backgroundImageDim.height,
                            backgroundImageDim.width, backgroundImageDim.height, backgroundImage.getImageObserver());
                }
            }
        }
        if(foregroundImage != null) {
            int y = Math.max(0, (dim.height - foregroundImageDim.height) / 2);

            g.drawImage(foregroundImage.getImage(), x, y, foregroundImageDim.width, foregroundImageDim.height, foregroundImage.getImageObserver());
        }        
    }
    
    public int getImageWidth() {
        return foregroundImageDim.width;
    }
    
    
}
