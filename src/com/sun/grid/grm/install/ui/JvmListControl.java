/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.bootstrap.GrmName;
import com.sun.grid.grm.bootstrap.InvalidGrmNameException;
import com.sun.grid.grm.bootstrap.JvmConfiguration;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerTask;
import com.sun.grid.grm.install.tasks.JvmDialog;
import com.sun.grid.grm.install.tasks.JvmListModel;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.regex.Pattern;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

/**
 *
 * 
 */
public class JvmListControl extends ListControl {
    
    private JvmListModel jvmListModel;

    public JvmListControl(InstallerTask task, InstallerModel model, String dataKey, JvmListModel listModel) {
        super(task, model, dataKey, listModel);
        this.jvmListModel = listModel;
        
        addAction(new AddJvmAction());
        addAction(new EditJvmAction(), true, true);
        addAction(new RemoveJvmAction(), true, false);
    }
    
    public JvmListControl(InstallerTask task, InstallerModel model, String dataKey, JvmListModel listModel, SystemConfiguration sc) {
        this(task, model, dataKey, listModel);
        
        if(sc != null) {
            jvmListModel.load(sc);
        }
        setListModel(listModel);
        
    }
    
    private JvmDialog dlg;
    
    private JvmDialog getJvmDialog() {
        if(dlg == null) {
            dlg = new JvmDialog(getController().getFrame(), getTask());
            dlg.setSize(450, 450);
            dlg.setLocationRelativeTo(getController().getFrame());
        }
        return dlg;
    }
    
    private class AddJvmAction extends AbstractAction {
        
        public AddJvmAction() {
            super("add");
        }
        
        public void actionPerformed(ActionEvent e) {    
            String name = JOptionPane.showInputDialog(getController().getFrame(),
                    rb().getString("JvmListControl.input.jvmname"),
                    rb().getString("JvmListControl.input.default"));
            try {
                GrmName.isValid(name);
                if(jvmListModel.contains(name)) {
                    log.log(Level.SEVERE, "JVM with name {0} is already defined", name);
                } else {
                    JvmDialog dlg = getJvmDialog(); 
                    try {
                        dlg.setJvmName(name);
                        if(dlg.showDialog()) {          
                            jvmListModel.addElement(name);
                            getList().setSelectedValue(name, true);
                        }
                    } catch(InstallerException ex) {
                        log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
                    }
                }
            } catch(InvalidGrmNameException igne) {
                if (name != null) {
                   JOptionPane.showMessageDialog(getController().getFrame(),
                    rb().getString("JvmListControl.message.body"),
                    rb().getString("JvmListControl.message.header"),
                    JOptionPane.WARNING_MESSAGE); 
                } 
            }
        }
    }
    
    private class RemoveJvmAction extends AbstractAction {
        public RemoveJvmAction() {
            super("remove");
        }
        
        public void actionPerformed(ActionEvent e) {
            
            String jvmName = (String)getList().getSelectedValue();
            if(jvmName != null) {
                jvmListModel.removeElement(jvmName);
            }
        }
    }
    
    private class EditJvmAction extends AbstractAction {
        public EditJvmAction() {
            super("edit");
        }
        private JvmDialog dlg;
        public void actionPerformed(ActionEvent e) {
            
            String jvmName = (String)getList().getSelectedValue();
            if(jvmName != null) {
                JvmDialog dlg = getJvmDialog();
                try {
                    dlg.setJvmName(jvmName);
                    dlg.showDialog();
                } catch(InstallerException ex) {
                    log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
                }
            }
        }
    }
    
    
}
