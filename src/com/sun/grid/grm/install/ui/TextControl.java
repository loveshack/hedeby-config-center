/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerModelListener;
import com.sun.grid.grm.install.InstallerTask;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.logging.Logger;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 */
public class TextControl extends AbstractControl implements ActionListener, DocumentListener, InstallerModelListener {
    
    private static final int DELAY = 500; 
            
    private static Logger log = Logger.getLogger(FileControl.class.getName());
    private JTextField textField = new JTextField();
    private String sysName;
    private Timer timer = new Timer(DELAY, this);
    
    /**
     * Creates a new instance of TextControl
     */
    public TextControl(InstallerTask task, InstallerModel model, String dataKey, int columns) {
        this(task, model, dataKey);
        textField.setColumns(columns);
        textField.setMaximumSize(textField.getPreferredSize());
    }
    
    public TextControl(InstallerTask task, InstallerModel model, String dataKey) {
        super(task, model, dataKey);
        setContent(textField);
        textField.getDocument().addDocumentListener(this);
        timer.setRepeats(true);
        timer.setInitialDelay(DELAY);
    }
    
    
    protected JTextField getTextField() {
        return textField;
    }
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        textField.setEditable(enabled);
    }
    public void init() {
        Object value = getModel().getData(getDataKey());
        if(value == null) {
            value = "";
        }
        String str = value.toString();
        textField.setText(str);
        setState(getStateForValue(str));
    }
    
    private boolean comesFromTextField;
    private int minLen = 1;
    private int maxLen = Integer.MAX_VALUE;
    
    public TextControl textLen(int min, int max) {
        maxLen = max;
        minLen = min;
        return this;
    }
    
    private String orgValue;
    
    protected void setText(String text) {
        if ( text != null) {
            if(!text.equals(orgValue)) {
                orgValue = text;
                if(!comesFromTextField) {
                    textField.setText(text);
                }
                setState(getStateForValue(text));
                if(!changedModelArmed) {
                    getModel().setData(getDataKey(), text);
                }
            }
        }
    }
    
    
    protected Control.State getStateForValue(String value) {
        int len = 0;
        if(value != null ) {
            len = value.length();
        }
        if(len < minLen) {
            setStateMessage(rb().getFormattedString("TextControl.tooShort", minLen));
            return Control.State.ERROR;
        } else if ( len > maxLen) {
            setStateMessage(rb().getFormattedString("TextControl.tooLong", maxLen));
            return Control.State.ERROR;
        } else {
            setStateMessage(null);
            return Control.State.OK;
        }
    }
    
    private boolean changedModelArmed;
    
    public final void changedModel(InstallerModelEvent evt) {
        try {
            changedModelArmed = true;
            setText(getStringForValue(evt.getNewValue()));
        } finally {
            changedModelArmed = false;
        }
    }
    
    protected String getStringForValue(Object value) {
        if(value != null) {
            return value.toString();
        } else {
            return null;
        }
    }
    

    long modifySequence;
    long lastModifySequence;
    
    private void modified() {
        modifySequence++;
        if(!timer.isRunning()) {
            timer.start();
        }
    }
    
    public void insertUpdate(DocumentEvent e) {
        modified();
    }

    public void removeUpdate(DocumentEvent e) {
        modified();
    }

    public void changedUpdate(DocumentEvent e) {
        modified();
    }

    public void actionPerformed(ActionEvent e) {
        long diff = modifySequence - lastModifySequence;
        if(lastModifySequence >= 0 && diff <= 1) {
            timer.stop();
            try {
                comesFromTextField = true;
                setText(textField.getText());
            } finally {
                comesFromTextField = false;
            }
            modifySequence = 0;
            lastModifySequence = -1; 
        } else {
            lastModifySequence = modifySequence;
        }
    }
}
