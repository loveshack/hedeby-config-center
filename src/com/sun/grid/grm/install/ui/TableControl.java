/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerTask;
import com.sun.grid.grm.install.RB;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.LinkedList;
import java.util.List;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;

/**
 * This control displays a table in the config-center.
 *
 */
public class TableControl extends AbstractControl implements ListSelectionListener {
    
    private JTable table = new JTable();
    private JPanel buttonPanel;
    private TableControlModel tablemodel;
    private int buttonCount;
    
    public TableControl(InstallerTask task, InstallerModel model, String datakey, TableControlModel tablemodel) {
        super(task, model, datakey);
        
        this.tablemodel = tablemodel;
        
        table.setModel(tablemodel);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        JScrollPane sc = new JScrollPane(table);
        
        Dimension dim = new Dimension(200, 100);
        sc.setPreferredSize(dim);
        
        buttonPanel = new JPanel(new GridBagLayout());
        
        buttonPanel.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
        
        JPanel p = new JPanel(new BorderLayout());
        p.add(sc, BorderLayout.CENTER);
        p.add(buttonPanel, BorderLayout.EAST);
        p.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
        setContent(p);
        
        table.getSelectionModel().addListSelectionListener(this);
    }
    
    public TableControlModel getTableModel() {
        return tablemodel;
    }
    
    public void init() throws InstallerException {
        tablemodel.fireTableStructureChanged();
    }
    
    public int getSelectedRow() {
        return table.getSelectedRow();
    }
    
    public boolean isSelectionEmpty() {
        return table.getSelectionModel().isSelectionEmpty();
    }
    
    
    private List<Action> requiresSelectionActionList = new LinkedList<Action>();
    private Action defaultAction;
    
    public void addAction(Action newAction) {
        addAction(newAction, false, false);
    }
    
    public void addDefaultAction(Action newAction) {
        addAction(newAction, false, true);
    }
    
    public void addAction(Action newAction, boolean requiresSelection, boolean isDefaultAction) {
        JButton newButton = new JButton();
        buttonCount ++;
        newButton.setAction(newAction);
        newButton.setVisible(newAction != null);
        GridBagConstraints gbc = new GridBagConstraints();
        int border = 2;
        gbc.gridx = 0;
        gbc.gridy = buttonCount;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.anchor = gbc.CENTER;
        gbc.insets = new Insets(border,border,border,border);
        gbc.fill = gbc.HORIZONTAL;
        buttonPanel.add(newButton, gbc);
        if(requiresSelection) {
            requiresSelectionActionList.add(newAction);
            newAction.setEnabled(!table.getSelectionModel().isSelectionEmpty());
        }
        if(isDefaultAction) {
            defaultAction = newAction;
        }
        
    }
    
    
    private static class LabelCellRenderer implements  TableCellRenderer {
        
        private RB rb;
        private String [] labels;
        private TableCellRenderer renderer;
        public LabelCellRenderer(RB rb, String [] labels, TableCellRenderer renderer) {
            this.renderer = renderer;
            this.rb = rb;
            this.labels = labels;
        }
        
        public Component getTableCellRendererComponent(JTable table,
                Object value,
                boolean isSelected,
                boolean hasFocus,
                int row,
                int column) {
             value= rb.getString(labels[column]);
             return renderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
        
    }
    
    private TableCellRenderer defaultTableCellRenderer;
    public void setColumnLabels(RB rb, String [] values) {
        
        JTableHeader th = table.getTableHeader();
        if (defaultTableCellRenderer == null) {
            defaultTableCellRenderer = th.getDefaultRenderer();
        }
        th.setDefaultRenderer(new LabelCellRenderer(rb, values, defaultTableCellRenderer));
    }
    
    public void changedModel(InstallerModelEvent evt) {
    }
    
    public void valueChanged(ListSelectionEvent e) {
        if(!e.getValueIsAdjusting() && !table.getSelectionModel().isSelectionEmpty()) {
            for(Action action: requiresSelectionActionList) {
                action.setEnabled(true);
            }
        } else {
            for(Action action: requiresSelectionActionList) {
                action.setEnabled(false);
            }
        }
    }
}
