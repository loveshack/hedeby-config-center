/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.bootstrap.PathConfiguration;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerTask;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;

/**
 *
 */
public class FileControl extends TextControl {
    
    private static Logger log = Logger.getLogger(FileControl.class.getName());
    private String dataKey;
    private JButton      selectButton;
    private String sysName;
    private SystemConfiguration sc = null;
    private PathConfiguration pc = null;
    
    private boolean optional;
    private JFileChooser fileChooser = new JFileChooser();
    
    /**
     * Creates a new instance of TextControl
     */
    public FileControl(InstallerTask task, InstallerModel model, String dataKey) {
        super(task, model, dataKey);
        initMembers();
    }
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        selectButton.setEnabled(enabled);
    }
    
    
    /**
     * Creates a new instance of TextControl
     */
    public FileControl(InstallerTask task, InstallerModel model, String dataKey, int columns) {
        super(task, model, dataKey, columns);
        this.dataKey = dataKey;
        initMembers();
    }
    
    private void initMembers() {
        selectButton = new JButton(new SelectAction());
        
        JPanel panel = new JPanel();
        BoxLayout bl = new BoxLayout(panel, BoxLayout.X_AXIS);
        panel.setLayout(bl);
        panel.add(getTextField());
        panel.add(Box.createHorizontalStrut(3));
        panel.add(selectButton);
        panel.add(Box.createHorizontalGlue());
        
        setContent(panel);
    }
    
    protected Control.State getStateForValue(String value) {
        if(optional && (value == null || value.length() == 0)) {
            return Control.State.OK;
        } else {
            File f = new File(value);
            String message = null;
            boolean accepted = false;
                
            if(f.exists()) {
                switch(fileChooser.getFileSelectionMode()) {
                    case JFileChooser.DIRECTORIES_ONLY:
                        accepted = f.isDirectory();
                        if(!accepted) {
                            message= rb().getFormattedString("FileControl.only.directories.accepted", f);
                        }
                        break;
                    case JFileChooser.FILES_ONLY:
                        accepted = f.isFile();
                        if(!accepted) {
                            message= rb().getFormattedString("FileControl.only.files.accepted", f);
                        }
                        break;
                    default:
                        accepted = true;
                }
            } else {
                accepted = true;
            }
            if(accepted) {
                accepted = fileChooser.accept(f);
                if(!accepted) {
                    message= rb().getFormattedString("FileControl.filtered", f);
                }
            }
            setStateMessage(message);
            if(accepted) {
                return Control.State.OK;
            } else {
                return Control.State.ERROR;
            }
        }
    }

    protected String getStringForValue(Object value) {
        if(value instanceof File) {
            return ((File)value).getAbsolutePath();
        } else {
            return super.getStringForValue(value);
        }
    }
    
    
    
    
    private class SelectAction extends AbstractAction {
        
        private JFileChooser fileChooster;
        public SelectAction() {
            super("...");
        }
        
        public void actionPerformed(ActionEvent e) {
            String value = getTextField().getText();
            if(value != null && value.length() > 0) {
                fileChooser.setSelectedFile(new File(value));
            }
            
            int ret = fileChooser.showOpenDialog(getTask().getController().getFrame());
            if(ret == JFileChooser.APPROVE_OPTION) {
                setText(fileChooser.getSelectedFile().getPath());
            }
        }
    }

    public boolean isOptional() {
        return optional;
    }

    public void setOptional(boolean optional) {
        this.optional = optional;
    }
    
    public FileControl optional(boolean optional) {
        setOptional(optional);
        return this;
    }

    public int getFileSelectionMode() {
        return fileChooser.getFileSelectionMode();
    }
    
    public FileControl fileSelectionMode(int fileSelectionMode) {
        setFileSelectionMode(fileSelectionMode);
        return this;
    }

    public void setFileSelectionMode(int fileSelectionMode) {
        fileChooser.setFileSelectionMode(fileSelectionMode);
    }

    public FileFilter getFileFilter() {
        return fileChooser.getFileFilter();
    }

    public void setFileFilter(FileFilter fileFilter) {
        fileChooser.setFileFilter(fileFilter);
    }
    
    public void addChoosableFileFilter(FileFilter filter) {
        fileChooser.addChoosableFileFilter(filter);
    }
    
    
}
