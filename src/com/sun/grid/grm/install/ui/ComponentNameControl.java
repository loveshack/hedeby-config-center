/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerTask;
import com.sun.grid.grm.install.tasks.InstallDataListModel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Control for storing GrmNames of the components. The class is not made for 
 * instantiation.
 * @author Michal Bachorik
 */
public abstract class ComponentNameControl extends AbstractControl {
    private static final int DELAY = 500;
    private Action defaultAction;
    private JTextField textField;
    private JPanel buttonPanel, containerPanel;
    private JButton newButton;
    int buttonCount = 0;
    private boolean optional;
    private boolean valueChangedArmed;
    private DefaultListModel namesModel;
    private DefaultListModel persistedNamesModel;
    
    /*
     * Default constructor
     * @param task Enclosing wizard task
     * @param model Installer ('global') model
     * @parem dataKey Data key for storing the values in 'global' model
     * @listModel Private data model containing the component name values
     */
    public ComponentNameControl(InstallerTask task, InstallerModel model, String dataKey, DefaultListModel namesModel) {
        this(task, model, dataKey);
        this.namesModel = namesModel;
    }
    
    /*
     * Constructor
     * @param task Enclosing wizard task
     * @param model Installer ('global') model
     * @parem dataKey Data key for storing the values in 'global' model     
     */
    protected ComponentNameControl(InstallerTask task, InstallerModel model, String dataKey) {
        super(task, model, dataKey);
        this.namesModel = new InstallDataListModel(model, dataKey, ":");
        textField = new JTextField();
        textField.setEditable(false);
        textField.setMaximumSize(textField.getPreferredSize());
        buttonPanel = new JPanel(new GridBagLayout());
        buttonPanel.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
        
        containerPanel = new JPanel(new BorderLayout());
        containerPanel.add(textField, BorderLayout.NORTH);
        containerPanel.add(buttonPanel, BorderLayout.EAST);
        containerPanel.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
        setContent(containerPanel);
    }
    
    /*
     * Adds a non-default action with corresponding button and behavior (without
     * a need for the 'item' to be selected, action will be enabled even if 'item' - 
     * the text in textfield - is not selected).
     */
    public void addAction(Action newAction) {
        addAction(newAction, false, false);
    }
    
    /*
     * Adds an action with corresponding button and makes it the
     * default action. There is no need for the 'item' to be selected, action 
     * will be enabled even if 'item' - the text in textfield - is not selected).
     */
    public void addDefaultAction(Action newAction) {
        addAction(newAction, false, true);
    }
    
    /*
     * Adds an action with corresponding button and behavior
     */
    public void addAction(Action newAction, boolean requiresSelection, boolean isDefaultAction) {
        newButton = new JButton();
        buttonCount ++;
        newButton.setAction(newAction);
        newButton.setVisible(newAction != null);
        GridBagConstraints gbc = new GridBagConstraints();
        int border = 2;
        gbc.gridx = buttonCount;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.anchor = gbc.LAST_LINE_END;
        gbc.insets = new Insets(border,border,border,border);
        gbc.fill = gbc.VERTICAL;
        buttonPanel.add(newButton, gbc);
        if(isDefaultAction) {
            defaultAction = newAction;
        }
    }
    
    /*
     * Sets the text in the textfield
     * @param value new text value
     */
    public void setTextValue(String value) {
        textField.setText(value);
    }
    
    /*
     * Gets the text from textfield
     */    
    public String getTextValue() {
        return textField.getText();
    }
    
    /*
     * Default initialization of the control
     */
    public void init() {
        updateState();
    }
    
    /*
     * Updates the state of the control
     */
    public void updateState() {
        if(optional) {
            setState(Control.State.OK);
        } else if (textField.getText() == null) {
            setState(Control.State.ERROR);
        } else if (textField.getText().equals("")) {
            setState(Control.State.ERROR);
        } else {
            setState(Control.State.OK);
        }
    }
    
    /*
     * Sets the 'enabled' state of the control (all inner controls)
     */
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        for (Component c : containerPanel.getComponents()) {
            c.setEnabled(enabled);
        }
        for (Component c : buttonPanel.getComponents()) {
            c.setEnabled(enabled);
        }
    }
        
    /*
     * True if component is optional
     */
    public boolean isOptional() {
        return optional;
    }
    
    /*
     * Sets the control to be optional or not
     */
    public void setOptional(boolean optional) {
        this.optional = optional;
    }       
    
    /*
     * Gets the private data model
     */
    public DefaultListModel getListModel() {
        return this.namesModel;
    }        
}


