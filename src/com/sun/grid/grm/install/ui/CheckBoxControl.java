/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerModelListener;
import com.sun.grid.grm.install.InstallerTask;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JCheckBox;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * 
 */
public class CheckBoxControl extends AbstractControl implements FocusListener, ChangeListener, ActionListener, InstallerModelListener {
    
    private JCheckBox checkBox;
    
    /**
     * Creates a new instance of CheckBoxControl
     */
    public CheckBoxControl(InstallerTask task, InstallerModel model, String dataKey, String label) {
        super(task, model, dataKey);
        checkBox = new JCheckBox(task.rb().getString(label), getModel().getBoolean(dataKey));
        setContent(checkBox);
        checkBox.addActionListener(this);
        checkBox.addChangeListener(this);
        setState(State.OK);
    }
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        checkBox.setEnabled(enabled);
    }
    

    public void init() {
    }

    public void focusGained(FocusEvent e) {
    }

    public void focusLost(FocusEvent e) {
    }

    public void stateChanged(ChangeEvent e) {
        getModel().setData(getDataKey(), checkBox.isSelected());
        setState(getState());
    }
    
    public void actionPerformed(ActionEvent e) {
        
    }
    
    public void changedModel(InstallerModelEvent evt) {
        Object value = evt.getNewValue();
        boolean selected = false;
        if(value instanceof Boolean) {
            selected = ((Boolean)value).booleanValue();
        } else if(value != null) {
            selected = Boolean.parseBoolean(value.toString());
        }
        checkBox.setSelected(selected);
        setState(getState());
    }

}
