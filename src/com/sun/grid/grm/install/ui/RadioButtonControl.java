/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerModelListener;
import com.sun.grid.grm.install.InstallerTask;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * 
 */
public class RadioButtonControl extends AbstractControl implements ChangeListener, InstallerModelListener {
    
    private JRadioButton radioButton = new JRadioButton();
    private Object selectValue;
    /**
     * Creates a new instance of CheckBoxControl
     */
    
    public RadioButtonControl(InstallerTask task, InstallerModel model, String dataKey, Object value, String label) {
        super(task, model, dataKey);
        this.setSelectValue(value);
        setContent(radioButton);
        radioButton.addChangeListener(this);
        setStateLabelVisible(false);
        radioButton.setText(task.rb().getString(label, value.toString()));
        setState(State.OK);
    }

    public RadioButtonControl(InstallerTask task, InstallerModel model, String dataKey, Object value) {
        this(task, model, dataKey, value, value.toString());
    }
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        radioButton.setEnabled(enabled);
    }
    
    
    public void setSelected(boolean selected) {
        radioButton.setSelected(selected);
    }
    
    public boolean isSelected() {
        return radioButton.isSelected();
    }
    
    public void addToGroup(ButtonGroup group) {
        group.add(radioButton);
    }
    
    public void init() {       
        boolean selected = false;
        if(getSelectValue() == null ) {
            selected = getModel().getData(getDataKey()) == null;
        } else {
            selected = getSelectValue().equals(getModel().getData(getDataKey()));
        }
        radioButton.setSelected(selected);
        setState(getState());
    }
    
    public void stateChanged(ChangeEvent e) {
        if(radioButton.isSelected()) {
            getModel().setData(getDataKey(), getSelectValue());
            setState(getState());
        }
    }
    
    public void changedModel(InstallerModelEvent evt) {
        radioButton.setSelected(getSelectValue().equals(getModel().getData(getDataKey())));
        setState(getState());
    }

    public Object getSelectValue() {
        return selectValue;
    }

    public void setSelectValue(Object selectValue) {
        this.selectValue = selectValue;
    }

}
