/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerTask;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import javax.swing.JCheckBox;

/**
 *
 */
public class CheckBoxListControl extends AbstractControl implements ActionListener, CheckBoxListControlModelListener {
    
    private JCheckBox checkbox = new JCheckBox();
    private String selectValue;
    private CheckBoxListControlModel listModel;
    
    /**
     * Creates a new instance of CheckBoxControl
     */
    
    public CheckBoxListControl(InstallerTask task, CheckBoxListControlModel listModel, String value, String label) {
        super(task, listModel.getModel(), listModel.getDataKey());
        this.setSelectValue(value);
        setContent(checkbox);
        checkbox.addActionListener(this);
        setStateLabelVisible(false);
        checkbox.setText(task.rb().getString(label, value.toString()));
        setListModel(listModel);
        setState(Control.State.OK);
    }

    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        checkbox.setEnabled(enabled);
    }
    
    
    public void setSelected(boolean selected) {
        checkbox.setSelected(selected);
    }
    
    public boolean isSelected() {
        return checkbox.isSelected();
    }
    
    public void init() {       
        setSelected(getListModel().contains(selectValue));
    }
    
    public void actionPerformed(ActionEvent e) {
        if(checkbox.isSelected()) {
            getListModel().add(selectValue);
        } else {
            getListModel().remove(selectValue);
        }
    }
    
    public String getSelectValue() {
        return selectValue;
    }

    public void setSelectValue(String selectValue) {
        this.selectValue = selectValue;
    }

    public CheckBoxListControlModel getListModel() {
        return listModel;
    }

    public void setListModel(CheckBoxListControlModel listModel) {
        if(this.listModel != null) {
            this.listModel.removeCheckBoxListControlModelListener(this);
        }
        this.listModel = listModel;
        this.listModel.addCheckBoxListControlModelListener(this);
    }

    public void changedModel(InstallerModelEvent evt) {
        // Ingore it
    }

    public void added(CheckBoxListControlModel model, Collection<String> names) {
        checkbox.setSelected(getListModel().contains(selectValue));
    }

    public void removed(CheckBoxListControlModel model, Collection<String> names) {
        checkbox.setSelected(getListModel().contains(selectValue));
    }

}
