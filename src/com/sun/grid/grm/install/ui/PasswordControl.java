/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerModelListener;
import com.sun.grid.grm.install.InstallerTask;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 */
public class PasswordControl extends AbstractControl implements ActionListener, DocumentListener {
    
    private static final int DELAY = 500;
    
    private static Logger log = Logger.getLogger(FileControl.class.getName());
    private JPasswordField textField = new JPasswordField();
    private String sysName;
    private Timer timer = new Timer(DELAY, this);
    
    /**
     * Creates a new instance of TextControl
     */
    public PasswordControl(InstallerTask task, InstallerModel model, String dataKey, int columns) {
        this(task, model, dataKey);
        textField.setColumns(columns);
        textField.setMaximumSize(textField.getPreferredSize());
    }
    
    public PasswordControl(InstallerTask task, InstallerModel model, String dataKey) {
        super(task, model, dataKey);
        setContent(textField);
        textField.getDocument().addDocumentListener(this);
        timer.setRepeats(true);
        timer.setInitialDelay(DELAY);
    }
    
    
    protected JTextField getTextField() {
        return textField;
    }
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        textField.setEditable(enabled);
    }
    public void init() {
        char [] value = (char[])getModel().getTmpData(getDataKey());
        if(value != null) {
            textField.setText(new String(value));
        }
    }
    
    private boolean comesFromTextField;
    private int minLen = 1;
    private int maxLen = Integer.MAX_VALUE;
    
    public PasswordControl textLen(int min, int max) {
        maxLen = max;
        minLen = min;
        return this;
    }
    
    
    
    long modifySequence;
    long lastModifySequence;
    
    private void modified() {
        modifySequence++;
        if(!timer.isRunning()) {
            timer.start();
        }
    }
    
    public void insertUpdate(DocumentEvent e) {
        modified();
    }
    
    public void removeUpdate(DocumentEvent e) {
        modified();
    }
    
    public void changedUpdate(DocumentEvent e) {
        modified();
    }
    
    public void actionPerformed(ActionEvent e) {
        long diff = modifySequence - lastModifySequence;
        if(lastModifySequence >= 0 && diff <= 1) {
            timer.stop();
            try {
                comesFromTextField = true;
                getModel().setTmpData(getDataKey(), textField.getPassword());
            } finally {
                comesFromTextField = false;
            }
            modifySequence = 0;
            lastModifySequence = -1;
        } else {
            lastModifySequence = modifySequence;
        }
    }

    public void changedModel(InstallerModelEvent evt) {
    }
}
