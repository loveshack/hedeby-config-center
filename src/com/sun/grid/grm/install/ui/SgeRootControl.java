/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerTask;
import java.io.File;
import javax.swing.JFileChooser;

/**
 * This file control is designed to select the SGE_ROOT directory
 * It is only in state <code>OK</code> if the selected directory the file
 * lib/juti.jar and lib/jgdi.jar exists.
 */
public class SgeRootControl extends FileControl {
    
    /**
     * Create a new SgeRootControl
     * @param task      the installer task
     * @param model     the installer model
     * @param dataKey   the datakey for the SGE_ROOT values is stored
     */
    public SgeRootControl(InstallerTask task, InstallerModel model, String dataKey) {        
        super(task, model, dataKey);
        setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }
    
    @Override
    protected Control.State getStateForValue(String value) {
        
        Control.State ret = super.getStateForValue(value);
        if(ret.equals(Control.State.OK)) {
            if(isOptional() && (value == null || value.length() == 0)) {
                return Control.State.OK;
            } else {
                File file = new File(value);

                String jutiFileStr = "lib" + File.separatorChar + "juti.jar";
                File jutiJar = new File(file, jutiFileStr);

                if (!jutiJar.exists()) {
                    setStateMessage(rb().getFormattedString("SgeRootControl.fileNotFound", jutiFileStr, file));
                    return Control.State.ERROR;
                }

                String jgdiFileStr = "lib" + File.separatorChar + "jgdi.jar";
                File jgdiFile = new File(file, jgdiFileStr);
                if (!jgdiFile.exists()) {
                    setStateMessage(rb().getFormattedString("SgeRootControl.fileNotFound", jgdiFileStr, file));
                    return Control.State.ERROR;
                }
                setStateMessage(null);
                return Control.State.OK;
            }
        } else {
            return ret;
        }
    }
    
    
}
