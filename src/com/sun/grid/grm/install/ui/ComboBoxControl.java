/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerModelListener;
import com.sun.grid.grm.install.InstallerTask;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * 
 */
public class ComboBoxControl extends AbstractControl implements ActionListener, InstallerModelListener {
    
    private JComboBox comboBox = new JComboBox();
    

    /** Creates a new instance of ComboBoxControl */
    public ComboBoxControl(InstallerTask task, InstallerModel model, String dataKey) {
        super(task, model, dataKey);
        setContent(comboBox);
        comboBox.addActionListener(this);
        setState(State.ERROR);
    }
    
    public ComboBoxControl(InstallerTask task, InstallerModel model, String dataKey, Object[] items) {
        this(task, model, dataKey);
        comboBox.setModel(new DefaultComboBoxModel(items));
    }
    
    public ComboBoxControl(InstallerTask task, InstallerModel model, String dataKey, Object[] items, String [] labels) {
        this(task, model, dataKey);                
        comboBox.setModel(new DefaultComboBoxModel(items));
        comboBox.setRenderer(new LabeledListCellRenderer(items, labels));
    }
    
    private static class LabeledListCellRenderer extends DefaultListCellRenderer {
        
        private Map<Object, String> labelMap = new HashMap<Object, String>();
        
        LabeledListCellRenderer(Object [] items, String [] labels) {
            for(int i = 0; i < items.length; i++) {
                labelMap.put(items[i], labels[i]);
            }
        }
        
        public Component getListCellRendererComponent(JList list,
                Object value,
                int index,
                boolean isSelected,
                boolean cellHasFocus) {
            
            String label = labelMap.get(value);
            
            if(label == null) {
                label = value == null ? "null" : value.toString();
            }
            return super.getListCellRendererComponent(list, label, index, isSelected, cellHasFocus);
        }
        
    }
    
    
    public void init() {
        getComboBox().getModel().setSelectedItem(getModel().getData(getDataKey()));
        updateState();
    }

    private void updateState() {
        String value = (String) getModel().getData(getDataKey());
        if(value == null || value.length() == 0) {
            setState(State.ERROR);
        } else {
            setState(State.OK);
        }
    }
            
    public void actionPerformed(ActionEvent e) {
        setState(getState());        
        Object value = getComboBox().getModel().getSelectedItem();
        getModel().setData(getDataKey(), value);
        updateState();
    }
    
    public JComboBox getComboBox() {
        return comboBox;
    }

    public void changedModel(InstallerModelEvent evt) {
        getComboBox().getModel().setSelectedItem(evt.getNewValue());
        updateState();
    }

    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        comboBox.setEnabled(enabled);
    }

    public void setEditable(boolean enabled) {
        comboBox.setEditable(enabled);
    }
}
