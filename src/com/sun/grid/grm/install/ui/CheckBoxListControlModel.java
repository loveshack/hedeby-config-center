/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerModelListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

/**
 *
 */
public class CheckBoxListControlModel implements InstallerModelListener {
    
    private InstallerModel model;
    private String dataKey;
    private Set<String> elems = new HashSet<String>();
    
    private String delimiter;
    private List<CheckBoxListControlModelListener> listeners = new LinkedList<CheckBoxListControlModelListener>();
    
    /** Creates a new instance of CheckBoxListControlModel */
    public CheckBoxListControlModel(InstallerModel model, String datakey) {
        this(model, datakey, "|");
    }
    
    public CheckBoxListControlModel(InstallerModel model, String dataKey, String delimiter) {
        setDelimiter(delimiter);
        setDataKey(dataKey);
        setModel(model);
        load();
    }
    
    public void addCheckBoxListControlModelListener(CheckBoxListControlModelListener lis) {
        synchronized(listeners) {
            listeners.add(lis);
        }
    }
    
    public void removeCheckBoxListControlModelListener(CheckBoxListControlModelListener lis) {
        synchronized(listeners) {
            listeners.remove(lis);
        }
    }
    
    protected void fireAdded(Collection<String> names) {
        List<CheckBoxListControlModelListener> listeners = null;
        
        synchronized(this.listeners) {
            listeners = new ArrayList<CheckBoxListControlModelListener>(this.listeners);
        }
        
        for(CheckBoxListControlModelListener lis: listeners) {
            lis.added(this, names);
        }
    }

    protected void fireRemoved(Collection<String> names) {
        List<CheckBoxListControlModelListener> listeners = null;
        
        synchronized(this.listeners) {
            listeners = new ArrayList<CheckBoxListControlModelListener>(this.listeners);
        }
        
        for(CheckBoxListControlModelListener lis: listeners) {
            lis.removed(this, names);
        }
    }
    
    public void load() {
        
        Set<String> removedElems = elems;
        Set<String> addedElems = new HashSet<String>();
        elems = new HashSet<String>();
        
        String str = (String)getModel().getData(getDataKey());
        if(str != null) {
            StringTokenizer st = new StringTokenizer(str, getDelimiter());
            while(st.hasMoreTokens()) {
                String elem = st.nextToken();
                elems.add(elem);
                if(!removedElems.remove(elem)) {
                    addedElems.add(elem);
                }
            }
        }
        fireRemoved(removedElems);
        fireAdded(addedElems);
    }
    
    private boolean saveArmed;
    private void save() {
        StringBuilder buf = new StringBuilder();
        for(String str: elems) {
            if(buf.length() > 0) {
                buf.append(getDelimiter());
            }
            buf.append(str);
        }
        try {
            saveArmed = true;
            getModel().setData(getDataKey(), buf.toString());            
        } finally {
            saveArmed = false;
        }
    }
    
    public void add(String value) {
        int oldSize = elems.size();
        if( elems.add(value)) {
            save();
            fireAdded(Collections.singleton(value));
        }
    }
    
    public void remove(String value) {
        if(elems.remove(value)) {
            save();
            fireRemoved(Collections.singleton(value));
        }
    }
    
    public boolean contains(String value) {
        return elems.contains(value);
    }
    
    public void clear() {
        Set<String> removedElems = elems;
        this.elems = new HashSet<String>();
        save();
        fireRemoved(removedElems);
    }
    
    public Set<String> values() {
        return Collections.unmodifiableSet(elems);
    }
    
    public int size() {
        return elems.size();
    }

    public InstallerModel getModel() {
        return model;
    }
    
    public void setModel(InstallerModel model) {
        if(this.model != null) {
            this.model.removeModelEventListener(this);
        }
        this.model = model;
        this.model.addModelEventListener(this, dataKey);
    }

    public void setDataKey(String dataKey) {
        this.dataKey = dataKey;
        if(model != null) {
           model.removeModelEventListener(this);
           model.addModelEventListener(this, dataKey);
        }
    }
    
    public String getDataKey() {
        return dataKey;
    }

    public String getDelimiter() {
        return delimiter;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    public void changedModel(InstallerModelEvent evt) {
        if(!saveArmed) {
            load();
        }
    }
    
}
