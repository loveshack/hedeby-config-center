/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.install.ui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;


/**
 *
 * 
 */

public class GrmPasswordDialog extends JDialog implements ActionListener {
    
    private boolean id = false;
    private JTextField username;
    private JPasswordField password;
    private JFrame frame;
    private JButton ok;
    private JButton can;
    
    public GrmPasswordDialog(JFrame frame){
        super(frame, "Welcome", true);
        this.frame = frame;
        setLayout(new BorderLayout());
        
        JPanel centerPane = new JPanel(new GridBagLayout());
        
        GridBagConstraints gbc = new GridBagConstraints();
        
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;

        centerPane.add(new Label("User :"), gbc);
        
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;

        username = new JTextField(15);
        centerPane.add(username, gbc);
        
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.anchor = gbc.EAST;
        gbc.fill = gbc.NONE;

        centerPane.add(new Label("Password :"), gbc);
        
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;

        password = new JPasswordField(15);
        centerPane.add(password, gbc);
        
        centerPane.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        
        getContentPane().add(centerPane, BorderLayout.CENTER);
        
        JPanel buttonPane = new JPanel(new GridBagLayout());

        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        gbc.insets = new Insets(2,20, 2, 20);
        
        ok = new JButton("OK");
        ok.addActionListener(this);
        ok.setDefaultCapable(true);
        
        buttonPane.add(ok, gbc);
        
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.anchor = gbc.WEST;
        gbc.fill = gbc.NONE;
        gbc.insets = new Insets(2,20, 2, 20);
        
        can = new JButton("Cancel");
        can.addActionListener(this);
        can.setDefaultCapable(false);
        
        buttonPane.add(can, gbc);
        
        buttonPane.setBorder(BorderFactory.createEmptyBorder(5, 20, 5, 20));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);
        
        getRootPane().setDefaultButton(ok);

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        pack();
    }
    
    public String getUsername() {
        if(id) {
            return username.getText();
        } else {
            return null;
        }
    }
    
    public char[] getPassword() {
        if(id) {
            return password.getPassword();
        } else {
            return null;
        }
    }
    
    public void actionPerformed(ActionEvent ae){
        if(ae.getSource() == ok) {
            id = true;
            setVisible(false);
        } else if(ae.getSource() == can) {
            id = false;
            setVisible(false);
        }
    }
    
    public boolean showDialog() {        
        setVisible(true);
        return id;        
    }

    private boolean wasAlreadyVisible;
    
    public void setVisible(boolean visible) {
        if(visible) {
            if(!wasAlreadyVisible) {
                if(frame != null) {
                    setLocationRelativeTo(frame);
                }
                wasAlreadyVisible = true;
            }
            id = false;
        }
        super.setVisible(visible);
    }
    
    public static void main(String [] args) {
        
        JFrame frame = new JFrame("test");
        
        frame.setSize(100, 100);
        frame.setLocation(100, 100);
        frame.setVisible(true);
        
        GrmPasswordDialog dlg = new GrmPasswordDialog(frame);
        dlg.showDialog();
        System.exit(0);
    }
}
