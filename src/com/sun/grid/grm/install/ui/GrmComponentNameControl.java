/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.bootstrap.GrmName;
import com.sun.grid.grm.bootstrap.InvalidGrmNameException;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.install.DefaultInstallerModel;
import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerTask;
import com.sun.grid.grm.install.tasks.ComponentListModel;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 * Control for storing unique GrmNames of the components.
 * @author Michal Bachorik
 */
public class GrmComponentNameControl extends ComponentNameControl{
    private String key;
    private Action add, edit, remove;
    
    /*
     * Default constructor
     * @param task Enclosing wizard task
     * @param model Installer ('global') model
     * @parem dataKey Data key for storing the values in 'global' model
     * @listModel Private data model containing the component name values
     * @sc System configuration to get already used component names
     */
    public GrmComponentNameControl(InstallerTask task, InstallerModel model, String dataKey, DefaultListModel listModel,SystemConfiguration sc) {
        super(task, model, dataKey, listModel);
        if (listModel instanceof ComponentListModel) {
            ((ComponentListModel)listModel).load(sc);
        }
        key = dataKey;
        add = new AddComponentAction();
        edit = new EditComponentAction();
        remove = new RemoveComponentAction();
        addAction(add);
        addAction(edit);
        addAction(remove); 
        init();
    }
    
    /*
     * Private action class for 'add' button and corresponding functionality
     */
    private class AddComponentAction extends AbstractAction {
        
        /*
         * Constructor
         */
        public AddComponentAction() {
            super("add");
        }
        
        /*
         * Occurs when action was performed (button was pressed). Underlying
         * functionality checks whether it is possible to use new component name,
         * if not warning message will be displayed.
         * @param e ActionEvent instance containg the event details
         */
        public void actionPerformed(ActionEvent e) {
            String name = JOptionPane.showInputDialog(getController().getFrame(),
                    rb().getString("GrmComponentNameControl.ComponentName"),
                    rb().getString("GrmComponentNameControl.DefaultName"));
            setComponentName(name);
        }
    }
    
    /*
     * Private action class for 'remove' button and corresponding functionality
     */
    private class RemoveComponentAction extends AbstractAction {
        /*
         * Constructor
         */
        public RemoveComponentAction() {
            super("remove");
        }
        
        /*
         * Occurs when action was performed (button was pressed). Underlying
         * functionality checks whether it is possible to remove the component name,
         * if not warning message will be displayed.
         * @param e ActionEvent instance containg the event details
         */
        public void actionPerformed(ActionEvent e) {
            String name = getTextValue();
            removeComponentName(name);
        }
    }
    
    
    /*
     * Private action class for 'remove' button and corresponding functionality
     */
    private class EditComponentAction extends AbstractAction {
        /*
         * Constructor
         */
        public EditComponentAction() {
            super("edit");
        }
        
        /*
         * Occurs when action was performed (button was pressed). Underlying
         * functionality checks whether it is possible to change the component name,
         * if not warning message will be displayed.
         * @param e ActionEvent instance containg the event details
         */
        public void actionPerformed(ActionEvent e) {
            String oldName = getTextValue();
            String newName = JOptionPane.showInputDialog(getController().getFrame(),
                    rb().getString("GrmComponentNameControl.ComponentName"),
                    rb().getString("GrmComponentNameControl.DefaultName"));
            changeComponentName(oldName, newName);
        }
    }
    
    /*
     * Updates state of the control. If component name is not valid
     * warning sign is displayed next to control.
     */
    public void updateState() {        
        String name = getTextValue();
        if (getState() == null) {
            setState(Control.State.ERROR);
        }
        try {
            GrmName.isValid(name);            
        } catch (InvalidGrmNameException ex) {
            setState(Control.State.ERROR);
            setStateMessage(rb().getFormattedString("GrmComponentNameControl.NameInvalid", name));
        }
        if (getState().equals(Control.State.OK)) {
            enableEditRemoveActions(true);
        }
        if (getState().equals(Control.State.ERROR)) {
            enableEditRemoveActions(false);            
        }
    }
    
    /*
     * Sets the 'enabled' state of the inner controls (textfield and buttons).
     */
    private void enableEditRemoveActions(boolean isFilledOut) {
        add.setEnabled(!isFilledOut);
        edit.setEnabled(isFilledOut);
        remove.setEnabled(isFilledOut);
    }
    
    /*
     * Gets a string representation of all component names stored in the private
     * data model.
     */
    private String getUsedComponentNames() {
        StringBuffer s = new StringBuffer();
        for (int i=0; i<getListModel().getSize();i++) {
            s.append((String)getListModel().getElementAt(i));
            s.append(", ");
        }
        s.delete(s.length()-2, s.length());
        return new String(s);
    }
    
    /*
     * Checks the name for validity and if it is OK sets it to textfield. If not,
     * warning message is displayed and value is erased from global model.
     * @param name component name
     */
    private void setComponentName(String name) {
        try {
            GrmName.isValid(name);
            setTextValue(name);
            if (!getListModel().contains(name)) {
                getListModel().addElement(name);
                ((DefaultInstallerModel)getModel()).setData(key, name);
                setState(Control.State.OK);
            } else {
                setStateMessage(rb().getFormattedString("GrmComponentNameControl.NameUsed", name, getUsedComponentNames()));
                setState(Control.State.ERROR);                
            }
        } catch (InvalidGrmNameException ex) {
            setStateMessage(rb().getFormattedString("GrmComponentNameControl.NameInvalid", name));
            setState(Control.State.ERROR);
        }
        updateState();
    }
    
    
    /*
     * Checks the new name for validity. If it is OK sets it to textfield and removes
     * old name. If not, warning message is displayed.
     * @param oldName name that should be changed
     * @param newName new component name
     */
    private void changeComponentName(String oldName, String newName) {
        try {
            GrmName.isValid(newName);
            if (!oldName.equals(newName)) {
                if (getListModel().contains(oldName)) {
                    setTextValue(newName);
                    if (!getListModel().contains(newName)) {
                        getListModel().removeElement(oldName);
                        getListModel().addElement(newName);
                        ((DefaultInstallerModel)getModel()).setData(key, newName);
                        setState(Control.State.OK);
                    } else {
                        setStateMessage(rb().getFormattedString("GrmComponentNameControl.NameUsed", newName, getUsedComponentNames()));
                        setState(Control.State.ERROR);                        
                    }
                } else {
                    log.warning(rb().getFormattedString("GrmComponentNameControl.NameNotFound", oldName, getUsedComponentNames()));
                }
            } else {
                log.warning(rb().getFormattedString("GrmComponentNameControl.NameTheSame", newName, oldName));
            }
        } catch (InvalidGrmNameException ex) {
            setStateMessage(rb().getFormattedString("GrmComponentNameControl.NameInvalid", newName));
            setState(Control.State.ERROR);
        }
        updateState();
    }
    
   /*
    * Removes the component name. If name is not found (should be impossible to
    * happen) warning message is displayed.
    * @param name name that should be removed
    */
    private void removeComponentName(String name) {
        try {
            GrmName.isValid(name);
            String newName = "";
            if (getListModel().contains(name)) {
                setTextValue(newName);
                getListModel().removeElement(name);
                ((DefaultInstallerModel)getModel()).setData(key, newName);
            } else {
                log.warning(rb().getFormattedString("GrmComponentNameControl.NameNotFound", name, getUsedComponentNames()));
            }
        } catch (InvalidGrmNameException ex) {
            log.warning(rb().getFormattedString("GrmComponentNameControl.NameInvalid", name));
        }
        updateState();
    }
    
    /*
     * Populates textfield with value (if it is stored in 'global' model) and
     * updates the state of the control.
     */
    public void init() {
        super.init();
        Object nameValue = getModel().getData(key);
        if ((nameValue == null)||(nameValue.equals(""))) {
            setStateMessage(rb().getFormattedString("GrmComponentNameControl.NameInvalid", nameValue));
            setState(Control.State.ERROR);
        } else {
            String name = (String) nameValue;
            if (!name.equals(getTextValue())) {
                setComponentName(name);
            } else {
                // clear block is intended - we are trying to set a name which
                // was stored to 'used names list' in this task, so we supress
                // uniqueness checking
            }
        }
        updateState();
    }
    
    /*
     * Updates component name if related value in global data model was changed.
     */
    public final void changedModel(InstallerModelEvent evt) {
        String newName = (String)evt.getNewValue();
        String oldName = (String)evt.getOldValue();
        changeComponentName(oldName, newName);
    }
}


