/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerModelListener;
import com.sun.grid.grm.install.InstallerTask;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * 
 */
public class ListControl extends AbstractControl implements ListSelectionListener, InstallerModelListener {
    
    private JList list;
    private JPanel buttonPanel;
    private JButton newButton;
//    private JButton editButton = new JButton();
//    private JButton addButton = new JButton();
//    private JButton removeButton = new JButton();
    int buttonCount = 0;
    
    private boolean optional;
    
    /** Creates a new instance of ComboBoxControl */
    public ListControl(InstallerTask task, InstallerModel model, String dataKey) {
        super(task, model, dataKey);
        
        
        list = new JList();
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane sc = new JScrollPane(list);
        
        buttonPanel = new JPanel(new GridBagLayout());

        buttonPanel.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
        
        JPanel p = new JPanel(new BorderLayout());
        p.add(sc, BorderLayout.CENTER);
        p.add(buttonPanel, BorderLayout.EAST);
        p.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
        setContent(p);
        list.addListSelectionListener(this);
        list.addMouseListener(new MyMouseListener());
        
        setVisibleRowCount(5);
    }
    
    public ListControl(InstallerTask task, InstallerModel model, String dataKey, Object[] items) {
        this(task, model, dataKey);
        setItems(items);
    }
    
    public ListControl(InstallerTask task, InstallerModel model, String dataKey, ListModel listModel) {
        this(task, model, dataKey);
        list.setModel(listModel);
    }
    
    public void setVisibleRowCount(int rows) {
        list.setVisibleRowCount(rows);
    }
    
    public int getVisibleRowCount() {
        return list.getVisibleRowCount();
    }
    
    private List<Action> requiresSelectionActionList = new LinkedList<Action>();
    private Action defaultAction;
    
    public void addAction(Action newAction) {
        addAction(newAction, false, false);
    }
    
    public void addDefaultAction(Action newAction) {
        addAction(newAction, false, true);
    }
    
    public void addAction(Action newAction, boolean requiresSelection, boolean isDefaultAction) {
        newButton = new JButton();
        buttonCount ++;
        newButton.setAction(newAction);
        newButton.setVisible(newAction != null);
        GridBagConstraints gbc = new GridBagConstraints();
        int border = 2;
        gbc.gridx = 0;
        gbc.gridy = buttonCount;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.anchor = gbc.CENTER;
        gbc.insets = new Insets(border,border,border,border);
        gbc.fill = gbc.HORIZONTAL;
        buttonPanel.add(newButton, gbc);
        if(requiresSelection) {
            requiresSelectionActionList.add(newAction);
            newAction.setEnabled(list.getSelectedValue() != null);
        }
        if(isDefaultAction) {
            defaultAction = newAction;
        }
    }
    
    
        
    public void setItems(Object [] items) {
        DefaultListModel model = new DefaultListModel();
        for(Object item: items) {
            model.addElement(item);
        }
        list.setModel(model);
    }
    
    
    public void setListModel(ListModel model) {
        list.setModel(model);
    }
    
    public void setItems(Collection<? extends Object> items) {
        DefaultListModel model = new DefaultListModel();
        for(Object item: items) {
            model.addElement(item);
        }
        list.setModel(model);
    }
    
    public void init() {
        list.setSelectedValue(getModel().getData(getDataKey()), true);
        updateState();
    }
    
    public void updateState() {
        if(optional) {
            setState(Control.State.OK);
        } else if (list.getSelectedValue() != null) {
            setState(Control.State.OK);
        } else {
            setState(Control.State.ERROR);
        }
    }
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        list.setEnabled(enabled);
    }
    
    
    public JList getList() {
        return list;
    }
    
    public ListModel getListModel() {
        return list.getModel();
    }
    
    public void changedModel(InstallerModelEvent evt) {
        if(!valueChangedArmed) {
            list.setSelectedValue(evt.getNewValue(), true);
            updateState();
        }
    }
    
    private boolean valueChangedArmed;
    public void valueChanged(ListSelectionEvent e) {
        if(!e.getValueIsAdjusting() && list.getSelectedValue() != null) {
            valueChangedArmed = true;
            try {
                getModel().setData(getDataKey(), getList().getSelectedValue());
                updateState();
                for(Action action: requiresSelectionActionList) {
                    action.setEnabled(true);
                }
            } finally {
                valueChangedArmed = false;
            }
        } else {
            for(Action action: requiresSelectionActionList) {
                action.setEnabled(false);
            }
        }
    }
    
    class MyMouseListener extends MouseAdapter {
        public void mouseClicked(MouseEvent e) {
            if(e.getClickCount() > 1 ) {
                if(defaultAction != null && defaultAction.isEnabled()) {
                    defaultAction.actionPerformed(new ActionEvent(this, 0, "double click"));
                }
            }
        }
        
    }

    public boolean isOptional() {
        return optional;
    }

    public void setOptional(boolean optional) {
        this.optional = optional;
    }
    
}
