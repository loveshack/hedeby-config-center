/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.ExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.PathConfiguration;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerTask;
import com.sun.grid.grm.install.security.RemoteSecurityInstaller;
import com.sun.grid.grm.install.security.ExecutionEnvProvider;
import com.sun.grid.grm.install.tasks.ExtendedAbstractInstallerTask;
import com.sun.grid.grm.security.ClientSecurityContext;
import com.sun.grid.grm.security.SecurityBootstrap;
import com.sun.grid.grm.security.SecurityFactory;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.logging.Level;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/**
 * Control which fetchs the keystore of a user from the 
 * Hedeby CA component
 */
public class KeystoreControl extends AbstractControl {
    
    private JButton theButton = new JButton();
    private JTextArea certTextArea = new JTextArea(5,30);
    private JScrollPane certScrollPane = new JScrollPane(certTextArea);
    
    private String usernameKey;
    private String passwordKey;
    
    /**
     * Create a new KeystoreControl
     * @param task         task which uses this control
     * @param model        the installer model
     * @param dataKey      key where the keystore is stored in the model
     * @param usernameKey  key with the username
     * @param passwordKey  key with the password
     */
    public KeystoreControl(ExtendedAbstractInstallerTask task, String dataKey, String usernameKey, String passwordKey) {
        super(task, task.getModel(), dataKey);
        
        certTextArea.setEditable(false);
        certTextArea.setOpaque(false);
        certTextArea.setLineWrap(true);
        this.usernameKey = usernameKey;
        this.passwordKey = passwordKey;
        
        JPanel p = new JPanel(new BorderLayout());
        p.add(theButton, BorderLayout.NORTH);
        p.add(certScrollPane, BorderLayout.CENTER);
        
        setContent(p);
        
        theButton.setAction(new FetchKeystoreAction());
        
        getModel().addModelEventListener(this, getBytesKey());
        getModel().addModelEventListener(this, usernameKey);
        getModel().addModelEventListener(this, passwordKey);
        
    }
    
    /**
     * Enabled/Disable this control
     * @param enabled the enable/disable flag
     */
    public void setEnabled(boolean enabled) {
        theButton.setEnabled(enabled);
    }
    
    /**
     * Reads the keystore from the model
     *
     * @throws com.sun.grid.grm.install.InstallerException 
     */
    public void init() throws InstallerException {
        initButtonState();
        readKeyStoreFromModel();
    }
    
    /**
     * Get the key under which the bytes of the keystore are stored
     *
     * @return the key under which the bytes of the keystore are stored 
     */
    private String getBytesKey() {
        return ExecutionEnvProvider.getBytesKey(getDataKey());
    }
    
    
    private KeyStore fetchKeyStore() throws InstallerException {
        
        SystemConfiguration sc = ((ExtendedAbstractInstallerTask)getTask()).getSystemConfiguration();
        
        try {
            CallbackHandler callbackHandler = new CallbackHandler() {
                
                public void handle(Callback[] callbacks) throws IOException,UnsupportedCallbackException {
                    
                    for(int i = 0; i < callbacks.length; i++) {
                        if(callbacks[i] instanceof NameCallback) {
                            ((NameCallback)callbacks[i]).setName((String)getModel().getData(usernameKey));
                        } else if (callbacks[i] instanceof PasswordCallback) {
                            Object obj = getModel().getTmpData(passwordKey);
                            char [] pw = null;
                            if(obj instanceof String) {
                                pw = ((String)obj).toCharArray();
                            } else if (obj instanceof char[]) {
                                pw = (char[])obj;
                            } else if ( obj == null) {
                                pw = new char[0];
                            } else {
                                throw new IOException("Don't known how to handle a credential of type " + obj.getClass() );
                            }
                            ((PasswordCallback)callbacks[i]).setPassword(pw);
                        } else {
                            throw new UnsupportedCallbackException(callbacks[i]);
                        }
                    }
                }
            };
            
            String username = getUsername();
            
            PathConfiguration pc = sc.getPathConfiguration();
            ExecutionEnv env = ExecutionEnvFactory.newClientInstance(sc.getSystemName().getName(),
                                                                     pc.getSharedPath(),
                                                                     pc.getLocalSpoolPath(),
                                                                     pc.getDistPath(),
                                                                     "csHost",
                                                                     5000,
                                                                     Collections.<String,Object>emptyMap(),
                                                                     username,
                                                                     callbackHandler); 
            
            RemoteSecurityInstaller rsi = new RemoteSecurityInstaller(env);

            rsi.connect(env);

            KeyStore keystore = rsi.getCA().createKeyStore(username, new char[0], new char[0]);

            if(keystore == null) {
                throw new InstallerException("ca component did not return a keystore for user " + username);
            }

            return keystore;
        } catch (Exception ex) {
            throw new InstallerException("Fetching keystore failed: " + ex.getLocalizedMessage(), ex);
        }
        
    }
     
    
    private void readKeyStoreFromModel() {
        try {
            KeyStore ks = ExecutionEnvProvider.getKeyStoreFromModel(getModel(), getDataKey());
            if(ks == null) {
                certTextArea.setText("");
            } else {
                X509Certificate cert = (X509Certificate)ks.getCertificate(getUsername());
                
                certTextArea.setText(cert.toString());
                
                // Scroll to the first line
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        try {
                            certTextArea.scrollRectToVisible(certTextArea.modelToView(0));
                        } catch(Exception e) {
                            // Ignore
                        }
                    }
                });
            }
        } catch(Exception ex) {
            log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
    }
    
    private void initButtonState() {
        String username = (String)getModel().getData(usernameKey);
        setEnabled(username != null && username.length() > 0);
    }
    
    /* 
     *   Update the gui elements of the model has changed
     */
    public void changedModel(InstallerModelEvent evt) {
        
        if(evt.getKey().equals(usernameKey)) {
            initButtonState();
            certTextArea.setText("");
        } else if (evt.getKey().equals(passwordKey)) {
            certTextArea.setText("");
        } else if (evt.getKey().equals(getBytesKey())) {
            readKeyStoreFromModel();
        }
    }
    
    private String getUsername() {
        return (String)getModel().getData(usernameKey);
    }
    
    
    
    private class FetchKeystoreAction extends AbstractAction {
        
        public FetchKeystoreAction() {
            super("Get Keystore");
        }
        
        public void actionPerformed(ActionEvent e) {
            
            try {
                certTextArea.setText("");
                KeyStore ks = fetchKeyStore();
                ExecutionEnvProvider.putKeyStoreToModel(getModel(), getDataKey(), ks);
            } catch(Exception ex) {
                log.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            }
            
        }
    }
}
