/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.install.Audience;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelListener;
import com.sun.grid.grm.install.InstallerTask;
import com.sun.grid.grm.install.RB;
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 */
public abstract class AbstractControl extends JPanel implements Control, InstallerModelListener {
    
    protected Logger log = Logger.getLogger(getClass().getName());
    private Control.State state;
    private Audience  audience = Audience.NOVICE;
    private InstallerTask task;
    private InstallerModel model;
    private JLabel stateLabel = new JLabel();
    private Box    contentBox = Box.createHorizontalBox();
    private List<ControlStateChangedListener> listeners = new LinkedList<ControlStateChangedListener>();
    private String dataKey;
    private Component indent;
    
    /**
     * Creates a new instance of AbstractControl
     */
    public AbstractControl(InstallerTask task, InstallerModel model, String dataKey) {
        this.task = task;
        this.model = model;
        BoxLayout bl = new BoxLayout(this, BoxLayout.X_AXIS);
        setLayout(bl);
        indent = Box.createHorizontalStrut(0);
        add(indent);
        add(contentBox);
        add(Box.createHorizontalGlue());
        add(stateLabel,BorderLayout.EAST);        
        stateLabel.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));
        this.dataKey = dataKey;
        
        Icon icon = task.getController().getFrame().rb().getImage("warning.icon");
        if(icon != null) {
            add(Box.createVerticalStrut(icon.getIconHeight()));
        }
    }
    
    public RB rb() {
        return RB.getInstance(getClass());
    }
    
    private int indentWidth = 0;
    public void setIndent(int width) {
        remove(indent);
        indent = Box.createHorizontalStrut(width);
        indentWidth = width;
        add(indent,0);
    }
    public int getIndent() {
        return indentWidth;
    }
    
    public void addStateChangedListener(ControlStateChangedListener lis) {
        synchronized(listeners) {
            listeners.add(lis);
        }
    }
    
    public void removeStateChangedListener(ControlStateChangedListener lis) {
        synchronized(listeners) {
            listeners.remove(lis);
        }
    }
    
    protected void fireStateChanged() {
        ControlStateChangedListener [] tmpListeners = null;
        synchronized(listeners) {
            tmpListeners = new ControlStateChangedListener[listeners.size()];
            listeners.toArray(tmpListeners);
        }
        for(ControlStateChangedListener lis: tmpListeners) {
            lis.stateChanged(this);
        }
    }
    
    
    public void setStateLabelVisible(boolean show) {
        stateLabel.setVisible(show);
    }
    
    public boolean isStateLabelVisible() {
        return stateLabel.isVisible();
    }
    
    public InstallerTask getTask() {
        return task;
    }
    
    public InstallerModel getModel() {
        return model;
    }
    

    private JComponent comp;
    
    protected void setContent(JComponent comp) {
        if(this.comp != null) {
            contentBox.remove(comp);
        }
        this.comp = comp;
        contentBox.add(comp);
    }
    
            
    public final Control.State getState() {
        return state;
    }
    
    protected void setState(Control.State state) {
        if(this.state == null || !this.state.equals(state)) {
            this.state = state;
            updateIcon();
            //stateLabel.setText(state.toString());
            fireStateChanged();
        }
    }
    
    private void updateIcon() {
        Icon icon = null;
        if(isEnabled() && state != null) {
            switch(state) {
                case ERROR:
                    icon = task.getController().getFrame().rb().getImage("warning.icon");
                    break;
                case WARNING:
                    icon = task.getController().getFrame().rb().getImage("warning.icon");
                    break;
            }
        }
        stateLabel.setIcon(icon);
    }
    
    public void setStateMessage(String message) {
        stateLabel.setToolTipText(message);
    }

    public String getDataKey() {
        return dataKey;
    }

    public void setDataKey(String dataKey) throws InstallerException {
        if(this.dataKey != null) {
            getModel().removeModelEventListener(this);
        }        
        this.dataKey = dataKey;
        getModel().addModelEventListener(this, dataKey);
        init();
    }
    
    public InstallerController getController() {
        return task.getController();
    }

    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        updateIcon();
        fireStateChanged();
    }

    public Audience getAudience() {
        return audience;
    }

    public void setAudience(Audience audience) {
        this.audience = audience;
    }
    
    public AbstractControl audience(Audience audience) {
        setAudience(audience);
        return this;
    }
}
