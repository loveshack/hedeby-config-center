/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.install.Audience;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.RB;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 */
public class ControlPanel extends JPanel implements Control, ControlStateChangedListener {
    
    private JPanel contentPane = new JPanel();
    
    private JPanel titlePanel = new JPanel(new BorderLayout());
    private JLabel titleLabel = new JLabel();
    private Control.State state = Control.State.ERROR;
    private JCheckBox advancedCheckBox = new JCheckBox("Show advanced settings");
    
    private List<ControlStateChangedListener> listeners = new LinkedList<ControlStateChangedListener>();
    
    /** Creates a new instance of ControlPanel */
    public ControlPanel() {
        super(new BorderLayout());
        
        JPanel p = new JPanel(new BorderLayout());
        p.add(contentPane, BorderLayout.NORTH);
        JScrollPane sc = new JScrollPane(p);
        contentPane.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        add(sc, BorderLayout.CENTER);
        
        titlePanel.add(titleLabel, BorderLayout.CENTER);
        
        Box b = Box.createHorizontalBox();
        b.add(Box.createHorizontalGlue());
        b.add(advancedCheckBox);
        
        titlePanel.add(b, BorderLayout.SOUTH);
        
        advancedCheckBox.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent e) {
                updateAudience();
            }
            
        });
        add(titlePanel, BorderLayout.NORTH);
        contentPane.setMinimumSize(new Dimension(400, 100));
        
        contentPane.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
    }
    
    public void setTitle(String title) {
        titleLabel.setText("<html><body><H3>" + title + "</H3></body></html>");
    }
    
    private void updateAudience() {
        
        Audience audience = Audience.NOVICE;
        
        if(advancedCheckBox.isSelected()) {
            audience = Audience.EXPERT;
        }
        
        for(int i = 0; i < contentPane.getComponentCount(); i++) {
            Component c = contentPane.getComponent(i);
            if(c instanceof Control ) {
                boolean visible = ((Control)c).getAudience().compareTo(audience) <= 0;
                c.setVisible(visible);
                if(labelMap.containsKey(c)) {
                    labelMap.get(c).setVisible(visible);
                }
            }
        }
    }
    
    public void setControls(String [] labels, Control [] controls, RB rb) {
        
        for(int i = 0; i < contentPane.getComponentCount(); i++) {
            Component c = contentPane.getComponent(i);
            if(c instanceof Control) {
                ((Control)c).removeStateChangedListener(this);
            }
        }
        contentPane.removeAll();

        BoxLayout bl = new BoxLayout(contentPane, BoxLayout.Y_AXIS);
        contentPane.setLayout(bl);
        
        boolean hasExpertSettings = false;
        int distance = 12;
        if(labels.length > 7) {
            distance = 5;
        }
        for(int i = 0; i < labels.length; i++) {
            if(i > 0) {
                contentPane.add(Box.createVerticalStrut(distance));
            }
            if(labels[i] != null) {
                Box b = Box.createHorizontalBox();
                if(controls[i] instanceof AbstractControl &&
                   ((AbstractControl)controls[i]).getIndent() > 0) {
                    b.add(Box.createHorizontalStrut( ((AbstractControl)controls[i]).getIndent()));
                }
                JLabel label = rb.createLabel(labels[i]);
                b.add(label);
                b.add(Box.createHorizontalGlue());
                contentPane.add(b);
                labelMap.put((JComponent)controls[i], label);
            }
            contentPane.add((JComponent)controls[i]);
            controls[i].addStateChangedListener(this);       
            if(controls[i].getAudience().equals(Audience.EXPERT)) {
                hasExpertSettings = true;
            }
        }
        
        advancedCheckBox.setVisible(hasExpertSettings);
        contentPane.add(Box.createVerticalGlue());
        updateAudience();
    }
    
    private Map<JComponent, JLabel> labelMap = new HashMap<JComponent, JLabel>();
    
    
    public void init() throws InstallerException {
        for(int i = 0; i < contentPane.getComponentCount(); i++) {
            Component c = contentPane.getComponent(i);
            if(c instanceof Control) {
                ((Control)c).init();
            }
        }
        updateState();
    }
    
    public void stateChanged(Control control) {
        updateState();
    }
    
    private void updateState() {
        Control.State state = Control.State.OK;
        
        for(int i = 0; i < contentPane.getComponentCount(); i++) {
            Component c = contentPane.getComponent(i);
            if(c instanceof Control) {
                if(((Control)c).isEnabled() && Control.State.ERROR.equals(((Control)c).getState())) {
                    state = Control.State.ERROR;
                    break;
                }
            }
        }
        if(!this.state.equals(state)) {
            this.state = state;
            fireStateChanged();
        }
    }
    
    public void addStateChangedListener(ControlStateChangedListener lis) {
        synchronized(listeners) {
            listeners.add(lis);
        }
    }
    
    public void removeStateChangedListener(ControlStateChangedListener lis) {
        synchronized(listeners) {
            listeners.remove(lis);
        }
    }
    
    protected void fireStateChanged() {
        ControlStateChangedListener [] tmpListeners = null;
        synchronized(listeners) {
            tmpListeners = new ControlStateChangedListener[listeners.size()];
            listeners.toArray(tmpListeners);
        }
        for(ControlStateChangedListener lis: tmpListeners) {
            lis.stateChanged(this);
        }
    }

    public Control.State getState() {
        return state;
    }

    public Audience getAudience() {
        return Audience.NOVICE;
    }
    
}
