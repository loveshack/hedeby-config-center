/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.bootstrap.GrmName;
import com.sun.grid.grm.bootstrap.InvalidGrmNameException;
import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerTask;
import com.sun.grid.grm.install.ui.Control.State;
import java.util.logging.Logger;

/**
 *
 */
public class GrmNameControl extends TextControl {
    
    private static Logger log = Logger.getLogger(GrmNameControl.class.getName());
    private String dataKey;
    private String sysName;
    /**
     * Creates a new instance of GrmNameControl
     */
    public GrmNameControl(InstallerTask task, InstallerModel model, String dataKey, int columns) {
        super(task, model, dataKey, columns);
        this.dataKey = dataKey;
    }
    
    protected Control.State getStateForValue(String value) {
        State ret = Control.State.ERROR;
        try {
            if(value != null && value.length() > 0) {
                new GrmName(value);
                ret = Control.State.OK;
                setStateMessage(null);
            }
        } catch(InvalidGrmNameException ex) {
            setStateMessage(ex.getLocalizedMessage());
        }
        return ret;
    }
}
