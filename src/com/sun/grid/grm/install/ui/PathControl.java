/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerTask;
import com.sun.grid.grm.install.tasks.InstallDataListModel;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

/**
 * Control for editing path structures (LD_LIBRARY_PATH, CLASSPATH)
 */
public class PathControl extends ListControl {
    
    private InstallDataListModel listModel;
    
    public PathControl(InstallerTask task, InstallerModel model, String dataKey, InstallDataListModel listModel) {
        super(task, model, dataKey, listModel);
        this.listModel = listModel;
        
        addAction(new AddPathAction());
        addAction(new EditPathAction(), true, true);
        addAction(new RemovePathAction(), true, false);
    }
    
    private JFileChooser fileChooser = new JFileChooser();
    
    public void setFileSelectionMode(int mode) {
        fileChooser.setFileSelectionMode(mode);
    }
    
    public int getFileSelectionMode() {
        return fileChooser.getFileSelectionMode();
    }
    
    public void setFileFilter(FileFilter filter) {
        fileChooser.setFileFilter(filter);
    }
    
    public FileFilter getFileFilter() {
        return fileChooser.getFileFilter();
    }
    
    private class AddPathAction extends AbstractAction {
        
        public AddPathAction() {
            super("add");
        }
        
        public void actionPerformed(ActionEvent e) {
            if(fileChooser.showOpenDialog(getController().getFrame()) == JFileChooser.APPROVE_OPTION) {
                listModel.addElement(fileChooser.getSelectedFile().getAbsolutePath());
            }
        }
    }
    
    private class RemovePathAction extends AbstractAction {
        public RemovePathAction() {
            super("remove");
        }
        
        public void actionPerformed(ActionEvent e) {
            String name = (String)getList().getSelectedValue();
            if(name != null) {
                listModel.removeElement(name);
            }
        }
    }
    
    private class EditPathAction extends AbstractAction {
        public EditPathAction() {
            super("edit");
        }
        public void actionPerformed(ActionEvent e) {
            int index = getList().getSelectedIndex();
            if(index >= 0) {
                fileChooser.setSelectedFile(new File((String)listModel.getElementAt(index)));
                if(fileChooser.showOpenDialog(getController().getFrame()) == JFileChooser.APPROVE_OPTION) {
                    listModel.setElementAt(fileChooser.getSelectedFile().getAbsolutePath(), index);
                }
            }
        }
    }
    
}
