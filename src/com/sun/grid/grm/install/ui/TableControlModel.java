/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.RB;
import javax.swing.table.AbstractTableModel;

/**
 *
 */
public class TableControlModel extends AbstractTableModel {
    
    private String [] columns;
    private Class<?> [] columnTypes;
    private InstallerModel model;
    private String datakey;
    
    public TableControlModel(InstallerModel model, String datakey, String [] columns, Class<?> [] columnTypes) {
        this.model = model;
        this.columns = columns;
        this.datakey = datakey;
        this.columnTypes = columnTypes;
    }
    
    private String lengthKey;
    
    private String getLengthKey() {
        if(lengthKey == null) {
            lengthKey = datakey + ".length";
        }
        return lengthKey;
    }
    
    public int getRowCount() {
        if(model.getData(getLengthKey()) != null) { 
            return model.getInt(getLengthKey());
        } else {
            return 0;
        }
    }
    
    private void setRowCount(int rowCount) {
        model.setData(getLengthKey(), rowCount);
    } 
    
    public int getColumnCount() {
        return columns.length;
    }
    
    private String getCellKey(int rowIndex, int columnIndex) {
        StringBuilder ret = new StringBuilder();
        ret.append(datakey);
        ret.append("[");
        ret.append(rowIndex);
        ret.append("].");
        ret.append(columns[columnIndex]);
        return ret.toString();
    }

    public void addRow(Object [] values) {
        int rowIndex = getRowCount();
        for(int i = 0; i < columns.length; i++) {
            setValueAt(values[i], rowIndex, i);
        }
        setRowCount(rowIndex + 1);
        fireTableRowsInserted(rowIndex, rowIndex);
    }
    
    public void removeRow(int rowIndex) {
        int rowCount = getRowCount();
        for(int i = rowIndex + 1; i < rowCount; i++) {
            for(int col = 0; col < getColumnCount(); col++) {
                setValueAt(getValueAt(rowIndex, col), rowIndex - 1, col);
            }
        }
        setRowCount(rowCount - 1);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            Class<?> type = getColumnClass(columnIndex);
            if(type == null) {
                return model.getData(getCellKey(rowIndex, columnIndex));
            } else if(type.equals(Integer.TYPE) || type.equals(Integer.class)) {
                return model.getInt(getCellKey(rowIndex, columnIndex));
            } else if (type.equals(Double.TYPE) || type.equals(Double.class)) {
                return model.getDouble(getCellKey(rowIndex, columnIndex));
            } else if (type.equals(Boolean.TYPE) || type.equals(Boolean.class)) {
                return model.getBoolean(getCellKey(rowIndex, columnIndex));
            } else {
                return model.getData(getCellKey(rowIndex, columnIndex));
            }
        } catch(Exception e) {
            return "Error: " + e.getLocalizedMessage();
        }
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }
    
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        model.setData(getCellKey(rowIndex, columnIndex), aValue);
    }

    public String getColumnName(int column) {
        return columns[column];
    }

    public Class<?> getColumnClass(int columnIndex) {
        if(columnTypes == null) {
            return super.getColumnClass(columnIndex);
        } else {
            return columnTypes[columnIndex];
        }
    }
    
    public int getColumnIndex(String columnName) {
        for(int i = 0; i < getColumnCount(); i++) {
            if(columnName.equals(getColumnName(i))) {
                return i;
            }
        }
        return -1;
    }
    
    
}
