/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.ui;

import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerModelListener;
import com.sun.grid.grm.install.InstallerTask;
import javax.swing.Box;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * 
 */
public class SpinnerControl extends AbstractControl implements ChangeListener, InstallerModelListener {
    
    private int minValue;
    private int maxValue;
    private int stepSize;
    private JSpinner spinner;
    private Class type;  /* integer, long, float or double */
    
    /** Creates a new instance of SpinnerControl */
    public SpinnerControl(InstallerTask task, InstallerModel model, String dataKey) {
        super(task, model, dataKey);
        spinner = new JSpinner();
        spinner.setEditor(new JSpinner.NumberEditor(spinner, "#####"));
        spinner.addChangeListener(this);
        Box box = Box.createVerticalBox();
        Box b1 = Box.createHorizontalBox();
        b1.add(spinner);
        b1.add(Box.createHorizontalGlue());
        box.add(b1);
        box.add(Box.createVerticalGlue());
        setContent(box);
        setState(State.OK);
    }    
    

    public SpinnerControl(InstallerTask task, InstallerModel model, String dataKey, int minValue, int maxValue, int stepSize) {
        this(task, model, dataKey);
        SpinnerNumberModel spinnerModel = new SpinnerNumberModel(minValue, minValue, maxValue, stepSize);
        spinner.setModel(spinnerModel);
        type = Integer.TYPE;
    }
    
    public SpinnerControl(InstallerTask task, InstallerModel model, String dataKey, SpinnerModel spinnerModel) {
        this(task, model, dataKey);
        spinner.setModel(spinnerModel);
    }
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        spinner.setEnabled(enabled);
    }
    
    public JSpinner getSpinner() {
        return spinner;
    }
    
    private void setValue(Object value) {
        
        SpinnerNumberModel model = (SpinnerNumberModel)spinner.getModel();
        
        // Special hack to ensure that the value in the spinner
        // is set. Please do not remove this
        if(model.getValue().equals(value)) {
            
            
            if(value.equals(model.getMinimum())) {
                model.setValue(model.getMaximum());
            } else if(model.getMinimum() != null) {
                model.setValue(model.getMinimum());
            }
        }
        model.setValue(value);
    }
    
    public void init() throws InstallerException {
        Object value = getModel().getData(getDataKey());
        if(value instanceof String) {
            if(Integer.TYPE.equals(type)) {
                try {
                    value = Integer.parseInt((String)value);
                } catch(NumberFormatException ex) {
                    throw new InstallerException("SpinnerControl.int.required", rb(), getDataKey(), value);
                }
            } else {
                try {
                    value = Double.parseDouble((String)value);
                } catch(NumberFormatException ex) {
                    throw new InstallerException("SpinnerControl.double.required", rb(), getDataKey(), value);
                }
            }
            setValue(value);
        } else if(value instanceof Number) {
            setValue(value);
        } else if (value != null) {
            throw new InstallerException("SpinnerControl.invalidValue", rb(), getDataKey());
        } else {
            
            SpinnerNumberModel model = (SpinnerNumberModel)spinner.getModel();
            if(model.getMinimum() != null) {
                setValue(model.getMinimum());
            } else if (model.getMaximum() != null) {
                setValue(model.getMaximum());
            } else {
                setValue(0);
            }
        }
    }
    
    public void stateChanged(ChangeEvent e) {
        getModel().setData(getDataKey(), spinner.getModel().getValue());
    }
    
    public void changedModel(InstallerModelEvent evt) {
        if (evt.getNewValue() != null) {
            spinner.getModel().setValue(evt.getNewValue());
        }
    }

}
