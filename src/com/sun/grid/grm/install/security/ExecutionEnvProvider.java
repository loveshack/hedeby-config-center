/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.security;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.ExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.PathConfiguration;
import com.sun.grid.grm.install.GrmInstallConstants;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerModelListener;
import com.sun.grid.grm.install.tasks.ExtendedAbstractInstallerTask;
import com.sun.grid.grm.security.GrmSecurityException;
import com.sun.grid.grm.security.SecurityBootstrap;
import com.sun.grid.grm.security.SecurityFactory;
import com.sun.grid.grm.util.FileUtil;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.util.Collections;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 *  The <code>ExecutionEnvProvider</code> provides the functionality to
 *  get the exeuction environment for the installer.
 *  It also observer the model and destroy the security context of some
 *  parameter changes
 */
public class ExecutionEnvProvider implements InstallerModelListener {
    
    private ExtendedAbstractInstallerTask task;
    private ExecutionEnv env;;
    
    public ExecutionEnvProvider(ExtendedAbstractInstallerTask task) {
        this.task = task;
    }
    
    private void register() {
        task.getModel().addModelEventListener(this, GrmInstallConstants.SECURITY_KEYSTORE_SOURCE);
        task.getModel().addModelEventListener(this, getBytesKey(GrmInstallConstants.SECURITY_KEYSTORE));
        task.getModel().addModelEventListener(this, getTypeKey(GrmInstallConstants.SECURITY_KEYSTORE));
        task.getModel().addModelEventListener(this, GrmInstallConstants.SECURITY_KEYSTORE_FILE);
        task.getModel().addModelEventListener(this, GrmInstallConstants.SECURITY_CAUSERNAME_CONFIG);
        task.getModel().addModelEventListener(this, GrmInstallConstants.SECURITY_CAPASSWORD_CONFIG);
        task.getModel().addModelEventListener(this, GrmInstallConstants.GRM_PREFERENCE_TYPE);
        task.getModel().addModelEventListener(this, GrmInstallConstants.GRM_DIST_DIR);
        task.getModel().addModelEventListener(this, GrmInstallConstants.GRM_LOCAL_SPOOL_DIR);
        task.getModel().addModelEventListener(this, GrmInstallConstants.GRM_SHARED_DIR);
        task.getModel().addModelEventListener(this, GrmInstallConstants.GRM_SGE_DIST_DIR);
        task.getModel().addModelEventListener(this, GrmInstallConstants.SYSTEM_INSTALL_MODE);
    }
    
    /**
     * Get the execution env
     * @throws com.sun.grid.grm.install.InstallerException  if the security context can not be initialized
     * @return the execution env
     */
    public ExecutionEnv getExecutionEnv() throws InstallerException {
        if(env == null) {
            env = createExecutionEnv();
        }
        return env;
    }
    
    private ExecutionEnv createExecutionEnv() throws InstallerException {
        PathConfiguration pc = task.getSystemConfiguration().getPathConfiguration();
        
        String keyStoreSource = (String)task.getModel().getData(GrmInstallConstants.SECURITY_KEYSTORE_SOURCE);
        SecurityFactory secFactory;
        try {
            secFactory = SecurityBootstrap.createFactory(pc.getDistLibPath());
        } catch (GrmSecurityException ex) {
            throw new InstallerException(ex.getLocalizedMessage(), ex);
        }
        
        if(GrmInstallConstants.SECURITY_KEYSTORE_FROM_STATE_FILE.equals(keyStoreSource)) {
            String username = (String)task.getModel().getData(GrmInstallConstants.SECURITY_CAUSERNAME_CONFIG);
            KeyStore keyStore = getKeyStoreFromModel(task.getModel(), GrmInstallConstants.SECURITY_KEYSTORE);
            if(keyStore != null) {
                try {
                    return ExecutionEnvFactory.newClientInstance(
                               (String)task.getModel().getData(GrmInstallConstants.GRM_SYSTEM_NAME),
                               task.getModel().getFile(GrmInstallConstants.GRM_SHARED_DIR),
                               task.getModel().getFile(GrmInstallConstants.GRM_LOCAL_SPOOL_DIR),                               
                               task.getModel().getFile(GrmInstallConstants.GRM_DIST_DIR),
                               "csHost",
                               5000,
                               Collections.<String,Object>emptyMap(),
                               (String)task.getModel().getData(GrmInstallConstants.SECURITY_CAUSERNAME_CONFIG),
                               keyStore);
                } catch (GrmSecurityException ex) {
                    throw new InstallerException("Can not create security context: " + ex.getLocalizedMessage(), ex);
                } catch (GrmException ex) {
                    throw new InstallerException("Can not create CS context: " + ex.getLocalizedMessage(), ex);
                }
            } else {
                // Try username password authentication
                CallbackHandler callbackHandler = new CallbackHandler() {
                    
                    public void handle(Callback[] callbacks) throws IOException,UnsupportedCallbackException {
                        
                        for(int i = 0; i < callbacks.length; i++) {
                            if(callbacks[i] instanceof NameCallback) {
                                ((NameCallback)callbacks[i]).setName((String)task.getModel().getData(GrmInstallConstants.SECURITY_CAUSERNAME_CONFIG));
                            } else if (callbacks[i] instanceof PasswordCallback) {
                                Object obj = task.getModel().getTmpData(GrmInstallConstants.SECURITY_CAPASSWORD_CONFIG);
                                char [] pw = null;
                                if(obj instanceof String) {
                                    pw = ((String)obj).toCharArray();
                                } else if (obj instanceof char[]) {
                                    pw = (char[])obj;
                                } else {
                                    throw new IOException("Don't known how to handle a credential of type " + obj.getClass() );
                                }
                                ((PasswordCallback)callbacks[i]).setPassword(pw);
                            }
                        }
                    }
                };
                try {      
                    return ExecutionEnvFactory.newClientInstance(
                                  (String)task.getModel().getData(GrmInstallConstants.GRM_SYSTEM_NAME),
                                  task.getModel().getFile(GrmInstallConstants.GRM_SHARED_DIR), 
                                  task.getModel().getFile(GrmInstallConstants.GRM_LOCAL_SPOOL_DIR), 
                                  task.getModel().getFile(GrmInstallConstants.GRM_DIST_DIR), 
                                  "csHost",
                                  5000,
                                  Collections.<String,Object>emptyMap(),
                                  (String)task.getModel().getData(GrmInstallConstants.SECURITY_CAUSERNAME_CONFIG), 
                                  callbackHandler);
                } catch (GrmSecurityException ex) {
                    throw new InstallerException("Can not create security context: " + ex.getLocalizedMessage(), ex);
                } catch (GrmException ex) {
                    throw new InstallerException("Can not create CS context: " + ex.getLocalizedMessage(), ex);
                }
            }
        } else if (GrmInstallConstants.SECURITY_KEYSTORE_FROM_FILE.equals(keyStoreSource)) {
            
            String keyStoreFileStr = (String)task.getModel().getData(GrmInstallConstants.SECURITY_KEYSTORE_FILE);
            File keystoreFile = new File(keyStoreFileStr);
            try {
                    return ExecutionEnvFactory.newClientInstance(
                               (String)task.getModel().getData(GrmInstallConstants.GRM_SYSTEM_NAME),
                               task.getModel().getFile(GrmInstallConstants.GRM_SHARED_DIR),
                               task.getModel().getFile(GrmInstallConstants.GRM_LOCAL_SPOOL_DIR),                               
                               task.getModel().getFile(GrmInstallConstants.GRM_DIST_DIR),
                               "csHost",
                               5000,
                               Collections.<String,Object>emptyMap(),
                               (String)task.getModel().getData(GrmInstallConstants.SECURITY_CAUSERNAME_CONFIG),
                               keystoreFile);
            } catch (GrmSecurityException ex) {
                throw new InstallerException("Can not create security context: " + ex.getLocalizedMessage(), ex);
            } catch (GrmException ex) {
                throw new InstallerException("Can not create CS context: " + ex.getLocalizedMessage(), ex);
            }
            
        } else {
            throw new InstallerException("Unknown keystore source '" + keyStoreSource + "'");
        }
    }
    
    
    public void changedModel(InstallerModelEvent evt) {
        env = null;
    }
    
    /**
     * get the keystore from a installer model
     * @param model     the model
     * @param dataKey   the base datakey
     * @throws com.sun.grid.grm.install.InstallerException if the keystore is defined in the model, but it
     *              can not be loaded
     * @return the keystore or <code>null</code> if the model contains no keystore under the datakey
     */
    public static KeyStore getKeyStoreFromModel(InstallerModel model, String dataKey) throws InstallerException {
        String s = (String)model.getData(getBytesKey(dataKey));
        String type = (String)model.getData(getTypeKey(dataKey));
        if(s != null && type != null) {
            try {
                return stringToKeyStore(s, type);
            } catch (KeyStoreException ex) {
                throw new InstallerException("Error while loading keystore: " + ex.getLocalizedMessage(), ex);
            } catch (IOException ex) {
                throw new InstallerException("Error while loading keystore: " + ex.getLocalizedMessage(), ex);
            }
        }
        return null;
    }
    
    public static void putKeyStoreToModel(InstallerModel model, String dataKey, KeyStore keystore) {
        model.setData(getTypeKey(dataKey), keystore.getType());
        model.setData(getBytesKey(dataKey), keyStoreToString(keystore));
    }
    
    private static String keyStoreToString(KeyStore ks) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            ks.store(bos, new char[0]);
        } catch (Exception ex) {
            throw new IllegalStateException("Can not store keystore: " + ex.getLocalizedMessage(), ex);
        }
        
        byte [] bytes = bos.toByteArray();
        
        return FileUtil.byteArrayToHexString(bytes);
    }
    
    private static KeyStore stringToKeyStore(String str, String keyStoreType) throws KeyStoreException, IOException {
        
        byte [] bytes = FileUtil.hexStringToByteArray(str);
        
        ByteArrayInputStream bin = new ByteArrayInputStream(bytes);
        KeyStore ks = KeyStore.getInstance(keyStoreType);
        
        try {
            ks.load(bin, new char[0]);
        } catch (Exception ex) {
            throw new IllegalStateException("Can not load keystore: " + ex.getLocalizedMessage(), ex);
        }
        
        return ks;
    }
    
    /**
     * Get the key under which type of the keystore is stored
     * @param dataKey the base datakey
     * @return  key under which type of the keystore is stored (<code>&lt;datakey<&gt>.type</code>)
     */
    public static String getTypeKey(String dataKey) {
        return dataKey + ".type";
    }
    /**
     * Get the key under which the bytes of the keystore are stored
     *
     * @param dataKey the base datakey
     * @return key under which the bytes of the keystore is stored (<code>&lt;datakey<&gt>.bytes</code>)
     */
    public static String getBytesKey(String dataKey) {
        return dataKey + ".bytes";
    }
    
    
    
}
