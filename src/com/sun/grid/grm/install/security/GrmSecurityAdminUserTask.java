/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.security;

import com.sun.grid.ca.GridCAException;
import com.sun.grid.ca.InitCAParameters;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmServiceException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.GrmName;
import com.sun.grid.grm.bootstrap.JvmConfiguration;
import com.sun.grid.grm.bootstrap.PathConfiguration;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.install.*;
import com.sun.grid.grm.install.tasks.ExtendedAbstractInstallerTask;
import com.sun.grid.grm.install.tasks.InstallDataListModel;
import com.sun.grid.grm.install.tasks.InstallUtil;
import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.ListControl;
import com.sun.grid.grm.util.Platform;
import com.sun.grid.grm.util.ProgressListener;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import static java.awt.GridBagConstraints.*;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.prefs.BackingStoreException;

/**
 *
 *
 */
public class GrmSecurityAdminUserTask extends ExtendedAbstractInstallerTask implements InstallerTask {
    
    private UI ui;
    private InstallDataListModel userListModel;
    private ListControl adminUserList;
    
    private class UI extends ControlPanel {
        
        public UI() {
            InstallerController controller = getController();
            adminUserList = new ListControl(GrmSecurityAdminUserTask.this, getModel(), "SECURITY_ADMINUSER", userListModel);
            
            adminUserList.addAction(new AddAction());
            adminUserList.addAction(new RemoveAction(), true, false);
            adminUserList.addAction(new AddFileAction());
            adminUserList.addAction(new AddGroupAction());
            
            setTitle(rb().getString("grmSecurityAdminUser.Header"));
            String name = GrmSecurityAdminUserTask.this.getName();
            setControls(
                    new String [] {
                "grmSecurityAdminUser.AddedUser"
            },
                    new Control [] {
                adminUserList
            },
                    rb()
                    );
        }
    }
    
    /**
     * Creates a new instance of GrmSecurityAdminUserTask
     */
    public GrmSecurityAdminUserTask(InstallerController controller, String name) {
        super(controller, name);
        userListModel = new InstallDataListModel(controller.getModel(), "SECURITY_ADMINUSER_LIST", "|");
        
        StringTokenizer tok = new StringTokenizer((String)getModel().getData("AVAILABLE_JVMS"), "|");
        
        while (tok.hasMoreTokens()) {
            String elem = tok.nextToken();
            String prefix = "jvm[" + elem + "].";
            Object user =  getModel().getData(prefix + "adminuser");
            if (!userListModel.contains(user)) {
                userListModel.addElement(user);
            }
        }
    }
    
    private GenericNextObserver nextObserver;
    
    public JPanel getUI() {
        if(ui == null) {
            ui = new UI();
            nextObserver = createGenericNextObserver(ui);
        }
        return ui;
    }
    
    
    
    private class AddAction extends AbstractAction {
        
        public AddAction() {
            rb().initAction(this, "AddUserAction");
        }
        
        public void actionPerformed( ActionEvent e ) {
            String user = (String)JOptionPane.showInputDialog(ui.getParent(),
                    rb().getString("AddUserDialog"), "grmAdmin");
            if(user != null && user.length() > 0) {
                InstallDataListModel tmpList = (InstallDataListModel) adminUserList.getListModel();
                tmpList.addElement(user);
                adminUserList.setListModel(tmpList);
            }
        }
    };
    
    
    private class RemoveAction extends AbstractAction {
        
        public RemoveAction() {
            rb().initAction(this, "RemoveUserAction");
        }
        
        public void actionPerformed( ActionEvent e ) {
            if ( adminUserList.getList().isSelectionEmpty() ) {
                return;
            } else {
                InstallDataListModel tmpList = (InstallDataListModel) adminUserList.getListModel();
                for ( Object elem : adminUserList.getList().getSelectedValues()) {
                    tmpList.removeElement(elem);
                }
                adminUserList.setListModel(tmpList);
            }
        }
    };
    
    private class AddFileAction extends AbstractAction {
        
        public AddFileAction() {
            rb().initAction(this, "AddUserFileAction");
        }
        
        public void actionPerformed( ActionEvent e ) {
            
            JFileChooser uFileChooser = new JFileChooser();
            InstallDataListModel tmpList = (InstallDataListModel) adminUserList.getListModel();
            
            int returnVal = uFileChooser.showOpenDialog(ui.getParent());
            if(returnVal == JFileChooser.APPROVE_OPTION) {
                StringTokenizer tok = new StringTokenizer
                        (AddHostsFromFile(uFileChooser.getSelectedFile().getAbsolutePath()));
                int c = tok.countTokens();
                while (c != 0) {
                    tmpList.addElement(tok.nextToken());
                    c--;
                }
                adminUserList.setListModel(tmpList);
            }
            
        }
    }
    
    
    private class AddGroupAction extends AbstractAction {
        
        public AddGroupAction() {
            rb().initAction(this,"AddGroupButton");
        }
        
        public void actionPerformed( ActionEvent e ) {
            InstallDataListModel tmpList = (InstallDataListModel) adminUserList.getListModel();
            String group = (String)JOptionPane.showInputDialog(ui.getParent(),
                    rb().getString("AddGroupDialog"), "grmGroup");
            if(group != null && group.length() > 0) {
                tmpList.addElement(group);
            }
            adminUserList.setListModel(tmpList);
        }
    };
    
    private String AddHostsFromFile(String file) {
        BufferedReader buf = null;
        String line = null;
        String tmp = "";
        try {
            buf = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            while ((line = buf.readLine()) != null) {
                tmp = tmp + " " + line;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            buf.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return tmp;
    }
    
    
    private BootstrapSecurityInstaller createBoostrapSecurityInstaller() throws InstallerException {
        
        SystemConfiguration sc = getSystemConfiguration();
        BootstrapSecurityInstaller si = null;
        
        try {
            InetAddress thisHost = null;
            try {
                thisHost = InetAddress.getLocalHost();
            } catch (java.net.UnknownHostException ex) {
                throw new InstallerException("Host is not resolveable!", ex);
            }
            si = new BootstrapSecurityInstaller(getExecutionEnv(), (String)getData(GrmInstallConstants.GRM_ADMIN_USER), thisHost);
        } catch (GridCAException ex) {
            throw new InstallerException("grm ca reported error", ex);
        } catch (com.sun.grid.grm.config.ConfigurationException ex) {
            throw new InstallerException("grm ca reported error", ex);
        } catch (GrmException ex) {
            throw new InstallerException("grm ca reported error", ex);
        }
        
        String authMethod = (String)getController().getModel().getData(GrmInstallConstants.SECURITY_AUTH_METHOD);
        
        if(log.isLoggable(Level.FINER)) {
            log.log(Level.FINER,"{0}: {1}", new Object [] {
                this.getName(),"execute"
            });
        }
        
        if(log.isLoggable(Level.FINER)) {
            log.log(Level.FINER,"{0}: {1} System Conf: {2}", new Object [] {
                this.getName(), "execute", sc.toString()
            });
        }
        
        
        if ( authMethod.equals(GrmInstallConstants.AUTH_NIS)) {
            
            String server = (String)getController().getModel().getData(GrmInstallConstants.SECURITY_AUTH_NIS_SERVER);
            String domain = (String)getController().getModel().getData(GrmInstallConstants.SECURITY_AUTH_NIS_DOMAIN);
            si.addNisLoginModule(server, domain,
                    BootstrapSecurityInstaller.LoginModuleFlag.optional);
        } else if ( authMethod.equals(GrmInstallConstants.AUTH_PAM)) {
            
            String service = (String)getController().getModel().getData(GrmInstallConstants.SECURITY_AUTH_PAM_SERVICE);
            si.addPamLoginModule(service,
                    BootstrapSecurityInstaller.LoginModuleFlag.optional);
            
        } else if ( authMethod.equals(GrmInstallConstants.AUTH_SYSTEM)) {
            si.addSystemLoginModule(BootstrapSecurityInstaller.LoginModuleFlag.optional);
        }
        
        JvmConfig jvmConf = null;
        String selectJvm = (String)getModel().getData(GrmInstallConstants.SECURITY_AUTH_CA_JVM);
        try {
        for (JvmConfig tmpJvmConf: SystemUtil.getJvmConfigs(getExecutionEnv())) {
            if(log.isLoggable(Level.FINER)) {
                log.log(Level.FINER,"{0}: {1} JVM Name: {2}", new Object [] {
                    this.getName(), "execute", tmpJvmConf.getName()
                });
            }
            if (tmpJvmConf.getName().equals(selectJvm)) {
                jvmConf = tmpJvmConf;
                break;
            }
        }
        } catch (GrmException ex) {
           throw new InstallerException("grm ca reported error", ex); 
        }
        if(log.isLoggable(Level.FINER)) {
            log.log(Level.FINER,"{0}: {1} JVM Conf: {2}", new Object [] {
                this.getName(), "execute", jvmConf
            });
        }
        if (jvmConf == null) {
            throw new InstallerException("No jvm for ca component selected (" + GrmInstallConstants.SECURITY_AUTH_CA_JVM + ")");
        }
        si.addToJvm(jvmConf);
        
        if(log.isLoggable(Level.FINER)) {
            log.log(Level.FINER,"{0}: {1} JVM Conf: {2}", new Object [] {
                this.getName(), "execute", jvmConf
            });
        }
        
        for (int i = 0; i < userListModel.size(); i++) {
            String adminUser = (String)userListModel.get(i);
            si.addAdminUser(adminUser);
        }
        
        try {
            InitCAParameters params = new InitCAParameters();
            
            params.setCountry((String)getController().getModel().getData(GrmInstallConstants.CA_COUNTRY_CODE));
            params.setState((String)getController().getModel().getData(GrmInstallConstants.CA_STATE));
            params.setLocation((String)getController().getModel().getData(GrmInstallConstants.CA_LOCATION));
            params.setOrganization((String)getController().getModel().getData(GrmInstallConstants.CA_ORGANISATION));
            params.setOrganizationUnit((String)getController().getModel().getData(GrmInstallConstants.CA_ORGA_UNIT));
            params.setAdminEmailAddress((String)getController().getModel().getData(GrmInstallConstants.CA_ADMIN_EMAIL));
            
            si.setCAParams(params);
        } catch(GridCAException ex) {
            throw new InstallerException("Invalid ca parameters: " + ex.getMessage(), ex);
        }
        
        return si;
        
/*
        try {
            sc.writeToBackStore();
            percent += percentPerStep;
            fireProgress("System configuration stored", (int)percent);
            si.writeCAConfig();
            percent += percentPerStep;
            fireProgress("Configuration of CA component stored", (int)percent);
            si.createSharedConfigFiles();
            percent += percentPerStep;
            fireProgress("Shared security files created", (int)percent);
        } catch (IOException ex) {
            throw new InstallerException("IO error while writting system configuration", ex);
        } catch (BackingStoreException ex) {
            throw new InstallerException("IO error while writting system configuration", ex);
        }
 
 
 
        File tmpDir = sc.getPathConfiguration().getTmpDirForComponent(BootstrapSecurityInstaller.CA_COMP_NAME);
        File daemonDir = sc.getPathConfiguration().getDaemonKeystorePath();
        File userDir = sc.getPathConfiguration().getUserKeyStorePath();
 
        try {
 
            mkdir(tmpDir, false);
            mkdir(daemonDir, false);
            mkdir(userDir, false);
 
            Platform.getPlatform().chown(daemonDir, getAdminUser());
            Platform.getPlatform().chown(userDir, getAdminUser());
 
            percent += percentPerStep;
            fireProgress("Local CA directories created", (int)percent);
 
            si.initCA(params);
 
            percent += percentPerStep;
            fireProgress("CA initialized", (int)percent);
 
        } catch (GrmServiceException ex) {
            throw new InstallerException(ex.getLocalizedMessage(), ex);
        } catch (IOException ex) {
            throw new InstallerException(ex.getLocalizedMessage(), ex);
        } catch (GridCAException ex) {
            throw new InstallerException(ex.getLocalizedMessage(), ex);
        } catch (InterruptedException ex) {
            throw new InstallerException(ex.getLocalizedMessage(), ex);
        } catch (GrmException ex) {
            throw new InstallerException(ex.getLocalizedMessage(), ex);
        }
 
        setData(GrmInstallConstants.SECURITY_CONFIG, si);
 */
    }
    
    public void execute() throws InstallerException {
        
        fireStarted();
        
        SystemConfiguration sc = getSystemConfiguration();
        
        // Prepare the bootstrap installer
        BootstrapSecurityInstaller si = createBoostrapSecurityInstaller();
        
        final int siOffset = 4;
        int stepCount = 0;
        try {
            stepCount = si.getStepCount() + siOffset;
        } catch (GrmException ex) {
            throw new InstallerException(ex.getLocalizedMessage(), ex);
        }
        final double percentagePerStep = stepCount / 100.0;
        
        File tmpDir = PathUtil.getTmpDirForComponent(getExecutionEnv(), BootstrapSecurityInstaller.CA_COMP_NAME);
        File daemonDir = sc.getPathConfiguration().getDaemonKeystorePath();
        File userDir = sc.getPathConfiguration().getUserKeyStorePath();
        
        
        int step = 0;
        mkdir(tmpDir, false);
        
        createPathConfiguration();
        fireProgress("...path Configuration created!", (int)(++step *percentagePerStep));
        try {
            
            si.storeCAConfig();
            si.createSharedConfigFiles();
        } catch (GrmException ex) {
            throw new InstallerException(ex.getLocalizedMessage(), ex);
        } catch (IOException ex) {
            throw new InstallerException(ex.getLocalizedMessage(), ex);
        }
    
    
    fireProgress("directory " + tmpDir + " created", (int)(++step * percentagePerStep));
    mkdir(daemonDir, false);
    try {
        Platform.getPlatform().chown(daemonDir, getAdminUser());
    } catch (Exception ex) {
        throw new InstallerException("chown of directory " + daemonDir + " failed");
    }
    fireProgress("directory " + daemonDir + " created", (int)(++step * percentagePerStep));
    
    mkdir(userDir, false);
    try {
        Platform.getPlatform().chown(userDir, getAdminUser());
    } catch (Exception ex) {
        throw new InstallerException("chown of directory " + userDir + " failed");
    }
    fireProgress("directory " + userDir + " created", (int)(++step * percentagePerStep));
    
    ProgressListener lis = new ProgressListener() {
        
        public void progress(String message, int step) {
            fireProgress(message, (int)((siOffset+step) * percentagePerStep));
        }
        
        public void started(int steps) {
            // Ignore
        }
    };
    
    si.addProgressListener(lis);
    try {
        si.install();
    } catch (Exception ex) {
        throw new InstallerException("Bootstraping security failed: " + ex.getMessage(), ex);
    } finally {
        si.removeProgressListener(lis);
    }
    
    fireFinished();
    
}

public void undo() {
}

public void beginDisplay() throws InstallerException {
    getUI();
    ui.init();
    nextObserver.register();
}

public void finishDisplay() throws InstallerException {
    if(nextObserver != null) {
        nextObserver.unregister();
    }
}

private void createPathConfiguration() throws InstallerException {
    
    String sysname = (String) getModel().getData(GrmInstallConstants.GRM_SYSTEM_NAME);
    log.log(Level.FINE, "system name is \"{0}\"", sysname );
    
    SystemConfiguration sc;
    try {
        sc = getSystemConfiguration();
    } catch (InstallerException ex) {
        throw new InstallerException(ex.getLocalizedMessage());
    }
    PathConfiguration pc = sc.getPathConfiguration();
    boolean ret = true;
    
    GrmName grmName = sc.getSystemName();
    String[] env = { "SHELL=/bin/sh", "TERM=/bin/xterm" };
    File dist = null;
    File dist_lib = null;
    File dist_grm_util = null;
    File dist_man = null;
    File dist_bin = null;
    File localSpool = null;
    File localSpool_lib = null;
    File localSpool_log = null;
    File localSpool_run = null;
    File localSpool_security = null;
    File localSpool_sec_daemons = null;
    File localSpool_sec_users = null;
    File localSpool_sec_ca = null;
    File localSpool_spool = null;
    File localSpool_tmp = null;
    File shared = null;
    File shared_config = null;
    File shared_run = null;
    File shared_security = null;
    
    
    dist = new File(pc.getDistPath().getAbsolutePath());
    dist_lib = new File(dist.getPath() + File.separatorChar + "lib");
    dist_grm_util = new File(dist.getPath() + File.separatorChar + "grm_util");
    dist_man = new File(dist.getPath() + File.separatorChar + "man");
    dist_bin = new File(dist.getPath() + File.separatorChar + "bin");
    
    localSpool = new File(pc.getLocalSpoolPath().getAbsolutePath());
    localSpool_lib = new File(localSpool.getPath() + File.separatorChar + "lib");
    localSpool_log = new File(localSpool.getPath() + File.separatorChar + "log");
    localSpool_run = new File(localSpool.getPath() + File.separatorChar + "run");
    localSpool_security = new File(localSpool.getPath() + File.separatorChar + "security");
    localSpool_sec_daemons = new File(localSpool_security.getPath() + File.separatorChar + "daemons");
    localSpool_sec_users = new File(localSpool_security.getPath() + File.separatorChar + "users");
    localSpool_sec_ca = new File(localSpool_security.getPath() + File.separatorChar + "ca");
    localSpool_spool = new File(localSpool.getPath() + File.separatorChar + "spool");
    localSpool_tmp = new File(localSpool.getPath() + File.separatorChar + "tmp");      
    
    shared = new File(pc.getSharedPath().getAbsolutePath());
    shared_config = new File(shared.getPath() + File.separatorChar + "config");
    shared_run = new File(shared.getPath() + File.separatorChar + "run");
    shared_security = new File(shared.getPath() + File.separatorChar + "security");
    
    System.setProperty(BootstrapConstants.SYSTEM_NAME_PROPERTY, sc.getSystemName().getName());
    System.setProperty(BootstrapConstants.DIST_PATH_PROPERTY, pc.getDistPath().getAbsolutePath());
    System.setProperty(BootstrapConstants.LOCAL_SPOOL_PATH_PROPERTY, pc.getLocalSpoolPath().getAbsolutePath());
    System.setProperty(BootstrapConstants.SHARED_PATH_PROPERTY, pc.getSharedPath().getAbsolutePath());
    System.setProperty(BootstrapConstants.SGEROOT_PATH_PROPERTY, sc.getSgeRoot());
    
    
    try {
        pc.setSystemName(grmName);
        pc.setDistPath(dist);
        pc.setLocalSpoolPath(localSpool);
        pc.setSharedPath(shared);
        pc.write();
                         
        InstallUtil.mkdir(getModel(), shared, true);
        InstallUtil.mkdir(getModel(), shared_config, true);
        InstallUtil.mkdir(getModel(), shared_run, true);
        InstallUtil.mkdir(getModel(), shared_security, true);
        InstallUtil.mkdir(getModel(), dist, true);
        InstallUtil.mkdir(getModel(), dist_lib, true);
        InstallUtil.mkdir(getModel(), dist_grm_util, true);
        InstallUtil.mkdir(getModel(), dist_man, true);
        InstallUtil.mkdir(getModel(), dist_bin, true);
        InstallUtil.mkdir(getModel(), localSpool, true);
        InstallUtil.mkdir(getModel(), localSpool_lib, true);
        InstallUtil.mkdir(getModel(), localSpool_log, true);
        InstallUtil.mkdir(getModel(), localSpool_run, true);
        InstallUtil.mkdir(getModel(), localSpool_security, true);
        InstallUtil.mkdir(getModel(), localSpool_sec_daemons, true);
        InstallUtil.mkdir(getModel(), localSpool_sec_users, true);
        InstallUtil.mkdir(getModel(), localSpool_sec_ca, true);
        InstallUtil.mkdir(getModel(), localSpool_spool, true);
        InstallUtil.mkdir(getModel(), localSpool_tmp, true);

    } catch (IllegalStateException ex) {
        throw new InstallerException(ex.getLocalizedMessage(),ex);
    } catch (InstallerException ex) {
        throw new InstallerException(ex.getLocalizedMessage(),ex);
    } catch (BackingStoreException ex) {
        throw new InstallerException(ex.getLocalizedMessage(),ex);
    }
}

}
