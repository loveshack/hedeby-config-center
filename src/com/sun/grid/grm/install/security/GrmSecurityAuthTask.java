/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.security;

import com.sun.grid.ca.GridCAException;
import com.sun.grid.ca.InitCAParameters;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmServiceException;
import com.sun.grid.grm.bootstrap.JvmConfiguration;
import com.sun.grid.grm.bootstrap.SystemConfiguration;
import com.sun.grid.grm.install.Audience;
import com.sun.grid.grm.install.GrmInstallConstants;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.InstallerModel;
import com.sun.grid.grm.install.InstallerModelEvent;
import com.sun.grid.grm.install.InstallerModelListener;
import com.sun.grid.grm.install.tasks.ExtendedAbstractInstallerTask;
import com.sun.grid.grm.install.tasks.InstallDataListModel;
import com.sun.grid.grm.install.tasks.InstallUtil;
import com.sun.grid.grm.install.tasks.JvmDialog;
import com.sun.grid.grm.install.tasks.JvmListModel;
import com.sun.grid.grm.install.ui.ComboBoxControl;
import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.JvmListControl;
import com.sun.grid.grm.install.ui.TextControl;
import com.sun.grid.grm.util.Platform;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.prefs.BackingStoreException;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * 
 */
public class GrmSecurityAuthTask extends ExtendedAbstractInstallerTask{
    
    public String[] authMethods = new String[] { GrmInstallConstants.AUTH_NIS, GrmInstallConstants.AUTH_PAM, GrmInstallConstants.AUTH_SYSTEM };
    
    private UI ui;
    private JvmListModel jvmListModel;
    
    private class UI extends ControlPanel {
        
        private JvmListControl jvmListControl;
        private TextControl nisServerControl;
        private TextControl nisDomainControl;
        private TextControl pamServiceControl;
        
        public UI() throws InstallerException {
            
            setTitle(rb().getString("grmSecurityAuth.header"));
            
            String name = GrmSecurityAuthTask.this.getName();
            String [] labels = {
                "CAJvm",
                "AuthMethod",
                "NisServer",
                "NisDomain",
                "PamService",
            };
            
            jvmListControl = new JvmListControl(GrmSecurityAuthTask.this, getModel(), GrmInstallConstants.SECURITY_AUTH_CA_JVM, jvmListModel, getSystemConfiguration());
            
            nisServerControl = new TextControl(GrmSecurityAuthTask.this, getModel(), GrmInstallConstants.SECURITY_AUTH_NIS_SERVER);
            nisDomainControl = new TextControl(GrmSecurityAuthTask.this, getModel(), GrmInstallConstants.SECURITY_AUTH_NIS_DOMAIN);
            pamServiceControl = new TextControl(GrmSecurityAuthTask.this, getModel(), GrmInstallConstants.SECURITY_AUTH_PAM_SERVICE);
            
            Control controls [] = {
                jvmListControl,
                new ComboBoxControl(GrmSecurityAuthTask.this, getModel(), GrmInstallConstants.SECURITY_AUTH_METHOD, authMethods).audience(Audience.EXPERT),
                nisServerControl.audience(Audience.EXPERT),
                nisDomainControl.audience(Audience.EXPERT),
                pamServiceControl.audience(Audience.EXPERT),
            };
            
            setControls(labels, controls, rb());
            
        }
        
    }
    
    /** Creates a new instance of GrmSecurityAuthTask */
    public GrmSecurityAuthTask(InstallerController controller, String name) {
        super(controller, name);
        jvmListModel = JvmListModel.getInstance(controller.getModel());
    }
    
    private GenericNextObserver nextObserver;
    private AuthMethodObserver authMethodObserver = new AuthMethodObserver();
    
    class AuthMethodObserver implements InstallerModelListener {
        
        public void register() {
            getModel().addModelEventListener(this, GrmInstallConstants.SECURITY_AUTH_METHOD);
            update();
        }
        
        public void unregister() {
            getModel().removeModelEventListener(this);
        }
        
        public void changedModel(InstallerModelEvent evt) {
            update();
        }
        
        public void update() {
            Object value = getModel().getData(GrmInstallConstants.SECURITY_AUTH_METHOD);
            if(value.equals(GrmInstallConstants.AUTH_NIS)) {
                ui.nisDomainControl.setEnabled(true);
                ui.nisServerControl.setEnabled(true);
                ui.pamServiceControl.setEnabled(false);
            } else if (value.equals(GrmInstallConstants.AUTH_PAM) ) {
                ui.nisDomainControl.setEnabled(false);
                ui.nisServerControl.setEnabled(false);
                ui.pamServiceControl.setEnabled(true);
            } else if (value.equals(GrmInstallConstants.AUTH_SYSTEM)) {
                ui.nisDomainControl.setEnabled(false);
                ui.nisServerControl.setEnabled(false);
                ui.pamServiceControl.setEnabled(false);
            }
        }
        
    }
    
    public JPanel getUI() throws InstallerException {
        if(ui == null) {
            ui = new UI();
            nextObserver = createGenericNextObserver(ui);
        }
        return ui;
    }
    
    public void beginDisplay() throws InstallerException {
        getUI();
        ui.init();
        nextObserver.register();
        authMethodObserver.register();
        
    }
    
    public void finishDisplay() throws InstallerException {
        if(nextObserver != null) {
            nextObserver.unregister();
        }
        authMethodObserver.unregister();
    }
    
    
    public void execute() throws InstallerException {
        
        fireStarted();
        
        
        this.fireFinished();
    }
    
    public void undo() {
    }
    
}
