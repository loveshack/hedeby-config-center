/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.install.security;

import com.sun.grid.grm.install.AbstractInstallerTask;
import com.sun.grid.grm.install.GrmInstallConstants;
import com.sun.grid.grm.install.InstallerController;
import com.sun.grid.grm.install.InstallerException;
import com.sun.grid.grm.install.ui.Control;
import com.sun.grid.grm.install.ui.ControlPanel;
import com.sun.grid.grm.install.ui.ControlStateChangedListener;
import com.sun.grid.grm.install.ui.TextControl;
import javax.swing.JPanel;


/**
 *
 * 
 */
public class GrmSecuritySystemTask extends AbstractInstallerTask {

    public static final String AUTH_NIS = "NIS";
    public static final String AUTH_PAM = "PAM";
    public static final String AUTH_SHADOW = "Shadow";
    
    private UI ui;
    
    private class UI extends ControlPanel {
    
        public UI() {
            super();
            
            setTitle(rb().getString("grmSecuritySystem.header"));
            String name = GrmSecuritySystemTask.this.getName();
            String [] labels = {
                "CountryCode",
                "State",
                "Location",
                "Organisation",
                "OrgaUnit",
                "MailAdress"
            };
            
            Control controls [] = {
                new TextControl(GrmSecuritySystemTask.this, getModel(), GrmInstallConstants.CA_COUNTRY_CODE, 3).textLen(2,2),
                new TextControl(GrmSecuritySystemTask.this, getModel(), GrmInstallConstants.CA_STATE, 30),
                new TextControl(GrmSecuritySystemTask.this, getModel(), GrmInstallConstants.CA_LOCATION, 30),
                new TextControl(GrmSecuritySystemTask.this, getModel(), GrmInstallConstants.CA_ORGANISATION, 30),
                new TextControl(GrmSecuritySystemTask.this, getModel(), GrmInstallConstants.CA_ORGA_UNIT, 30),
                new TextControl(GrmSecuritySystemTask.this, getModel(), GrmInstallConstants.CA_ADMIN_EMAIL,30),                
            };
            
            setControls(labels, controls, rb());
            
        }
        
    }
    /**
     * Creates a new instance of GrmSecuritySystemTask
     */
    public GrmSecuritySystemTask(InstallerController controller, String name) {
        super(controller, name);
    }
    
    private GenericNextObserver observer;
    
    public JPanel getUI() {
        if(ui == null) {
            ui = new UI();
            observer = createGenericNextObserver(ui);
        }
        return ui;
    }

    public void beginDisplay() throws InstallerException {
        getUI();
        ui.init();
        observer.register();
    }
    
    public void finishDisplay() throws InstallerException {        
        if(observer != null) {
            observer.unregister();
        }
    }
    

    public void execute() throws InstallerException {
        
        fireStarted();
        fireFinished();
    }

    public void undo() {
    }
    
    
}
