/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.install;

import java.util.ListResourceBundle;

/**
 *
 * 
 */
public class HaithabuResources extends ListResourceBundle {
    
    public Object[][] getContents() {return contents;}
    
    /**
     * The contents of the message catalog - this is an array of
     * arrays, where the first index is a particular message, and the
     * second index has [0] being the key and [1] being the message.
     */
    
    static final Object[][] contents =
    {
        { "grm-install-ProductName","Haithabu"},
        { "grm-jar-ComponentName", "Haithabu Binaryfiles"},
        
        /* Navigation Button Labels.*/
        { "Back-Button"," Back "},
        { "Exit-Button"," Cancel "},
        { "Next-Button"," Next "},
        { "Help-Button"," Help "},
        { "Close-Button"," Close "},
        { "Exit-Dialog-Button"," Exit "},
        { "Exit-Dialog-Continue-Button"," Continue "},
        
        /* Error Messages */
        { "grm-systemname-exists", "Systemname already exists, loading configuration!" },
        { "grm-pathconfig-write-error", "An error happened writing the configuration!" },
        { "grm-pathconfig-illegal-systemname", "An illegal Systemname has been entered!" },
        { "grm-pathconfig-preferences-unavail", "The requested preferences are not available!"},
        { "grm-rpconfig-illegal-grmname", "An illegal Name or Identifier has been entered!" },
        { "grm-rpconfig-cant_create_config_obj", "Can't create Resource Provider configuration object!" },
        
        
        /* Information Messages */
        { "grm-systemname-ok", "Systemname can be used!" },
        
        /* Messagedialog Labels */
        { "grm-messagebox-error", "Error" },
        { "grm-messagebox-info", "Information" },
        
        { "grmCommon-Gui-AvailableJvm", "Available JVM's:"},
        
        
        /* Commandline TTY Messages */
        { "wb-ConsoleInteraction-HelpCommandLineOptionText", 
	"Options recognized by CNP SDK:"+
	"\n\t-debug         general debug info "+
	"\n\t-nodisplay     run installer in text only (non graphical) mode"+
	"\n\t-noconsole     don't display non graphical text. Used with"+
	"\n\t               nodisplay option for silent installs." + 
	"\n\t-no            Use to run through the installer without installing any bits" +
	"\n\t-novalidate    no text field validation checking "+
	"\n\t-saveState [statefile] save state of installer input" +
	"\n\t-state [statefile] use the statefile for setting input values" +
	"\n\t-debugMessage  output debugging information, includes errors and warnings" +
	"\n\t-debugWarning  output warning message, includes errors" +
	"\n\t-debugError    output error messages, on by default"}, 

        { "wb-ConsoleInteraction-UnknownCommandLineOptionText", "Unknown option {0}"},
        
        /* Dialog and Panel Messages, Labels, Text */
        { "grm-install-Gui-OnlineHelp", "This will become the Onlinehelp text!" },
        { "grm-Cancel-Sure", "You are going to cancel the installer! Are you sure?"},
        { "grm-Exit-Sure", "You are going to exit the installer! Are you sure?"},
        { "grmPanel-AboutText", "This is the about text" },
        { "grmInstallationModeSelection-Gui-NewSystem", "Install a New System"},
        { "grmInstallationModeSelection-Gui-AddHostToSystem", "Add a Host to a existing System"},
        { "grmInstallationModeSelection-Gui-AddComponentToSystem", "Add a Component to a existing System"},
        { "grmCompSelect-Gui-Header", "Component Selection" },
        { "grmCompSelect-Gui-CompSelectLabel", "Please select the components you wish to install on this host!" },
        { "grmCompSelect-Gui-BootInstall", "Bootstrap Installation" },
        { "grmCompSelect-Gui-ExecInstall", "Executor Installation" },
        { "grmCompSelect-Gui-RpInstall", "Resource Provider Installation" },
        { "grmCompSelect-Gui-ScInstall", "Service Container/N1GE Adapter Installation" },
        { "grmCompSelect-Gui-OspInstall", "OS Provisioner Installation" },
        { "grmCompSelect-Gui-OsdSmaInstall", "OS Dispatcher/N1SM Adapter Installation" },
        { "grmCompSelect-install-Gui-OnlineHelp", "Component Installation Selection OnlineHelp" },
        { "grmCompSelect-Gui-NoCompSelected", "Please select a component for installation!" },
        { "grmRadioLicense-Gui-HeaderText", "License Agreement" },
        { "grmRadioLicense-Gui-IntroText", "Do you agree?" },
        { "grmRadioLicense-Gui-yesButton", "Yes, I agree!" },
        { "grmRadioLicense-Gui-noButton", "No, I do not agree!" },
        { "grmRadioLicense-Gui-LicenseText", "This is the license text, still missing!" },
        { "grmRadioLicense-install-Gui-OnlineHelp", "This is the onlinehelp!" },
        { "grmUserWarn-Gui-HeaderText", "User Information" },
        { "grmUserWarn-Gui-IntroText", "The installer is not running as user \"root\". If you want to run as non-root user you have to start the installer with the following command: \n\"inst_grm.sh -user\" " },
        { "grmUserWarn-Gui-OnlineHelp", "This installer has to run as user \"root\". Please exit the installation, and restart as user \"root\"" },
        { "grmSystemBasics-install-Gui-OnlineHelp", "This dialog asks the user for Basic System Information, like Systen Name, Distribution Directory ..."},
        { "grmSelectSystemFromPrefs-Gui-PreferenceType", "Preference Type:" },
        { "grmSelectSystemFromPrefs-Gui-Systems", "Available Systems:" },
        { "grmSelectSystemFromShared-Gui-SystemName", "Systemname:" },
        { "grmSelectSystemFromShared-Gui-grmUserName","grmAdmin Username:" },
        { "grmSelectSystemFromShared-Gui-DistDir", "Distribution Directory:" },
        { "grmSelectSystemFromShared-Gui-LocalSpoolDir", "Local Spool Directory:" },
        { "grmSelectSystemFromShared-Gui-SharedDir", "Shared Directory:" },
        { "grmSelectSystemFromShared-Gui-SgedistDir", "SGE Distribution Directory:" },
        { "grmSelectSystemFromShared-Gui-PreferenceType", "Preference Type:" },
        { "grmSystemBasics-Gui-PreferenceType", "Preference Type:" },
        { "grmSystemBasics-Gui-SystemName", "Systemname:" },
        { "grmSystemBasics-Gui-grmUserName","grmAdmin Username:" },
        { "grmSystemBasics-Gui-DistDir", "Distribution Directory:" },
        { "grmSystemBasics-Gui-LocalSpoolDir", "Local Spool Directory:" },
        { "grmSystemBasics-Gui-SharedDir", "Shared Directory:" },
        { "grmSystemBasics-Gui-SgedistDir", "SGE Distribution Directory:" },
        { "grmSystemBasics-Gui-testbutton", "Test button" },
        { "grmSystemBasics-Gui-CheckButton", "Check Input"},
        { "grmSystemBasics-Gui-Header", "Basic System Information"},
        { "grmSystemBasics-Gui-SystemName--Solaris", "Systemname:" },
        { "grmSystemBasics-Gui-grmUserName--Solaris","grmAdmin Username:" },
        { "grmSystemBasics-Gui-DistDir--Solaris", "Distribution Directory:" },
        { "grmSystemBasics-Gui-LocalSpoolDir--Solaris", "Local Spool Directory:" },
        { "grmSystemBasics-Gui-SharedDir--Solaris", "Shared Directory:" },
        { "grmSystemBasics-Gui-SgedistDir--Solaris", "SGE Distribution Directory:" },
        { "grmSystemBasics-Gui-CheckButton--Solaris", "Check Input"},
        { "grmSystemBasics-Gui-Header--Solaris", "Basic System Information"},
        { "grmPortSelect-install-Gui-OnlineHelp", "Please enter the Port on which, the Executor VM and on which the Component VM are listening"},
        { "grmPortSelect-Gui-Header", "Port Information"},
        { "grmPortSelect-Gui-ExecutorPort", "Please enter the Portnumber of the VM where the Executor is running:"},
        { "grmPortSelect-Gui-InstallThisHost", "Install a Executor on this host" },
        { "grmPortSelect-Gui-ComponentPort", "Please enter the Portnumber of the VM where other Components are running:" },
        { "grmAddHosts-Gui-Header", "Adding System Hosts" },
        { "grmAddHosts-Gui-AddedHosts", "Already Added Hosts:" },
        { "grmAddHosts-Gui-AddHostButton", "Add Host" },
        { "grmAddHosts-Gui-RemoveHostButton", "Remove Host" },
        { "grmAddHosts-Gui-AddHostsFile", "Add Hosts from File" },
        { "grmAddHosts-Gui-AddHostsDialog", "Please enter a hostname:" },
        { "grmAddHosts-install-Gui-OnlineHelp", "Add Hosts Online Help" },
        { "grmExecutorHostSelect-Gui-Header","Executor Host Selection"},
        { "grmExecutorHostSelect-Gui-AvailableHosts", "Available Hosts:" },
        { "grmExecutorHostSelect-Gui-SelectedHosts", "Selected Hosts:" },
        { "grmExecutorHostSelect-Gui-SelectHostButton", "     >     " },
        { "grmExecutorHostSelect-Gui-DeselectHostButton", "     <     " },
        { "grmExecutorHostSelect-Gui-SelectAllHostButton", "    >>    " },
        { "grmExecutorHostSelect-Gui-DeselectAllHostButton", "    <<    " },
        { "grmExecutorHostSelect-install-Gui-OnlineHelp","Executor Host Selection Online Help<br>In this Dialog the hosts, which should <b>not</b> become an executor, can be selected." +
                  "Be careful. All hosts on the left side will become an executor, all selected hosts on the right side are excluded. Only the executor dialog excludes the seleced hosts."},
        { "grmScHostSelect-Gui-Header","Service Container Host Selection"},
        { "grmScHostSelect-Gui-AvailableHosts", "Available Hosts:" },
        { "grmScHostSelect-Gui-SelectedHosts", "Selected Hosts:" },
        { "grmScHostSelect-Gui-SelectHostButton", "     >     " },
        { "grmScHostSelect-Gui-DeselectHostButton", "     <     " },
        { "grmScHostSelect-Gui-SelectAllHostButton", "    >>    " },
        { "grmScHostSelect-Gui-DeselectAllHostButton", "    <<    " },
        { "grmScHostSelect-install-Gui-OnlineHelp","Service Container Host Selection Online Help"},
        { "grmOsdHostSelect-Gui-Header","OS Dispatcher Host Selection"},
        { "grmOsdHostSelect-Gui-AvailableHosts", "Available Hosts:" },
        { "grmOsdHostSelect-Gui-SelectedHosts", "Selected Hosts:" },
        { "grmOsdHostSelect-Gui-SelectHostButton", "     >     " },
        { "grmOsdHostSelect-Gui-DeselectHostButton", "     <     " },
        { "grmOsdHostSelect-Gui-SelectAllHostButton", "    >>    " },
        { "grmOsdHostSelect-Gui-DeselectAllHostButton", "    <<    " },
        { "grmOsdHostSelect-install-Gui-OnlineHelp","OS Dispatcher Host Selection Online Help"},
        { "grmOspHostSelect-Gui-Header","OS Provisioner Host Selection"},
        { "grmOspHostSelect-Gui-SelectHost","Please select your OS Provisioner Host" },
        { "grmOspHostSelect-install-Gui-OnlineHelp","OS Provisioner Host Selction Online Help" },
        { "grmRpHostSelect-Gui-Header","Resource Provider Host Selection"},
        { "grmRpHostSelect-Gui-SelectHost","Please select your Resource Provider Host" },
        { "grmRpHostSelect-install-Gui-OnlineHelp","Resource Provider Host selction Online Help" },
        { "grmVerify-install-ready","Verify Installation Settings"},
        { "grmVerify-install-items","Installation Parameters:"},
        { "grmVerify-product","Product Name"},
        { "grmVerify-location","Product Location"},
        { "grmVerify-install-size","Product Install Size"},
        { "grmVerify-install-now","Install Now"},
        { "grmVerify-checking_disk_space","Checking disk space..." },
        { "grmVerify-install-Gui-OnlineHelp", "Verify Installation Online Help" },
        { "grmVerify-total_string", "Total:" },
        { "grmPreInstallSummary-Gui-Header", "Configration Summary" },
        { "grmPreInstallSummary-textArea-head", "The following settings will be written:"},
        { "grmPreInstallSummary-textArea-systemName", "System Name:"},
        { "grmPreInstallSummary-textArea-grmUser", "Grm User Name:"},
        { "grmPreInstallSummary-textArea-installDirs", "Installation Directories:"},
        { "grmPreInstallSummary-textArea-distDir", "Distribution Directory:"},
        { "grmPreInstallSummary-textArea-localSpoolDir", "Spooling Directory:"},
        { "grmPreInstallSummary-textArea-sharedDir", "Shared Directory:"},
        { "grmPreInstallSummary-textArea-grmPorts", "Selected GRM Ports:"},
        { "grmPreInstallSummary-textArea-execPort", "Executor Port:"},
        { "grmPreInstallSummary-textArea-compPort", "Component Port:"},
        { "grmPreInstallSummary-textArea-grmHosts", "Available Hosts:"},
        { "grmPreInstallSummary-textArea-execHosts", "Executor Hosts:"},
        { "grmPreInstallSummary-textArea-osdHosts", "OS Dispatcher Hosts:"},
        { "grmPreInstallSummary-textArea-ospHosts", "OS Provisioner Host:"},
        { "grmPreInstallSummary-textArea-rpHosts", "Resource Provider Hosts:"},
        { "grmPreInstallSummary-textArea-scHosts", "Service Container Hosts:"},
        { "grmPreInstallSummary-install-Gui-OnlineHelp", "PreInstallSummary Online Help"},
        { "grmSecurityInfo-Gui-Header", "Security System Setup Information"},
        { "grmSecuritySystem-Gui-Header", "Security System Setup"},
        { "grmSecuritySystem-Gui-CountryCode", "Country Code: (eg. DE)"},
        { "grmSecuritySystem-Gui-State", "State:"},
        { "grmSecuritySystem-Gui-Location", "Location:"},
        { "grmSecuritySystem-Gui-Organisation", "Organisation:"},
        { "grmSecuritySystem-Gui-OrgaUnit", "Organisation Unit:"},
        { "grmSecuritySystem-Gui-MailAdress", "E-Mail Adress:"},
        { "grmSecurityAuth-Gui-AuthMethod", "Authentification Method"},
        { "grmSecurityAuth-Gui-NisServer", "NIS Server"},
        { "grmSecurityAuth-Gui-NisDomain", "NIS Domain"},
        { "grmSecurityAuth-Gui-PamService", "pam Service"},
        { "grmSecurityAuth-Gui-AdminUser", "Adminuser:"},
        { "grmSecurityAuth-Gui-JvmPort", "JVM Port:"},
        { "grmSecurityAuth-Gui-SgeDistDir", "SGE Distribution Directory:"},
        { "grmSecurityAuth-Gui-LdLibraryPath", "LD Library Path:"},
        { "grmSecurityAuth-Gui-JVM", "JVM where CA component is running"},
        { "grmSecurityAdminUser-Gui-Header", "Security Adminuser and Groups"},
        { "grmSecurityAdminUser-Gui-AddedUser", "Already added User and Groups"},
        { "grmSecurityAdminUser-Gui-AddUserButton", "Add User"},
        { "grmSecurityAdminUser-Gui-RemoveUserButton", "Remove User"},
        { "grmSecurityAdminUser-Gui-AddUserFile", "Add User from File"},
        { "grmSecurityAdminUser-Gui-AddGroupButton", "Add Group"},
        { "grmSecurityAdminUser-Gui-AddUserDialog", "Please enter a Username:"},
        { "grmSecurityAdminUser-Gui-AddGroupDialog", "Please enter a Groupname:"},
        { "grmSecurityAdminUser-install-Gui-OnlineHelp", "Security Adminuser and Groups Online Help"},
        { "grmJvmConfiguration-Gui-AvailableJvms", "Available JVM's:"},
        { "grmJvmConfiguration-Gui-AdminUser", "Adminuser Name:"},
        { "grmJvmConfiguration-Gui-JvmPort", "JVM Port:"},
        { "grmJvmConfiguration-Gui-SgeDistDir", "SGE Distribution Directory:"},
        { "grmJvmConfiguration-Gui-LdLibraryPath", "LD Library Path:"},
        { "grmExecutorConfig-Gui-Header", "Executor Configuration" },
        { "grmExecutorConfig-Gui-Identifier", "Identifier:" },
        { "grmExecutorConfig-Gui-ShutdownTimeout", "Shutdown Timeout:" },
        { "grmExecutorConfig-Gui-KeepFiles", "Keep Files" },
        { "grmExecutorConfig-Gui-Dummy", " " },
        { "grmExecutorConfig-Gui-CorePoolSize", "Core Pool Size:" },
        { "grmExecutorConfig-Gui-MaxPoolSize", "Max Pool Size:" },
        { "grmExecutorConfig-Gui-IdleTimeout", "Idle Timeout:" },
        { "grmExecutorConfig-install-Gui-OnlineHelp", "Executor Configuration Online Help" },
        { "grmRpConfiguration-Gui-Header", "Resource Provider Configuration" },
        { "grmRpConfiguration-Gui-PollingPeriod", "Polling Period" },
        { "grmRpConfiguration-Gui-Identifier", "Identifier" },
        { "grmRpConfiguration-Gui-SpoolDir", "Spooling Directory" },
        { "grmRpConfiguration-Gui-SparePool", "Spare Pool" },
        { "grmRpConfiguration-Gui-Usage", "Usage" },
        { "grmRpConfiguration-Gui-Policies", "Policies" },
        { "grmRpConfiguration-Gui-PolicyName", "Policyname" },
        { "grmRpConfiguration-Gui-PolicyType", "PolicyType" },
        { "grmRpConfiguration-Gui-PolicySettings", "Policysettings" },
        { "grmRpConfiguration-install-Gui-OnlineHelp", "Resource Provider Online Help" },
        { "grmOsdN1SmBasicConfig-Gui-Header", "OSD/N1SM Basic Configuration" },
        { "grmOsdN1SmBasicConfig-Gui-OsdIdentifier", "OSD Identifier" },
        { "grmOsdN1SmBasicConfig-Gui-OsdClassPath", "OSD Class Path" },
        { "grmOsdN1SmBasicConfig-Gui-OsdClass", "OSD Class" },
        { "grmOsdN1SmBasicConfig-Gui-OsdConfig", "OSD Config" },
        { "grmOsdN1SmBasicConfig-Gui-AdapterIdentifier", "N1SM Adapter Identifier" },
        { "grmOsdN1SmBasicConfig-Gui-AdapterN1smShPath", "N1SM Shell Path" },
        { "grmOsdN1SmBasicConfig-Gui-AdapterExpectPath", "Expect Path" },
        { "grmOsdN1SmBasicConfig-install-Gui-OnlineHelp", "OSD/N1SM Adapter Online Help" },
        { "grmN1SmConfig-Gui-Header", "N1SM Adapter Configuration" },
        { "grmN1SmConfig-Gui-AddOsButton", "Add Os" },
        { "grmN1SmConfig-Gui-AddResourceButton", "Add Resource" },
        { "grmN1SmConfig-install-Gui-OnlineHelp", "N1SM Adapter Online Help" },
        { "grmOspConfig-Gui-Header", "OS Provisioner Configuration" },
        { "grmOspConfig-Gui-Identifier", "Identifier" },
        { "grmOspConfig-Gui-LookupInterval", "Lookup Interval" },
        { "grmOspConfig-install-Gui-OnlineHelp", "OSP Online Help" },
        { "grmScConfig-Gui-Header", "Service Container Configuration" },
        { "grmScConfig-Gui-Identifier", "Identifier" },
        { "grmScConfig-Gui-ShutdownTime", "Shutdown Time" },
        { "grmScConfig-Gui-AdapterName", "Adapter Name" },
        { "grmScConfig-Gui-AdapterClassName", "Adapter Class Name" },
        { "grmScConfig-Gui-AdapterClassPath", "Adapter Class Path" },
        { "grmScConfig-install-Gui-OnlineHelp", "SC Online Help" },
        { "grmScConnectionConfig-Gui-Header", "Service Container Connection Configuration" },
        { "grmScConnectionConfig-Gui-SgeRoot", "SGE Root Directory" },
        { "grmScConnectionConfig-Gui-SgeCell", "SGE Cell Directory" },
        { "grmScConnectionConfig-Gui-SgeMasterPort", "SGE Master Communication Port" },
        { "grmScConnectionConfig-Gui-SgeExecdPort", "SGE Execd Communication Port" },
        { "grmScConnectionConfig-install-Gui-OnlineHelp", "SC Connection Config Online Help" },
        { "grmAddFixedSlaHosts-Gui-Header", "Adding SLA fixed Hosts" },
        { "grmAddFixedSlaHosts-Gui-AddedHosts", "Added Hosts" },
        { "grmAddFixedSlaHosts-Gui-AddHostButton", "Add Host" },
        { "grmAddFixedSlaHosts-Gui-RemoveHostButton", "Remove Host" },
        { "grmAddFixedSlaHosts-Gui-AddHostsFile", "Add Hosts from File" },
        { "grmAddFixedSlaHosts-Gui-AddHostsDialog", "Enter a Hostname:" },
        { "grmAddFixedSlaHosts-install-Gui-OnlineHelp", "SLA Add Hosts Online Help" },
        { "grmScSlaConfig-Gui-Header", "Add Slo MinResource" },
        { "grmScSlaConfig-Gui-AddResourceButton", "Add Resource" },
        { "grmScSlaConfig-Gui-RemoveResourceButton", "Remove Resource" },
        { "grmScSlaConfig-install-Gui-OnlineHelp", "SC Sla Config Online Help" },
        { "grmRcConfig-Gui-Header", "System start / rc script installation" },
        { "grmRcConfig-Gui-StartCompLabel", "Shall the Installer try to start the system?" },
        { "grmRcConfig-Gui-StartComponent", "Start the system" },
        { "grmRcConfig-Gui-InstRcLabel", "Shall the Installer add the rc scripts to the startup process?" },
        { "grmRcConfig-Gui-InstallRcClassic", "Install rc script with classic method (init.d)!" },
        { "grmRcConfig-Gui-InstallRcOsTypical", "Install rc script OS typical (greenline/insserv)" },
        { "grmRcConfig-install-Gui-OnlineHelp", "RC install Online Help" }
    };
}
