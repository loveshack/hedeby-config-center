/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.install;

import com.sun.grid.grm.install.InstallerUI.Confirmation;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;

/**
 *
 */
public class InstallerFrame extends JFrame implements WindowListener, InstallerUI {
    
    private static Logger log = Logger.getLogger(InstallerFrame.class.getName());
    
    /**
     * This panel contains the image(s) that appear on the left side
     * of the wizard.
     */
    private transient JPanel     imageContainer = null;
    

    private InstallerController controller;
    private JPanel imagePanel = null;
    private JPanel mainPanel = new JPanel(new BorderLayout());
    
    
    public RB rb() {
        return RB.getInstance(getClass());
    }
    /**
     *  Create a new InstallerFrame
     */
    public InstallerFrame() {
        initUI();
        addWindowListener(this);
    }
    
    private void initUI() {
    
        setTitle(rb().getString("title"));
        JPanel centerPanel = new JPanel(new BorderLayout());
        
        imagePanel = new JPanel();
        imagePanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        
        imagePanel.setLayout(new BorderLayout());
        
        ImagePanel imagePanel = new ImagePanel("Image.background", "Image.foreground", rb());
        
        centerPanel.add(imagePanel, BorderLayout.WEST);
        centerPanel.add(mainPanel, BorderLayout.CENTER);
        
        JPanel navigationPanel = createNavigationPanel(imagePanel.getImageWidth());
        
        centerPanel.add(navigationPanel, BorderLayout.SOUTH);
        
        getContentPane().add(centerPanel);
        
        int width = 680;
        int height = 505;
        setSize(width, height);
        
        Dimension screenSize = getToolkit().getScreenSize();
        
        int x = (screenSize.width - width) / 2;
        int y = (screenSize.height - height) / 2;
        
        setLocation(x, y);
        validate();
    }
    
    /**
     * Invoked when a window has been opened.
     */
    public void windowOpened(WindowEvent e) {
        
    }
    
    public void setTaskPanel(JPanel panel) {
        mainPanel.removeAll();
        mainPanel.add(panel);
        validate();
        repaint();
    }
    
    public void windowClosing(WindowEvent e) {
        if(controller != null) {
            controller.cancel();
        }
    }
    
    /**
     * Invoked when a window has been closed.
     */
    public void windowClosed(WindowEvent e) {}
    
    /**
     * Invoked when a window is iconified.
     */
    public void windowIconified(WindowEvent e) {}
    
    /**
     * Invoked when a window is de-iconified.
     */
    public void windowDeiconified(WindowEvent e) {}
    
    /**
     * Invoked when a window is activated.
     */
    public void windowActivated(WindowEvent e) {}
    
    /**
     * Invoked when a window is de-activated.
     */
    public void windowDeactivated(WindowEvent e) {}
    
    /**
     * The WizardConsole has a gui.  Indicate that images should
     * be created.
     */
    public boolean hasGUI() {
        return (true);
    }
    
    private JButton backButton;
    private AbstractAction backAction;
    
    private JButton nextButton;
    private AbstractAction nextAction;
    
    private JButton exitButton;
    private ExitAction exitAction = new ExitAction();
    
    public void setExitAction(InstallerUI.ExitAction action) {
        exitAction.setAction(action);
        exitButton.setVisible(!action.equals(InstallerUI.ExitAction.INVISIBLE));
    }
    
    class  ExitAction extends AbstractAction {
          InstallerUI.ExitAction exitAction;
          
          public ExitAction() {
              setAction(InstallerUI.ExitAction.INVISIBLE);
          }
          
          public void setAction(InstallerUI.ExitAction action) {
              exitAction = action;
              switch(exitAction) {
                  case INSTALL:
                      rb().initAction(this, "Install");
                      setEnabled(true);
                      break;
                  case FINISH:
                      rb().initAction(this, "Exit");
                      setEnabled(true);
                      break;
                  case DISABLED:
                      setEnabled(false);
                      break;
                  case INVISIBLE:
                      setEnabled(false);
                      break;
              }
          }
           public void actionPerformed(ActionEvent evt) {
               if(controller != null) {
                   switch(exitAction) {
                       case INSTALL:
                           controller.install();
                           break;
                       case FINISH:
                           controller.exit();
                           break;
                       default:
                           // Ignore
                   }
               }
           } 
        };

    
    protected JPanel createNavigationPanel(int imageWidth) {
        
        JPanel navigationPanel = new JPanel(new BorderLayout());

        JSeparator sep = new JSeparator();
        
        navigationPanel.add(sep, BorderLayout.NORTH);
        
        JPanel buttonBox = new JPanel();
        BoxLayout bl = new BoxLayout(buttonBox, BoxLayout.X_AXIS);
        buttonBox.setLayout(bl);
        buttonBox.setBorder(BorderFactory.createEmptyBorder(6, 10, 6, 10));
        
        int xgap = 4;
        
        navigationPanel.add(buttonBox, BorderLayout.SOUTH);
        
        if(imageWidth == 0) {
            buttonBox.add(Box.createHorizontalGlue());
        } else {
            buttonBox.add(Box.createHorizontalStrut(imageWidth));
        }
        
        

        backAction = new AbstractAction() {
           public void actionPerformed(ActionEvent evt) {
               if(controller != null) {
                    try {
                        controller.back();
                    } catch(Exception ex) {
                       log.log(Level.SEVERE, "Unexpected error", ex); 
                    }
               }
           } 
        };
        rb().initAction(backAction, "Back");
        
        backButton = new JButton(backAction);
        buttonBox.add((JButton)backButton);
        buttonBox.add(Box.createHorizontalStrut(xgap));
        
        
        nextAction = new AbstractAction() {
           public void actionPerformed(ActionEvent evt) {
               if(controller != null) {
                   try {
                        controller.next();
                    } catch(Exception ex) {
                       log.log(Level.SEVERE, "Unexpected error", ex); 
                    }
               }
           } 
        };
        rb().initAction(nextAction, "Next");
        nextButton = new JButton(nextAction);
        buttonBox.add(nextButton);
        buttonBox.add(Box.createHorizontalStrut(xgap));
        
        buttonBox.add(Box.createHorizontalGlue());
        
        
        AbstractAction cancelAction = new AbstractAction() {
           public void actionPerformed(ActionEvent evt) {
               if(controller != null) {
                   controller.cancel();
               }
           } 
        };
        rb().initAction(cancelAction, "Cancel");
        JButton cancelButton = new JButton(cancelAction);
        buttonBox.add((JButton)cancelButton);
        buttonBox.add(Box.createHorizontalStrut(xgap));
        
        
        exitButton = new JButton(exitAction);
        buttonBox.add(exitButton);
        buttonBox.add(Box.createHorizontalStrut(xgap));

        
        AbstractAction helpAction = new AbstractAction() {
           public void actionPerformed(ActionEvent evt) {
               if(controller != null) {
                   controller.help();
               }
           } 
        };
        rb().initAction(helpAction, "Help");
        JButton helpButton = new JButton(helpAction);
        
        buttonBox.add(helpButton);
        buttonBox.add(Box.createHorizontalStrut(xgap));
        
        return navigationPanel;
    }
    
    public InstallerController getController() {
        return controller;
    }

    public void setController(InstallerController controller) {
        this.controller = controller;
    }

    public void start() {
        setVisible(true);
    }

    public void setNextEnabled(boolean enabled) {
        nextButton.setEnabled(enabled);
    }

    public void setBackEnabled(boolean enabled) {
        backButton.setEnabled(enabled);
    }
    
    public Confirmation confirm(String question) {        
        switch(JOptionPane.showConfirmDialog(this, question, "Question?", JOptionPane.YES_NO_CANCEL_OPTION)) {
            case JOptionPane.YES_OPTION:
                return Confirmation.YES;
            case JOptionPane.NO_OPTION:
                return Confirmation.NO;
            default:
                return Confirmation.CANCEL;
        }
    }    
    
}
