/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.install;

import java.io.File;
import javax.help.HelpBroker;
import javax.help.HelpSetException;

/**
 *
 */
public interface InstallerController {
    
    public void next();
    public void setNextEnabled(boolean enabled);
    
    public void back();
    public void setBackEnabled(boolean enabled);
    
    public void cancel();
    public void install();
    
    public void exit();
    public void help();
    
    public InstallerModel getModel();
    
    public void run();
    
    public void registerTask(String name, String implClass, InstallClassLoaderDefinition classLoaderDef);
    
    public InstallerTaskDefinition getTask(String name) throws InstallerException;
    
    
    public InstallerTaskSequence getSequence();
    
    public void setStateFile(File stateFile);
    
    public enum Mode {
        GUI,
        CONSOLE,
        AUTO
    }
    
    public void setMode(Mode mode);
    public Mode getMode();
    
    public InstallerFrame getFrame();
    
    public InstallerUI getUi();
    public void setUi(InstallerUI ui);
    
    public HelpBroker getHelpBroker() throws HelpSetException;
    
}
